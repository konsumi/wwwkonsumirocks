(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __defProps = Object.defineProperties;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getOwnPropSymbols = Object.getOwnPropertySymbols;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __propIsEnum = Object.prototype.propertyIsEnumerable;
  var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
  var __spreadValues = (a3, b2) => {
    for (var prop in b2 || (b2 = {}))
      if (__hasOwnProp.call(b2, prop))
        __defNormalProp(a3, prop, b2[prop]);
    if (__getOwnPropSymbols)
      for (var prop of __getOwnPropSymbols(b2)) {
        if (__propIsEnum.call(b2, prop))
          __defNormalProp(a3, prop, b2[prop]);
      }
    return a3;
  };
  var __spreadProps = (a3, b2) => __defProps(a3, __getOwnPropDescs(b2));
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __export = (target, all) => {
    for (var name in all)
      __defProp(target, name, { get: all[name], enumerable: true });
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));
  var __publicField = (obj, key, value) => {
    __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
    return value;
  };
  var __async = (__this, __arguments, generator) => {
    return new Promise((resolve2, reject) => {
      var fulfilled = (value) => {
        try {
          step(generator.next(value));
        } catch (e2) {
          reject(e2);
        }
      };
      var rejected = (value) => {
        try {
          step(generator.throw(value));
        } catch (e2) {
          reject(e2);
        }
      };
      var step = (x2) => x2.done ? resolve2(x2.value) : Promise.resolve(x2.value).then(fulfilled, rejected);
      step((generator = generator.apply(__this, __arguments)).next());
    });
  };

  // node_modules/mithril/render/vnode.js
  var require_vnode = __commonJS({
    "node_modules/mithril/render/vnode.js"(exports, module) {
      "use strict";
      function Vnode(tag, key, attrs, children, text2, dom) {
        return { tag, key, attrs, children, text: text2, dom, domSize: void 0, state: void 0, events: void 0, instance: void 0 };
      }
      Vnode.normalize = function(node) {
        if (Array.isArray(node))
          return Vnode("[", void 0, void 0, Vnode.normalizeChildren(node), void 0, void 0);
        if (node == null || typeof node === "boolean")
          return null;
        if (typeof node === "object")
          return node;
        return Vnode("#", void 0, void 0, String(node), void 0, void 0);
      };
      Vnode.normalizeChildren = function(input) {
        var children = [];
        if (input.length) {
          var isKeyed = input[0] != null && input[0].key != null;
          for (var i2 = 1; i2 < input.length; i2++) {
            if ((input[i2] != null && input[i2].key != null) !== isKeyed) {
              throw new TypeError(
                isKeyed && (input[i2] != null || typeof input[i2] === "boolean") ? "In fragments, vnodes must either all have keys or none have keys. You may wish to consider using an explicit keyed empty fragment, m.fragment({key: ...}), instead of a hole." : "In fragments, vnodes must either all have keys or none have keys."
              );
            }
          }
          for (var i2 = 0; i2 < input.length; i2++) {
            children[i2] = Vnode.normalize(input[i2]);
          }
        }
        return children;
      };
      module.exports = Vnode;
    }
  });

  // node_modules/mithril/render/hyperscriptVnode.js
  var require_hyperscriptVnode = __commonJS({
    "node_modules/mithril/render/hyperscriptVnode.js"(exports, module) {
      "use strict";
      var Vnode = require_vnode();
      module.exports = function() {
        var attrs = arguments[this], start = this + 1, children;
        if (attrs == null) {
          attrs = {};
        } else if (typeof attrs !== "object" || attrs.tag != null || Array.isArray(attrs)) {
          attrs = {};
          start = this;
        }
        if (arguments.length === start + 1) {
          children = arguments[start];
          if (!Array.isArray(children))
            children = [children];
        } else {
          children = [];
          while (start < arguments.length)
            children.push(arguments[start++]);
        }
        return Vnode("", attrs.key, attrs, children);
      };
    }
  });

  // node_modules/mithril/util/hasOwn.js
  var require_hasOwn = __commonJS({
    "node_modules/mithril/util/hasOwn.js"(exports, module) {
      "use strict";
      module.exports = {}.hasOwnProperty;
    }
  });

  // node_modules/mithril/render/hyperscript.js
  var require_hyperscript = __commonJS({
    "node_modules/mithril/render/hyperscript.js"(exports, module) {
      "use strict";
      var Vnode = require_vnode();
      var hyperscriptVnode = require_hyperscriptVnode();
      var hasOwn = require_hasOwn();
      var selectorParser = /(?:(^|#|\.)([^#\.\[\]]+))|(\[(.+?)(?:\s*=\s*("|'|)((?:\\["'\]]|.)*?)\5)?\])/g;
      var selectorCache = {};
      function isEmpty(object2) {
        for (var key in object2)
          if (hasOwn.call(object2, key))
            return false;
        return true;
      }
      function compileSelector(selector) {
        var match, tag = "div", classes2 = [], attrs = {};
        while (match = selectorParser.exec(selector)) {
          var type = match[1], value = match[2];
          if (type === "" && value !== "")
            tag = value;
          else if (type === "#")
            attrs.id = value;
          else if (type === ".")
            classes2.push(value);
          else if (match[3][0] === "[") {
            var attrValue = match[6];
            if (attrValue)
              attrValue = attrValue.replace(/\\(["'])/g, "$1").replace(/\\\\/g, "\\");
            if (match[4] === "class")
              classes2.push(attrValue);
            else
              attrs[match[4]] = attrValue === "" ? attrValue : attrValue || true;
          }
        }
        if (classes2.length > 0)
          attrs.className = classes2.join(" ");
        return selectorCache[selector] = { tag, attrs };
      }
      function execSelector(state, vnode) {
        var attrs = vnode.attrs;
        var hasClass = hasOwn.call(attrs, "class");
        var className = hasClass ? attrs.class : attrs.className;
        vnode.tag = state.tag;
        vnode.attrs = {};
        if (!isEmpty(state.attrs) && !isEmpty(attrs)) {
          var newAttrs = {};
          for (var key in attrs) {
            if (hasOwn.call(attrs, key))
              newAttrs[key] = attrs[key];
          }
          attrs = newAttrs;
        }
        for (var key in state.attrs) {
          if (hasOwn.call(state.attrs, key) && key !== "className" && !hasOwn.call(attrs, key)) {
            attrs[key] = state.attrs[key];
          }
        }
        if (className != null || state.attrs.className != null)
          attrs.className = className != null ? state.attrs.className != null ? String(state.attrs.className) + " " + String(className) : className : state.attrs.className != null ? state.attrs.className : null;
        if (hasClass)
          attrs.class = null;
        for (var key in attrs) {
          if (hasOwn.call(attrs, key) && key !== "key") {
            vnode.attrs = attrs;
            break;
          }
        }
        return vnode;
      }
      function hyperscript(selector) {
        if (selector == null || typeof selector !== "string" && typeof selector !== "function" && typeof selector.view !== "function") {
          throw Error("The selector must be either a string or a component.");
        }
        var vnode = hyperscriptVnode.apply(1, arguments);
        if (typeof selector === "string") {
          vnode.children = Vnode.normalizeChildren(vnode.children);
          if (selector !== "[")
            return execSelector(selectorCache[selector] || compileSelector(selector), vnode);
        }
        vnode.tag = selector;
        return vnode;
      }
      module.exports = hyperscript;
    }
  });

  // node_modules/mithril/render/trust.js
  var require_trust = __commonJS({
    "node_modules/mithril/render/trust.js"(exports, module) {
      "use strict";
      var Vnode = require_vnode();
      module.exports = function(html3) {
        if (html3 == null)
          html3 = "";
        return Vnode("<", void 0, void 0, html3, void 0, void 0);
      };
    }
  });

  // node_modules/mithril/render/fragment.js
  var require_fragment = __commonJS({
    "node_modules/mithril/render/fragment.js"(exports, module) {
      "use strict";
      var Vnode = require_vnode();
      var hyperscriptVnode = require_hyperscriptVnode();
      module.exports = function() {
        var vnode = hyperscriptVnode.apply(0, arguments);
        vnode.tag = "[";
        vnode.children = Vnode.normalizeChildren(vnode.children);
        return vnode;
      };
    }
  });

  // node_modules/mithril/hyperscript.js
  var require_hyperscript2 = __commonJS({
    "node_modules/mithril/hyperscript.js"(exports, module) {
      "use strict";
      var hyperscript = require_hyperscript();
      hyperscript.trust = require_trust();
      hyperscript.fragment = require_fragment();
      module.exports = hyperscript;
    }
  });

  // node_modules/mithril/promise/polyfill.js
  var require_polyfill = __commonJS({
    "node_modules/mithril/promise/polyfill.js"(exports, module) {
      "use strict";
      var PromisePolyfill = function(executor) {
        if (!(this instanceof PromisePolyfill))
          throw new Error("Promise must be called with 'new'.");
        if (typeof executor !== "function")
          throw new TypeError("executor must be a function.");
        var self = this, resolvers = [], rejectors = [], resolveCurrent = handler(resolvers, true), rejectCurrent = handler(rejectors, false);
        var instance = self._instance = { resolvers, rejectors };
        var callAsync = typeof setImmediate === "function" ? setImmediate : setTimeout;
        function handler(list, shouldAbsorb) {
          return function execute(value) {
            var then;
            try {
              if (shouldAbsorb && value != null && (typeof value === "object" || typeof value === "function") && typeof (then = value.then) === "function") {
                if (value === self)
                  throw new TypeError("Promise can't be resolved with itself.");
                executeOnce(then.bind(value));
              } else {
                callAsync(function() {
                  if (!shouldAbsorb && list.length === 0)
                    console.error("Possible unhandled promise rejection:", value);
                  for (var i2 = 0; i2 < list.length; i2++)
                    list[i2](value);
                  resolvers.length = 0, rejectors.length = 0;
                  instance.state = shouldAbsorb;
                  instance.retry = function() {
                    execute(value);
                  };
                });
              }
            } catch (e2) {
              rejectCurrent(e2);
            }
          };
        }
        function executeOnce(then) {
          var runs = 0;
          function run2(fn) {
            return function(value) {
              if (runs++ > 0)
                return;
              fn(value);
            };
          }
          var onerror = run2(rejectCurrent);
          try {
            then(run2(resolveCurrent), onerror);
          } catch (e2) {
            onerror(e2);
          }
        }
        executeOnce(executor);
      };
      PromisePolyfill.prototype.then = function(onFulfilled, onRejection) {
        var self = this, instance = self._instance;
        function handle(callback, list, next, state) {
          list.push(function(value) {
            if (typeof callback !== "function")
              next(value);
            else
              try {
                resolveNext(callback(value));
              } catch (e2) {
                if (rejectNext)
                  rejectNext(e2);
              }
          });
          if (typeof instance.retry === "function" && state === instance.state)
            instance.retry();
        }
        var resolveNext, rejectNext;
        var promise = new PromisePolyfill(function(resolve2, reject) {
          resolveNext = resolve2, rejectNext = reject;
        });
        handle(onFulfilled, instance.resolvers, resolveNext, true), handle(onRejection, instance.rejectors, rejectNext, false);
        return promise;
      };
      PromisePolyfill.prototype.catch = function(onRejection) {
        return this.then(null, onRejection);
      };
      PromisePolyfill.prototype.finally = function(callback) {
        return this.then(
          function(value) {
            return PromisePolyfill.resolve(callback()).then(function() {
              return value;
            });
          },
          function(reason) {
            return PromisePolyfill.resolve(callback()).then(function() {
              return PromisePolyfill.reject(reason);
            });
          }
        );
      };
      PromisePolyfill.resolve = function(value) {
        if (value instanceof PromisePolyfill)
          return value;
        return new PromisePolyfill(function(resolve2) {
          resolve2(value);
        });
      };
      PromisePolyfill.reject = function(value) {
        return new PromisePolyfill(function(resolve2, reject) {
          reject(value);
        });
      };
      PromisePolyfill.all = function(list) {
        return new PromisePolyfill(function(resolve2, reject) {
          var total = list.length, count = 0, values = [];
          if (list.length === 0)
            resolve2([]);
          else
            for (var i2 = 0; i2 < list.length; i2++) {
              (function(i3) {
                function consume(value) {
                  count++;
                  values[i3] = value;
                  if (count === total)
                    resolve2(values);
                }
                if (list[i3] != null && (typeof list[i3] === "object" || typeof list[i3] === "function") && typeof list[i3].then === "function") {
                  list[i3].then(consume, reject);
                } else
                  consume(list[i3]);
              })(i2);
            }
        });
      };
      PromisePolyfill.race = function(list) {
        return new PromisePolyfill(function(resolve2, reject) {
          for (var i2 = 0; i2 < list.length; i2++) {
            list[i2].then(resolve2, reject);
          }
        });
      };
      module.exports = PromisePolyfill;
    }
  });

  // node_modules/mithril/promise/promise.js
  var require_promise = __commonJS({
    "node_modules/mithril/promise/promise.js"(exports, module) {
      "use strict";
      var PromisePolyfill = require_polyfill();
      if (typeof window !== "undefined") {
        if (typeof window.Promise === "undefined") {
          window.Promise = PromisePolyfill;
        } else if (!window.Promise.prototype.finally) {
          window.Promise.prototype.finally = PromisePolyfill.prototype.finally;
        }
        module.exports = window.Promise;
      } else if (typeof window !== "undefined") {
        if (typeof window.Promise === "undefined") {
          window.Promise = PromisePolyfill;
        } else if (!window.Promise.prototype.finally) {
          window.Promise.prototype.finally = PromisePolyfill.prototype.finally;
        }
        module.exports = window.Promise;
      } else {
        module.exports = PromisePolyfill;
      }
    }
  });

  // node_modules/mithril/render/render.js
  var require_render = __commonJS({
    "node_modules/mithril/render/render.js"(exports, module) {
      "use strict";
      var Vnode = require_vnode();
      module.exports = function($window) {
        var $doc = $window && $window.document;
        var currentRedraw;
        var nameSpace = {
          svg: "http://www.w3.org/2000/svg",
          math: "http://www.w3.org/1998/Math/MathML"
        };
        function getNameSpace(vnode) {
          return vnode.attrs && vnode.attrs.xmlns || nameSpace[vnode.tag];
        }
        function checkState(vnode, original) {
          if (vnode.state !== original)
            throw new Error("'vnode.state' must not be modified.");
        }
        function callHook(vnode) {
          var original = vnode.state;
          try {
            return this.apply(original, arguments);
          } finally {
            checkState(vnode, original);
          }
        }
        function activeElement() {
          try {
            return $doc.activeElement;
          } catch (e2) {
            return null;
          }
        }
        function createNodes(parent, vnodes, start, end, hooks, nextSibling, ns) {
          for (var i2 = start; i2 < end; i2++) {
            var vnode = vnodes[i2];
            if (vnode != null) {
              createNode(parent, vnode, hooks, ns, nextSibling);
            }
          }
        }
        function createNode(parent, vnode, hooks, ns, nextSibling) {
          var tag = vnode.tag;
          if (typeof tag === "string") {
            vnode.state = {};
            if (vnode.attrs != null)
              initLifecycle(vnode.attrs, vnode, hooks);
            switch (tag) {
              case "#":
                createText(parent, vnode, nextSibling);
                break;
              case "<":
                createHTML(parent, vnode, ns, nextSibling);
                break;
              case "[":
                createFragment(parent, vnode, hooks, ns, nextSibling);
                break;
              default:
                createElement(parent, vnode, hooks, ns, nextSibling);
            }
          } else
            createComponent(parent, vnode, hooks, ns, nextSibling);
        }
        function createText(parent, vnode, nextSibling) {
          vnode.dom = $doc.createTextNode(vnode.children);
          insertNode(parent, vnode.dom, nextSibling);
        }
        var possibleParents = { caption: "table", thead: "table", tbody: "table", tfoot: "table", tr: "tbody", th: "tr", td: "tr", colgroup: "table", col: "colgroup" };
        function createHTML(parent, vnode, ns, nextSibling) {
          var match = vnode.children.match(/^\s*?<(\w+)/im) || [];
          var temp = $doc.createElement(possibleParents[match[1]] || "div");
          if (ns === "http://www.w3.org/2000/svg") {
            temp.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg">' + vnode.children + "</svg>";
            temp = temp.firstChild;
          } else {
            temp.innerHTML = vnode.children;
          }
          vnode.dom = temp.firstChild;
          vnode.domSize = temp.childNodes.length;
          vnode.instance = [];
          var fragment = $doc.createDocumentFragment();
          var child;
          while (child = temp.firstChild) {
            vnode.instance.push(child);
            fragment.appendChild(child);
          }
          insertNode(parent, fragment, nextSibling);
        }
        function createFragment(parent, vnode, hooks, ns, nextSibling) {
          var fragment = $doc.createDocumentFragment();
          if (vnode.children != null) {
            var children = vnode.children;
            createNodes(fragment, children, 0, children.length, hooks, null, ns);
          }
          vnode.dom = fragment.firstChild;
          vnode.domSize = fragment.childNodes.length;
          insertNode(parent, fragment, nextSibling);
        }
        function createElement(parent, vnode, hooks, ns, nextSibling) {
          var tag = vnode.tag;
          var attrs = vnode.attrs;
          var is = attrs && attrs.is;
          ns = getNameSpace(vnode) || ns;
          var element = ns ? is ? $doc.createElementNS(ns, tag, { is }) : $doc.createElementNS(ns, tag) : is ? $doc.createElement(tag, { is }) : $doc.createElement(tag);
          vnode.dom = element;
          if (attrs != null) {
            setAttrs(vnode, attrs, ns);
          }
          insertNode(parent, element, nextSibling);
          if (!maybeSetContentEditable(vnode)) {
            if (vnode.children != null) {
              var children = vnode.children;
              createNodes(element, children, 0, children.length, hooks, null, ns);
              if (vnode.tag === "select" && attrs != null)
                setLateSelectAttrs(vnode, attrs);
            }
          }
        }
        function initComponent(vnode, hooks) {
          var sentinel;
          if (typeof vnode.tag.view === "function") {
            vnode.state = Object.create(vnode.tag);
            sentinel = vnode.state.view;
            if (sentinel.$$reentrantLock$$ != null)
              return;
            sentinel.$$reentrantLock$$ = true;
          } else {
            vnode.state = void 0;
            sentinel = vnode.tag;
            if (sentinel.$$reentrantLock$$ != null)
              return;
            sentinel.$$reentrantLock$$ = true;
            vnode.state = vnode.tag.prototype != null && typeof vnode.tag.prototype.view === "function" ? new vnode.tag(vnode) : vnode.tag(vnode);
          }
          initLifecycle(vnode.state, vnode, hooks);
          if (vnode.attrs != null)
            initLifecycle(vnode.attrs, vnode, hooks);
          vnode.instance = Vnode.normalize(callHook.call(vnode.state.view, vnode));
          if (vnode.instance === vnode)
            throw Error("A view cannot return the vnode it received as argument");
          sentinel.$$reentrantLock$$ = null;
        }
        function createComponent(parent, vnode, hooks, ns, nextSibling) {
          initComponent(vnode, hooks);
          if (vnode.instance != null) {
            createNode(parent, vnode.instance, hooks, ns, nextSibling);
            vnode.dom = vnode.instance.dom;
            vnode.domSize = vnode.dom != null ? vnode.instance.domSize : 0;
          } else {
            vnode.domSize = 0;
          }
        }
        function updateNodes(parent, old, vnodes, hooks, nextSibling, ns) {
          if (old === vnodes || old == null && vnodes == null)
            return;
          else if (old == null || old.length === 0)
            createNodes(parent, vnodes, 0, vnodes.length, hooks, nextSibling, ns);
          else if (vnodes == null || vnodes.length === 0)
            removeNodes(parent, old, 0, old.length);
          else {
            var isOldKeyed = old[0] != null && old[0].key != null;
            var isKeyed = vnodes[0] != null && vnodes[0].key != null;
            var start = 0, oldStart = 0;
            if (!isOldKeyed)
              while (oldStart < old.length && old[oldStart] == null)
                oldStart++;
            if (!isKeyed)
              while (start < vnodes.length && vnodes[start] == null)
                start++;
            if (isOldKeyed !== isKeyed) {
              removeNodes(parent, old, oldStart, old.length);
              createNodes(parent, vnodes, start, vnodes.length, hooks, nextSibling, ns);
            } else if (!isKeyed) {
              var commonLength = old.length < vnodes.length ? old.length : vnodes.length;
              start = start < oldStart ? start : oldStart;
              for (; start < commonLength; start++) {
                o2 = old[start];
                v2 = vnodes[start];
                if (o2 === v2 || o2 == null && v2 == null)
                  continue;
                else if (o2 == null)
                  createNode(parent, v2, hooks, ns, getNextSibling(old, start + 1, nextSibling));
                else if (v2 == null)
                  removeNode(parent, o2);
                else
                  updateNode(parent, o2, v2, hooks, getNextSibling(old, start + 1, nextSibling), ns);
              }
              if (old.length > commonLength)
                removeNodes(parent, old, start, old.length);
              if (vnodes.length > commonLength)
                createNodes(parent, vnodes, start, vnodes.length, hooks, nextSibling, ns);
            } else {
              var oldEnd = old.length - 1, end = vnodes.length - 1, map, o2, v2, oe, ve, topSibling;
              while (oldEnd >= oldStart && end >= start) {
                oe = old[oldEnd];
                ve = vnodes[end];
                if (oe.key !== ve.key)
                  break;
                if (oe !== ve)
                  updateNode(parent, oe, ve, hooks, nextSibling, ns);
                if (ve.dom != null)
                  nextSibling = ve.dom;
                oldEnd--, end--;
              }
              while (oldEnd >= oldStart && end >= start) {
                o2 = old[oldStart];
                v2 = vnodes[start];
                if (o2.key !== v2.key)
                  break;
                oldStart++, start++;
                if (o2 !== v2)
                  updateNode(parent, o2, v2, hooks, getNextSibling(old, oldStart, nextSibling), ns);
              }
              while (oldEnd >= oldStart && end >= start) {
                if (start === end)
                  break;
                if (o2.key !== ve.key || oe.key !== v2.key)
                  break;
                topSibling = getNextSibling(old, oldStart, nextSibling);
                moveNodes(parent, oe, topSibling);
                if (oe !== v2)
                  updateNode(parent, oe, v2, hooks, topSibling, ns);
                if (++start <= --end)
                  moveNodes(parent, o2, nextSibling);
                if (o2 !== ve)
                  updateNode(parent, o2, ve, hooks, nextSibling, ns);
                if (ve.dom != null)
                  nextSibling = ve.dom;
                oldStart++;
                oldEnd--;
                oe = old[oldEnd];
                ve = vnodes[end];
                o2 = old[oldStart];
                v2 = vnodes[start];
              }
              while (oldEnd >= oldStart && end >= start) {
                if (oe.key !== ve.key)
                  break;
                if (oe !== ve)
                  updateNode(parent, oe, ve, hooks, nextSibling, ns);
                if (ve.dom != null)
                  nextSibling = ve.dom;
                oldEnd--, end--;
                oe = old[oldEnd];
                ve = vnodes[end];
              }
              if (start > end)
                removeNodes(parent, old, oldStart, oldEnd + 1);
              else if (oldStart > oldEnd)
                createNodes(parent, vnodes, start, end + 1, hooks, nextSibling, ns);
              else {
                var originalNextSibling = nextSibling, vnodesLength = end - start + 1, oldIndices = new Array(vnodesLength), li = 0, i2 = 0, pos = 2147483647, matched = 0, map, lisIndices;
                for (i2 = 0; i2 < vnodesLength; i2++)
                  oldIndices[i2] = -1;
                for (i2 = end; i2 >= start; i2--) {
                  if (map == null)
                    map = getKeyMap(old, oldStart, oldEnd + 1);
                  ve = vnodes[i2];
                  var oldIndex = map[ve.key];
                  if (oldIndex != null) {
                    pos = oldIndex < pos ? oldIndex : -1;
                    oldIndices[i2 - start] = oldIndex;
                    oe = old[oldIndex];
                    old[oldIndex] = null;
                    if (oe !== ve)
                      updateNode(parent, oe, ve, hooks, nextSibling, ns);
                    if (ve.dom != null)
                      nextSibling = ve.dom;
                    matched++;
                  }
                }
                nextSibling = originalNextSibling;
                if (matched !== oldEnd - oldStart + 1)
                  removeNodes(parent, old, oldStart, oldEnd + 1);
                if (matched === 0)
                  createNodes(parent, vnodes, start, end + 1, hooks, nextSibling, ns);
                else {
                  if (pos === -1) {
                    lisIndices = makeLisIndices(oldIndices);
                    li = lisIndices.length - 1;
                    for (i2 = end; i2 >= start; i2--) {
                      v2 = vnodes[i2];
                      if (oldIndices[i2 - start] === -1)
                        createNode(parent, v2, hooks, ns, nextSibling);
                      else {
                        if (lisIndices[li] === i2 - start)
                          li--;
                        else
                          moveNodes(parent, v2, nextSibling);
                      }
                      if (v2.dom != null)
                        nextSibling = vnodes[i2].dom;
                    }
                  } else {
                    for (i2 = end; i2 >= start; i2--) {
                      v2 = vnodes[i2];
                      if (oldIndices[i2 - start] === -1)
                        createNode(parent, v2, hooks, ns, nextSibling);
                      if (v2.dom != null)
                        nextSibling = vnodes[i2].dom;
                    }
                  }
                }
              }
            }
          }
        }
        function updateNode(parent, old, vnode, hooks, nextSibling, ns) {
          var oldTag = old.tag, tag = vnode.tag;
          if (oldTag === tag) {
            vnode.state = old.state;
            vnode.events = old.events;
            if (shouldNotUpdate(vnode, old))
              return;
            if (typeof oldTag === "string") {
              if (vnode.attrs != null) {
                updateLifecycle(vnode.attrs, vnode, hooks);
              }
              switch (oldTag) {
                case "#":
                  updateText(old, vnode);
                  break;
                case "<":
                  updateHTML(parent, old, vnode, ns, nextSibling);
                  break;
                case "[":
                  updateFragment(parent, old, vnode, hooks, nextSibling, ns);
                  break;
                default:
                  updateElement(old, vnode, hooks, ns);
              }
            } else
              updateComponent(parent, old, vnode, hooks, nextSibling, ns);
          } else {
            removeNode(parent, old);
            createNode(parent, vnode, hooks, ns, nextSibling);
          }
        }
        function updateText(old, vnode) {
          if (old.children.toString() !== vnode.children.toString()) {
            old.dom.nodeValue = vnode.children;
          }
          vnode.dom = old.dom;
        }
        function updateHTML(parent, old, vnode, ns, nextSibling) {
          if (old.children !== vnode.children) {
            removeHTML(parent, old);
            createHTML(parent, vnode, ns, nextSibling);
          } else {
            vnode.dom = old.dom;
            vnode.domSize = old.domSize;
            vnode.instance = old.instance;
          }
        }
        function updateFragment(parent, old, vnode, hooks, nextSibling, ns) {
          updateNodes(parent, old.children, vnode.children, hooks, nextSibling, ns);
          var domSize = 0, children = vnode.children;
          vnode.dom = null;
          if (children != null) {
            for (var i2 = 0; i2 < children.length; i2++) {
              var child = children[i2];
              if (child != null && child.dom != null) {
                if (vnode.dom == null)
                  vnode.dom = child.dom;
                domSize += child.domSize || 1;
              }
            }
            if (domSize !== 1)
              vnode.domSize = domSize;
          }
        }
        function updateElement(old, vnode, hooks, ns) {
          var element = vnode.dom = old.dom;
          ns = getNameSpace(vnode) || ns;
          if (vnode.tag === "textarea") {
            if (vnode.attrs == null)
              vnode.attrs = {};
          }
          updateAttrs(vnode, old.attrs, vnode.attrs, ns);
          if (!maybeSetContentEditable(vnode)) {
            updateNodes(element, old.children, vnode.children, hooks, null, ns);
          }
        }
        function updateComponent(parent, old, vnode, hooks, nextSibling, ns) {
          vnode.instance = Vnode.normalize(callHook.call(vnode.state.view, vnode));
          if (vnode.instance === vnode)
            throw Error("A view cannot return the vnode it received as argument");
          updateLifecycle(vnode.state, vnode, hooks);
          if (vnode.attrs != null)
            updateLifecycle(vnode.attrs, vnode, hooks);
          if (vnode.instance != null) {
            if (old.instance == null)
              createNode(parent, vnode.instance, hooks, ns, nextSibling);
            else
              updateNode(parent, old.instance, vnode.instance, hooks, nextSibling, ns);
            vnode.dom = vnode.instance.dom;
            vnode.domSize = vnode.instance.domSize;
          } else if (old.instance != null) {
            removeNode(parent, old.instance);
            vnode.dom = void 0;
            vnode.domSize = 0;
          } else {
            vnode.dom = old.dom;
            vnode.domSize = old.domSize;
          }
        }
        function getKeyMap(vnodes, start, end) {
          var map = /* @__PURE__ */ Object.create(null);
          for (; start < end; start++) {
            var vnode = vnodes[start];
            if (vnode != null) {
              var key = vnode.key;
              if (key != null)
                map[key] = start;
            }
          }
          return map;
        }
        var lisTemp = [];
        function makeLisIndices(a3) {
          var result = [0];
          var u2 = 0, v2 = 0, i2 = 0;
          var il = lisTemp.length = a3.length;
          for (var i2 = 0; i2 < il; i2++)
            lisTemp[i2] = a3[i2];
          for (var i2 = 0; i2 < il; ++i2) {
            if (a3[i2] === -1)
              continue;
            var j2 = result[result.length - 1];
            if (a3[j2] < a3[i2]) {
              lisTemp[i2] = j2;
              result.push(i2);
              continue;
            }
            u2 = 0;
            v2 = result.length - 1;
            while (u2 < v2) {
              var c2 = (u2 >>> 1) + (v2 >>> 1) + (u2 & v2 & 1);
              if (a3[result[c2]] < a3[i2]) {
                u2 = c2 + 1;
              } else {
                v2 = c2;
              }
            }
            if (a3[i2] < a3[result[u2]]) {
              if (u2 > 0)
                lisTemp[i2] = result[u2 - 1];
              result[u2] = i2;
            }
          }
          u2 = result.length;
          v2 = result[u2 - 1];
          while (u2-- > 0) {
            result[u2] = v2;
            v2 = lisTemp[v2];
          }
          lisTemp.length = 0;
          return result;
        }
        function getNextSibling(vnodes, i2, nextSibling) {
          for (; i2 < vnodes.length; i2++) {
            if (vnodes[i2] != null && vnodes[i2].dom != null)
              return vnodes[i2].dom;
          }
          return nextSibling;
        }
        function moveNodes(parent, vnode, nextSibling) {
          var frag = $doc.createDocumentFragment();
          moveChildToFrag(parent, frag, vnode);
          insertNode(parent, frag, nextSibling);
        }
        function moveChildToFrag(parent, frag, vnode) {
          while (vnode.dom != null && vnode.dom.parentNode === parent) {
            if (typeof vnode.tag !== "string") {
              vnode = vnode.instance;
              if (vnode != null)
                continue;
            } else if (vnode.tag === "<") {
              for (var i2 = 0; i2 < vnode.instance.length; i2++) {
                frag.appendChild(vnode.instance[i2]);
              }
            } else if (vnode.tag !== "[") {
              frag.appendChild(vnode.dom);
            } else if (vnode.children.length === 1) {
              vnode = vnode.children[0];
              if (vnode != null)
                continue;
            } else {
              for (var i2 = 0; i2 < vnode.children.length; i2++) {
                var child = vnode.children[i2];
                if (child != null)
                  moveChildToFrag(parent, frag, child);
              }
            }
            break;
          }
        }
        function insertNode(parent, dom, nextSibling) {
          if (nextSibling != null)
            parent.insertBefore(dom, nextSibling);
          else
            parent.appendChild(dom);
        }
        function maybeSetContentEditable(vnode) {
          if (vnode.attrs == null || vnode.attrs.contenteditable == null && vnode.attrs.contentEditable == null)
            return false;
          var children = vnode.children;
          if (children != null && children.length === 1 && children[0].tag === "<") {
            var content = children[0].children;
            if (vnode.dom.innerHTML !== content)
              vnode.dom.innerHTML = content;
          } else if (children != null && children.length !== 0)
            throw new Error("Child node of a contenteditable must be trusted.");
          return true;
        }
        function removeNodes(parent, vnodes, start, end) {
          for (var i2 = start; i2 < end; i2++) {
            var vnode = vnodes[i2];
            if (vnode != null)
              removeNode(parent, vnode);
          }
        }
        function removeNode(parent, vnode) {
          var mask = 0;
          var original = vnode.state;
          var stateResult, attrsResult;
          if (typeof vnode.tag !== "string" && typeof vnode.state.onbeforeremove === "function") {
            var result = callHook.call(vnode.state.onbeforeremove, vnode);
            if (result != null && typeof result.then === "function") {
              mask = 1;
              stateResult = result;
            }
          }
          if (vnode.attrs && typeof vnode.attrs.onbeforeremove === "function") {
            var result = callHook.call(vnode.attrs.onbeforeremove, vnode);
            if (result != null && typeof result.then === "function") {
              mask |= 2;
              attrsResult = result;
            }
          }
          checkState(vnode, original);
          if (!mask) {
            onremove(vnode);
            removeChild(parent, vnode);
          } else {
            if (stateResult != null) {
              var next = function() {
                if (mask & 1) {
                  mask &= 2;
                  if (!mask)
                    reallyRemove();
                }
              };
              stateResult.then(next, next);
            }
            if (attrsResult != null) {
              var next = function() {
                if (mask & 2) {
                  mask &= 1;
                  if (!mask)
                    reallyRemove();
                }
              };
              attrsResult.then(next, next);
            }
          }
          function reallyRemove() {
            checkState(vnode, original);
            onremove(vnode);
            removeChild(parent, vnode);
          }
        }
        function removeHTML(parent, vnode) {
          for (var i2 = 0; i2 < vnode.instance.length; i2++) {
            parent.removeChild(vnode.instance[i2]);
          }
        }
        function removeChild(parent, vnode) {
          while (vnode.dom != null && vnode.dom.parentNode === parent) {
            if (typeof vnode.tag !== "string") {
              vnode = vnode.instance;
              if (vnode != null)
                continue;
            } else if (vnode.tag === "<") {
              removeHTML(parent, vnode);
            } else {
              if (vnode.tag !== "[") {
                parent.removeChild(vnode.dom);
                if (!Array.isArray(vnode.children))
                  break;
              }
              if (vnode.children.length === 1) {
                vnode = vnode.children[0];
                if (vnode != null)
                  continue;
              } else {
                for (var i2 = 0; i2 < vnode.children.length; i2++) {
                  var child = vnode.children[i2];
                  if (child != null)
                    removeChild(parent, child);
                }
              }
            }
            break;
          }
        }
        function onremove(vnode) {
          if (typeof vnode.tag !== "string" && typeof vnode.state.onremove === "function")
            callHook.call(vnode.state.onremove, vnode);
          if (vnode.attrs && typeof vnode.attrs.onremove === "function")
            callHook.call(vnode.attrs.onremove, vnode);
          if (typeof vnode.tag !== "string") {
            if (vnode.instance != null)
              onremove(vnode.instance);
          } else {
            var children = vnode.children;
            if (Array.isArray(children)) {
              for (var i2 = 0; i2 < children.length; i2++) {
                var child = children[i2];
                if (child != null)
                  onremove(child);
              }
            }
          }
        }
        function setAttrs(vnode, attrs, ns) {
          if (vnode.tag === "input" && attrs.type != null)
            vnode.dom.setAttribute("type", attrs.type);
          var isFileInput = attrs != null && vnode.tag === "input" && attrs.type === "file";
          for (var key in attrs) {
            setAttr(vnode, key, null, attrs[key], ns, isFileInput);
          }
        }
        function setAttr(vnode, key, old, value, ns, isFileInput) {
          if (key === "key" || key === "is" || value == null || isLifecycleMethod(key) || old === value && !isFormAttribute(vnode, key) && typeof value !== "object" || key === "type" && vnode.tag === "input")
            return;
          if (key[0] === "o" && key[1] === "n")
            return updateEvent(vnode, key, value);
          if (key.slice(0, 6) === "xlink:")
            vnode.dom.setAttributeNS("http://www.w3.org/1999/xlink", key.slice(6), value);
          else if (key === "style")
            updateStyle(vnode.dom, old, value);
          else if (hasPropertyKey(vnode, key, ns)) {
            if (key === "value") {
              if ((vnode.tag === "input" || vnode.tag === "textarea") && vnode.dom.value === "" + value && (isFileInput || vnode.dom === activeElement()))
                return;
              if (vnode.tag === "select" && old !== null && vnode.dom.value === "" + value)
                return;
              if (vnode.tag === "option" && old !== null && vnode.dom.value === "" + value)
                return;
              if (isFileInput && "" + value !== "") {
                console.error("`value` is read-only on file inputs!");
                return;
              }
            }
            vnode.dom[key] = value;
          } else {
            if (typeof value === "boolean") {
              if (value)
                vnode.dom.setAttribute(key, "");
              else
                vnode.dom.removeAttribute(key);
            } else
              vnode.dom.setAttribute(key === "className" ? "class" : key, value);
          }
        }
        function removeAttr(vnode, key, old, ns) {
          if (key === "key" || key === "is" || old == null || isLifecycleMethod(key))
            return;
          if (key[0] === "o" && key[1] === "n")
            updateEvent(vnode, key, void 0);
          else if (key === "style")
            updateStyle(vnode.dom, old, null);
          else if (hasPropertyKey(vnode, key, ns) && key !== "className" && key !== "title" && !(key === "value" && (vnode.tag === "option" || vnode.tag === "select" && vnode.dom.selectedIndex === -1 && vnode.dom === activeElement())) && !(vnode.tag === "input" && key === "type")) {
            vnode.dom[key] = null;
          } else {
            var nsLastIndex = key.indexOf(":");
            if (nsLastIndex !== -1)
              key = key.slice(nsLastIndex + 1);
            if (old !== false)
              vnode.dom.removeAttribute(key === "className" ? "class" : key);
          }
        }
        function setLateSelectAttrs(vnode, attrs) {
          if ("value" in attrs) {
            if (attrs.value === null) {
              if (vnode.dom.selectedIndex !== -1)
                vnode.dom.value = null;
            } else {
              var normalized = "" + attrs.value;
              if (vnode.dom.value !== normalized || vnode.dom.selectedIndex === -1) {
                vnode.dom.value = normalized;
              }
            }
          }
          if ("selectedIndex" in attrs)
            setAttr(vnode, "selectedIndex", null, attrs.selectedIndex, void 0);
        }
        function updateAttrs(vnode, old, attrs, ns) {
          if (old && old === attrs) {
            console.warn("Don't reuse attrs object, use new object for every redraw, this will throw in next major");
          }
          if (attrs != null) {
            if (vnode.tag === "input" && attrs.type != null)
              vnode.dom.setAttribute("type", attrs.type);
            var isFileInput = vnode.tag === "input" && attrs.type === "file";
            for (var key in attrs) {
              setAttr(vnode, key, old && old[key], attrs[key], ns, isFileInput);
            }
          }
          var val;
          if (old != null) {
            for (var key in old) {
              if ((val = old[key]) != null && (attrs == null || attrs[key] == null)) {
                removeAttr(vnode, key, val, ns);
              }
            }
          }
        }
        function isFormAttribute(vnode, attr) {
          return attr === "value" || attr === "checked" || attr === "selectedIndex" || attr === "selected" && vnode.dom === activeElement() || vnode.tag === "option" && vnode.dom.parentNode === $doc.activeElement;
        }
        function isLifecycleMethod(attr) {
          return attr === "oninit" || attr === "oncreate" || attr === "onupdate" || attr === "onremove" || attr === "onbeforeremove" || attr === "onbeforeupdate";
        }
        function hasPropertyKey(vnode, key, ns) {
          return ns === void 0 && (vnode.tag.indexOf("-") > -1 || vnode.attrs != null && vnode.attrs.is || key !== "href" && key !== "list" && key !== "form" && key !== "width" && key !== "height") && key in vnode.dom;
        }
        var uppercaseRegex = /[A-Z]/g;
        function toLowerCase(capital) {
          return "-" + capital.toLowerCase();
        }
        function normalizeKey(key) {
          return key[0] === "-" && key[1] === "-" ? key : key === "cssFloat" ? "float" : key.replace(uppercaseRegex, toLowerCase);
        }
        function updateStyle(element, old, style) {
          if (old === style) {
          } else if (style == null) {
            element.style.cssText = "";
          } else if (typeof style !== "object") {
            element.style.cssText = style;
          } else if (old == null || typeof old !== "object") {
            element.style.cssText = "";
            for (var key in style) {
              var value = style[key];
              if (value != null)
                element.style.setProperty(normalizeKey(key), String(value));
            }
          } else {
            for (var key in style) {
              var value = style[key];
              if (value != null && (value = String(value)) !== String(old[key])) {
                element.style.setProperty(normalizeKey(key), value);
              }
            }
            for (var key in old) {
              if (old[key] != null && style[key] == null) {
                element.style.removeProperty(normalizeKey(key));
              }
            }
          }
        }
        function EventDict() {
          this._ = currentRedraw;
        }
        EventDict.prototype = /* @__PURE__ */ Object.create(null);
        EventDict.prototype.handleEvent = function(ev) {
          var handler = this["on" + ev.type];
          var result;
          if (typeof handler === "function")
            result = handler.call(ev.currentTarget, ev);
          else if (typeof handler.handleEvent === "function")
            handler.handleEvent(ev);
          if (this._ && ev.redraw !== false)
            (0, this._)();
          if (result === false) {
            ev.preventDefault();
            ev.stopPropagation();
          }
        };
        function updateEvent(vnode, key, value) {
          if (vnode.events != null) {
            vnode.events._ = currentRedraw;
            if (vnode.events[key] === value)
              return;
            if (value != null && (typeof value === "function" || typeof value === "object")) {
              if (vnode.events[key] == null)
                vnode.dom.addEventListener(key.slice(2), vnode.events, false);
              vnode.events[key] = value;
            } else {
              if (vnode.events[key] != null)
                vnode.dom.removeEventListener(key.slice(2), vnode.events, false);
              vnode.events[key] = void 0;
            }
          } else if (value != null && (typeof value === "function" || typeof value === "object")) {
            vnode.events = new EventDict();
            vnode.dom.addEventListener(key.slice(2), vnode.events, false);
            vnode.events[key] = value;
          }
        }
        function initLifecycle(source, vnode, hooks) {
          if (typeof source.oninit === "function")
            callHook.call(source.oninit, vnode);
          if (typeof source.oncreate === "function")
            hooks.push(callHook.bind(source.oncreate, vnode));
        }
        function updateLifecycle(source, vnode, hooks) {
          if (typeof source.onupdate === "function")
            hooks.push(callHook.bind(source.onupdate, vnode));
        }
        function shouldNotUpdate(vnode, old) {
          do {
            if (vnode.attrs != null && typeof vnode.attrs.onbeforeupdate === "function") {
              var force = callHook.call(vnode.attrs.onbeforeupdate, vnode, old);
              if (force !== void 0 && !force)
                break;
            }
            if (typeof vnode.tag !== "string" && typeof vnode.state.onbeforeupdate === "function") {
              var force = callHook.call(vnode.state.onbeforeupdate, vnode, old);
              if (force !== void 0 && !force)
                break;
            }
            return false;
          } while (false);
          vnode.dom = old.dom;
          vnode.domSize = old.domSize;
          vnode.instance = old.instance;
          vnode.attrs = old.attrs;
          vnode.children = old.children;
          vnode.text = old.text;
          return true;
        }
        var currentDOM;
        return function(dom, vnodes, redraw) {
          if (!dom)
            throw new TypeError("DOM element being rendered to does not exist.");
          if (currentDOM != null && dom.contains(currentDOM)) {
            throw new TypeError("Node is currently being rendered to and thus is locked.");
          }
          var prevRedraw = currentRedraw;
          var prevDOM = currentDOM;
          var hooks = [];
          var active = activeElement();
          var namespace = dom.namespaceURI;
          currentDOM = dom;
          currentRedraw = typeof redraw === "function" ? redraw : void 0;
          try {
            if (dom.vnodes == null)
              dom.textContent = "";
            vnodes = Vnode.normalizeChildren(Array.isArray(vnodes) ? vnodes : [vnodes]);
            updateNodes(dom, dom.vnodes, vnodes, hooks, null, namespace === "http://www.w3.org/1999/xhtml" ? void 0 : namespace);
            dom.vnodes = vnodes;
            if (active != null && activeElement() !== active && typeof active.focus === "function")
              active.focus();
            for (var i2 = 0; i2 < hooks.length; i2++)
              hooks[i2]();
          } finally {
            currentRedraw = prevRedraw;
            currentDOM = prevDOM;
          }
        };
      };
    }
  });

  // node_modules/mithril/render.js
  var require_render2 = __commonJS({
    "node_modules/mithril/render.js"(exports, module) {
      "use strict";
      module.exports = require_render()(typeof window !== "undefined" ? window : null);
    }
  });

  // node_modules/mithril/api/mount-redraw.js
  var require_mount_redraw = __commonJS({
    "node_modules/mithril/api/mount-redraw.js"(exports, module) {
      "use strict";
      var Vnode = require_vnode();
      module.exports = function(render, schedule, console2) {
        var subscriptions = [];
        var pending = false;
        var offset = -1;
        function sync() {
          for (offset = 0; offset < subscriptions.length; offset += 2) {
            try {
              render(subscriptions[offset], Vnode(subscriptions[offset + 1]), redraw);
            } catch (e2) {
              console2.error(e2);
            }
          }
          offset = -1;
        }
        function redraw() {
          if (!pending) {
            pending = true;
            schedule(function() {
              pending = false;
              sync();
            });
          }
        }
        redraw.sync = sync;
        function mount(root, component) {
          if (component != null && component.view == null && typeof component !== "function") {
            throw new TypeError("m.mount expects a component, not a vnode.");
          }
          var index = subscriptions.indexOf(root);
          if (index >= 0) {
            subscriptions.splice(index, 2);
            if (index <= offset)
              offset -= 2;
            render(root, []);
          }
          if (component != null) {
            subscriptions.push(root, component);
            render(root, Vnode(component), redraw);
          }
        }
        return { mount, redraw };
      };
    }
  });

  // node_modules/mithril/mount-redraw.js
  var require_mount_redraw2 = __commonJS({
    "node_modules/mithril/mount-redraw.js"(exports, module) {
      "use strict";
      var render = require_render2();
      module.exports = require_mount_redraw()(render, typeof requestAnimationFrame !== "undefined" ? requestAnimationFrame : null, typeof console !== "undefined" ? console : null);
    }
  });

  // node_modules/mithril/querystring/build.js
  var require_build = __commonJS({
    "node_modules/mithril/querystring/build.js"(exports, module) {
      "use strict";
      module.exports = function(object2) {
        if (Object.prototype.toString.call(object2) !== "[object Object]")
          return "";
        var args = [];
        for (var key in object2) {
          destructure(key, object2[key]);
        }
        return args.join("&");
        function destructure(key2, value) {
          if (Array.isArray(value)) {
            for (var i2 = 0; i2 < value.length; i2++) {
              destructure(key2 + "[" + i2 + "]", value[i2]);
            }
          } else if (Object.prototype.toString.call(value) === "[object Object]") {
            for (var i2 in value) {
              destructure(key2 + "[" + i2 + "]", value[i2]);
            }
          } else
            args.push(encodeURIComponent(key2) + (value != null && value !== "" ? "=" + encodeURIComponent(value) : ""));
        }
      };
    }
  });

  // node_modules/mithril/util/assign.js
  var require_assign = __commonJS({
    "node_modules/mithril/util/assign.js"(exports, module) {
      "use strict";
      var hasOwn = require_hasOwn();
      module.exports = Object.assign || function(target, source) {
        for (var key in source) {
          if (hasOwn.call(source, key))
            target[key] = source[key];
        }
      };
    }
  });

  // node_modules/mithril/pathname/build.js
  var require_build2 = __commonJS({
    "node_modules/mithril/pathname/build.js"(exports, module) {
      "use strict";
      var buildQueryString = require_build();
      var assign = require_assign();
      module.exports = function(template, params) {
        if (/:([^\/\.-]+)(\.{3})?:/.test(template)) {
          throw new SyntaxError("Template parameter names must be separated by either a '/', '-', or '.'.");
        }
        if (params == null)
          return template;
        var queryIndex = template.indexOf("?");
        var hashIndex = template.indexOf("#");
        var queryEnd = hashIndex < 0 ? template.length : hashIndex;
        var pathEnd = queryIndex < 0 ? queryEnd : queryIndex;
        var path = template.slice(0, pathEnd);
        var query = {};
        assign(query, params);
        var resolved = path.replace(/:([^\/\.-]+)(\.{3})?/g, function(m39, key, variadic) {
          delete query[key];
          if (params[key] == null)
            return m39;
          return variadic ? params[key] : encodeURIComponent(String(params[key]));
        });
        var newQueryIndex = resolved.indexOf("?");
        var newHashIndex = resolved.indexOf("#");
        var newQueryEnd = newHashIndex < 0 ? resolved.length : newHashIndex;
        var newPathEnd = newQueryIndex < 0 ? newQueryEnd : newQueryIndex;
        var result = resolved.slice(0, newPathEnd);
        if (queryIndex >= 0)
          result += template.slice(queryIndex, queryEnd);
        if (newQueryIndex >= 0)
          result += (queryIndex < 0 ? "?" : "&") + resolved.slice(newQueryIndex, newQueryEnd);
        var querystring = buildQueryString(query);
        if (querystring)
          result += (queryIndex < 0 && newQueryIndex < 0 ? "?" : "&") + querystring;
        if (hashIndex >= 0)
          result += template.slice(hashIndex);
        if (newHashIndex >= 0)
          result += (hashIndex < 0 ? "" : "&") + resolved.slice(newHashIndex);
        return result;
      };
    }
  });

  // node_modules/mithril/request/request.js
  var require_request = __commonJS({
    "node_modules/mithril/request/request.js"(exports, module) {
      "use strict";
      var buildPathname = require_build2();
      var hasOwn = require_hasOwn();
      module.exports = function($window, Promise2, oncompletion) {
        var callbackCount = 0;
        function PromiseProxy(executor) {
          return new Promise2(executor);
        }
        PromiseProxy.prototype = Promise2.prototype;
        PromiseProxy.__proto__ = Promise2;
        function makeRequest(factory) {
          return function(url, args) {
            if (typeof url !== "string") {
              args = url;
              url = url.url;
            } else if (args == null)
              args = {};
            var promise = new Promise2(function(resolve2, reject) {
              factory(buildPathname(url, args.params), args, function(data) {
                if (typeof args.type === "function") {
                  if (Array.isArray(data)) {
                    for (var i2 = 0; i2 < data.length; i2++) {
                      data[i2] = new args.type(data[i2]);
                    }
                  } else
                    data = new args.type(data);
                }
                resolve2(data);
              }, reject);
            });
            if (args.background === true)
              return promise;
            var count = 0;
            function complete() {
              if (--count === 0 && typeof oncompletion === "function")
                oncompletion();
            }
            return wrap(promise);
            function wrap(promise2) {
              var then = promise2.then;
              promise2.constructor = PromiseProxy;
              promise2.then = function() {
                count++;
                var next = then.apply(promise2, arguments);
                next.then(complete, function(e2) {
                  complete();
                  if (count === 0)
                    throw e2;
                });
                return wrap(next);
              };
              return promise2;
            }
          };
        }
        function hasHeader(args, name) {
          for (var key in args.headers) {
            if (hasOwn.call(args.headers, key) && key.toLowerCase() === name)
              return true;
          }
          return false;
        }
        return {
          request: makeRequest(function(url, args, resolve2, reject) {
            var method = args.method != null ? args.method.toUpperCase() : "GET";
            var body = args.body;
            var assumeJSON = (args.serialize == null || args.serialize === JSON.serialize) && !(body instanceof $window.FormData || body instanceof $window.URLSearchParams);
            var responseType = args.responseType || (typeof args.extract === "function" ? "" : "json");
            var xhr = new $window.XMLHttpRequest(), aborted = false, isTimeout = false;
            var original = xhr, replacedAbort;
            var abort = xhr.abort;
            xhr.abort = function() {
              aborted = true;
              abort.call(this);
            };
            xhr.open(method, url, args.async !== false, typeof args.user === "string" ? args.user : void 0, typeof args.password === "string" ? args.password : void 0);
            if (assumeJSON && body != null && !hasHeader(args, "content-type")) {
              xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            }
            if (typeof args.deserialize !== "function" && !hasHeader(args, "accept")) {
              xhr.setRequestHeader("Accept", "application/json, text/*");
            }
            if (args.withCredentials)
              xhr.withCredentials = args.withCredentials;
            if (args.timeout)
              xhr.timeout = args.timeout;
            xhr.responseType = responseType;
            for (var key in args.headers) {
              if (hasOwn.call(args.headers, key)) {
                xhr.setRequestHeader(key, args.headers[key]);
              }
            }
            xhr.onreadystatechange = function(ev) {
              if (aborted)
                return;
              if (ev.target.readyState === 4) {
                try {
                  var success = ev.target.status >= 200 && ev.target.status < 300 || ev.target.status === 304 || /^file:\/\//i.test(url);
                  var response = ev.target.response, message;
                  if (responseType === "json") {
                    if (!ev.target.responseType && typeof args.extract !== "function") {
                      try {
                        response = JSON.parse(ev.target.responseText);
                      } catch (e2) {
                        response = null;
                      }
                    }
                  } else if (!responseType || responseType === "text") {
                    if (response == null)
                      response = ev.target.responseText;
                  }
                  if (typeof args.extract === "function") {
                    response = args.extract(ev.target, args);
                    success = true;
                  } else if (typeof args.deserialize === "function") {
                    response = args.deserialize(response);
                  }
                  if (success)
                    resolve2(response);
                  else {
                    var completeErrorResponse = function() {
                      try {
                        message = ev.target.responseText;
                      } catch (e2) {
                        message = response;
                      }
                      var error = new Error(message);
                      error.code = ev.target.status;
                      error.response = response;
                      reject(error);
                    };
                    if (xhr.status === 0) {
                      setTimeout(function() {
                        if (isTimeout)
                          return;
                        completeErrorResponse();
                      });
                    } else
                      completeErrorResponse();
                  }
                } catch (e2) {
                  reject(e2);
                }
              }
            };
            xhr.ontimeout = function(ev) {
              isTimeout = true;
              var error = new Error("Request timed out");
              error.code = ev.target.status;
              reject(error);
            };
            if (typeof args.config === "function") {
              xhr = args.config(xhr, args, url) || xhr;
              if (xhr !== original) {
                replacedAbort = xhr.abort;
                xhr.abort = function() {
                  aborted = true;
                  replacedAbort.call(this);
                };
              }
            }
            if (body == null)
              xhr.send();
            else if (typeof args.serialize === "function")
              xhr.send(args.serialize(body));
            else if (body instanceof $window.FormData || body instanceof $window.URLSearchParams)
              xhr.send(body);
            else
              xhr.send(JSON.stringify(body));
          }),
          jsonp: makeRequest(function(url, args, resolve2, reject) {
            var callbackName = args.callbackName || "_mithril_" + Math.round(Math.random() * 1e16) + "_" + callbackCount++;
            var script = $window.document.createElement("script");
            $window[callbackName] = function(data) {
              delete $window[callbackName];
              script.parentNode.removeChild(script);
              resolve2(data);
            };
            script.onerror = function() {
              delete $window[callbackName];
              script.parentNode.removeChild(script);
              reject(new Error("JSONP request failed"));
            };
            script.src = url + (url.indexOf("?") < 0 ? "?" : "&") + encodeURIComponent(args.callbackKey || "callback") + "=" + encodeURIComponent(callbackName);
            $window.document.documentElement.appendChild(script);
          })
        };
      };
    }
  });

  // node_modules/mithril/request.js
  var require_request2 = __commonJS({
    "node_modules/mithril/request.js"(exports, module) {
      "use strict";
      var PromisePolyfill = require_promise();
      var mountRedraw = require_mount_redraw2();
      module.exports = require_request()(typeof window !== "undefined" ? window : null, PromisePolyfill, mountRedraw.redraw);
    }
  });

  // node_modules/mithril/querystring/parse.js
  var require_parse = __commonJS({
    "node_modules/mithril/querystring/parse.js"(exports, module) {
      "use strict";
      function decodeURIComponentSave(str) {
        try {
          return decodeURIComponent(str);
        } catch (err) {
          return str;
        }
      }
      module.exports = function(string) {
        if (string === "" || string == null)
          return {};
        if (string.charAt(0) === "?")
          string = string.slice(1);
        var entries = string.split("&"), counters = {}, data = {};
        for (var i2 = 0; i2 < entries.length; i2++) {
          var entry = entries[i2].split("=");
          var key = decodeURIComponentSave(entry[0]);
          var value = entry.length === 2 ? decodeURIComponentSave(entry[1]) : "";
          if (value === "true")
            value = true;
          else if (value === "false")
            value = false;
          var levels = key.split(/\]\[?|\[/);
          var cursor = data;
          if (key.indexOf("[") > -1)
            levels.pop();
          for (var j2 = 0; j2 < levels.length; j2++) {
            var level = levels[j2], nextLevel = levels[j2 + 1];
            var isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10));
            if (level === "") {
              var key = levels.slice(0, j2).join();
              if (counters[key] == null) {
                counters[key] = Array.isArray(cursor) ? cursor.length : 0;
              }
              level = counters[key]++;
            } else if (level === "__proto__")
              break;
            if (j2 === levels.length - 1)
              cursor[level] = value;
            else {
              var desc = Object.getOwnPropertyDescriptor(cursor, level);
              if (desc != null)
                desc = desc.value;
              if (desc == null)
                cursor[level] = desc = isNumber ? [] : {};
              cursor = desc;
            }
          }
        }
        return data;
      };
    }
  });

  // node_modules/mithril/pathname/parse.js
  var require_parse2 = __commonJS({
    "node_modules/mithril/pathname/parse.js"(exports, module) {
      "use strict";
      var parseQueryString2 = require_parse();
      module.exports = function(url) {
        var queryIndex = url.indexOf("?");
        var hashIndex = url.indexOf("#");
        var queryEnd = hashIndex < 0 ? url.length : hashIndex;
        var pathEnd = queryIndex < 0 ? queryEnd : queryIndex;
        var path = url.slice(0, pathEnd).replace(/\/{2,}/g, "/");
        if (!path)
          path = "/";
        else {
          if (path[0] !== "/")
            path = "/" + path;
          if (path.length > 1 && path[path.length - 1] === "/")
            path = path.slice(0, -1);
        }
        return {
          path,
          params: queryIndex < 0 ? {} : parseQueryString2(url.slice(queryIndex + 1, queryEnd))
        };
      };
    }
  });

  // node_modules/mithril/pathname/compileTemplate.js
  var require_compileTemplate = __commonJS({
    "node_modules/mithril/pathname/compileTemplate.js"(exports, module) {
      "use strict";
      var parsePathname = require_parse2();
      module.exports = function(template) {
        var templateData = parsePathname(template);
        var templateKeys = Object.keys(templateData.params);
        var keys = [];
        var regexp = new RegExp("^" + templateData.path.replace(
          /:([^\/.-]+)(\.{3}|\.(?!\.)|-)?|[\\^$*+.()|\[\]{}]/g,
          function(m39, key, extra) {
            if (key == null)
              return "\\" + m39;
            keys.push({ k: key, r: extra === "..." });
            if (extra === "...")
              return "(.*)";
            if (extra === ".")
              return "([^/]+)\\.";
            return "([^/]+)" + (extra || "");
          }
        ) + "$");
        return function(data) {
          for (var i2 = 0; i2 < templateKeys.length; i2++) {
            if (templateData.params[templateKeys[i2]] !== data.params[templateKeys[i2]])
              return false;
          }
          if (!keys.length)
            return regexp.test(data.path);
          var values = regexp.exec(data.path);
          if (values == null)
            return false;
          for (var i2 = 0; i2 < keys.length; i2++) {
            data.params[keys[i2].k] = keys[i2].r ? values[i2 + 1] : decodeURIComponent(values[i2 + 1]);
          }
          return true;
        };
      };
    }
  });

  // node_modules/mithril/util/censor.js
  var require_censor = __commonJS({
    "node_modules/mithril/util/censor.js"(exports, module) {
      "use strict";
      var hasOwn = require_hasOwn();
      var magic = new RegExp("^(?:key|oninit|oncreate|onbeforeupdate|onupdate|onbeforeremove|onremove)$");
      module.exports = function(attrs, extras) {
        var result = {};
        if (extras != null) {
          for (var key in attrs) {
            if (hasOwn.call(attrs, key) && !magic.test(key) && extras.indexOf(key) < 0) {
              result[key] = attrs[key];
            }
          }
        } else {
          for (var key in attrs) {
            if (hasOwn.call(attrs, key) && !magic.test(key)) {
              result[key] = attrs[key];
            }
          }
        }
        return result;
      };
    }
  });

  // node_modules/mithril/api/router.js
  var require_router = __commonJS({
    "node_modules/mithril/api/router.js"(exports, module) {
      "use strict";
      var Vnode = require_vnode();
      var m39 = require_hyperscript();
      var Promise2 = require_promise();
      var buildPathname = require_build2();
      var parsePathname = require_parse2();
      var compileTemplate = require_compileTemplate();
      var assign = require_assign();
      var censor = require_censor();
      var sentinel = {};
      function decodeURIComponentSave(component) {
        try {
          return decodeURIComponent(component);
        } catch (e2) {
          return component;
        }
      }
      module.exports = function($window, mountRedraw) {
        var callAsync = $window == null ? null : typeof $window.setImmediate === "function" ? $window.setImmediate : $window.setTimeout;
        var p2 = Promise2.resolve();
        var scheduled = false;
        var ready = false;
        var state = 0;
        var compiled, fallbackRoute;
        var currentResolver = sentinel, component, attrs, currentPath, lastUpdate;
        var RouterRoot = {
          onbeforeupdate: function() {
            state = state ? 2 : 1;
            return !(!state || sentinel === currentResolver);
          },
          onremove: function() {
            $window.removeEventListener("popstate", fireAsync, false);
            $window.removeEventListener("hashchange", resolveRoute, false);
          },
          view: function() {
            if (!state || sentinel === currentResolver)
              return;
            var vnode = [Vnode(component, attrs.key, attrs)];
            if (currentResolver)
              vnode = currentResolver.render(vnode[0]);
            return vnode;
          }
        };
        var SKIP = route.SKIP = {};
        function resolveRoute() {
          scheduled = false;
          var prefix = $window.location.hash;
          if (route.prefix[0] !== "#") {
            prefix = $window.location.search + prefix;
            if (route.prefix[0] !== "?") {
              prefix = $window.location.pathname + prefix;
              if (prefix[0] !== "/")
                prefix = "/" + prefix;
            }
          }
          var path = prefix.concat().replace(/(?:%[a-f89][a-f0-9])+/gim, decodeURIComponentSave).slice(route.prefix.length);
          var data = parsePathname(path);
          assign(data.params, $window.history.state);
          function reject(e2) {
            console.error(e2);
            setPath(fallbackRoute, null, { replace: true });
          }
          loop(0);
          function loop(i2) {
            for (; i2 < compiled.length; i2++) {
              if (compiled[i2].check(data)) {
                var payload = compiled[i2].component;
                var matchedRoute = compiled[i2].route;
                var localComp = payload;
                var update = lastUpdate = function(comp) {
                  if (update !== lastUpdate)
                    return;
                  if (comp === SKIP)
                    return loop(i2 + 1);
                  component = comp != null && (typeof comp.view === "function" || typeof comp === "function") ? comp : "div";
                  attrs = data.params, currentPath = path, lastUpdate = null;
                  currentResolver = payload.render ? payload : null;
                  if (state === 2)
                    mountRedraw.redraw();
                  else {
                    state = 2;
                    mountRedraw.redraw.sync();
                  }
                };
                if (payload.view || typeof payload === "function") {
                  payload = {};
                  update(localComp);
                } else if (payload.onmatch) {
                  p2.then(function() {
                    return payload.onmatch(data.params, path, matchedRoute);
                  }).then(update, path === fallbackRoute ? null : reject);
                } else
                  update("div");
                return;
              }
            }
            if (path === fallbackRoute) {
              throw new Error("Could not resolve default route " + fallbackRoute + ".");
            }
            setPath(fallbackRoute, null, { replace: true });
          }
        }
        function fireAsync() {
          if (!scheduled) {
            scheduled = true;
            callAsync(resolveRoute);
          }
        }
        function setPath(path, data, options2) {
          path = buildPathname(path, data);
          if (ready) {
            fireAsync();
            var state2 = options2 ? options2.state : null;
            var title = options2 ? options2.title : null;
            if (options2 && options2.replace)
              $window.history.replaceState(state2, title, route.prefix + path);
            else
              $window.history.pushState(state2, title, route.prefix + path);
          } else {
            $window.location.href = route.prefix + path;
          }
        }
        function route(root, defaultRoute, routes) {
          if (!root)
            throw new TypeError("DOM element being rendered to does not exist.");
          compiled = Object.keys(routes).map(function(route2) {
            if (route2[0] !== "/")
              throw new SyntaxError("Routes must start with a '/'.");
            if (/:([^\/\.-]+)(\.{3})?:/.test(route2)) {
              throw new SyntaxError("Route parameter names must be separated with either '/', '.', or '-'.");
            }
            return {
              route: route2,
              component: routes[route2],
              check: compileTemplate(route2)
            };
          });
          fallbackRoute = defaultRoute;
          if (defaultRoute != null) {
            var defaultData = parsePathname(defaultRoute);
            if (!compiled.some(function(i2) {
              return i2.check(defaultData);
            })) {
              throw new ReferenceError("Default route doesn't match any known routes.");
            }
          }
          if (typeof $window.history.pushState === "function") {
            $window.addEventListener("popstate", fireAsync, false);
          } else if (route.prefix[0] === "#") {
            $window.addEventListener("hashchange", resolveRoute, false);
          }
          ready = true;
          mountRedraw.mount(root, RouterRoot);
          resolveRoute();
        }
        route.set = function(path, data, options2) {
          if (lastUpdate != null) {
            options2 = options2 || {};
            options2.replace = true;
          }
          lastUpdate = null;
          setPath(path, data, options2);
        };
        route.get = function() {
          return currentPath;
        };
        route.prefix = "#!";
        route.Link = {
          view: function(vnode) {
            var child = m39(
              vnode.attrs.selector || "a",
              censor(vnode.attrs, ["options", "params", "selector", "onclick"]),
              vnode.children
            );
            var options2, onclick, href;
            if (child.attrs.disabled = Boolean(child.attrs.disabled)) {
              child.attrs.href = null;
              child.attrs["aria-disabled"] = "true";
            } else {
              options2 = vnode.attrs.options;
              onclick = vnode.attrs.onclick;
              href = buildPathname(child.attrs.href, vnode.attrs.params);
              child.attrs.href = route.prefix + href;
              child.attrs.onclick = function(e2) {
                var result;
                if (typeof onclick === "function") {
                  result = onclick.call(e2.currentTarget, e2);
                } else if (onclick == null || typeof onclick !== "object") {
                } else if (typeof onclick.handleEvent === "function") {
                  onclick.handleEvent(e2);
                }
                if (result !== false && !e2.defaultPrevented && (e2.button === 0 || e2.which === 0 || e2.which === 1) && (!e2.currentTarget.target || e2.currentTarget.target === "_self") && !e2.ctrlKey && !e2.metaKey && !e2.shiftKey && !e2.altKey) {
                  e2.preventDefault();
                  e2.redraw = false;
                  route.set(href, null, options2);
                }
              };
            }
            return child;
          }
        };
        route.param = function(key) {
          return attrs && key != null ? attrs[key] : attrs;
        };
        return route;
      };
    }
  });

  // node_modules/mithril/route.js
  var require_route = __commonJS({
    "node_modules/mithril/route.js"(exports, module) {
      "use strict";
      var mountRedraw = require_mount_redraw2();
      module.exports = require_router()(typeof window !== "undefined" ? window : null, mountRedraw);
    }
  });

  // node_modules/mithril/index.js
  var require_mithril = __commonJS({
    "node_modules/mithril/index.js"(exports, module) {
      "use strict";
      var hyperscript = require_hyperscript2();
      var request = require_request2();
      var mountRedraw = require_mount_redraw2();
      var m39 = function m40() {
        return hyperscript.apply(this, arguments);
      };
      m39.m = hyperscript;
      m39.trust = hyperscript.trust;
      m39.fragment = hyperscript.fragment;
      m39.Fragment = "[";
      m39.mount = mountRedraw.mount;
      m39.route = require_route();
      m39.render = require_render2();
      m39.redraw = mountRedraw.redraw;
      m39.request = request.request;
      m39.jsonp = request.jsonp;
      m39.parseQueryString = require_parse();
      m39.buildQueryString = require_build();
      m39.parsePathname = require_parse2();
      m39.buildPathname = require_build2();
      m39.vnode = require_vnode();
      m39.PromisePolyfill = require_polyfill();
      m39.censor = require_censor();
      module.exports = m39;
    }
  });

  // public/assets/images/hedgerabbhog-v3.png
  var require_hedgerabbhog_v3 = __commonJS({
    "public/assets/images/hedgerabbhog-v3.png"(exports, module) {
      module.exports = "./hedgerabbhog-v3-E7VT4VRW.png";
    }
  });

  // src/index.js
  var import_mithril37 = __toESM(require_mithril());

  // src/views/Home.js
  var import_mithril23 = __toESM(require_mithril());

  // src/models/StaticHome.js
  var import_mithril = __toESM(require_mithril());

  // content/home.json
  var home_default = {
    cases: {
      description: "",
      items: [
        {
          colorClasses: "text-red-500",
          content: "Langzeit Medien-Archive und Share.",
          id: "archive",
          title: "Archiv"
        },
        {
          colorClasses: "text-red-500",
          content: "Bespiele deine eigene Social-Media Plattform. F\xFCr laute und leise Quasselstrippen.",
          id: "socialmedia",
          title: "Social Media"
        },
        {
          colorClasses: "text-red-500",
          content: "CRM hei\xDFt Beziehungen aufbauen! Dem Support-Team hilft es. Wie erleichternd!",
          id: "crm",
          title: "CRM"
        },
        {
          colorClasses: "text-red-500",
          content: "Und hoffentlich NICHT zum dealen von Medien Daten mit fremden Lizenzen!",
          id: "not",
          title: "Legal & Eigenverantwortlich"
        },
        {
          colorClasses: "text-red-500",
          content: "Angedockt an deine internen oder \xF6ffentlichen Kan\xE4le. Da es privat und sicher sein muss.",
          id: "collab",
          title: "Business Collaboration"
        }
      ],
      media: {
        src: "",
        type: ""
      },
      subtitle: "",
      title: "Offensichtliche Einsatzgebiete"
    },
    collaborate: {
      actions: [
        {
          action: "",
          classes: "ext-white mx-2 bg-purple-600 hover:bg-purple-700 sm:w-auto",
          href: "https://codeberg.org/konsumi",
          id: "cvision",
          priority: "10",
          title: "Praktisch mitwirken"
        }
      ],
      description: "Dir gef\xE4llt unsere Idee? Wir freuen uns, wenn du uns hilfst sie Realit\xE4t werden zu lassen. Besuche uns auf Codeberg oder schreibe und eine Nachricht.",
      title: "Unterst\xFCtze uns"
    },
    fakten: {
      description: "Konsumi ist ein Kollaborations-Werkzeug auf Basis von Video-Live-Streams und Feeds. Es erleichtert dir den digitalen Austausch, sowie die Zusammenarbeit mit Freunden und Arbeitskollegen. Ein Plugin-System erm\xF6glicht die nahtlose Integration in deinen digitalen Alltag.",
      items: [
        {
          colorClasses: "text-red-500",
          content: "Auf Basis offener Protokolle und Technologien - zur Sicherung der Interoperabilit\xE4t",
          id: "technik",
          title: "Bew\xE4hrte Technik"
        },
        {
          colorClasses: "text-red-500",
          content: "Eigenst\xE4ndige und f\xF6derierende Systeme - community driven",
          id: "dezentral",
          title: "Dezentral"
        },
        {
          colorClasses: "text-lime-500",
          content: "Als Service mit erstklassiger Dienstleistung oder mit voller Daten und Zugriffskontrolle nur f\xFCr dich auf deinem Server. Supreme Vision Support.",
          id: "selfhosted",
          title: "Eigenbetrieb oder in der Wolke"
        },
        {
          colorClasses: "text-red-500",
          content: "Kontrolle \xFCber Daten und Zugriffe",
          id: "datenschutz",
          title: "Datenschutz"
        },
        {
          colorClasses: "text-lime-500",
          content: "Moderne Verschl\xFCsselung bei Transport und Ablage - f\xFCr bestm\xF6gliche Privatsph\xE4re",
          id: "sicherheit",
          title: "Sicherheit"
        },
        {
          colorClasses: "text-blue-500",
          content: "Nahtlose Arbeitserlebnisse und Abl\xE4ufe durch etablierte, offene Anwendungen",
          id: "offen",
          title: "Offen & kompatibel"
        },
        {
          colorClasses: "text-red-500",
          content: "Erweiterbar und wartungsfrei durch h\xF6chstm\xF6gliche Automatisierung",
          id: "einfach",
          title: "Einfach & zeitsparend"
        }
      ],
      media: {
        src: "",
        type: ""
      },
      subtitle: "",
      title: "Harte Fakten"
    },
    future: {
      description: "",
      items: [
        {
          icon: "30",
          id: "payment",
          priority: "40",
          subtitle: "Integrierte Bezahlung",
          title: "Payments"
        },
        {
          icon: "square",
          id: "hub",
          priority: "20",
          subtitle: "Indexierung, um konsumees und deren \xF6ffentlichen Inhalte zu durchsuchen",
          title: "Hub"
        },
        {
          icon: "square",
          id: "analytics",
          priority: "10",
          subtitle: "Analytics und Reports f\xFCr den Creator",
          title: "Auswertung"
        }
      ],
      subtitle: "Konsumi ist ein wachsendes, offenes \xD6kosystem, mit dem Ziel Inhalte einfacher, sicherer, gro\xDFfl\xE4chiger und zuverl\xE4ssiger bereitzustellen. Egal ob im privaten oder \xF6ffentlichen Raum. Ohne eine zwischengeschaltete Instanz. Direkt. Im Mesh. Mit dem Fokus auf Portabilit\xE4t und Wartungsfreiheit.",
      title: "Mit dabei"
    },
    "hero-video": {
      actions: [
        {
          action: "",
          classes: "ext-white bg-gray-200 hover:bg-gray-300 sm:w-auto mx-2",
          href: "https://codeberg.org/konsumi",
          id: "cvision",
          priority: "10",
          title: "Christoph`s Vision"
        },
        {
          action: "",
          classes: "text-white bg-purple-600 hover:bg-purple-700 sm:w-auto mx-2",
          href: "https://codeberg.org/konsumi",
          id: "vision",
          priority: "20",
          title: "Vision"
        },
        {
          action: "",
          classes: "bg-gray-900 text-purple-600 sm:w-auto",
          href: "https://docs.konsumi.rocks/roadmap/long-term/",
          id: "ovision",
          priority: "30",
          title: "Oliver`s Vision"
        }
      ],
      description: "Erlebe ein neues Kollaborations-Werkzeug um virtuell zusammen zu sein - unter deinen Bedingungen ohne Einschr\xE4nkung und mit maximaler Vernetzung",
      media: {
        type: "konsumi",
        url: ""
      },
      title: "Deine Gedanken Trompete"
    },
    kirschen: {
      description: "Konsumi er\xF6ffnet dir neue M\xF6glichkeiten deiner Community einzigartige Inhalte zu bieten. Lasse dein Publikum an deiner Diskussion teilnehmen und entwickle dadurch eine besondere Beziehung. Veranstalte interaktive Treffen in den virtuellen R\xE4umen deiner Systeme.",
      tabs: [
        {
          content: "Bisher gab es nur die M\xF6glichkeit der sehr einseitigen Funktion des Screen-Sharings in deinen\nOnline-Sessions - das wird sich mit dieser Ausbaustufe \xE4ndern.\n\nWir entwickeln die Basis, damit konsumi deine Lieblings-App in die Session l\xE4d.\nStell dir vor, wie produktiv es w\xE4re, wenn du mit deinen Kollegen gemeinsam das\nWireframe bearbeiten k\xF6nnt, w\xE4hrend ihr W\xFCnsche und \xC4nderungen besprecht?\n\nNiemand muss mehr lang umschreiben wie er es meint, sondern kann direkt Hand anlegen\nund es zeigen!\n\nZun\xE4chst bereiten wir die Grundlage vor, dass jeder Entwickler eigene Apps anbinden kann und\nschicken selbst St\xFCck f\xFCr St\xFCck Apps ins Rennen!",
          icon: "",
          id: "epic3",
          media: {
            src: "assets/images/mountains-sunset.png",
            type: ""
          },
          priority: "30",
          "tab-label": "\xDCberzug",
          title: "Lade deine Lieblingsanwendung in eine interaktive Sitzung ein"
        },
        {
          content: 'Tauche ab in eine virtuelle Welt, in der es m\xF6glich ist mit deinen Freunden abzuh\xE4ngen, Serien zu sehen oder Multiplayer Spiele zu spielen.\nLasse dich als "Spielfigur" durch einen pers\xF6nlich gestalteten Avatar vertreten oder\nzeige dich durch ein Live-Video im Stream an.\n\nDeine Spielfigur interagiert direkt mit der Bedienoberfl\xE4che und ist Teil der\nInteraktion mit deiner Gemeinschaft und dem System selbst.\n\nBestimme das Spiel! Du gibst vor, was in deiner Session gespielt wird. Dabei m\xFCssen es\nnicht wortw\xF6rtlich Spiele sein - lade dir z.B. den Sketch-Table und setze dich im n\xE4chsten\nBusiness-Meeting mit deinen Kollegen virtuell ran, um das neue Projekt auszuarbeiten.',
          icon: "",
          id: "epic1",
          media: {
            src: "assets/images/rocket.jpg",
            type: ""
          },
          priority: "10",
          "tab-label": "Interaktive R\xE4ume",
          title: "Geschmeidige Video-Kollaboration in gamifizierten R\xE4umen"
        },
        {
          content: 'Diese Ausbaustufe wird es erm\xF6glichen noch mehr Kommunikationsebenen in konsumi zu nutzen.\nVideo-Streaming war nur der Anfang!\n\nNutze nahtlos Text-Chats, reines Audio oder Video-Streaming in Verbindung mit deinem Avatar.\nWir bauen die Basis aus, damit die integration innerhalb von konsumi nahtlos zwischen den Medien\nwie Text, Audio oder Video funktioniert.\n\nDas bietet auch die Grundlage f\xFCr die "Tandem-Br\xFCcke". Die Nutzung von bekannten und oft genutzten Protokollen erm\xF6glicht \nn\xE4mlich auch die Anbindung von externen Plattformen und erm\xF6glichen so z.B. eine pr\xE4senz Ausserhalb von\nkonsumi mit deiner Lieblings-Messenger App.',
          icon: "",
          id: "epic2",
          media: {
            src: "assets/images/portal.jpg",
            type: ""
          },
          priority: "20",
          "tab-label": "Br\xFCcken bauen",
          title: "Tandem-Br\xFCcke von Medien und Kan\xE4len und externen Services"
        }
      ],
      title: "Kirschen auf der Torte"
    },
    roadmap: {
      actions: [
        {
          action: "",
          classes: "ext-white bg-purple-600 hover:bg-gray-300 sm:w-auto mx-2",
          href: "https://docs.konsumi.rocks/roadmap/",
          id: "docs",
          priority: "10",
          title: "Roadmap ansehen"
        }
      ],
      subtitle: "Unsere Vision enth\xE4lt viele wichtige Aspekte, die wir in einer Gesamt-Roadmap zusammenfassen und in kleine Epics aufteilen. Jeder dieser Abschnitte f\xFCgt ein vollst\xE4ndiges Features-Set zu konsumi hinzu.",
      title: "Roadmap"
    },
    title: "konsumi",
    "what-is-konsumi": {
      description: "",
      items: [
        {
          icon: "todo",
          priority: "30",
          subtitle: "Wo Systeme sich selbst warten!",
          title: "Einfach"
        },
        {
          icon: "todo",
          priority: "10",
          subtitle: "Wo Klartext gesprochen wird: Standardisierte Protokolle f\xFCr ein nahtloses Zusammenspiel!",
          title: "Offen"
        },
        {
          icon: "todo",
          priority: "20",
          subtitle: "Wo Selbstbestimmung und Datenhoheit angesagt sind!",
          title: "Selbstbestimmt"
        }
      ],
      subtitle: "Ihr verdient eine offene, kollaborative & interaktive Kommunikationsplattform!",
      title: "Hand aufs Herz"
    }
  };

  // src/models/StaticHome.js
  var Home = {
    data: null,
    getElements: function() {
      return home_default;
    },
    load: function() {
      return __async(this, null, function* () {
        yield fetch(home_default).then((response) => __async(this, null, function* () {
          this.data = yield response.json();
        })).catch(function(err) {
        });
      });
    }
  };

  // src/components/Process/Process.js
  var import_mithril7 = __toESM(require_mithril());

  // src/helper/sort.js
  var sort = {
    sortBy: function(obj, property) {
      obj.sort((a3, b2) => a3[property] > b2[property] ? 1 : -1);
      return obj;
    }
  };

  // src/components/Section/Header.js
  var import_mithril2 = __toESM(require_mithril());
  var Header = {
    view: function(vnode) {
      return (0, import_mithril2.default)(".max-w-3xl mx-auto text-center pb-12 md:pb-20", { class: vnode.attrs.class }, [
        (0, import_mithril2.default)("h2.h2 mb-4", { class: vnode.attrs.headlineClass }, vnode.attrs.title),
        (0, import_mithril2.default)(
          "p.text-xl text-gray-400",
          vnode.attrs.subtitle
        ),
        vnode.children
      ]);
    }
  };

  // src/components/Process/Item.js
  var import_mithril4 = __toESM(require_mithril());

  // node_modules/polythene-core/dist/polythene-core.mjs
  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function ownKeys(object2, enumerableOnly) {
    var keys = Object.keys(object2);
    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object2);
      if (enumerableOnly)
        symbols = symbols.filter(function(sym) {
          return Object.getOwnPropertyDescriptor(object2, sym).enumerable;
        });
      keys.push.apply(keys, symbols);
    }
    return keys;
  }
  function _objectSpread2(target) {
    for (var i2 = 1; i2 < arguments.length; i2++) {
      var source = arguments[i2] != null ? arguments[i2] : {};
      if (i2 % 2) {
        ownKeys(source, true).forEach(function(key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(source).forEach(function(key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }
    return target;
  }
  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null)
      return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i2;
    for (i2 = 0; i2 < sourceKeys.length; i2++) {
      key = sourceKeys[i2];
      if (excluded.indexOf(key) >= 0)
        continue;
      target[key] = source[key];
    }
    return target;
  }
  function _objectWithoutProperties(source, excluded) {
    if (source == null)
      return {};
    var target = _objectWithoutPropertiesLoose(source, excluded);
    var key, i2;
    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);
      for (i2 = 0; i2 < sourceSymbolKeys.length; i2++) {
        key = sourceSymbolKeys[i2];
        if (excluded.indexOf(key) >= 0)
          continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key))
          continue;
        target[key] = source[key];
      }
    }
    return target;
  }
  function _slicedToArray(arr, i2) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i2) || _nonIterableRest();
  }
  function _arrayWithHoles(arr) {
    if (Array.isArray(arr))
      return arr;
  }
  function _iterableToArrayLimit(arr, i2) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = void 0;
    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);
        if (i2 && _arr.length === i2)
          break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null)
          _i["return"]();
      } finally {
        if (_d)
          throw _e;
      }
    }
    return _arr;
  }
  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }
  var r = function r2(acc, p2) {
    return acc[p2] = 1, acc;
  };
  var defaultAttrs = [
    "key",
    "style",
    "href",
    "id",
    "data-index",
    "tabIndex",
    "tabindex",
    "oninit",
    "oncreate",
    "onupdate",
    "onbeforeremove",
    "onremove",
    "onbeforeupdate"
  ];
  var filterSupportedAttributes = function filterSupportedAttributes2(attrs) {
    var _ref = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : {}, add = _ref.add, remove = _ref.remove;
    var removeLookup = remove ? remove.reduce(r, {}) : {};
    var attrsList = add ? defaultAttrs.concat(add) : defaultAttrs;
    var supported = attrsList.filter(function(item) {
      return !removeLookup[item];
    }).reduce(r, {});
    return Object.keys(attrs).reduce(
      function(acc, key) {
        return supported[key] ? acc[key] = attrs[key] : null, acc;
      },
      {}
    );
  };
  var unpackAttrs = function unpackAttrs2(attrs) {
    return typeof attrs === "function" ? attrs() : attrs;
  };
  var isClient = typeof document !== "undefined";
  var isServer = !isClient;
  var isTouch = isServer ? false : "ontouchstart" in document.documentElement;
  var pointerEndEvent = isTouch ? ["click", "mouseup"] : ["mouseup"];
  if (isClient) {
    htmlElement = document.querySelector("html");
    if (htmlElement) {
      htmlElement.classList.add(isTouch ? "pe-touch" : "pe-no-touch");
    }
  }
  var htmlElement;
  var listeners = {};
  var throttle = function throttle2(func) {
    var s2 = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : 0.05;
    var context = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : isClient ? window : {};
    var wait = false;
    return function() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      var later = function later2() {
        return func.apply(context, args);
      };
      if (!wait) {
        later();
        wait = true;
        setTimeout(function() {
          return wait = false;
        }, s2);
      }
    };
  };
  var subscribe = function subscribe2(eventName, listener, delay) {
    listeners[eventName] = listeners[eventName] || [];
    listeners[eventName].push(delay ? throttle(listener, delay) : listener);
  };
  var unsubscribe = function unsubscribe2(eventName, listener) {
    if (!listeners[eventName]) {
      return;
    }
    var index = listeners[eventName].indexOf(listener);
    if (index > -1) {
      listeners[eventName].splice(index, 1);
    }
  };
  var emit = function emit2(eventName, event) {
    if (!listeners[eventName]) {
      return;
    }
    listeners[eventName].forEach(function(listener) {
      return listener(event);
    });
  };
  if (isClient) {
    window.addEventListener("resize", function(e2) {
      return emit("resize", e2);
    });
    window.addEventListener("scroll", function(e2) {
      return emit("scroll", e2);
    });
    window.addEventListener("keydown", function(e2) {
      return emit("keydown", e2);
    });
    pointerEndEvent.forEach(function(eventName) {
      return window.addEventListener(eventName, function(e2) {
        return emit(eventName, e2);
      });
    });
  }
  var Multi = function Multi2(_ref) {
    var mOptions = _ref.options;
    var items = [];
    var onChange = function onChange2(e2) {
      emit(mOptions.name, e2);
    };
    var itemIndex = function itemIndex2(id) {
      var item = findItem(id);
      return items.indexOf(item);
    };
    var removeItem = function removeItem2(id) {
      var index = itemIndex(id);
      if (index !== -1) {
        items.splice(index, 1);
        onChange({
          id,
          name: "removeItem"
        });
      }
    };
    var replaceItem = function replaceItem2(id, newItem) {
      var index = itemIndex(id);
      if (index !== -1) {
        items[index] = newItem;
      }
    };
    var findItem = function findItem2(id) {
      for (var i2 = 0; i2 < items.length; i2++) {
        if (items[i2].instanceId === id) {
          return items[i2];
        }
      }
    };
    var next = function next2() {
      if (items.length) {
        items[0].show = true;
      }
      onChange({
        id: items.length ? items[0].instanceId : null,
        name: "next"
      });
    };
    var remove = function remove2() {
      var instanceId = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : mOptions.defaultId;
      if (mOptions.queue) {
        items.shift();
        next();
      } else {
        removeItem(instanceId);
      }
    };
    var removeAll = function removeAll2() {
      items.length = 0;
      onChange({
        id: null,
        name: "removeAll"
      });
    };
    var setPauseState = function setPauseState2(pause2, instanceId) {
      var item = findItem(instanceId);
      if (item) {
        item.pause = pause2;
        item.unpause = !pause2;
        onChange({
          id: instanceId,
          name: pause2 ? "pause" : "unpause"
        });
      }
    };
    var createItem = function createItem2(itemAttrs, instanceId, spawn) {
      var resolveShow;
      var resolveHide;
      var props = unpackAttrs(itemAttrs);
      var didShow = function didShow2() {
        if (props.didShow) {
          props.didShow(instanceId);
        }
        onChange({
          id: instanceId,
          name: "didShow"
        });
        return resolveShow(instanceId);
      };
      var showPromise = new Promise(function(resolve2) {
        return resolveShow = resolve2;
      });
      var hidePromise = new Promise(function(resolve2) {
        return resolveHide = resolve2;
      });
      var didHide = function didHide2() {
        if (props.didHide) {
          props.didHide(instanceId);
        }
        onChange({
          id: instanceId,
          name: "didHide"
        });
        remove(instanceId);
        return resolveHide(instanceId);
      };
      return _objectSpread2({}, mOptions, {
        instanceId,
        spawn,
        props: itemAttrs,
        show: mOptions.queue ? false : true,
        showPromise,
        hidePromise,
        didShow,
        didHide
      });
    };
    var count = function count2() {
      return items.length;
    };
    var pause = function pause2() {
      var instanceId = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : mOptions.defaultId;
      return setPauseState(true, instanceId);
    };
    var unpause = function unpause2() {
      var instanceId = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : mOptions.defaultId;
      return setPauseState(false, instanceId);
    };
    var show = function show2() {
      var props = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : {};
      var spawnOpts = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : {};
      var instanceId = spawnOpts.id || mOptions.defaultId;
      var spawn = spawnOpts.spawn || mOptions.defaultId;
      var item = createItem(props, instanceId, spawn);
      onChange({
        id: instanceId,
        name: "show"
      });
      if (mOptions.queue) {
        items.push(item);
        if (items.length === 1) {
          next();
        }
      } else {
        var storedItem = findItem(instanceId);
        if (!storedItem) {
          items.push(item);
        } else {
          replaceItem(instanceId, item);
        }
      }
      return item.showPromise;
    };
    var hide = function hide2() {
      var spawnOpts = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : {};
      var instanceId = spawnOpts.id || mOptions.defaultId;
      var item = mOptions.queue && items.length ? items[0] : findItem(instanceId);
      if (item) {
        item.hide = true;
      }
      onChange({
        id: instanceId,
        name: "hide"
      });
      return item ? item.hidePromise : Promise.resolve(instanceId);
    };
    var clear = removeAll;
    var render = function render2(_ref2) {
      var h3 = _ref2.h, useState3 = _ref2.useState, useEffect2 = _ref2.useEffect, props = _objectWithoutProperties(_ref2, ["h", "useState", "useEffect"]);
      var _useState = useState3(), _useState2 = _slicedToArray(_useState, 2), setCurrent = _useState2[1];
      useEffect2(function() {
        subscribe(mOptions.name, setCurrent);
        return function() {
          unsubscribe(mOptions.name, setCurrent);
        };
      }, []);
      var spawn = props.spawn || mOptions.defaultId;
      var candidates = items.filter(function(item) {
        return item.show && item.spawn === spawn;
      });
      if (mOptions.htmlShowClass && isClient && document.documentElement) {
        document.documentElement.classList[candidates.length ? "add" : "remove"](mOptions.htmlShowClass);
      }
      return !candidates.length ? h3(mOptions.placeholder) : h3(mOptions.holderSelector, {
        className: props.position === "container" ? "pe-multiple--container" : "pe-multiple--screen"
      }, candidates.map(function(itemData) {
        return h3(mOptions.instance, _objectSpread2({}, unpackAttrs(props), {
          fromMultipleClear: clear,
          spawnId: spawn,
          fromMultipleClassName: mOptions.className,
          holderSelector: mOptions.holderSelector,
          transitions: mOptions.transitions,
          fromMultipleDidHide: itemData.didHide,
          fromMultipleDidShow: itemData.didShow,
          hide: itemData.hide,
          instanceId: itemData.instanceId,
          key: itemData.key !== void 0 ? itemData.key : itemData.keyId,
          pause: itemData.pause,
          show: itemData.show,
          unpause: itemData.unpause
        }, unpackAttrs(itemData.props)));
      }));
    };
    return {
      clear,
      count,
      hide,
      pause,
      remove,
      show,
      unpause,
      render
    };
  };
  Multi["displayName"] = "Multi";

  // node_modules/cyano-mithril/dist/cyano-mithril.module.js
  var import_mithril3 = __toESM(require_mithril());
  function ownKeys2(object2, enumerableOnly) {
    var keys = Object.keys(object2);
    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object2);
      if (enumerableOnly) {
        symbols = symbols.filter(function(sym) {
          return Object.getOwnPropertyDescriptor(object2, sym).enumerable;
        });
      }
      keys.push.apply(keys, symbols);
    }
    return keys;
  }
  function _objectSpread22(target) {
    for (var i2 = 1; i2 < arguments.length; i2++) {
      var source = arguments[i2] != null ? arguments[i2] : {};
      if (i2 % 2) {
        ownKeys2(Object(source), true).forEach(function(key) {
          _defineProperty2(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys2(Object(source)).forEach(function(key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }
    return target;
  }
  function _typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function(obj2) {
        return typeof obj2;
      };
    } else {
      _typeof = function(obj2) {
        return obj2 && typeof Symbol === "function" && obj2.constructor === Symbol && obj2 !== Symbol.prototype ? "symbol" : typeof obj2;
      };
    }
    return _typeof(obj);
  }
  function _defineProperty2(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function _extends() {
    _extends = Object.assign || function(target) {
      for (var i2 = 1; i2 < arguments.length; i2++) {
        var source = arguments[i2];
        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }
      return target;
    };
    return _extends.apply(this, arguments);
  }
  var $stringify = JSON.stringify;
  var Primitive = String;
  var primitive = "string";
  var object = "object";
  var noop = function noop2(_2, value) {
    return value;
  };
  var set = function set2(known, input, value) {
    var index = Primitive(input.push(value) - 1);
    known.set(value, index);
    return index;
  };
  var stringify = function stringify2(value, replacer, space) {
    var $2 = replacer && _typeof(replacer) === object ? function(k2, v2) {
      return k2 === "" || -1 < replacer.indexOf(k2) ? v2 : void 0;
    } : replacer || noop;
    var known = /* @__PURE__ */ new Map();
    var input = [];
    var output = [];
    var i2 = +set(known, input, $2.call({
      "": value
    }, "", value));
    var firstRun = !i2;
    while (i2 < input.length) {
      firstRun = true;
      output[i2] = $stringify(input[i2++], replace, space);
    }
    return "[" + output.join(",") + "]";
    function replace(key, value2) {
      if (firstRun) {
        firstRun = !firstRun;
        return value2;
      }
      var after = $2.call(this, key, value2);
      switch (_typeof(after)) {
        case object:
          if (after === null)
            return after;
        case primitive:
          return known.get(after) || set(known, input, after);
      }
      return after;
    }
  };
  var currentState;
  var call = Function.prototype.call.bind(Function.prototype.call);
  var scheduleRender = function scheduleRender2() {
    return import_mithril3.default.redraw();
  };
  var updateDeps = function updateDeps2(deps) {
    var state = currentState;
    var depsIndex = state.depsIndex;
    state.depsIndex += 1;
    var prevDeps = state.depsStates[depsIndex] || [];
    var shouldRecompute = deps === void 0 ? true : Array.isArray(deps) ? deps.length > 0 ? !deps.every(function(x2, i2) {
      return x2 === prevDeps[i2];
    }) : !state.setup : false;
    if (deps !== void 0) {
      state.depsStates[depsIndex] = deps;
    }
    return shouldRecompute;
  };
  var effect = function effect2() {
    var isAsync = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : false;
    return function(fn, deps) {
      var state = currentState;
      var shouldRecompute = updateDeps(deps);
      if (shouldRecompute) {
        var depsIndex = state.depsIndex;
        var runCallbackFn = function runCallbackFn2() {
          var teardown2 = fn();
          if (typeof teardown2 === "function") {
            state.teardowns.set(depsIndex, teardown2);
            state.teardowns.set("_", scheduleRender);
          }
        };
        var teardown = state.teardowns.get(depsIndex);
        try {
          if (typeof teardown === "function") {
            teardown();
          }
        } finally {
          state.teardowns["delete"](depsIndex);
        }
        state.updates.push(isAsync ? function() {
          return new Promise(function(resolve2) {
            return requestAnimationFrame(resolve2);
          }).then(runCallbackFn);
        } : runCallbackFn);
      }
    };
  };
  var updateState = function updateState2(initialState, newValueFn) {
    var state = currentState;
    var index = state.statesIndex;
    state.statesIndex += 1;
    if (!state.setup) {
      state.states[index] = initialState;
    }
    return [state.states[index], function(value) {
      var previousValue = state.states[index];
      var newValue = newValueFn ? newValueFn(value, index) : value;
      state.states[index] = newValue;
      if (stringify(newValue) !== stringify(previousValue)) {
        scheduleRender();
      }
    }, index];
  };
  var useState = function useState2(initialState) {
    var state = currentState;
    var newValueFn = function newValueFn2(value, index) {
      return typeof value === "function" ? value(state.states[index], index) : value;
    };
    return updateState(initialState, newValueFn);
  };
  var useEffect = effect(true);
  var useLayoutEffect = effect();
  var withHooks = function withHooks2(renderFunction, initialAttrs) {
    var init2 = function init3(vnode) {
      _extends(vnode.state, {
        setup: false,
        states: [],
        statesIndex: 0,
        depsStates: [],
        depsIndex: 0,
        updates: [],
        cleanups: /* @__PURE__ */ new Map(),
        teardowns: /* @__PURE__ */ new Map()
      });
    };
    var update = function update2(vnode) {
      var prevState = currentState;
      currentState = vnode.state;
      try {
        vnode.state.updates.forEach(call);
      } finally {
        _extends(vnode.state, {
          setup: true,
          updates: [],
          depsIndex: 0,
          statesIndex: 0
        });
        currentState = prevState;
      }
    };
    var render = function render2(vnode) {
      var prevState = currentState;
      currentState = vnode.state;
      try {
        return renderFunction(_objectSpread22(_objectSpread22(_objectSpread22({}, initialAttrs), vnode.attrs), {}, {
          vnode,
          children: vnode.children
        }));
      } catch (e2) {
        console.error(e2);
      } finally {
        currentState = prevState;
      }
    };
    var teardown = function teardown2(vnode) {
      var prevState = currentState;
      currentState = vnode.state;
      try {
        vnode.state.teardowns.forEach(call);
      } finally {
        currentState = prevState;
      }
    };
    return {
      oninit: init2,
      oncreate: update,
      onupdate: update,
      view: render,
      onremove: teardown
    };
  };
  var htmlAttributes = {
    accept: "accept",
    acceptcharset: "acceptcharset",
    accesskey: "accesskey",
    action: "action",
    allowfullscreen: "allowfullscreen",
    allowtransparency: "allowtransparency",
    alt: "alt",
    async: "async",
    autocomplete: "autocomplete",
    autofocus: "autofocus",
    autoplay: "autoplay",
    capture: "capture",
    cellpadding: "cellpadding",
    cellspacing: "cellspacing",
    challenge: "challenge",
    charset: "charset",
    checked: "checked",
    "class": "className",
    classid: "classid",
    classname: "className",
    className: "className",
    colspan: "colspan",
    cols: "cols",
    content: "content",
    contenteditable: "contenteditable",
    contextmenu: "contextmenu",
    controls: "controls",
    coords: "coords",
    crossorigin: "crossorigin",
    data: "data",
    datetime: "datetime",
    "default": "default",
    defer: "defer",
    dir: "dir",
    disabled: "disabled",
    download: "download",
    draggable: "draggable",
    enctype: "enctype",
    "for": "for",
    form: "form",
    formaction: "formaction",
    formenctype: "formenctype",
    formmethod: "formmethod",
    formnovalidate: "formnovalidate",
    formtarget: "formtarget",
    frameborder: "frameborder",
    headers: "headers",
    height: "height",
    hidden: "hidden",
    high: "high",
    href: "href",
    hreflang: "hreflang",
    htmlfor: "htmlfor",
    httpequiv: "httpequiv",
    icon: "icon",
    id: "id",
    inputmode: "inputmode",
    integrity: "integrity",
    is: "is",
    keyparams: "keyparams",
    keytype: "keytype",
    kind: "kind",
    label: "label",
    lang: "lang",
    list: "list",
    loop: "loop",
    low: "low",
    manifest: "manifest",
    marginheight: "marginheight",
    marginwidth: "marginwidth",
    max: "max",
    maxlength: "maxlength",
    media: "media",
    mediagroup: "mediagroup",
    method: "method",
    min: "min",
    minlength: "minlength",
    multiple: "multiple",
    muted: "muted",
    name: "name",
    novalidate: "novalidate",
    nonce: "nonce",
    onblur: "onblur",
    onchange: "onchange",
    onclick: "onclick",
    onfocus: "onfocus",
    oninput: "oninput",
    onkeydown: "onkeydown",
    onkeyup: "onkeyup",
    onmousedown: "onmousedown",
    onmouseout: "onmouseout",
    onmouseover: "onmouseover",
    onmouseup: "onmouseup",
    onscroll: "onscroll",
    onsubmit: "onsubmit",
    ontouchend: "ontouchend",
    ontouchmove: "ontouchmove",
    ontouchstart: "ontouchstart",
    open: "open",
    optimum: "optimum",
    pattern: "pattern",
    placeholder: "placeholder",
    poster: "poster",
    preload: "preload",
    radiogroup: "radiogroup",
    readonly: "readonly",
    rel: "rel",
    required: "required",
    reversed: "reversed",
    role: "role",
    rowspan: "rowspan",
    rows: "rows",
    sandbox: "sandbox",
    scope: "scope",
    scoped: "scoped",
    scrolling: "scrolling",
    seamless: "seamless",
    selected: "selected",
    shape: "shape",
    size: "size",
    sizes: "sizes",
    span: "span",
    spellcheck: "spellcheck",
    src: "src",
    srcdoc: "srcdoc",
    srclang: "srclang",
    srcset: "srcset",
    start: "start",
    step: "step",
    style: "style",
    summary: "summary",
    tabindex: "tabindex",
    target: "target",
    title: "title",
    type: "type",
    usemap: "usemap",
    value: "value",
    width: "width",
    wmode: "wmode",
    wrap: "wrap"
  };
  var a = htmlAttributes;
  var h = import_mithril3.default || {};
  var trust = h.trust;
  h.trust = function(html3, wrapper) {
    return wrapper ? (0, import_mithril3.default)(wrapper, trust(html3)) : trust(html3);
  };
  h.displayName = "mithril";
  h.fragment = import_mithril3.default.fragment;
  var getRef = function getRef2(fn) {
    return {
      oncreate: function oncreate(vnode) {
        return fn(vnode.dom);
      }
    };
  };
  var cast = withHooks;

  // node_modules/polythene-core-svg/dist/polythene-core-svg.mjs
  function _extends2() {
    _extends2 = Object.assign || function(target) {
      for (var i2 = 1; i2 < arguments.length; i2++) {
        var source = arguments[i2];
        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }
      return target;
    };
    return _extends2.apply(this, arguments);
  }
  function _objectWithoutPropertiesLoose2(source, excluded) {
    if (source == null)
      return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i2;
    for (i2 = 0; i2 < sourceKeys.length; i2++) {
      key = sourceKeys[i2];
      if (excluded.indexOf(key) >= 0)
        continue;
      target[key] = source[key];
    }
    return target;
  }
  function _objectWithoutProperties2(source, excluded) {
    if (source == null)
      return {};
    var target = _objectWithoutPropertiesLoose2(source, excluded);
    var key, i2;
    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);
      for (i2 = 0; i2 < sourceSymbolKeys.length; i2++) {
        key = sourceSymbolKeys[i2];
        if (excluded.indexOf(key) >= 0)
          continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key))
          continue;
        target[key] = source[key];
      }
    }
    return target;
  }
  function _slicedToArray2(arr, i2) {
    return _arrayWithHoles2(arr) || _iterableToArrayLimit2(arr, i2) || _nonIterableRest2();
  }
  function _arrayWithHoles2(arr) {
    if (Array.isArray(arr))
      return arr;
  }
  function _iterableToArrayLimit2(arr, i2) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = void 0;
    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);
        if (i2 && _arr.length === i2)
          break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null)
          _i["return"]();
      } finally {
        if (_d)
          throw _e;
      }
    }
    return _arr;
  }
  function _nonIterableRest2() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }
  var classes = {
    component: "pe-svg"
  };
  var _SVG = function _SVG2(_ref) {
    var h3 = _ref.h, a3 = _ref.a, useEffect2 = _ref.useEffect, useState3 = _ref.useState, getRef3 = _ref.getRef, props = _objectWithoutProperties2(_ref, ["h", "a", "useEffect", "useState", "getRef"]);
    var _useState = useState3(), _useState2 = _slicedToArray2(_useState, 2), domElement = _useState2[0], setDomElement = _useState2[1];
    useEffect2(function() {
      if (!domElement) {
        return;
      }
      var svgElement = domElement.querySelector("svg");
      if (svgElement) {
        svgElement.setAttribute("focusable", "false");
      }
    }, [domElement]);
    var componentProps = _extends2({}, filterSupportedAttributes(props), getRef3(function(dom) {
      return dom && !domElement && (setDomElement(dom), props.getRef && props.getRef(dom));
    }), props.testId && {
      "data-test-id": props.testId
    }, {
      className: [classes.component, props.tone === "dark" ? "pe-dark-tone" : null, props.tone === "light" ? "pe-light-tone" : null, props.className || props[a3["class"]]].join(" ")
    });
    var content = [props.before, props.content ? props.content : props.children, props.after];
    return h3(props.element || "div", componentProps, content);
  };

  // node_modules/polythene-mithril-svg/dist/polythene-mithril-svg.mjs
  var SVG = cast(_SVG, {
    h,
    a,
    useEffect,
    useState,
    getRef
  });

  // src/components/Process/Item.js
  var Item = {
    view: function(vnode) {
      return (0, import_mithril4.default)(".relative flex flex-col items-center", [
        (0, import_mithril4.default)(
          ".absolute h-1 border-t border-dashed border-gray-700 hidden md:block",
          ""
        ),
        (0, import_mithril4.default)(SVG, vnode.attrs.icon),
        (0, import_mithril4.default)("h4.h4 mb-2", [(0, import_mithril4.default)("span.text-gray-400", vnode.attrs.title)]),
        (0, import_mithril4.default)("p.text-lg text-gray-400 text-center", vnode.attrs.description)
      ]);
    }
  };

  // src/components/Section/Section.js
  var import_mithril5 = __toESM(require_mithril());
  var Section = {
    view: function(vnode) {
      return (0, import_mithril5.default)("section", [
        (0, import_mithril5.default)("div", { class: vnode.attrs.class }, [
          (0, import_mithril5.default)("div", { class: vnode.attrs.childrenClass }, vnode.children)
        ])
      ]);
    }
  };

  // src/components/Icons/Content.js
  var import_mithril6 = __toESM(require_mithril());
  var Content = {
    todo: import_mithril6.default.trust(`<svg class="w-16 h-16 mb-4" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
<rect class="fill-current text-purple-600" width="64" height="64" rx="32" />
<g fill="none" fill-rule="evenodd">
    <path class="stroke-current text-purple-300" d="M40 22a2 2 0 012 2v16a2 2 0 01-2 2H24a2 2 0 01-2-2V24a2 2 0 012-2" stroke-width="2" stroke-linecap="square" />
    <path class="stroke-current text-purple-100" stroke-width="2" stroke-linecap="square" d="M36 32l-4-3-4 3V22h8z" />
</g>
</svg>`)
  };

  // src/components/Process/Process.js
  var Process = {
    view: function(vnode) {
      return (0, import_mithril7.default)(
        Section,
        {
          class: "max-w-6xl mx-auto px-4 sm:px-6",
          childrenClass: "pt-10 pb-12 md:pt-16 md:pb-20"
        },
        [
          (0, import_mithril7.default)(Header, {
            title: vnode.attrs.title,
            subtitle: vnode.attrs.subtitle
          }),
          (0, import_mithril7.default)("div.max-w-3xl mx-auto text-center pb-12", [
            (0, import_mithril7.default)("p.text-xl text-gray-400", vnode.attrs.description)
          ]),
          (0, import_mithril7.default)(
            "div.max-w-sm mx-auto grid gap-8 md:grid-cols-3 lg:gap-16 items-start md:max-w-none",
            sort.sortBy(vnode.attrs.items, "priority").map(function(item) {
              return (0, import_mithril7.default)("div", [
                (0, import_mithril7.default)(Item, { title: item.title, description: item.subtitle, icon: Content[item.icon] })
              ]);
            })
          )
        ]
      );
    }
  };

  // src/components/Hero/Hero.js
  var import_mithril12 = __toESM(require_mithril());

  // src/components/Chat/Widget.js
  var import_mithril9 = __toESM(require_mithril());

  // src/components/Chat/Popup.js
  var import_mithril8 = __toESM(require_mithril());

  // gamja/node_modules/preact/dist/preact.module.js
  var n;
  var l;
  var u;
  var i;
  var t;
  var o;
  var r3;
  var f;
  var e = {};
  var c = [];
  var s = /acit|ex(?:s|g|n|p|$)|rph|grid|ows|mnc|ntw|ine[ch]|zoo|^ord|itera/i;
  function a2(n3, l2) {
    for (var u2 in l2)
      n3[u2] = l2[u2];
    return n3;
  }
  function h2(n3) {
    var l2 = n3.parentNode;
    l2 && l2.removeChild(n3);
  }
  function v(l2, u2, i2) {
    var t3, o2, r4, f2 = {};
    for (r4 in u2)
      "key" == r4 ? t3 = u2[r4] : "ref" == r4 ? o2 = u2[r4] : f2[r4] = u2[r4];
    if (arguments.length > 2 && (f2.children = arguments.length > 3 ? n.call(arguments, 2) : i2), "function" == typeof l2 && null != l2.defaultProps)
      for (r4 in l2.defaultProps)
        void 0 === f2[r4] && (f2[r4] = l2.defaultProps[r4]);
    return y(l2, f2, t3, o2, null);
  }
  function y(n3, i2, t3, o2, r4) {
    var f2 = { type: n3, props: i2, key: t3, ref: o2, __k: null, __: null, __b: 0, __e: null, __d: void 0, __c: null, __h: null, constructor: void 0, __v: null == r4 ? ++u : r4 };
    return null == r4 && null != l.vnode && l.vnode(f2), f2;
  }
  function p() {
    return { current: null };
  }
  function d(n3) {
    return n3.children;
  }
  function _(n3, l2) {
    this.props = n3, this.context = l2;
  }
  function k(n3, l2) {
    if (null == l2)
      return n3.__ ? k(n3.__, n3.__.__k.indexOf(n3) + 1) : null;
    for (var u2; l2 < n3.__k.length; l2++)
      if (null != (u2 = n3.__k[l2]) && null != u2.__e)
        return u2.__e;
    return "function" == typeof n3.type ? k(n3) : null;
  }
  function b(n3) {
    var l2, u2;
    if (null != (n3 = n3.__) && null != n3.__c) {
      for (n3.__e = n3.__c.base = null, l2 = 0; l2 < n3.__k.length; l2++)
        if (null != (u2 = n3.__k[l2]) && null != u2.__e) {
          n3.__e = n3.__c.base = u2.__e;
          break;
        }
      return b(n3);
    }
  }
  function m9(n3) {
    (!n3.__d && (n3.__d = true) && t.push(n3) && !g.__r++ || r3 !== l.debounceRendering) && ((r3 = l.debounceRendering) || o)(g);
  }
  function g() {
    for (var n3; g.__r = t.length; )
      n3 = t.sort(function(n4, l2) {
        return n4.__v.__b - l2.__v.__b;
      }), t = [], n3.some(function(n4) {
        var l2, u2, i2, t3, o2, r4;
        n4.__d && (o2 = (t3 = (l2 = n4).__v).__e, (r4 = l2.__P) && (u2 = [], (i2 = a2({}, t3)).__v = t3.__v + 1, j(r4, t3, i2, l2.__n, void 0 !== r4.ownerSVGElement, null != t3.__h ? [o2] : null, u2, null == o2 ? k(t3) : o2, t3.__h), z(u2, t3), t3.__e != o2 && b(t3)));
      });
  }
  function w(n3, l2, u2, i2, t3, o2, r4, f2, s2, a3) {
    var h3, v2, p2, _2, b2, m39, g2, w2 = i2 && i2.__k || c, A = w2.length;
    for (u2.__k = [], h3 = 0; h3 < l2.length; h3++)
      if (null != (_2 = u2.__k[h3] = null == (_2 = l2[h3]) || "boolean" == typeof _2 ? null : "string" == typeof _2 || "number" == typeof _2 || "bigint" == typeof _2 ? y(null, _2, null, null, _2) : Array.isArray(_2) ? y(d, { children: _2 }, null, null, null) : _2.__b > 0 ? y(_2.type, _2.props, _2.key, null, _2.__v) : _2)) {
        if (_2.__ = u2, _2.__b = u2.__b + 1, null === (p2 = w2[h3]) || p2 && _2.key == p2.key && _2.type === p2.type)
          w2[h3] = void 0;
        else
          for (v2 = 0; v2 < A; v2++) {
            if ((p2 = w2[v2]) && _2.key == p2.key && _2.type === p2.type) {
              w2[v2] = void 0;
              break;
            }
            p2 = null;
          }
        j(n3, _2, p2 = p2 || e, t3, o2, r4, f2, s2, a3), b2 = _2.__e, (v2 = _2.ref) && p2.ref != v2 && (g2 || (g2 = []), p2.ref && g2.push(p2.ref, null, _2), g2.push(v2, _2.__c || b2, _2)), null != b2 ? (null == m39 && (m39 = b2), "function" == typeof _2.type && _2.__k === p2.__k ? _2.__d = s2 = x(_2, s2, n3) : s2 = P(n3, _2, p2, w2, b2, s2), "function" == typeof u2.type && (u2.__d = s2)) : s2 && p2.__e == s2 && s2.parentNode != n3 && (s2 = k(p2));
      }
    for (u2.__e = m39, h3 = A; h3--; )
      null != w2[h3] && ("function" == typeof u2.type && null != w2[h3].__e && w2[h3].__e == u2.__d && (u2.__d = k(i2, h3 + 1)), N(w2[h3], w2[h3]));
    if (g2)
      for (h3 = 0; h3 < g2.length; h3++)
        M(g2[h3], g2[++h3], g2[++h3]);
  }
  function x(n3, l2, u2) {
    for (var i2, t3 = n3.__k, o2 = 0; t3 && o2 < t3.length; o2++)
      (i2 = t3[o2]) && (i2.__ = n3, l2 = "function" == typeof i2.type ? x(i2, l2, u2) : P(u2, i2, i2, t3, i2.__e, l2));
    return l2;
  }
  function P(n3, l2, u2, i2, t3, o2) {
    var r4, f2, e2;
    if (void 0 !== l2.__d)
      r4 = l2.__d, l2.__d = void 0;
    else if (null == u2 || t3 != o2 || null == t3.parentNode)
      n:
        if (null == o2 || o2.parentNode !== n3)
          n3.appendChild(t3), r4 = null;
        else {
          for (f2 = o2, e2 = 0; (f2 = f2.nextSibling) && e2 < i2.length; e2 += 2)
            if (f2 == t3)
              break n;
          n3.insertBefore(t3, o2), r4 = o2;
        }
    return void 0 !== r4 ? r4 : t3.nextSibling;
  }
  function C(n3, l2, u2, i2, t3) {
    var o2;
    for (o2 in u2)
      "children" === o2 || "key" === o2 || o2 in l2 || H(n3, o2, null, u2[o2], i2);
    for (o2 in l2)
      t3 && "function" != typeof l2[o2] || "children" === o2 || "key" === o2 || "value" === o2 || "checked" === o2 || u2[o2] === l2[o2] || H(n3, o2, l2[o2], u2[o2], i2);
  }
  function $(n3, l2, u2) {
    "-" === l2[0] ? n3.setProperty(l2, u2) : n3[l2] = null == u2 ? "" : "number" != typeof u2 || s.test(l2) ? u2 : u2 + "px";
  }
  function H(n3, l2, u2, i2, t3) {
    var o2;
    n:
      if ("style" === l2)
        if ("string" == typeof u2)
          n3.style.cssText = u2;
        else {
          if ("string" == typeof i2 && (n3.style.cssText = i2 = ""), i2)
            for (l2 in i2)
              u2 && l2 in u2 || $(n3.style, l2, "");
          if (u2)
            for (l2 in u2)
              i2 && u2[l2] === i2[l2] || $(n3.style, l2, u2[l2]);
        }
      else if ("o" === l2[0] && "n" === l2[1])
        o2 = l2 !== (l2 = l2.replace(/Capture$/, "")), l2 = l2.toLowerCase() in n3 ? l2.toLowerCase().slice(2) : l2.slice(2), n3.l || (n3.l = {}), n3.l[l2 + o2] = u2, u2 ? i2 || n3.addEventListener(l2, o2 ? T : I, o2) : n3.removeEventListener(l2, o2 ? T : I, o2);
      else if ("dangerouslySetInnerHTML" !== l2) {
        if (t3)
          l2 = l2.replace(/xlink[H:h]/, "h").replace(/sName$/, "s");
        else if ("href" !== l2 && "list" !== l2 && "form" !== l2 && "tabIndex" !== l2 && "download" !== l2 && l2 in n3)
          try {
            n3[l2] = null == u2 ? "" : u2;
            break n;
          } catch (n4) {
          }
        "function" == typeof u2 || (null != u2 && (false !== u2 || "a" === l2[0] && "r" === l2[1]) ? n3.setAttribute(l2, u2) : n3.removeAttribute(l2));
      }
  }
  function I(n3) {
    this.l[n3.type + false](l.event ? l.event(n3) : n3);
  }
  function T(n3) {
    this.l[n3.type + true](l.event ? l.event(n3) : n3);
  }
  function j(n3, u2, i2, t3, o2, r4, f2, e2, c2) {
    var s2, h3, v2, y2, p2, k2, b2, m39, g2, x2, A, P2 = u2.type;
    if (void 0 !== u2.constructor)
      return null;
    null != i2.__h && (c2 = i2.__h, e2 = u2.__e = i2.__e, u2.__h = null, r4 = [e2]), (s2 = l.__b) && s2(u2);
    try {
      n:
        if ("function" == typeof P2) {
          if (m39 = u2.props, g2 = (s2 = P2.contextType) && t3[s2.__c], x2 = s2 ? g2 ? g2.props.value : s2.__ : t3, i2.__c ? b2 = (h3 = u2.__c = i2.__c).__ = h3.__E : ("prototype" in P2 && P2.prototype.render ? u2.__c = h3 = new P2(m39, x2) : (u2.__c = h3 = new _(m39, x2), h3.constructor = P2, h3.render = O), g2 && g2.sub(h3), h3.props = m39, h3.state || (h3.state = {}), h3.context = x2, h3.__n = t3, v2 = h3.__d = true, h3.__h = []), null == h3.__s && (h3.__s = h3.state), null != P2.getDerivedStateFromProps && (h3.__s == h3.state && (h3.__s = a2({}, h3.__s)), a2(h3.__s, P2.getDerivedStateFromProps(m39, h3.__s))), y2 = h3.props, p2 = h3.state, v2)
            null == P2.getDerivedStateFromProps && null != h3.componentWillMount && h3.componentWillMount(), null != h3.componentDidMount && h3.__h.push(h3.componentDidMount);
          else {
            if (null == P2.getDerivedStateFromProps && m39 !== y2 && null != h3.componentWillReceiveProps && h3.componentWillReceiveProps(m39, x2), !h3.__e && null != h3.shouldComponentUpdate && false === h3.shouldComponentUpdate(m39, h3.__s, x2) || u2.__v === i2.__v) {
              h3.props = m39, h3.state = h3.__s, u2.__v !== i2.__v && (h3.__d = false), h3.__v = u2, u2.__e = i2.__e, u2.__k = i2.__k, u2.__k.forEach(function(n4) {
                n4 && (n4.__ = u2);
              }), h3.__h.length && f2.push(h3);
              break n;
            }
            null != h3.componentWillUpdate && h3.componentWillUpdate(m39, h3.__s, x2), null != h3.componentDidUpdate && h3.__h.push(function() {
              h3.componentDidUpdate(y2, p2, k2);
            });
          }
          h3.context = x2, h3.props = m39, h3.state = h3.__s, (s2 = l.__r) && s2(u2), h3.__d = false, h3.__v = u2, h3.__P = n3, s2 = h3.render(h3.props, h3.state, h3.context), h3.state = h3.__s, null != h3.getChildContext && (t3 = a2(a2({}, t3), h3.getChildContext())), v2 || null == h3.getSnapshotBeforeUpdate || (k2 = h3.getSnapshotBeforeUpdate(y2, p2)), A = null != s2 && s2.type === d && null == s2.key ? s2.props.children : s2, w(n3, Array.isArray(A) ? A : [A], u2, i2, t3, o2, r4, f2, e2, c2), h3.base = u2.__e, u2.__h = null, h3.__h.length && f2.push(h3), b2 && (h3.__E = h3.__ = null), h3.__e = false;
        } else
          null == r4 && u2.__v === i2.__v ? (u2.__k = i2.__k, u2.__e = i2.__e) : u2.__e = L(i2.__e, u2, i2, t3, o2, r4, f2, c2);
      (s2 = l.diffed) && s2(u2);
    } catch (n4) {
      u2.__v = null, (c2 || null != r4) && (u2.__e = e2, u2.__h = !!c2, r4[r4.indexOf(e2)] = null), l.__e(n4, u2, i2);
    }
  }
  function z(n3, u2) {
    l.__c && l.__c(u2, n3), n3.some(function(u3) {
      try {
        n3 = u3.__h, u3.__h = [], n3.some(function(n4) {
          n4.call(u3);
        });
      } catch (n4) {
        l.__e(n4, u3.__v);
      }
    });
  }
  function L(l2, u2, i2, t3, o2, r4, f2, c2) {
    var s2, a3, v2, y2 = i2.props, p2 = u2.props, d2 = u2.type, _2 = 0;
    if ("svg" === d2 && (o2 = true), null != r4) {
      for (; _2 < r4.length; _2++)
        if ((s2 = r4[_2]) && "setAttribute" in s2 == !!d2 && (d2 ? s2.localName === d2 : 3 === s2.nodeType)) {
          l2 = s2, r4[_2] = null;
          break;
        }
    }
    if (null == l2) {
      if (null === d2)
        return document.createTextNode(p2);
      l2 = o2 ? document.createElementNS("http://www.w3.org/2000/svg", d2) : document.createElement(d2, p2.is && p2), r4 = null, c2 = false;
    }
    if (null === d2)
      y2 === p2 || c2 && l2.data === p2 || (l2.data = p2);
    else {
      if (r4 = r4 && n.call(l2.childNodes), a3 = (y2 = i2.props || e).dangerouslySetInnerHTML, v2 = p2.dangerouslySetInnerHTML, !c2) {
        if (null != r4)
          for (y2 = {}, _2 = 0; _2 < l2.attributes.length; _2++)
            y2[l2.attributes[_2].name] = l2.attributes[_2].value;
        (v2 || a3) && (v2 && (a3 && v2.__html == a3.__html || v2.__html === l2.innerHTML) || (l2.innerHTML = v2 && v2.__html || ""));
      }
      if (C(l2, p2, y2, o2, c2), v2)
        u2.__k = [];
      else if (_2 = u2.props.children, w(l2, Array.isArray(_2) ? _2 : [_2], u2, i2, t3, o2 && "foreignObject" !== d2, r4, f2, r4 ? r4[0] : i2.__k && k(i2, 0), c2), null != r4)
        for (_2 = r4.length; _2--; )
          null != r4[_2] && h2(r4[_2]);
      c2 || ("value" in p2 && void 0 !== (_2 = p2.value) && (_2 !== l2.value || "progress" === d2 && !_2 || "option" === d2 && _2 !== y2.value) && H(l2, "value", _2, y2.value, false), "checked" in p2 && void 0 !== (_2 = p2.checked) && _2 !== l2.checked && H(l2, "checked", _2, y2.checked, false));
    }
    return l2;
  }
  function M(n3, u2, i2) {
    try {
      "function" == typeof n3 ? n3(u2) : n3.current = u2;
    } catch (n4) {
      l.__e(n4, i2);
    }
  }
  function N(n3, u2, i2) {
    var t3, o2;
    if (l.unmount && l.unmount(n3), (t3 = n3.ref) && (t3.current && t3.current !== n3.__e || M(t3, null, u2)), null != (t3 = n3.__c)) {
      if (t3.componentWillUnmount)
        try {
          t3.componentWillUnmount();
        } catch (n4) {
          l.__e(n4, u2);
        }
      t3.base = t3.__P = null;
    }
    if (t3 = n3.__k)
      for (o2 = 0; o2 < t3.length; o2++)
        t3[o2] && N(t3[o2], u2, "function" != typeof n3.type);
    i2 || null == n3.__e || h2(n3.__e), n3.__e = n3.__d = void 0;
  }
  function O(n3, l2, u2) {
    return this.constructor(n3, u2);
  }
  function S(u2, i2, t3) {
    var o2, r4, f2;
    l.__ && l.__(u2, i2), r4 = (o2 = "function" == typeof t3) ? null : t3 && t3.__k || i2.__k, f2 = [], j(i2, u2 = (!o2 && t3 || i2).__k = v(d, null, [u2]), r4 || e, e, void 0 !== i2.ownerSVGElement, !o2 && t3 ? [t3] : r4 ? null : i2.firstChild ? n.call(i2.childNodes) : null, f2, !o2 && t3 ? t3 : r4 ? r4.__e : i2.firstChild, o2), z(f2, u2);
  }
  n = c.slice, l = { __e: function(n3, l2) {
    for (var u2, i2, t3; l2 = l2.__; )
      if ((u2 = l2.__c) && !u2.__)
        try {
          if ((i2 = u2.constructor) && null != i2.getDerivedStateFromError && (u2.setState(i2.getDerivedStateFromError(n3)), t3 = u2.__d), null != u2.componentDidCatch && (u2.componentDidCatch(n3), t3 = u2.__d), t3)
            return u2.__E = u2;
        } catch (l3) {
          n3 = l3;
        }
    throw n3;
  } }, u = 0, i = function(n3) {
    return null != n3 && void 0 === n3.constructor;
  }, _.prototype.setState = function(n3, l2) {
    var u2;
    u2 = null != this.__s && this.__s !== this.state ? this.__s : this.__s = a2({}, this.state), "function" == typeof n3 && (n3 = n3(a2({}, u2), this.props)), n3 && a2(u2, n3), null != n3 && this.__v && (l2 && this.__h.push(l2), m9(this));
  }, _.prototype.forceUpdate = function(n3) {
    this.__v && (this.__e = true, n3 && this.__h.push(n3), m9(this));
  }, _.prototype.render = d, t = [], o = "function" == typeof Promise ? Promise.prototype.then.bind(Promise.resolve()) : setTimeout, g.__r = 0, f = 0;

  // gamja/node_modules/htm/dist/htm.module.js
  var n2 = function(t3, s2, r4, e2) {
    var u2;
    s2[0] = 0;
    for (var h3 = 1; h3 < s2.length; h3++) {
      var p2 = s2[h3++], a3 = s2[h3] ? (s2[0] |= p2 ? 1 : 2, r4[s2[h3++]]) : s2[++h3];
      3 === p2 ? e2[0] = a3 : 4 === p2 ? e2[1] = Object.assign(e2[1] || {}, a3) : 5 === p2 ? (e2[1] = e2[1] || {})[s2[++h3]] = a3 : 6 === p2 ? e2[1][s2[++h3]] += a3 + "" : p2 ? (u2 = t3.apply(a3, n2(t3, a3, r4, ["", null])), e2.push(u2), a3[0] ? s2[0] |= 2 : (s2[h3 - 2] = 0, s2[h3] = u2)) : e2.push(a3);
    }
    return e2;
  };
  var t2 = /* @__PURE__ */ new Map();
  function htm_module_default(s2) {
    var r4 = t2.get(this);
    return r4 || (r4 = /* @__PURE__ */ new Map(), t2.set(this, r4)), (r4 = n2(this, r4.get(s2) || (r4.set(s2, r4 = function(n3) {
      for (var t3, s3, r5 = 1, e2 = "", u2 = "", h3 = [0], p2 = function(n4) {
        1 === r5 && (n4 || (e2 = e2.replace(/^\s*\n\s*|\s*\n\s*$/g, ""))) ? h3.push(0, n4, e2) : 3 === r5 && (n4 || e2) ? (h3.push(3, n4, e2), r5 = 2) : 2 === r5 && "..." === e2 && n4 ? h3.push(4, n4, 0) : 2 === r5 && e2 && !n4 ? h3.push(5, 0, true, e2) : r5 >= 5 && ((e2 || !n4 && 5 === r5) && (h3.push(r5, 0, e2, s3), r5 = 6), n4 && (h3.push(r5, n4, 0, s3), r5 = 6)), e2 = "";
      }, a3 = 0; a3 < n3.length; a3++) {
        a3 && (1 === r5 && p2(), p2(a3));
        for (var l2 = 0; l2 < n3[a3].length; l2++)
          t3 = n3[a3][l2], 1 === r5 ? "<" === t3 ? (p2(), h3 = [h3], r5 = 3) : e2 += t3 : 4 === r5 ? "--" === e2 && ">" === t3 ? (r5 = 1, e2 = "") : e2 = t3 + e2[0] : u2 ? t3 === u2 ? u2 = "" : e2 += t3 : '"' === t3 || "'" === t3 ? u2 = t3 : ">" === t3 ? (p2(), r5 = 1) : r5 && ("=" === t3 ? (r5 = 5, s3 = e2, e2 = "") : "/" === t3 && (r5 < 5 || ">" === n3[a3][l2 + 1]) ? (p2(), 3 === r5 && (h3 = h3[0]), r5 = h3, (h3 = h3[0]).push(2, 0, r5), r5 = 0) : " " === t3 || "	" === t3 || "\n" === t3 || "\r" === t3 ? (p2(), r5 = 2) : e2 += t3), 3 === r5 && "!--" === e2 && (r5 = 4, h3 = h3[0]);
      }
      return p2(), h3;
    }(s2)), r4), arguments, [])).length > 1 ? r4 : r4[0];
  }

  // gamja/lib/irc.js
  var RPL_WELCOME = "001";
  var RPL_YOURHOST = "002";
  var RPL_MYINFO = "004";
  var RPL_ISUPPORT = "005";
  var RPL_UMODEIS = "221";
  var RPL_TRYAGAIN = "263";
  var RPL_AWAY = "301";
  var RPL_WHOISUSER = "311";
  var RPL_WHOISSERVER = "312";
  var RPL_WHOISOPERATOR = "313";
  var RPL_WHOISIDLE = "317";
  var RPL_ENDOFWHOIS = "318";
  var RPL_WHOISCHANNELS = "319";
  var RPL_ENDOFWHO = "315";
  var RPL_CHANNELMODEIS = "324";
  var RPL_NOTOPIC = "331";
  var RPL_TOPIC = "332";
  var RPL_TOPICWHOTIME = "333";
  var RPL_INVITING = "341";
  var RPL_INVITELIST = "346";
  var RPL_ENDOFINVITELIST = "347";
  var RPL_EXCEPTLIST = "348";
  var RPL_ENDOFEXCEPTLIST = "349";
  var RPL_WHOREPLY = "352";
  var RPL_NAMREPLY = "353";
  var RPL_WHOSPCRPL = "354";
  var RPL_ENDOFNAMES = "366";
  var RPL_BANLIST = "367";
  var RPL_ENDOFBANLIST = "368";
  var RPL_MOTD = "372";
  var RPL_ENDOFMOTD = "376";
  var ERR_UNKNOWNERROR = "400";
  var ERR_NOSUCHNICK = "401";
  var ERR_NOSUCHCHANNEL = "403";
  var ERR_TOOMANYCHANNELS = "405";
  var ERR_UNKNOWNCOMMAND = "421";
  var ERR_NOMOTD = "422";
  var ERR_ERRONEUSNICKNAME = "432";
  var ERR_NICKNAMEINUSE = "433";
  var ERR_NICKCOLLISION = "436";
  var ERR_NEEDMOREPARAMS = "461";
  var ERR_NOPERMFORHOST = "463";
  var ERR_PASSWDMISMATCH = "464";
  var ERR_YOUREBANNEDCREEP = "465";
  var ERR_CHANNELISFULL = "471";
  var ERR_INVITEONLYCHAN = "473";
  var ERR_BANNEDFROMCHAN = "474";
  var ERR_BADCHANNELKEY = "475";
  var ERR_UNAVAILRESOURCE = "437";
  var RPL_CREATIONTIME = "329";
  var RPL_QUIETLIST = "728";
  var RPL_ENDOFQUIETLIST = "729";
  var RPL_MONONLINE = "730";
  var RPL_MONOFFLINE = "731";
  var ERR_MONLISTFULL = "734";
  var RPL_LOGGEDIN = "900";
  var RPL_LOGGEDOUT = "901";
  var ERR_NICKLOCKED = "902";
  var RPL_SASLSUCCESS = "903";
  var ERR_SASLFAIL = "904";
  var ERR_SASLTOOLONG = "905";
  var ERR_SASLABORTED = "906";
  var ERR_SASLALREADY = "907";
  var STD_MEMBERSHIPS = "~&@%+";
  var STD_CHANTYPES = "#&+!";
  var tagEscapeMap = {
    ";": "\\:",
    " ": "\\s",
    "\\": "\\\\",
    "\r": "\\r",
    "\n": "\\n"
  };
  var tagUnescapeMap = Object.fromEntries(Object.entries(tagEscapeMap).map(([from, to]) => [to, from]));
  function escapeTag(s2) {
    return String(s2).replace(/[; \\\r\n]/g, (ch) => tagEscapeMap[ch]);
  }
  function unescapeTag(s2) {
    return s2.replace(/\\[:s\\rn]/g, (seq) => tagUnescapeMap[seq]);
  }
  function parseTags(s2) {
    let tags = {};
    s2.split(";").forEach((s3) => {
      if (!s3) {
        return;
      }
      let parts = s3.split("=", 2);
      let k2 = parts[0];
      let v2 = null;
      if (parts.length == 2) {
        v2 = unescapeTag(parts[1]);
        if (v2.endsWith("\\")) {
          v2 = v2.slice(0, v2.length - 1);
        }
      }
      tags[k2] = v2;
    });
    return tags;
  }
  function formatTags(tags) {
    let l2 = [];
    for (let k2 in tags) {
      if (tags[k2] === void 0 || tags[k2] === null) {
        l2.push(k2);
        continue;
      }
      let v2 = escapeTag(tags[k2]);
      l2.push(k2 + "=" + v2);
    }
    return l2.join(";");
  }
  function parsePrefix(s2) {
    let prefix = {
      name: null,
      user: null,
      host: null
    };
    let host = null;
    let i2 = s2.indexOf("@");
    if (i2 > 0) {
      host = s2.slice(i2 + 1);
      s2 = s2.slice(0, i2);
    }
    let user = null;
    i2 = s2.indexOf("!");
    if (i2 > 0) {
      user = s2.slice(i2 + 1);
      s2 = s2.slice(0, i2);
    }
    return { name: s2, user, host };
  }
  function formatPrefix(prefix) {
    let s2 = prefix.name;
    if (prefix.user) {
      s2 += "!" + prefix.user;
    }
    if (prefix.host) {
      s2 += "@" + prefix.host;
    }
    return s2;
  }
  function parseMessage(s2) {
    if (s2.endsWith("\r\n")) {
      s2 = s2.slice(0, s2.length - 2);
    }
    let msg = {
      tags: {},
      prefix: null,
      command: null,
      params: []
    };
    if (s2.startsWith("@")) {
      let i3 = s2.indexOf(" ");
      if (i3 < 0) {
        throw new Error("expected a space after tags");
      }
      msg.tags = parseTags(s2.slice(1, i3));
      s2 = s2.slice(i3 + 1);
    }
    if (s2.startsWith(":")) {
      let i3 = s2.indexOf(" ");
      if (i3 < 0) {
        throw new Error("expected a space after prefix");
      }
      msg.prefix = parsePrefix(s2.slice(1, i3));
      s2 = s2.slice(i3 + 1);
    }
    let i2 = s2.indexOf(" ");
    if (i2 < 0) {
      msg.command = s2;
      return msg;
    }
    msg.command = s2.slice(0, i2);
    s2 = s2.slice(i2 + 1);
    while (true) {
      if (s2.startsWith(":")) {
        msg.params.push(s2.slice(1));
        break;
      }
      i2 = s2.indexOf(" ");
      if (i2 < 0) {
        msg.params.push(s2);
        break;
      }
      msg.params.push(s2.slice(0, i2));
      s2 = s2.slice(i2 + 1);
    }
    return msg;
  }
  function formatMessage(msg) {
    let s2 = "";
    if (msg.tags && Object.keys(msg.tags).length > 0) {
      s2 += "@" + formatTags(msg.tags) + " ";
    }
    if (msg.prefix) {
      s2 += ":" + formatPrefix(msg.prefix) + " ";
    }
    s2 += msg.command;
    if (msg.params && msg.params.length > 0) {
      for (let i2 = 0; i2 < msg.params.length - 1; i2++) {
        s2 += " " + msg.params[i2];
      }
      let last = String(msg.params[msg.params.length - 1]);
      if (last.length === 0 || last.startsWith(":") || last.indexOf(" ") >= 0) {
        s2 += " :" + last;
      } else {
        s2 += " " + last;
      }
    }
    return s2;
  }
  function parseTargetPrefix(s2, allowedPrefixes = STD_MEMBERSHIPS) {
    let i2;
    for (i2 = 0; i2 < s2.length; i2++) {
      if (allowedPrefixes.indexOf(s2[i2]) < 0) {
        break;
      }
    }
    return {
      prefix: s2.slice(0, i2),
      name: s2.slice(i2)
    };
  }
  var alphaNum = (() => {
    try {
      return new RegExp(/^[\p{L}0-9]$/, "u");
    } catch (e2) {
      return new RegExp(/^[a-zA-Z0-9]$/, "u");
    }
  })();
  function isWordBoundary(ch) {
    switch (ch) {
      case "-":
      case "_":
      case "|":
        return false;
      default:
        return !alphaNum.test(ch);
    }
  }
  function isHighlight(msg, nick, cm) {
    if (msg.command != "PRIVMSG" && msg.command != "NOTICE") {
      return false;
    }
    nick = cm(nick);
    if (msg.prefix && cm(msg.prefix.name) == nick) {
      return false;
    }
    let text2 = cm(msg.params[1]);
    while (true) {
      let i2 = text2.indexOf(nick);
      if (i2 < 0) {
        return false;
      }
      let left = "\0", right = "\0";
      if (i2 > 0) {
        left = text2[i2 - 1];
      }
      if (i2 + nick.length < text2.length) {
        right = text2[i2 + nick.length];
      }
      if (isWordBoundary(left) && isWordBoundary(right)) {
        return true;
      }
      text2 = text2.slice(i2 + nick.length);
    }
  }
  function isServerBroadcast(msg) {
    if (msg.command != "PRIVMSG" && msg.command != "NOTICE") {
      return false;
    }
    return msg.params[0].startsWith("$");
  }
  function isError(cmd) {
    if (cmd >= "400" && cmd <= "568") {
      return true;
    }
    switch (cmd) {
      case ERR_NICKLOCKED:
      case ERR_SASLFAIL:
      case ERR_SASLTOOLONG:
      case ERR_SASLABORTED:
      case ERR_SASLALREADY:
      case ERR_MONLISTFULL:
        return true;
      case "FAIL":
        return true;
      default:
        return false;
    }
  }
  function formatDate(date) {
    let YYYY = date.getUTCFullYear().toString().padStart(4, "0");
    let MM = (date.getUTCMonth() + 1).toString().padStart(2, "0");
    let DD = date.getUTCDate().toString().padStart(2, "0");
    let hh = date.getUTCHours().toString().padStart(2, "0");
    let mm = date.getUTCMinutes().toString().padStart(2, "0");
    let ss = date.getUTCSeconds().toString().padStart(2, "0");
    let sss = date.getUTCMilliseconds().toString().padStart(3, "0");
    return `${YYYY}-${MM}-${DD}T${hh}:${mm}:${ss}.${sss}Z`;
  }
  function parseCTCP(msg) {
    if (msg.command != "PRIVMSG" && msg.command != "NOTICE") {
      return null;
    }
    let text2 = msg.params[1];
    if (!text2.startsWith("")) {
      return null;
    }
    text2 = text2.slice(1);
    if (text2.endsWith("")) {
      text2 = text2.slice(0, -1);
    }
    let ctcp;
    let i2 = text2.indexOf(" ");
    if (i2 >= 0) {
      ctcp = { command: text2.slice(0, i2), param: text2.slice(i2 + 1) };
    } else {
      ctcp = { command: text2, param: "" };
    }
    ctcp.command = ctcp.command.toUpperCase();
    return ctcp;
  }
  function unescapeISUPPORTValue(s2) {
    return s2.replace(/\\x[0-9A-Z]{2}/gi, (esc) => {
      let hex = esc.slice(2);
      return String.fromCharCode(parseInt(hex, 16));
    });
  }
  var Isupport = class {
    constructor() {
      __publicField(this, "raw", /* @__PURE__ */ new Map());
    }
    parse(tokens) {
      tokens.forEach((tok) => {
        if (tok.startsWith("-")) {
          let k3 = tok.slice(1);
          this.raw.delete(k3.toUpperCase());
          return;
        }
        let i2 = tok.indexOf("=");
        let k2 = tok, v2 = "";
        if (i2 >= 0) {
          k2 = tok.slice(0, i2);
          v2 = unescapeISUPPORTValue(tok.slice(i2 + 1));
        }
        k2 = k2.toUpperCase();
        this.raw.set(k2, v2);
      });
    }
    caseMapping() {
      let name = this.raw.get("CASEMAPPING");
      if (!name) {
        return CaseMapping.RFC1459;
      }
      let cm = CaseMapping.byName(name);
      if (!cm) {
        console.error("Unsupported case-mapping '" + name + "', falling back to RFC 1459");
        return CaseMapping.RFC1459;
      }
      return cm;
    }
    monitor() {
      if (!this.raw.has("MONITOR")) {
        return 0;
      }
      let v2 = this.raw.get("MONITOR");
      if (v2 === "") {
        return Infinity;
      }
      return parseInt(v2, 10);
    }
    whox() {
      return this.raw.has("WHOX");
    }
    prefix() {
      return this.raw.get("PREFIX") || "";
    }
    chanTypes() {
      return this.raw.get("CHANTYPES") || STD_CHANTYPES;
    }
    statusMsg() {
      return this.raw.get("STATUSMSG");
    }
    network() {
      return this.raw.get("NETWORK");
    }
    chatHistory() {
      if (!this.raw.has("CHATHISTORY")) {
        return 0;
      }
      let n3 = parseInt(this.raw.get("CHATHISTORY"), 10);
      if (n3 <= 0) {
        return Infinity;
      }
      return n3;
    }
    bouncerNetID() {
      return this.raw.get("BOUNCER_NETID");
    }
    chanModes() {
      const stdChanModes = ["beI", "k", "l", "imnst"];
      if (!this.raw.has("CHANMODES")) {
        return stdChanModes;
      }
      let chanModes = this.raw.get("CHANMODES").split(",");
      if (chanModes.length != 4) {
        console.error("Invalid CHANMODES: ", this.raw.get("CHANMODES"));
        return stdChanModes;
      }
      return chanModes;
    }
  };
  var CaseMapping = {
    ASCII(str) {
      let out = "";
      for (let i2 = 0; i2 < str.length; i2++) {
        let ch = str[i2];
        if ("A" <= ch && ch <= "Z") {
          ch = ch.toLowerCase();
        }
        out += ch;
      }
      return out;
    },
    RFC1459(str) {
      let out = "";
      for (let i2 = 0; i2 < str.length; i2++) {
        let ch = str[i2];
        if ("A" <= ch && ch <= "Z") {
          ch = ch.toLowerCase();
        } else if (ch == "{") {
          ch = "[";
        } else if (ch == "}") {
          ch = "]";
        } else if (ch == "\\") {
          ch = "|";
        } else if (ch == "~") {
          ch = "^";
        }
        out += ch;
      }
      return out;
    },
    RFC1459Strict(str) {
      let out = "";
      for (let i2 = 0; i2 < str.length; i2++) {
        let ch = str[i2];
        if ("A" <= ch && ch <= "Z") {
          ch = ch.toLowerCase();
        } else if (ch == "{") {
          ch = "[";
        } else if (ch == "}") {
          ch = "]";
        } else if (ch == "\\") {
          ch = "|";
        }
        out += ch;
      }
      return out;
    },
    byName(name) {
      switch (name) {
        case "ascii":
          return CaseMapping.ASCII;
        case "rfc1459":
          return CaseMapping.RFC1459;
        case "rfc1459-strict":
          return CaseMapping.RFC1459Strict;
      }
      return null;
    }
  };
  function createIterator(next) {
    let it = { next };
    it[Symbol.iterator] = () => it;
    return it;
  }
  function mapIterator(it, f2) {
    return createIterator(() => {
      let { value, done } = it.next();
      if (done) {
        return { done: true };
      }
      return { value: f2(value), done: false };
    });
  }
  var CaseMapMap = class {
    constructor(iterable, cm) {
      __publicField(this, "caseMap", null);
      __publicField(this, "map", null);
      if (iterable instanceof CaseMapMap && (iterable.caseMap === cm || !cm)) {
        this.caseMap = iterable.caseMap;
        this.map = new Map(iterable.map);
      } else {
        if (!cm) {
          throw new Error("Missing case-mapping when creating CaseMapMap");
        }
        this.caseMap = cm;
        this.map = /* @__PURE__ */ new Map();
        if (iterable) {
          for (let [key, value] of iterable) {
            this.set(key, value);
          }
        }
      }
    }
    get size() {
      return this.map.size;
    }
    has(key) {
      return this.map.has(this.caseMap(key));
    }
    get(key) {
      let kv = this.map.get(this.caseMap(key));
      if (kv) {
        return kv.value;
      }
      return void 0;
    }
    set(key, value) {
      this.map.set(this.caseMap(key), { key, value });
    }
    delete(key) {
      this.map.delete(this.caseMap(key));
    }
    entries() {
      let it = this.map.values();
      return mapIterator(it, (kv) => {
        return [kv.key, kv.value];
      });
    }
    keys() {
      let it = this.map.values();
      return mapIterator(it, (kv) => {
        return kv.key;
      });
    }
    values() {
      let it = this.map.values();
      return mapIterator(it, (kv) => {
        return kv.value;
      });
    }
    [Symbol.iterator]() {
      return this.entries();
    }
  };
  function parseMembershipModes(str) {
    if (str[0] !== "(") {
      throw new Error("malformed ISUPPORT PREFIX value: expected opening parenthesis");
    }
    let sep = str.indexOf(")");
    if (sep < 0) {
      throw new Error("malformed ISUPPORT PREFIX value: expected closing parenthesis");
    }
    let n3 = str.length - sep - 1;
    let memberships = [];
    for (let i2 = 0; i2 < n3; i2++) {
      let mode = str[i2 + 1];
      let prefix = str[sep + i2 + 1];
      memberships.push({ mode, prefix });
    }
    return memberships;
  }
  function findBatchByType(msg, type) {
    let batch = msg.batch;
    while (batch) {
      if (batch.type === type) {
        return batch;
      }
      batch = batch.parent;
    }
    return null;
  }
  function getMessageLabel(msg) {
    if (msg.tags.label) {
      return msg.tags.label;
    }
    let batch = msg.batch;
    while (batch) {
      if (batch.tags.label) {
        return batch.tags.label;
      }
      batch = batch.parent;
    }
    return null;
  }
  function forEachChannelModeUpdate(msg, isupport, callback) {
    let [a3, b2, c2, d2] = isupport.chanModes();
    let prefix = isupport.prefix();
    let typeByMode = /* @__PURE__ */ new Map();
    Array.from(a3).forEach((mode) => typeByMode.set(mode, "A"));
    Array.from(b2).forEach((mode) => typeByMode.set(mode, "B"));
    Array.from(c2).forEach((mode) => typeByMode.set(mode, "C"));
    Array.from(d2).forEach((mode) => typeByMode.set(mode, "D"));
    parseMembershipModes(prefix).forEach((membership) => typeByMode.set(membership.mode, "B"));
    if (msg.command !== "MODE") {
      throw new Error("Expected a MODE message");
    }
    let change = msg.params[1];
    let args = msg.params.slice(2);
    let plusMinus = null;
    let j2 = 0;
    for (let i2 = 0; i2 < change.length; i2++) {
      if (change[i2] === "+" || change[i2] === "-") {
        plusMinus = change[i2];
        continue;
      }
      if (!plusMinus) {
        throw new Error("malformed mode string: missing plus/minus");
      }
      let mode = change[i2];
      let add = plusMinus === "+";
      let modeType = typeByMode.get(mode);
      if (!modeType) {
        continue;
      }
      let arg = null;
      if (modeType === "A" || modeType === "B" || modeType === "C" && add) {
        arg = args[j2];
        j2++;
      }
      callback(mode, add, arg);
    }
  }
  function isMeaningfulRealname(realname, nick) {
    if (!realname || realname === nick) {
      return false;
    }
    if (realname.toLowerCase() === "realname" || realname.toLowerCase() === "unknown" || realname.toLowerCase() === "fullname") {
      return false;
    }
    return true;
  }
  function parseURL(str) {
    if (!str.startsWith("irc://") && !str.startsWith("ircs://")) {
      return null;
    }
    str = str.slice(str.indexOf(":") + "://".length);
    let loc;
    let i2 = str.indexOf("/");
    if (i2 < 0) {
      loc = str;
      str = "";
    } else {
      loc = str.slice(0, i2);
      str = str.slice(i2 + 1);
    }
    let host = loc;
    i2 = loc.indexOf("@");
    if (i2 >= 0) {
      host = loc.slice(i2 + 1);
    }
    i2 = str.indexOf("?");
    if (i2 >= 0) {
      str = str.slice(0, i2);
    }
    let enttype;
    i2 = str.indexOf(",");
    if (i2 >= 0) {
      let flags = str.slice(i2 + 1).split(",");
      str = str.slice(0, i2);
      if (flags.indexOf("isuser") >= 0) {
        enttype = "user";
      } else if (flags.indexOf("ischannel") >= 0) {
        enttype = "channel";
      }
    }
    let entity = decodeURIComponent(str);
    if (!enttype) {
      enttype = entity.startsWith("#") ? "channel" : "user";
    }
    return { host, enttype, entity };
  }
  var CapRegistry = class {
    constructor() {
      __publicField(this, "available", /* @__PURE__ */ new Map());
      __publicField(this, "enabled", /* @__PURE__ */ new Set());
    }
    addAvailable(s2) {
      let l2 = s2.split(" ");
      l2.forEach((s3) => {
        let i2 = s3.indexOf("=");
        let k2 = s3, v2 = "";
        if (i2 >= 0) {
          k2 = s3.slice(0, i2);
          v2 = s3.slice(i2 + 1);
        }
        this.available.set(k2.toLowerCase(), v2);
      });
    }
    parse(msg) {
      if (msg.command !== "CAP") {
        return;
      }
      let subCmd = msg.params[1];
      let args = msg.params.slice(2);
      switch (subCmd) {
        case "LS":
          this.addAvailable(args[args.length - 1]);
          break;
        case "NEW":
          this.addAvailable(args[0]);
          break;
        case "DEL":
          args[0].split(" ").forEach((cap) => {
            cap = cap.toLowerCase();
            this.available.delete(cap);
            this.enabled.delete(cap);
          });
          break;
        case "ACK":
          args[0].split(" ").forEach((cap) => {
            cap = cap.toLowerCase();
            if (cap.startsWith("-")) {
              this.enabled.delete(cap.slice(1));
            } else {
              this.enabled.add(cap);
            }
          });
          break;
      }
    }
    requestAvailable(l2) {
      l2 = l2.filter((cap) => {
        return this.available.has(cap) && !this.enabled.has(cap);
      });
      if (l2.length === 0) {
        return null;
      }
      return { command: "CAP", params: ["REQ", l2.join(" ")] };
    }
  };

  // gamja/lib/client.js
  var permanentCaps = [
    "account-notify",
    "away-notify",
    "batch",
    "chghost",
    "echo-message",
    "extended-join",
    "invite-notify",
    "labeled-response",
    "message-tags",
    "multi-prefix",
    "sasl",
    "server-time",
    "setname",
    "draft/account-registration",
    "draft/chathistory",
    "draft/event-playback",
    "draft/extended-monitor",
    "soju.im/bouncer-networks",
    "soju.im/read"
  ];
  var RECONNECT_MIN_DELAY_MSEC = 10 * 1e3;
  var RECONNECT_MAX_DELAY_MSEC = 10 * 60 * 1e3;
  var NORMAL_CLOSURE = 1e3;
  var GOING_AWAY = 1001;
  var UNSUPPORTED_DATA = 1003;
  var WHOX_FIELDS = {
    "channel": "c",
    "username": "u",
    "hostname": "h",
    "server": "s",
    "nick": "n",
    "flags": "f",
    "account": "a",
    "realname": "r"
  };
  var lastLabel = 0;
  var lastWhoxToken = 0;
  var IRCError = class extends Error {
    constructor(msg) {
      let text2;
      if (msg.params.length > 0) {
        text2 = msg.params[msg.params.length - 1];
      } else {
        text2 = `unknown error (${msg.command})`;
      }
      super(text2);
      this.msg = msg;
    }
  };
  var Backoff = class {
    constructor(min, max) {
      __publicField(this, "n", 0);
      this.min = min;
      this.max = max;
    }
    reset() {
      this.n = 0;
    }
    next() {
      if (this.n === 0) {
        this.n = 1;
        return this.min;
      }
      let dur = this.n * this.min;
      if (dur > this.max) {
        dur = this.max;
      } else {
        this.n *= 2;
      }
      return dur;
    }
  };
  var _Client = class extends EventTarget {
    constructor(params) {
      super();
      __publicField(this, "status", _Client.Status.DISCONNECTED);
      __publicField(this, "serverPrefix", { name: "*" });
      __publicField(this, "nick", null);
      __publicField(this, "supportsCap", false);
      __publicField(this, "caps", new CapRegistry());
      __publicField(this, "isupport", new Isupport());
      __publicField(this, "ws", null);
      __publicField(this, "params", {
        url: null,
        username: null,
        realname: null,
        nick: null,
        pass: null,
        saslPlain: null,
        saslExternal: false,
        bouncerNetwork: null,
        ping: 0
      });
      __publicField(this, "debug", false);
      __publicField(this, "batches", /* @__PURE__ */ new Map());
      __publicField(this, "autoReconnect", true);
      __publicField(this, "reconnectTimeoutID", null);
      __publicField(this, "reconnectBackoff", new Backoff(RECONNECT_MIN_DELAY_MSEC, RECONNECT_MAX_DELAY_MSEC));
      __publicField(this, "pingIntervalID", null);
      __publicField(this, "pendingCmds", {
        WHO: Promise.resolve(null),
        CHATHISTORY: Promise.resolve(null)
      });
      __publicField(this, "cm", CaseMapping.RFC1459);
      __publicField(this, "monitored", new CaseMapMap(null, CaseMapping.RFC1459));
      __publicField(this, "pendingLists", new CaseMapMap(null, CaseMapping.RFC1459));
      __publicField(this, "whoxQueries", /* @__PURE__ */ new Map());
      this.params = __spreadValues(__spreadValues({}, this.params), params);
      this.reconnect();
    }
    reconnect() {
      let autoReconnect = this.autoReconnect;
      this.disconnect();
      this.autoReconnect = autoReconnect;
      console.log("Connecting to " + this.params.url);
      this.setStatus(_Client.Status.CONNECTING);
      try {
        this.ws = new WebSocket(this.params.url);
      } catch (err) {
        console.error("Failed to create connection:", err);
        setTimeout(() => {
          this.dispatchError(new Error("Failed to create connection", { cause: err }));
          this.setStatus(_Client.Status.DISCONNECTED);
        }, 0);
        return;
      }
      this.ws.addEventListener("open", this.handleOpen.bind(this));
      this.ws.addEventListener("message", (event) => {
        try {
          this.handleMessage(event);
        } catch (err) {
          this.dispatchError(err);
          this.disconnect();
        }
      });
      this.ws.addEventListener("close", (event) => {
        console.log("Connection closed (code: " + event.code + ")");
        if (event.code !== NORMAL_CLOSURE && event.code !== GOING_AWAY) {
          this.dispatchError(new Error("Connection error"));
        }
        this.ws = null;
        this.setStatus(_Client.Status.DISCONNECTED);
        this.nick = null;
        this.serverPrefix = null;
        this.caps = new CapRegistry();
        this.batches = /* @__PURE__ */ new Map();
        Object.keys(this.pendingCmds).forEach((k2) => {
          this.pendingCmds[k2] = Promise.resolve(null);
        });
        this.isupport = new Isupport();
        this.monitored = new CaseMapMap(null, CaseMapping.RFC1459);
        if (this.autoReconnect) {
          if (!navigator.onLine) {
            console.info("Waiting for network to go back online");
            const handleOnline = () => {
              window.removeEventListener("online", handleOnline);
              this.reconnect();
            };
            window.addEventListener("online", handleOnline);
          } else {
            let delay = this.reconnectBackoff.next();
            console.info("Reconnecting to server in " + delay / 1e3 + " seconds");
            clearTimeout(this.reconnectTimeoutID);
            this.reconnectTimeoutID = setTimeout(() => {
              this.reconnect();
            }, delay);
          }
        }
      });
    }
    disconnect() {
      this.autoReconnect = false;
      clearTimeout(this.reconnectTimeoutID);
      this.reconnectTimeoutID = null;
      this.setPingInterval(0);
      if (this.ws) {
        this.ws.close(NORMAL_CLOSURE);
      }
    }
    setStatus(status) {
      if (this.status === status) {
        return;
      }
      this.status = status;
      this.dispatchEvent(new CustomEvent("status"));
    }
    dispatchError(err) {
      this.dispatchEvent(new CustomEvent("error", { detail: err }));
    }
    handleOpen() {
      console.log("Connection opened");
      this.setStatus(_Client.Status.REGISTERING);
      this.reconnectBackoff.reset();
      this.setPingInterval(this.params.ping);
      this.nick = this.params.nick;
      this.send({ command: "CAP", params: ["LS", "302"] });
      if (this.params.pass) {
        this.send({ command: "PASS", params: [this.params.pass] });
      }
      this.send({ command: "NICK", params: [this.nick] });
      this.send({
        command: "USER",
        params: [this.params.username, "0", "*", this.params.realname]
      });
    }
    pushPendingList(k2, msg) {
      let l2 = this.pendingLists.get(k2);
      if (!l2) {
        l2 = [];
        this.pendingLists.set(k2, l2);
      }
      l2.push(msg);
    }
    endPendingList(k2, msg) {
      msg.list = this.pendingLists.get(k2) || [];
      this.pendingLists.delete(k2);
    }
    handleMessage(event) {
      if (typeof event.data !== "string") {
        console.error("Received unsupported data type:", event.data);
        this.ws.close(UNSUPPORTED_DATA);
        return;
      }
      let msg = parseMessage(event.data);
      if (this.debug) {
        console.debug("Received:", msg);
      }
      if (!msg.prefix) {
        msg.prefix = this.serverPrefix;
      }
      let msgBatch = null;
      if (msg.tags["batch"]) {
        msgBatch = this.batches.get(msg.tags["batch"]);
        if (msgBatch) {
          msg.batch = msgBatch;
        }
      }
      let deleteBatch = null;
      let k2;
      switch (msg.command) {
        case RPL_WELCOME:
          if (this.params.saslPlain && !this.supportsCap) {
            this.dispatchError(new Error("Server doesn't support SASL PLAIN"));
            this.disconnect();
            return;
          }
          if (msg.prefix) {
            this.serverPrefix = msg.prefix;
          }
          this.nick = msg.params[0];
          console.log("Registration complete");
          this.setStatus(_Client.Status.REGISTERED);
          break;
        case RPL_ISUPPORT:
          let prevMaxMonitorTargets = this.isupport.monitor();
          let tokens = msg.params.slice(1, -1);
          this.isupport.parse(tokens);
          this.updateCaseMapping();
          let maxMonitorTargets = this.isupport.monitor();
          if (prevMaxMonitorTargets === 0 && this.monitored.size > 0 && maxMonitorTargets > 0) {
            let targets = Array.from(this.monitored.keys()).slice(0, maxMonitorTargets);
            this.send({ command: "MONITOR", params: ["+", targets.join(",")] });
          }
          break;
        case RPL_ENDOFMOTD:
        case ERR_NOMOTD:
          if (!this.isupport.raw.has("CASEMAPPING")) {
            this.updateCaseMapping();
          }
          break;
        case "CAP":
          this.handleCap(msg);
          break;
        case "AUTHENTICATE":
          let challengeStr = msg.params[0];
          if (challengeStr != "+") {
            this.dispatchError(new Error("Expected an empty challenge, got: " + challengeStr));
            this.send({ command: "AUTHENTICATE", params: ["*"] });
          }
          break;
        case RPL_LOGGEDIN:
          console.log("Logged in");
          break;
        case RPL_LOGGEDOUT:
          console.log("Logged out");
          break;
        case RPL_NAMREPLY:
          this.pushPendingList("NAMES " + msg.params[2], msg);
          break;
        case RPL_ENDOFNAMES:
          this.endPendingList("NAMES " + msg.params[1], msg);
          break;
        case RPL_WHOISUSER:
        case RPL_WHOISSERVER:
        case RPL_WHOISOPERATOR:
        case RPL_WHOISIDLE:
        case RPL_WHOISCHANNELS:
          this.pushPendingList("WHOIS " + msg.params[1], msg);
          break;
        case RPL_ENDOFWHOIS:
          this.endPendingList("WHOIS " + msg.params[1], msg);
          break;
        case RPL_WHOREPLY:
        case RPL_WHOSPCRPL:
          this.pushPendingList("WHO", msg);
          break;
        case RPL_ENDOFWHO:
          this.endPendingList("WHO", msg);
          break;
        case "PING":
          this.send({ command: "PONG", params: [msg.params[0]] });
          break;
        case "NICK":
          let newNick = msg.params[0];
          if (this.isMyNick(msg.prefix.name)) {
            this.nick = newNick;
          }
          break;
        case "BATCH":
          let enter = msg.params[0].startsWith("+");
          let name = msg.params[0].slice(1);
          if (enter) {
            let batch = {
              name,
              type: msg.params[1],
              params: msg.params.slice(2),
              tags: msg.tags,
              parent: msgBatch
            };
            this.batches.set(name, batch);
          } else {
            deleteBatch = name;
          }
          break;
        case "ERROR":
          this.dispatchError(new IRCError(msg));
          this.disconnect();
          break;
        case ERR_PASSWDMISMATCH:
        case ERR_ERRONEUSNICKNAME:
        case ERR_NICKNAMEINUSE:
        case ERR_NICKCOLLISION:
        case ERR_UNAVAILRESOURCE:
        case ERR_NOPERMFORHOST:
        case ERR_YOUREBANNEDCREEP:
          this.dispatchError(new IRCError(msg));
          if (this.status != _Client.Status.REGISTERED) {
            this.disconnect();
          }
          break;
        case "FAIL":
          if (this.status === _Client.Status.REGISTERED) {
            break;
          }
          let reason = msg.params[msg.params.length - 1];
          if (msg.params[0] === "BOUNCER" && msg.params[2] === "BIND") {
            this.dispatchError(new Error("Failed to bind to bouncer network", {
              cause: new IRCError(msg)
            }));
            this.disconnect();
          }
          if (msg.params[1] === "ACCOUNT_REQUIRED") {
            this.dispatchError(new IRCError(msg));
            this.disconnect();
          }
          break;
      }
      this.dispatchEvent(new CustomEvent("message", {
        detail: { message: msg, batch: msgBatch }
      }));
      if (deleteBatch) {
        this.batches.delete(deleteBatch);
      }
    }
    authenticate(mechanism, params) {
      if (!this.supportsSASL(mechanism)) {
        throw new Error(`${mechanism} authentication not supported by the server`);
      }
      console.log(`Starting SASL ${mechanism} authentication`);
      let initialResp = null;
      switch (mechanism) {
        case "PLAIN":
          let respStr = btoa("\0" + params.username + "\0" + params.password);
          initialResp = { command: "AUTHENTICATE", params: [respStr] };
          break;
        case "EXTERNAL":
          initialResp = { command: "AUTHENTICATE", params: [btoa("")] };
          break;
        default:
          throw new Error(`Unknown authentication mechanism '${mechanism}'`);
      }
      let startMsg = { command: "AUTHENTICATE", params: [mechanism] };
      let promise = this.roundtrip(startMsg, (msg) => {
        switch (msg.command) {
          case RPL_SASLSUCCESS:
            return true;
          case ERR_NICKLOCKED:
          case ERR_SASLFAIL:
          case ERR_SASLTOOLONG:
          case ERR_SASLABORTED:
          case ERR_SASLALREADY:
            throw new IRCError(msg);
        }
      });
      this.send(initialResp);
      return promise;
    }
    who(mask, options2) {
      let params = [mask];
      let fields = "", token = "";
      if (options2 && this.isupport.whox()) {
        let match = "";
        fields = "t";
        if (options2.fields) {
          options2.fields.forEach((k2) => {
            if (!WHOX_FIELDS[k2]) {
              throw new Error(`Unknown WHOX field ${k2}`);
            }
            fields += WHOX_FIELDS[k2];
          });
        }
        token = String(lastWhoxToken % 1e3);
        lastWhoxToken++;
        params.push(`${match}%${fields},${token}`);
        this.whoxQueries.set(token, fields);
      }
      let msg = { command: "WHO", params };
      let l2 = [];
      let promise = this.pendingCmds.WHO.then(() => {
        return this.roundtrip(msg, (msg2) => {
          switch (msg2.command) {
            case RPL_WHOREPLY:
              l2.push(this.parseWhoReply(msg2));
              break;
            case RPL_WHOSPCRPL:
              if (msg2.params.length !== fields.length || msg2.params[1] !== token) {
                break;
              }
              l2.push(this.parseWhoReply(msg2));
              break;
            case RPL_ENDOFWHO:
              if (msg2.params[1] === mask) {
                return l2;
              }
              break;
          }
        }).finally(() => {
          this.whoxQueries.delete(token);
        });
      });
      this.pendingCmds.WHO = promise.catch(() => {
      });
      return promise;
    }
    parseWhoReply(msg) {
      switch (msg.command) {
        case RPL_WHOREPLY:
          let last = msg.params[msg.params.length - 1];
          return {
            username: msg.params[2],
            hostname: msg.params[3],
            server: msg.params[4],
            nick: msg.params[5],
            flags: msg.params[6],
            realname: last.slice(last.indexOf(" ") + 1)
          };
        case RPL_WHOSPCRPL:
          let token = msg.params[1];
          let fields = this.whoxQueries.get(token);
          if (!fields) {
            throw new Error("Unknown WHOX token: " + token);
          }
          let who = {};
          let i2 = 0;
          Object.keys(WHOX_FIELDS).forEach((k2) => {
            if (fields.indexOf(WHOX_FIELDS[k2]) < 0) {
              return;
            }
            who[k2] = msg.params[2 + i2];
            i2++;
          });
          if (who.account === "0") {
            who.account = null;
          }
          return who;
        default:
          throw new Error("Not a WHO reply: " + msg.command);
      }
    }
    whois(target) {
      let targetCM = this.cm(target);
      let msg = { command: "WHOIS", params: [target] };
      return this.roundtrip(msg, (msg2) => {
        let nick;
        switch (msg2.command) {
          case RPL_ENDOFWHOIS:
            nick = msg2.params[1];
            if (this.cm(nick) === targetCM) {
              let whois = {};
              msg2.list.forEach((reply) => {
                whois[reply.command] = reply;
              });
              return whois;
            }
            break;
          case ERR_NOSUCHNICK:
            nick = msg2.params[1];
            if (this.cm(nick) === targetCM) {
              throw new IRCError(msg2);
            }
            break;
        }
      });
    }
    supportsSASL(mech) {
      let saslCap = this.caps.available.get("sasl");
      if (saslCap === void 0) {
        return false;
      }
      return saslCap.split(",").includes(mech);
    }
    checkAccountRegistrationCap(k2) {
      let v2 = this.caps.available.get("draft/account-registration");
      if (v2 === void 0) {
        return false;
      }
      return v2.split(",").includes(k2);
    }
    requestCaps() {
      let wantCaps = [].concat(permanentCaps);
      if (!this.params.bouncerNetwork) {
        wantCaps.push("soju.im/bouncer-networks-notify");
      }
      let msg = this.caps.requestAvailable(wantCaps);
      if (msg) {
        this.send(msg);
      }
    }
    handleCap(msg) {
      this.caps.parse(msg);
      let subCmd = msg.params[1];
      let args = msg.params.slice(2);
      switch (subCmd) {
        case "LS":
          this.supportsCap = true;
          if (args[0] == "*") {
            break;
          }
          console.log("Available server caps:", this.caps.available);
          this.requestCaps();
          if (this.status !== _Client.Status.REGISTERED) {
            if (this.caps.available.has("sasl")) {
              let promise;
              if (this.params.saslPlain) {
                promise = this.authenticate("PLAIN", this.params.saslPlain);
              } else if (this.params.saslExternal) {
                promise = this.authenticate("EXTERNAL");
              }
              (promise || Promise.resolve()).catch((err) => {
                this.dispatchError(err);
                this.disconnect();
              });
            }
            if (this.caps.available.has("soju.im/bouncer-networks") && this.params.bouncerNetwork) {
              this.send({ command: "BOUNCER", params: ["BIND", this.params.bouncerNetwork] });
            }
            this.send({ command: "CAP", params: ["END"] });
          }
          break;
        case "NEW":
          console.log("Server added available caps:", args[0]);
          this.requestCaps();
          break;
        case "DEL":
          console.log("Server removed available caps:", args[0]);
          break;
        case "ACK":
          console.log("Server ack'ed caps:", args[0]);
          break;
        case "NAK":
          console.log("Server nak'ed caps:", args[0]);
          if (this.status !== _Client.Status.REGISTERED) {
            this.send({ command: "CAP", params: ["END"] });
          }
          break;
      }
    }
    send(msg) {
      if (!this.ws) {
        throw new Error("Failed to send IRC message " + msg.command + ": socket is closed");
      }
      this.ws.send(formatMessage(msg));
      if (this.debug) {
        console.debug("Sent:", msg);
      }
    }
    updateCaseMapping() {
      this.cm = this.isupport.caseMapping();
      this.pendingLists = new CaseMapMap(this.pendingLists, this.cm);
      this.monitored = new CaseMapMap(this.monitored, this.cm);
    }
    isServer(name) {
      return name === "*" || this.cm(name) === this.cm(this.serverPrefix.name);
    }
    isMyNick(nick) {
      return this.cm(nick) == this.cm(this.nick);
    }
    isChannel(name) {
      let chanTypes = this.isupport.chanTypes();
      return chanTypes.indexOf(name[0]) >= 0;
    }
    setPingInterval(sec) {
      clearInterval(this.pingIntervalID);
      this.pingIntervalID = null;
      if (sec <= 0) {
        return;
      }
      this.pingIntervalID = setInterval(() => {
        if (this.ws) {
          this.send({ command: "PING", params: ["gamja"] });
        }
      }, sec * 1e3);
    }
    roundtrip(msg, done) {
      let cmd = msg.command;
      let label;
      if (this.caps.enabled.has("labeled-response")) {
        lastLabel++;
        label = String(lastLabel);
        msg.tags = __spreadProps(__spreadValues({}, msg.tags), { label });
      }
      return new Promise((resolve2, reject) => {
        let removeEventListeners;
        let handleMessage = (event) => {
          let msg2 = event.detail.message;
          let msgLabel = getMessageLabel(msg2);
          if (msgLabel && msgLabel != label) {
            return;
          }
          let isError2 = false;
          switch (msg2.command) {
            case "FAIL":
              isError2 = msg2.params[0] === cmd;
              break;
            case ERR_UNKNOWNERROR:
            case ERR_UNKNOWNCOMMAND:
            case ERR_NEEDMOREPARAMS:
            case RPL_TRYAGAIN:
              isError2 = msg2.params[1] === cmd;
              break;
          }
          if (isError2) {
            removeEventListeners();
            reject(new IRCError(msg2));
            return;
          }
          let result;
          try {
            result = done(msg2);
          } catch (err) {
            removeEventListeners();
            reject(err);
          }
          if (result) {
            removeEventListeners();
            resolve2(result);
          }
        };
        let handleStatus = () => {
          if (this.status === _Client.Status.DISCONNECTED) {
            removeEventListeners();
            reject(new Error("Connection closed"));
          }
        };
        removeEventListeners = () => {
          this.removeEventListener("message", handleMessage);
          this.removeEventListener("status", handleStatus);
        };
        this.addEventListener("message", handleMessage);
        this.addEventListener("status", handleStatus);
        this.send(msg);
      });
    }
    join(channel, password) {
      let params = [channel];
      if (password) {
        params.push(password);
      }
      let msg = {
        command: "JOIN",
        params
      };
      return this.roundtrip(msg, (msg2) => {
        switch (msg2.command) {
          case ERR_NOSUCHCHANNEL:
          case ERR_TOOMANYCHANNELS:
          case ERR_BADCHANNELKEY:
          case ERR_BANNEDFROMCHAN:
          case ERR_CHANNELISFULL:
          case ERR_INVITEONLYCHAN:
            if (this.cm(msg2.params[1]) === this.cm(channel)) {
              throw new IRCError(msg2);
            }
            break;
          case "JOIN":
            if (this.isMyNick(msg2.prefix.name) && this.cm(msg2.params[0]) === this.cm(channel)) {
              return true;
            }
            break;
        }
      });
    }
    fetchBatch(msg, batchType) {
      let batchName = null;
      let messages = [];
      let cmd = msg.command;
      return this.roundtrip(msg, (msg2) => {
        if (batchName) {
          let batch = msg2.batch;
          while (batch) {
            if (batch.name === batchName) {
              messages.push(msg2);
              break;
            }
            batch = batch.parent;
          }
        }
        if (msg2.command !== "BATCH") {
          return;
        }
        let enter = msg2.params[0].startsWith("+");
        let name = msg2.params[0].slice(1);
        if (enter && msg2.params[1] === batchType) {
          batchName = name;
          return;
        }
        if (!enter && name === batchName) {
          return __spreadProps(__spreadValues({}, this.batches.get(name)), { messages });
        }
      });
    }
    roundtripChatHistory(params) {
      let promise = this.pendingCmds.CHATHISTORY.then(() => {
        let msg = {
          command: "CHATHISTORY",
          params
        };
        return this.fetchBatch(msg, "chathistory").then((batch) => batch.messages);
      });
      this.pendingCmds.CHATHISTORY = promise.catch(() => {
      });
      return promise;
    }
    fetchHistoryBefore(target, before, limit) {
      let max = Math.min(limit, this.isupport.chatHistory());
      let params = ["BEFORE", target, "timestamp=" + before, max];
      return this.roundtripChatHistory(params).then((messages) => {
        return { more: messages.length >= max };
      });
    }
    fetchHistoryBetween(target, after, before, limit) {
      let max = Math.min(limit, this.isupport.chatHistory());
      let params = ["AFTER", target, "timestamp=" + after.time, max];
      return this.roundtripChatHistory(params).then((messages) => {
        limit -= messages.length;
        if (limit <= 0) {
          throw new Error("Cannot fetch all chat history: too many messages");
        }
        if (messages.length == max) {
          after.time = messages[messages.length - 1].tags.time;
          return this.fetchHistoryBetween(target, after, before, limit);
        }
        return null;
      });
    }
    fetchHistoryTargets(t1, t22) {
      let msg = {
        command: "CHATHISTORY",
        params: ["TARGETS", "timestamp=" + t1, "timestamp=" + t22, 1e3]
      };
      return this.fetchBatch(msg, "draft/chathistory-targets").then((batch) => {
        return batch.messages.map((msg2) => {
          if (msg2.command != "CHATHISTORY" || msg2.params[0] != "TARGETS") {
            throw new Error("Cannot fetch chat history targets: unexpected message " + msg2);
          }
          return {
            name: msg2.params[1],
            latestMessage: msg2.params[2]
          };
        });
      });
    }
    listBouncerNetworks() {
      let req = { command: "BOUNCER", params: ["LISTNETWORKS"] };
      return this.fetchBatch(req, "soju.im/bouncer-networks").then((batch) => {
        let networks = /* @__PURE__ */ new Map();
        for (let msg of batch.messages) {
          console.assert(msg.command === "BOUNCER" && msg.params[0] === "NETWORK");
          let id = msg.params[1];
          let params = parseTags(msg.params[2]);
          networks.set(id, params);
        }
        return networks;
      });
    }
    monitor(target) {
      if (this.monitored.has(target)) {
        return;
      }
      this.monitored.set(target, true);
      if (this.monitored.size + 1 > this.isupport.monitor()) {
        return;
      }
      this.send({ command: "MONITOR", params: ["+", target] });
    }
    unmonitor(target) {
      if (!this.monitored.has(target)) {
        return;
      }
      this.monitored.delete(target);
      if (this.isupport.monitor() <= 0) {
        return;
      }
      this.send({ command: "MONITOR", params: ["-", target] });
    }
    createBouncerNetwork(attrs) {
      let msg = {
        command: "BOUNCER",
        params: ["ADDNETWORK", formatTags(attrs)]
      };
      return this.roundtrip(msg, (msg2) => {
        if (msg2.command === "BOUNCER" && msg2.params[0] === "ADDNETWORK") {
          return msg2.params[1];
        }
      });
    }
    registerAccount(email, password) {
      let msg = {
        command: "REGISTER",
        params: ["*", email || "*", password]
      };
      return this.roundtrip(msg, (msg2) => {
        if (msg2.command !== "REGISTER") {
          return;
        }
        let result = msg2.params[0];
        return {
          verificationRequired: result === "VERIFICATION_REQUIRED",
          account: msg2.params[1],
          message: msg2.params[2]
        };
      });
    }
    verifyAccount(account, code) {
      let msg = {
        command: "VERIFY",
        params: [account, code]
      };
      return this.roundtrip(msg, (msg2) => {
        if (msg2.command !== "VERIFY") {
          return;
        }
        return { message: msg2.params[2] };
      });
    }
  };
  var Client = _Client;
  __publicField(Client, "Status", {
    DISCONNECTED: "disconnected",
    CONNECTING: "connecting",
    REGISTERING: "registering",
    REGISTERED: "registered"
  });

  // gamja/node_modules/linkifyjs/dist/linkify.module.js
  var linkify_module_exports = {};
  __export(linkify_module_exports, {
    Options: () => Options,
    find: () => find,
    init: () => init,
    options: () => options,
    registerCustomProtocol: () => registerCustomProtocol,
    registerPlugin: () => registerPlugin,
    reset: () => reset,
    test: () => test,
    tokenize: () => tokenize
  });
  function State(token) {
    this.j = {};
    this.jr = [];
    this.jd = null;
    this.t = token;
  }
  State.prototype = {
    accepts: function accepts() {
      return !!this.t;
    },
    tt: function tt(input, tokenOrState) {
      if (tokenOrState && tokenOrState.j) {
        this.j[input] = tokenOrState;
        return tokenOrState;
      }
      var token = tokenOrState;
      var nextState = this.j[input];
      if (nextState) {
        if (token) {
          nextState.t = token;
        }
        return nextState;
      }
      nextState = makeState();
      var templateState = takeT(this, input);
      if (templateState) {
        Object.assign(nextState.j, templateState.j);
        nextState.jr.append(templateState.jr);
        nextState.jr = templateState.jd;
        nextState.t = token || templateState.t;
      } else {
        nextState.t = token;
      }
      this.j[input] = nextState;
      return nextState;
    }
  };
  var makeState = function makeState2() {
    return new State();
  };
  var makeAcceptingState = function makeAcceptingState2(token) {
    return new State(token);
  };
  var makeT = function makeT2(startState, input, nextState) {
    if (!startState.j[input]) {
      startState.j[input] = nextState;
    }
  };
  var makeRegexT = function makeRegexT2(startState, regex, nextState) {
    startState.jr.push([regex, nextState]);
  };
  var takeT = function takeT2(state, input) {
    var nextState = state.j[input];
    if (nextState) {
      return nextState;
    }
    for (var i2 = 0; i2 < state.jr.length; i2++) {
      var regex = state.jr[i2][0];
      var _nextState = state.jr[i2][1];
      if (regex.test(input)) {
        return _nextState;
      }
    }
    return state.jd;
  };
  var makeMultiT = function makeMultiT2(startState, chars, nextState) {
    for (var i2 = 0; i2 < chars.length; i2++) {
      makeT(startState, chars[i2], nextState);
    }
  };
  var makeBatchT = function makeBatchT2(startState, transitions) {
    for (var i2 = 0; i2 < transitions.length; i2++) {
      var input = transitions[i2][0];
      var nextState = transitions[i2][1];
      makeT(startState, input, nextState);
    }
  };
  var makeChainT = function makeChainT2(state, str, endState, defaultStateFactory) {
    var i2 = 0, len = str.length, nextState;
    while (i2 < len && (nextState = state.j[str[i2]])) {
      state = nextState;
      i2++;
    }
    if (i2 >= len) {
      return [];
    }
    while (i2 < len - 1) {
      nextState = defaultStateFactory();
      makeT(state, str[i2], nextState);
      state = nextState;
      i2++;
    }
    makeT(state, str[len - 1], endState);
  };
  var DOMAIN = "DOMAIN";
  var LOCALHOST = "LOCALHOST";
  var TLD = "TLD";
  var NUM = "NUM";
  var PROTOCOL = "PROTOCOL";
  var MAILTO = "MAILTO";
  var WS = "WS";
  var NL = "NL";
  var OPENBRACE = "OPENBRACE";
  var OPENBRACKET = "OPENBRACKET";
  var OPENANGLEBRACKET = "OPENANGLEBRACKET";
  var OPENPAREN = "OPENPAREN";
  var CLOSEBRACE = "CLOSEBRACE";
  var CLOSEBRACKET = "CLOSEBRACKET";
  var CLOSEANGLEBRACKET = "CLOSEANGLEBRACKET";
  var CLOSEPAREN = "CLOSEPAREN";
  var AMPERSAND = "AMPERSAND";
  var APOSTROPHE = "APOSTROPHE";
  var ASTERISK = "ASTERISK";
  var AT = "AT";
  var BACKSLASH = "BACKSLASH";
  var BACKTICK = "BACKTICK";
  var CARET = "CARET";
  var COLON = "COLON";
  var COMMA = "COMMA";
  var DOLLAR = "DOLLAR";
  var DOT = "DOT";
  var EQUALS = "EQUALS";
  var EXCLAMATION = "EXCLAMATION";
  var HYPHEN = "HYPHEN";
  var PERCENT = "PERCENT";
  var PIPE = "PIPE";
  var PLUS = "PLUS";
  var POUND = "POUND";
  var QUERY = "QUERY";
  var QUOTE = "QUOTE";
  var SEMI = "SEMI";
  var SLASH = "SLASH";
  var TILDE = "TILDE";
  var UNDERSCORE = "UNDERSCORE";
  var SYM = "SYM";
  var text = /* @__PURE__ */ Object.freeze({
    __proto__: null,
    DOMAIN,
    LOCALHOST,
    TLD,
    NUM,
    PROTOCOL,
    MAILTO,
    WS,
    NL,
    OPENBRACE,
    OPENBRACKET,
    OPENANGLEBRACKET,
    OPENPAREN,
    CLOSEBRACE,
    CLOSEBRACKET,
    CLOSEANGLEBRACKET,
    CLOSEPAREN,
    AMPERSAND,
    APOSTROPHE,
    ASTERISK,
    AT,
    BACKSLASH,
    BACKTICK,
    CARET,
    COLON,
    COMMA,
    DOLLAR,
    DOT,
    EQUALS,
    EXCLAMATION,
    HYPHEN,
    PERCENT,
    PIPE,
    PLUS,
    POUND,
    QUERY,
    QUOTE,
    SEMI,
    SLASH,
    TILDE,
    UNDERSCORE,
    SYM
  });
  var tlds = "aaa aarp abarth abb abbott abbvie abc able abogado abudhabi ac academy accenture accountant accountants aco actor ad adac ads adult ae aeg aero aetna af afamilycompany afl africa ag agakhan agency ai aig airbus airforce airtel akdn al alfaromeo alibaba alipay allfinanz allstate ally alsace alstom am amazon americanexpress americanfamily amex amfam amica amsterdam analytics android anquan anz ao aol apartments app apple aq aquarelle ar arab aramco archi army arpa art arte as asda asia associates at athleta attorney au auction audi audible audio auspost author auto autos avianca aw aws ax axa az azure ba baby baidu banamex bananarepublic band bank bar barcelona barclaycard barclays barefoot bargains baseball basketball bauhaus bayern bb bbc bbt bbva bcg bcn bd be beats beauty beer bentley berlin best bestbuy bet bf bg bh bharti bi bible bid bike bing bingo bio biz bj black blackfriday blockbuster blog bloomberg blue bm bms bmw bn bnpparibas bo boats boehringer bofa bom bond boo book booking bosch bostik boston bot boutique box br bradesco bridgestone broadway broker brother brussels bs bt budapest bugatti build builders business buy buzz bv bw by bz bzh ca cab cafe cal call calvinklein cam camera camp cancerresearch canon capetown capital capitalone car caravan cards care career careers cars casa case cash casino cat catering catholic cba cbn cbre cbs cc cd center ceo cern cf cfa cfd cg ch chanel channel charity chase chat cheap chintai christmas chrome church ci cipriani circle cisco citadel citi citic city cityeats ck cl claims cleaning click clinic clinique clothing cloud club clubmed cm cn co coach codes coffee college cologne com comcast commbank community company compare computer comsec condos construction consulting contact contractors cooking cookingchannel cool coop corsica country coupon coupons courses cpa cr credit creditcard creditunion cricket crown crs cruise cruises csc cu cuisinella cv cw cx cy cymru cyou cz dabur dad dance data date dating datsun day dclk dds de deal dealer deals degree delivery dell deloitte delta democrat dental dentist desi design dev dhl diamonds diet digital direct directory discount discover dish diy dj dk dm dnp do docs doctor dog domains dot download drive dtv dubai duck dunlop dupont durban dvag dvr dz earth eat ec eco edeka edu education ee eg email emerck energy engineer engineering enterprises epson equipment er ericsson erni es esq estate et etisalat eu eurovision eus events exchange expert exposed express extraspace fage fail fairwinds faith family fan fans farm farmers fashion fast fedex feedback ferrari ferrero fi fiat fidelity fido film final finance financial fire firestone firmdale fish fishing fit fitness fj fk flickr flights flir florist flowers fly fm fo foo food foodnetwork football ford forex forsale forum foundation fox fr free fresenius frl frogans frontdoor frontier ftr fujitsu fujixerox fun fund furniture futbol fyi ga gal gallery gallo gallup game games gap garden gay gb gbiz gd gdn ge gea gent genting george gf gg ggee gh gi gift gifts gives giving gl glade glass gle global globo gm gmail gmbh gmo gmx gn godaddy gold goldpoint golf goo goodyear goog google gop got gov gp gq gr grainger graphics gratis green gripe grocery group gs gt gu guardian gucci guge guide guitars guru gw gy hair hamburg hangout haus hbo hdfc hdfcbank health healthcare help helsinki here hermes hgtv hiphop hisamitsu hitachi hiv hk hkt hm hn hockey holdings holiday homedepot homegoods homes homesense honda horse hospital host hosting hot hoteles hotels hotmail house how hr hsbc ht hu hughes hyatt hyundai ibm icbc ice icu id ie ieee ifm ikano il im imamat imdb immo immobilien in inc industries infiniti info ing ink institute insurance insure int international intuit investments io ipiranga iq ir irish is ismaili ist istanbul it itau itv iveco jaguar java jcb je jeep jetzt jewelry jio jll jm jmp jnj jo jobs joburg jot joy jp jpmorgan jprs juegos juniper kaufen kddi ke kerryhotels kerrylogistics kerryproperties kfh kg kh ki kia kim kinder kindle kitchen kiwi km kn koeln komatsu kosher kp kpmg kpn kr krd kred kuokgroup kw ky kyoto kz la lacaixa lamborghini lamer lancaster lancia land landrover lanxess lasalle lat latino latrobe law lawyer lb lc lds lease leclerc lefrak legal lego lexus lgbt li lidl life lifeinsurance lifestyle lighting like lilly limited limo lincoln linde link lipsy live living lixil lk llc llp loan loans locker locus loft lol london lotte lotto love lpl lplfinancial lr ls lt ltd ltda lu lundbeck luxe luxury lv ly ma macys madrid maif maison makeup man management mango map market marketing markets marriott marshalls maserati mattel mba mc mckinsey md me med media meet melbourne meme memorial men menu merckmsd mg mh miami microsoft mil mini mint mit mitsubishi mk ml mlb mls mm mma mn mo mobi mobile moda moe moi mom monash money monster mormon mortgage moscow moto motorcycles mov movie mp mq mr ms msd mt mtn mtr mu museum mutual mv mw mx my mz na nab nagoya name nationwide natura navy nba nc ne nec net netbank netflix network neustar new news next nextdirect nexus nf nfl ng ngo nhk ni nico nike nikon ninja nissan nissay nl no nokia northwesternmutual norton now nowruz nowtv np nr nra nrw ntt nu nyc nz obi observer off office okinawa olayan olayangroup oldnavy ollo om omega one ong onl online onyourside ooo open oracle orange org organic origins osaka otsuka ott ovh pa page panasonic paris pars partners parts party passagens pay pccw pe pet pf pfizer pg ph pharmacy phd philips phone photo photography photos physio pics pictet pictures pid pin ping pink pioneer pizza pk pl place play playstation plumbing plus pm pn pnc pohl poker politie porn post pr pramerica praxi press prime pro prod productions prof progressive promo properties property protection pru prudential ps pt pub pw pwc py qa qpon quebec quest qvc racing radio raid re read realestate realtor realty recipes red redstone redumbrella rehab reise reisen reit reliance ren rent rentals repair report republican rest restaurant review reviews rexroth rich richardli ricoh ril rio rip rmit ro rocher rocks rodeo rogers room rs rsvp ru rugby ruhr run rw rwe ryukyu sa saarland safe safety sakura sale salon samsclub samsung sandvik sandvikcoromant sanofi sap sarl sas save saxo sb sbi sbs sc sca scb schaeffler schmidt scholarships school schule schwarz science scjohnson scot sd se search seat secure security seek select sener services ses seven sew sex sexy sfr sg sh shangrila sharp shaw shell shia shiksha shoes shop shopping shouji show showtime si silk sina singles site sj sk ski skin sky skype sl sling sm smart smile sn sncf so soccer social softbank software sohu solar solutions song sony soy spa space sport spot spreadbetting sr srl ss st stada staples star statebank statefarm stc stcgroup stockholm storage store stream studio study style su sucks supplies supply support surf surgery suzuki sv swatch swiftcover swiss sx sy sydney systems sz tab taipei talk taobao target tatamotors tatar tattoo tax taxi tc tci td tdk team tech technology tel temasek tennis teva tf tg th thd theater theatre tiaa tickets tienda tiffany tips tires tirol tj tjmaxx tjx tk tkmaxx tl tm tmall tn to today tokyo tools top toray toshiba total tours town toyota toys tr trade trading training travel travelchannel travelers travelersinsurance trust trv tt tube tui tunes tushu tv tvs tw tz ua ubank ubs ug uk unicom university uno uol ups us uy uz va vacations vana vanguard vc ve vegas ventures verisign versicherung vet vg vi viajes video vig viking villas vin vip virgin visa vision viva vivo vlaanderen vn vodka volkswagen volvo vote voting voto voyage vu vuelos wales walmart walter wang wanggou watch watches weather weatherchannel webcam weber website wed wedding weibo weir wf whoswho wien wiki williamhill win windows wine winners wme wolterskluwer woodside work works world wow ws wtc wtf xbox xerox xfinity xihuan xin xxx xyz yachts yahoo yamaxun yandex ye yodobashi yoga yokohama you youtube yt yun za zappos zara zero zip zm zone zuerich zw verm\xF6gensberater-ctb verm\xF6gensberatung-pwb \u03B5\u03BB \u03B5\u03C5 \u0431\u0433 \u0431\u0435\u043B \u0434\u0435\u0442\u0438 \u0435\u044E \u043A\u0430\u0442\u043E\u043B\u0438\u043A \u043A\u043E\u043C \u049B\u0430\u0437 \u043C\u043A\u0434 \u043C\u043E\u043D \u043C\u043E\u0441\u043A\u0432\u0430 \u043E\u043D\u043B\u0430\u0439\u043D \u043E\u0440\u0433 \u0440\u0443\u0441 \u0440\u0444 \u0441\u0430\u0439\u0442 \u0441\u0440\u0431 \u0443\u043A\u0440 \u10D2\u10D4 \u0570\u0561\u0575 \u05D9\u05E9\u05E8\u05D0\u05DC \u05E7\u05D5\u05DD \u0627\u0628\u0648\u0638\u0628\u064A \u0627\u062A\u0635\u0627\u0644\u0627\u062A \u0627\u0631\u0627\u0645\u0643\u0648 \u0627\u0644\u0627\u0631\u062F\u0646 \u0627\u0644\u0628\u062D\u0631\u064A\u0646 \u0627\u0644\u062C\u0632\u0627\u0626\u0631 \u0627\u0644\u0633\u0639\u0648\u062F\u064A\u0629 \u0627\u0644\u0639\u0644\u064A\u0627\u0646 \u0627\u0644\u0645\u063A\u0631\u0628 \u0627\u0645\u0627\u0631\u0627\u062A \u0627\u06CC\u0631\u0627\u0646 \u0628\u0627\u0631\u062A \u0628\u0627\u0632\u0627\u0631 \u0628\u06BE\u0627\u0631\u062A \u0628\u064A\u062A\u0643 \u067E\u0627\u06A9\u0633\u062A\u0627\u0646 \u0680\u0627\u0631\u062A \u062A\u0648\u0646\u0633 \u0633\u0648\u062F\u0627\u0646 \u0633\u0648\u0631\u064A\u0629 \u0634\u0628\u0643\u0629 \u0639\u0631\u0627\u0642 \u0639\u0631\u0628 \u0639\u0645\u0627\u0646 \u0641\u0644\u0633\u0637\u064A\u0646 \u0642\u0637\u0631 \u0643\u0627\u062B\u0648\u0644\u064A\u0643 \u0643\u0648\u0645 \u0645\u0635\u0631 \u0645\u0644\u064A\u0633\u064A\u0627 \u0645\u0648\u0631\u064A\u062A\u0627\u0646\u064A\u0627 \u0645\u0648\u0642\u0639 \u0647\u0645\u0631\u0627\u0647 \u0915\u0949\u092E \u0928\u0947\u091F \u092D\u093E\u0930\u0924 \u092D\u093E\u0930\u0924\u092E\u094D \u092D\u093E\u0930\u094B\u0924 \u0938\u0902\u0917\u0920\u0928 \u09AC\u09BE\u0982\u09B2\u09BE \u09AD\u09BE\u09B0\u09A4 \u09AD\u09BE\u09F0\u09A4 \u0A2D\u0A3E\u0A30\u0A24 \u0AAD\u0ABE\u0AB0\u0AA4 \u0B2D\u0B3E\u0B30\u0B24 \u0B87\u0BA8\u0BCD\u0BA4\u0BBF\u0BAF\u0BBE \u0B87\u0BB2\u0B99\u0BCD\u0B95\u0BC8 \u0B9A\u0BBF\u0B99\u0BCD\u0B95\u0BAA\u0BCD\u0BAA\u0BC2\u0BB0\u0BCD \u0C2D\u0C3E\u0C30\u0C24\u0C4D \u0CAD\u0CBE\u0CB0\u0CA4 \u0D2D\u0D3E\u0D30\u0D24\u0D02 \u0DBD\u0D82\u0D9A\u0DCF \u0E04\u0E2D\u0E21 \u0E44\u0E17\u0E22 \u0EA5\u0EB2\u0EA7 \uB2F7\uB137 \uB2F7\uCEF4 \uC0BC\uC131 \uD55C\uAD6D \u30A2\u30DE\u30BE\u30F3 \u30B0\u30FC\u30B0\u30EB \u30AF\u30E9\u30A6\u30C9 \u30B3\u30E0 \u30B9\u30C8\u30A2 \u30BB\u30FC\u30EB \u30D5\u30A1\u30C3\u30B7\u30E7\u30F3 \u30DD\u30A4\u30F3\u30C8 \u307F\u3093\u306A \u4E16\u754C \u4E2D\u4FE1 \u4E2D\u56FD \u4E2D\u570B \u4E2D\u6587\u7F51 \u4E9A\u9A6C\u900A \u4F01\u4E1A \u4F5B\u5C71 \u4FE1\u606F \u5065\u5EB7 \u516B\u5366 \u516C\u53F8 \u516C\u76CA \u53F0\u6E7E \u53F0\u7063 \u5546\u57CE \u5546\u5E97 \u5546\u6807 \u5609\u91CC \u5609\u91CC\u5927\u9152\u5E97 \u5728\u7EBF \u5927\u4F17\u6C7D\u8F66 \u5927\u62FF \u5929\u4E3B\u6559 \u5A31\u4E50 \u5BB6\u96FB \u5E7F\u4E1C \u5FAE\u535A \u6148\u5584 \u6211\u7231\u4F60 \u624B\u673A \u62DB\u8058 \u653F\u52A1 \u653F\u5E9C \u65B0\u52A0\u5761 \u65B0\u95FB \u65F6\u5C1A \u66F8\u7C4D \u673A\u6784 \u6DE1\u9A6C\u9521 \u6E38\u620F \u6FB3\u9580 \u70B9\u770B \u79FB\u52A8 \u7EC4\u7EC7\u673A\u6784 \u7F51\u5740 \u7F51\u5E97 \u7F51\u7AD9 \u7F51\u7EDC \u8054\u901A \u8BFA\u57FA\u4E9A \u8C37\u6B4C \u8D2D\u7269 \u901A\u8CA9 \u96C6\u56E2 \u96FB\u8A0A\u76C8\u79D1 \u98DE\u5229\u6D66 \u98DF\u54C1 \u9910\u5385 \u9999\u683C\u91CC\u62C9 \u9999\u6E2F".split(" ");
  var LETTER = /(?:[A-Za-z\xAA\xB5\xBA\xC0-\xD6\xD8-\xF6\xF8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u037F\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u052F\u0531-\u0556\u0559\u0560-\u0588\u05D0-\u05EA\u05EF-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u0860-\u086A\u0870-\u0887\u0889-\u088E\u08A0-\u08C9\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0980\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u09FC\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0AF9\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C39\u0C3D\u0C58-\u0C5A\u0C5D\u0C60\u0C61\u0C80\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D04-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D54-\u0D56\u0D5F-\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E86-\u0E8A\u0E8C-\u0EA3\u0EA5\u0EA7-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F5\u13F8-\u13FD\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u16F1-\u16F8\u1700-\u1711\u171F-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1878\u1880-\u1884\u1887-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191E\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19B0-\u19C9\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4C\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1C80-\u1C88\u1C90-\u1CBA\u1CBD-\u1CBF\u1CE9-\u1CEC\u1CEE-\u1CF3\u1CF5\u1CF6\u1CFA\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312F\u3131-\u318E\u31A0-\u31BF\u31F0-\u31FF\u3400-\u4DBF\u4E00-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA69D\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA7CA\uA7D0\uA7D1\uA7D3\uA7D5-\uA7D9\uA7F2-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA8FD\uA8FE\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uA9E0-\uA9E4\uA9E6-\uA9EF\uA9FA-\uA9FE\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA7E-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uAB30-\uAB5A\uAB5C-\uAB69\uAB70-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC]|\uD800[\uDC00-\uDC0B\uDC0D-\uDC26\uDC28-\uDC3A\uDC3C\uDC3D\uDC3F-\uDC4D\uDC50-\uDC5D\uDC80-\uDCFA\uDE80-\uDE9C\uDEA0-\uDED0\uDF00-\uDF1F\uDF2D-\uDF40\uDF42-\uDF49\uDF50-\uDF75\uDF80-\uDF9D\uDFA0-\uDFC3\uDFC8-\uDFCF]|\uD801[\uDC00-\uDC9D\uDCB0-\uDCD3\uDCD8-\uDCFB\uDD00-\uDD27\uDD30-\uDD63\uDD70-\uDD7A\uDD7C-\uDD8A\uDD8C-\uDD92\uDD94\uDD95\uDD97-\uDDA1\uDDA3-\uDDB1\uDDB3-\uDDB9\uDDBB\uDDBC\uDE00-\uDF36\uDF40-\uDF55\uDF60-\uDF67\uDF80-\uDF85\uDF87-\uDFB0\uDFB2-\uDFBA]|\uD802[\uDC00-\uDC05\uDC08\uDC0A-\uDC35\uDC37\uDC38\uDC3C\uDC3F-\uDC55\uDC60-\uDC76\uDC80-\uDC9E\uDCE0-\uDCF2\uDCF4\uDCF5\uDD00-\uDD15\uDD20-\uDD39\uDD80-\uDDB7\uDDBE\uDDBF\uDE00\uDE10-\uDE13\uDE15-\uDE17\uDE19-\uDE35\uDE60-\uDE7C\uDE80-\uDE9C\uDEC0-\uDEC7\uDEC9-\uDEE4\uDF00-\uDF35\uDF40-\uDF55\uDF60-\uDF72\uDF80-\uDF91]|\uD803[\uDC00-\uDC48\uDC80-\uDCB2\uDCC0-\uDCF2\uDD00-\uDD23\uDE80-\uDEA9\uDEB0\uDEB1\uDF00-\uDF1C\uDF27\uDF30-\uDF45\uDF70-\uDF81\uDFB0-\uDFC4\uDFE0-\uDFF6]|\uD804[\uDC03-\uDC37\uDC71\uDC72\uDC75\uDC83-\uDCAF\uDCD0-\uDCE8\uDD03-\uDD26\uDD44\uDD47\uDD50-\uDD72\uDD76\uDD83-\uDDB2\uDDC1-\uDDC4\uDDDA\uDDDC\uDE00-\uDE11\uDE13-\uDE2B\uDE80-\uDE86\uDE88\uDE8A-\uDE8D\uDE8F-\uDE9D\uDE9F-\uDEA8\uDEB0-\uDEDE\uDF05-\uDF0C\uDF0F\uDF10\uDF13-\uDF28\uDF2A-\uDF30\uDF32\uDF33\uDF35-\uDF39\uDF3D\uDF50\uDF5D-\uDF61]|\uD805[\uDC00-\uDC34\uDC47-\uDC4A\uDC5F-\uDC61\uDC80-\uDCAF\uDCC4\uDCC5\uDCC7\uDD80-\uDDAE\uDDD8-\uDDDB\uDE00-\uDE2F\uDE44\uDE80-\uDEAA\uDEB8\uDF00-\uDF1A\uDF40-\uDF46]|\uD806[\uDC00-\uDC2B\uDCA0-\uDCDF\uDCFF-\uDD06\uDD09\uDD0C-\uDD13\uDD15\uDD16\uDD18-\uDD2F\uDD3F\uDD41\uDDA0-\uDDA7\uDDAA-\uDDD0\uDDE1\uDDE3\uDE00\uDE0B-\uDE32\uDE3A\uDE50\uDE5C-\uDE89\uDE9D\uDEB0-\uDEF8]|\uD807[\uDC00-\uDC08\uDC0A-\uDC2E\uDC40\uDC72-\uDC8F\uDD00-\uDD06\uDD08\uDD09\uDD0B-\uDD30\uDD46\uDD60-\uDD65\uDD67\uDD68\uDD6A-\uDD89\uDD98\uDEE0-\uDEF2\uDFB0]|\uD808[\uDC00-\uDF99]|\uD809[\uDC80-\uDD43]|\uD80B[\uDF90-\uDFF0]|[\uD80C\uD81C-\uD820\uD822\uD840-\uD868\uD86A-\uD86C\uD86F-\uD872\uD874-\uD879\uD880-\uD883][\uDC00-\uDFFF]|\uD80D[\uDC00-\uDC2E]|\uD811[\uDC00-\uDE46]|\uD81A[\uDC00-\uDE38\uDE40-\uDE5E\uDE70-\uDEBE\uDED0-\uDEED\uDF00-\uDF2F\uDF40-\uDF43\uDF63-\uDF77\uDF7D-\uDF8F]|\uD81B[\uDE40-\uDE7F\uDF00-\uDF4A\uDF50\uDF93-\uDF9F\uDFE0\uDFE1\uDFE3]|\uD821[\uDC00-\uDFF7]|\uD823[\uDC00-\uDCD5\uDD00-\uDD08]|\uD82B[\uDFF0-\uDFF3\uDFF5-\uDFFB\uDFFD\uDFFE]|\uD82C[\uDC00-\uDD22\uDD50-\uDD52\uDD64-\uDD67\uDD70-\uDEFB]|\uD82F[\uDC00-\uDC6A\uDC70-\uDC7C\uDC80-\uDC88\uDC90-\uDC99]|\uD835[\uDC00-\uDC54\uDC56-\uDC9C\uDC9E\uDC9F\uDCA2\uDCA5\uDCA6\uDCA9-\uDCAC\uDCAE-\uDCB9\uDCBB\uDCBD-\uDCC3\uDCC5-\uDD05\uDD07-\uDD0A\uDD0D-\uDD14\uDD16-\uDD1C\uDD1E-\uDD39\uDD3B-\uDD3E\uDD40-\uDD44\uDD46\uDD4A-\uDD50\uDD52-\uDEA5\uDEA8-\uDEC0\uDEC2-\uDEDA\uDEDC-\uDEFA\uDEFC-\uDF14\uDF16-\uDF34\uDF36-\uDF4E\uDF50-\uDF6E\uDF70-\uDF88\uDF8A-\uDFA8\uDFAA-\uDFC2\uDFC4-\uDFCB]|\uD837[\uDF00-\uDF1E]|\uD838[\uDD00-\uDD2C\uDD37-\uDD3D\uDD4E\uDE90-\uDEAD\uDEC0-\uDEEB]|\uD839[\uDFE0-\uDFE6\uDFE8-\uDFEB\uDFED\uDFEE\uDFF0-\uDFFE]|\uD83A[\uDC00-\uDCC4\uDD00-\uDD43\uDD4B]|\uD83B[\uDE00-\uDE03\uDE05-\uDE1F\uDE21\uDE22\uDE24\uDE27\uDE29-\uDE32\uDE34-\uDE37\uDE39\uDE3B\uDE42\uDE47\uDE49\uDE4B\uDE4D-\uDE4F\uDE51\uDE52\uDE54\uDE57\uDE59\uDE5B\uDE5D\uDE5F\uDE61\uDE62\uDE64\uDE67-\uDE6A\uDE6C-\uDE72\uDE74-\uDE77\uDE79-\uDE7C\uDE7E\uDE80-\uDE89\uDE8B-\uDE9B\uDEA1-\uDEA3\uDEA5-\uDEA9\uDEAB-\uDEBB]|\uD869[\uDC00-\uDEDF\uDF00-\uDFFF]|\uD86D[\uDC00-\uDF38\uDF40-\uDFFF]|\uD86E[\uDC00-\uDC1D\uDC20-\uDFFF]|\uD873[\uDC00-\uDEA1\uDEB0-\uDFFF]|\uD87A[\uDC00-\uDFE0]|\uD87E[\uDC00-\uDE1D]|\uD884[\uDC00-\uDF4A])/;
  var EMOJI = /(?:[#\*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26A7\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDED5-\uDED7\uDEDD-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEFC\uDFE0-\uDFEB\uDFF0]|\uD83E[\uDD0C-\uDD3A\uDD3C-\uDD45\uDD47-\uDDFF\uDE70-\uDE74\uDE78-\uDE7C\uDE80-\uDE86\uDE90-\uDEAC\uDEB0-\uDEBA\uDEC0-\uDEC5\uDED0-\uDED9\uDEE0-\uDEE7\uDEF0-\uDEF6])/;
  var EMOJI_VARIATION = /\uFE0F/;
  var DIGIT = /\d/;
  var SPACE = /\s/;
  function init$2() {
    var customProtocols = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : [];
    var S_START = makeState();
    var S_NUM = makeAcceptingState(NUM);
    var S_DOMAIN = makeAcceptingState(DOMAIN);
    var S_DOMAIN_HYPHEN = makeState();
    var S_WS = makeAcceptingState(WS);
    var DOMAIN_REGEX_TRANSITIONS = [[DIGIT, S_DOMAIN], [LETTER, S_DOMAIN], [EMOJI, S_DOMAIN], [EMOJI_VARIATION, S_DOMAIN]];
    var makeDomainState = function makeDomainState2() {
      var state = makeAcceptingState(DOMAIN);
      state.j = {
        "-": S_DOMAIN_HYPHEN
      };
      state.jr = [].concat(DOMAIN_REGEX_TRANSITIONS);
      return state;
    };
    var makeNearDomainState = function makeNearDomainState2(token) {
      var state = makeDomainState();
      state.t = token;
      return state;
    };
    makeBatchT(S_START, [["'", makeAcceptingState(APOSTROPHE)], ["{", makeAcceptingState(OPENBRACE)], ["[", makeAcceptingState(OPENBRACKET)], ["<", makeAcceptingState(OPENANGLEBRACKET)], ["(", makeAcceptingState(OPENPAREN)], ["}", makeAcceptingState(CLOSEBRACE)], ["]", makeAcceptingState(CLOSEBRACKET)], [">", makeAcceptingState(CLOSEANGLEBRACKET)], [")", makeAcceptingState(CLOSEPAREN)], ["&", makeAcceptingState(AMPERSAND)], ["*", makeAcceptingState(ASTERISK)], ["@", makeAcceptingState(AT)], ["`", makeAcceptingState(BACKTICK)], ["^", makeAcceptingState(CARET)], [":", makeAcceptingState(COLON)], [",", makeAcceptingState(COMMA)], ["$", makeAcceptingState(DOLLAR)], [".", makeAcceptingState(DOT)], ["=", makeAcceptingState(EQUALS)], ["!", makeAcceptingState(EXCLAMATION)], ["-", makeAcceptingState(HYPHEN)], ["%", makeAcceptingState(PERCENT)], ["|", makeAcceptingState(PIPE)], ["+", makeAcceptingState(PLUS)], ["#", makeAcceptingState(POUND)], ["?", makeAcceptingState(QUERY)], ['"', makeAcceptingState(QUOTE)], ["/", makeAcceptingState(SLASH)], [";", makeAcceptingState(SEMI)], ["~", makeAcceptingState(TILDE)], ["_", makeAcceptingState(UNDERSCORE)], ["\\", makeAcceptingState(BACKSLASH)]]);
    makeT(S_START, "\n", makeAcceptingState(NL));
    makeRegexT(S_START, SPACE, S_WS);
    makeT(S_WS, "\n", makeState());
    makeRegexT(S_WS, SPACE, S_WS);
    for (var i2 = 0; i2 < tlds.length; i2++) {
      makeChainT(S_START, tlds[i2], makeNearDomainState(TLD), makeDomainState);
    }
    var S_PROTOCOL_FILE = makeDomainState();
    var S_PROTOCOL_FTP = makeDomainState();
    var S_PROTOCOL_HTTP = makeDomainState();
    var S_MAILTO = makeDomainState();
    makeChainT(S_START, "file", S_PROTOCOL_FILE, makeDomainState);
    makeChainT(S_START, "ftp", S_PROTOCOL_FTP, makeDomainState);
    makeChainT(S_START, "http", S_PROTOCOL_HTTP, makeDomainState);
    makeChainT(S_START, "mailto", S_MAILTO, makeDomainState);
    var S_PROTOCOL_SECURE = makeDomainState();
    var S_FULL_PROTOCOL = makeAcceptingState(PROTOCOL);
    var S_FULL_MAILTO = makeAcceptingState(MAILTO);
    makeT(S_PROTOCOL_FTP, "s", S_PROTOCOL_SECURE);
    makeT(S_PROTOCOL_FTP, ":", S_FULL_PROTOCOL);
    makeT(S_PROTOCOL_HTTP, "s", S_PROTOCOL_SECURE);
    makeT(S_PROTOCOL_HTTP, ":", S_FULL_PROTOCOL);
    makeT(S_PROTOCOL_FILE, ":", S_FULL_PROTOCOL);
    makeT(S_PROTOCOL_SECURE, ":", S_FULL_PROTOCOL);
    makeT(S_MAILTO, ":", S_FULL_MAILTO);
    var S_CUSTOM_PROTOCOL = makeDomainState();
    for (var _i = 0; _i < customProtocols.length; _i++) {
      makeChainT(S_START, customProtocols[_i], S_CUSTOM_PROTOCOL, makeDomainState);
    }
    makeT(S_CUSTOM_PROTOCOL, ":", S_FULL_PROTOCOL);
    makeChainT(S_START, "localhost", makeNearDomainState(LOCALHOST), makeDomainState);
    makeRegexT(S_START, DIGIT, S_NUM);
    makeRegexT(S_START, LETTER, S_DOMAIN);
    makeRegexT(S_START, EMOJI, S_DOMAIN);
    makeRegexT(S_START, EMOJI_VARIATION, S_DOMAIN);
    makeRegexT(S_NUM, DIGIT, S_NUM);
    makeRegexT(S_NUM, LETTER, S_DOMAIN);
    makeRegexT(S_NUM, EMOJI, S_DOMAIN);
    makeRegexT(S_NUM, EMOJI_VARIATION, S_DOMAIN);
    makeT(S_NUM, "-", S_DOMAIN_HYPHEN);
    makeT(S_DOMAIN, "-", S_DOMAIN_HYPHEN);
    makeT(S_DOMAIN_HYPHEN, "-", S_DOMAIN_HYPHEN);
    makeRegexT(S_DOMAIN, DIGIT, S_DOMAIN);
    makeRegexT(S_DOMAIN, LETTER, S_DOMAIN);
    makeRegexT(S_DOMAIN, EMOJI, S_DOMAIN);
    makeRegexT(S_DOMAIN, EMOJI_VARIATION, S_DOMAIN);
    makeRegexT(S_DOMAIN_HYPHEN, DIGIT, S_DOMAIN);
    makeRegexT(S_DOMAIN_HYPHEN, LETTER, S_DOMAIN);
    makeRegexT(S_DOMAIN_HYPHEN, EMOJI, S_DOMAIN);
    makeRegexT(S_DOMAIN_HYPHEN, EMOJI_VARIATION, S_DOMAIN);
    S_START.jd = makeAcceptingState(SYM);
    return S_START;
  }
  function run$1(start, str) {
    var iterable = stringToArray(str.replace(/[A-Z]/g, function(c2) {
      return c2.toLowerCase();
    }));
    var charCount = iterable.length;
    var tokens = [];
    var cursor = 0;
    var charCursor = 0;
    while (charCursor < charCount) {
      var state = start;
      var nextState = null;
      var tokenLength = 0;
      var latestAccepting = null;
      var sinceAccepts = -1;
      var charsSinceAccepts = -1;
      while (charCursor < charCount && (nextState = takeT(state, iterable[charCursor]))) {
        state = nextState;
        if (state.accepts()) {
          sinceAccepts = 0;
          charsSinceAccepts = 0;
          latestAccepting = state;
        } else if (sinceAccepts >= 0) {
          sinceAccepts += iterable[charCursor].length;
          charsSinceAccepts++;
        }
        tokenLength += iterable[charCursor].length;
        cursor += iterable[charCursor].length;
        charCursor++;
      }
      cursor -= sinceAccepts;
      charCursor -= charsSinceAccepts;
      tokenLength -= sinceAccepts;
      tokens.push({
        t: latestAccepting.t,
        v: str.substr(cursor - tokenLength, tokenLength),
        s: cursor - tokenLength,
        e: cursor
      });
    }
    return tokens;
  }
  function stringToArray(str) {
    var result = [];
    var len = str.length;
    var index = 0;
    while (index < len) {
      var first = str.charCodeAt(index);
      var second = void 0;
      var char = first < 55296 || first > 56319 || index + 1 === len || (second = str.charCodeAt(index + 1)) < 56320 || second > 57343 ? str[index] : str.slice(index, index + 2);
      result.push(char);
      index += char.length;
    }
    return result;
  }
  function _typeof2(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof2 = function(obj2) {
        return typeof obj2;
      };
    } else {
      _typeof2 = function(obj2) {
        return obj2 && typeof Symbol === "function" && obj2.constructor === Symbol && obj2 !== Symbol.prototype ? "symbol" : typeof obj2;
      };
    }
    return _typeof2(obj);
  }
  var defaults = {
    defaultProtocol: "http",
    events: null,
    format: noop3,
    formatHref: noop3,
    nl2br: false,
    tagName: "a",
    target: null,
    rel: null,
    validate: true,
    truncate: 0,
    className: null,
    attributes: null,
    ignoreTags: []
  };
  function Options(opts) {
    opts = opts || {};
    this.defaultProtocol = "defaultProtocol" in opts ? opts.defaultProtocol : defaults.defaultProtocol;
    this.events = "events" in opts ? opts.events : defaults.events;
    this.format = "format" in opts ? opts.format : defaults.format;
    this.formatHref = "formatHref" in opts ? opts.formatHref : defaults.formatHref;
    this.nl2br = "nl2br" in opts ? opts.nl2br : defaults.nl2br;
    this.tagName = "tagName" in opts ? opts.tagName : defaults.tagName;
    this.target = "target" in opts ? opts.target : defaults.target;
    this.rel = "rel" in opts ? opts.rel : defaults.rel;
    this.validate = "validate" in opts ? opts.validate : defaults.validate;
    this.truncate = "truncate" in opts ? opts.truncate : defaults.truncate;
    this.className = "className" in opts ? opts.className : defaults.className;
    this.attributes = opts.attributes || defaults.attributes;
    this.ignoreTags = [];
    var ignoredTags = "ignoreTags" in opts ? opts.ignoreTags : defaults.ignoreTags;
    for (var i2 = 0; i2 < ignoredTags.length; i2++) {
      this.ignoreTags.push(ignoredTags[i2].toUpperCase());
    }
  }
  Options.prototype = {
    resolve: function resolve(token) {
      var href = token.toHref(this.defaultProtocol);
      return {
        formatted: this.get("format", token.toString(), token),
        formattedHref: this.get("formatHref", href, token),
        tagName: this.get("tagName", href, token),
        className: this.get("className", href, token),
        target: this.get("target", href, token),
        rel: this.get("rel", href, token),
        events: this.getObject("events", href, token),
        attributes: this.getObject("attributes", href, token),
        truncate: this.get("truncate", href, token)
      };
    },
    check: function check(token) {
      return this.get("validate", token.toString(), token);
    },
    get: function get(key, operator, token) {
      var option = this[key];
      if (!option) {
        return option;
      }
      var optionValue;
      switch (_typeof2(option)) {
        case "function":
          return option(operator, token.t);
        case "object":
          optionValue = token.t in option ? option[token.t] : defaults[key];
          return typeof optionValue === "function" ? optionValue(operator, token.t) : optionValue;
      }
      return option;
    },
    getObject: function getObject(key, operator, token) {
      var option = this[key];
      return typeof option === "function" ? option(operator, token.t) : option;
    }
  };
  function noop3(val) {
    return val;
  }
  var options = /* @__PURE__ */ Object.freeze({
    __proto__: null,
    defaults,
    Options
  });
  function inherits(parent, child) {
    var props = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : {};
    var extended = Object.create(parent.prototype);
    for (var p2 in props) {
      extended[p2] = props[p2];
    }
    extended.constructor = child;
    child.prototype = extended;
    return child;
  }
  function MultiToken() {
  }
  MultiToken.prototype = {
    t: "token",
    isLink: false,
    toString: function toString() {
      return this.v;
    },
    toHref: function toHref() {
      return this.toString();
    },
    startIndex: function startIndex() {
      return this.tk[0].s;
    },
    endIndex: function endIndex() {
      return this.tk[this.tk.length - 1].e;
    },
    toObject: function toObject() {
      var protocol = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : defaults.defaultProtocol;
      return {
        type: this.t,
        value: this.v,
        isLink: this.isLink,
        href: this.toHref(protocol),
        start: this.startIndex(),
        end: this.endIndex()
      };
    }
  };
  function createTokenClass(type, props) {
    function Token(value, tokens) {
      this.t = type;
      this.v = value;
      this.tk = tokens;
    }
    inherits(MultiToken, Token, props);
    return Token;
  }
  var MailtoEmail = createTokenClass("email", {
    isLink: true
  });
  var Email = createTokenClass("email", {
    isLink: true,
    toHref: function toHref2() {
      return "mailto:" + this.toString();
    }
  });
  var Text = createTokenClass("text");
  var Nl = createTokenClass("nl");
  var Url = createTokenClass("url", {
    isLink: true,
    toHref: function toHref3() {
      var protocol = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : defaults.defaultProtocol;
      var tokens = this.tk;
      var hasProtocol2 = false;
      var hasSlashSlash = false;
      var result = [];
      var i2 = 0;
      while (tokens[i2].t === PROTOCOL) {
        hasProtocol2 = true;
        result.push(tokens[i2].v);
        i2++;
      }
      while (tokens[i2].t === SLASH) {
        hasSlashSlash = true;
        result.push(tokens[i2].v);
        i2++;
      }
      for (; i2 < tokens.length; i2++) {
        result.push(tokens[i2].v);
      }
      result = result.join("");
      if (!(hasProtocol2 || hasSlashSlash)) {
        result = "".concat(protocol, "://").concat(result);
      }
      return result;
    },
    hasProtocol: function hasProtocol() {
      return this.tk[0].t === PROTOCOL;
    }
  });
  var multi = /* @__PURE__ */ Object.freeze({
    __proto__: null,
    MultiToken,
    Base: MultiToken,
    createTokenClass,
    MailtoEmail,
    Email,
    Text,
    Nl,
    Url
  });
  function init$1() {
    var S_START = makeState();
    var S_PROTOCOL = makeState();
    var S_MAILTO = makeState();
    var S_PROTOCOL_SLASH = makeState();
    var S_PROTOCOL_SLASH_SLASH = makeState();
    var S_DOMAIN = makeState();
    var S_DOMAIN_DOT = makeState();
    var S_TLD = makeAcceptingState(Url);
    var S_TLD_COLON = makeState();
    var S_TLD_PORT = makeAcceptingState(Url);
    var S_URL = makeAcceptingState(Url);
    var S_URL_NON_ACCEPTING = makeState();
    var S_URL_OPENBRACE = makeState();
    var S_URL_OPENBRACKET = makeState();
    var S_URL_OPENANGLEBRACKET = makeState();
    var S_URL_OPENPAREN = makeState();
    var S_URL_OPENBRACE_Q = makeAcceptingState(Url);
    var S_URL_OPENBRACKET_Q = makeAcceptingState(Url);
    var S_URL_OPENANGLEBRACKET_Q = makeAcceptingState(Url);
    var S_URL_OPENPAREN_Q = makeAcceptingState(Url);
    var S_URL_OPENBRACE_SYMS = makeState();
    var S_URL_OPENBRACKET_SYMS = makeState();
    var S_URL_OPENANGLEBRACKET_SYMS = makeState();
    var S_URL_OPENPAREN_SYMS = makeState();
    var S_EMAIL_DOMAIN = makeState();
    var S_EMAIL_DOMAIN_DOT = makeState();
    var S_EMAIL = makeAcceptingState(Email);
    var S_EMAIL_COLON = makeState();
    var S_EMAIL_PORT = makeAcceptingState(Email);
    var S_MAILTO_EMAIL = makeAcceptingState(MailtoEmail);
    var S_MAILTO_EMAIL_NON_ACCEPTING = makeState();
    var S_LOCALPART = makeState();
    var S_LOCALPART_AT = makeState();
    var S_LOCALPART_DOT = makeState();
    var S_NL = makeAcceptingState(Nl);
    makeT(S_START, NL, S_NL);
    makeT(S_START, PROTOCOL, S_PROTOCOL);
    makeT(S_START, MAILTO, S_MAILTO);
    makeT(S_PROTOCOL, SLASH, S_PROTOCOL_SLASH);
    makeT(S_PROTOCOL_SLASH, SLASH, S_PROTOCOL_SLASH_SLASH);
    makeT(S_START, TLD, S_DOMAIN);
    makeT(S_START, DOMAIN, S_DOMAIN);
    makeT(S_START, LOCALHOST, S_TLD);
    makeT(S_START, NUM, S_DOMAIN);
    makeT(S_PROTOCOL_SLASH_SLASH, TLD, S_URL);
    makeT(S_PROTOCOL_SLASH_SLASH, DOMAIN, S_URL);
    makeT(S_PROTOCOL_SLASH_SLASH, NUM, S_URL);
    makeT(S_PROTOCOL_SLASH_SLASH, LOCALHOST, S_URL);
    makeT(S_DOMAIN, DOT, S_DOMAIN_DOT);
    makeT(S_EMAIL_DOMAIN, DOT, S_EMAIL_DOMAIN_DOT);
    makeT(S_DOMAIN_DOT, TLD, S_TLD);
    makeT(S_DOMAIN_DOT, DOMAIN, S_DOMAIN);
    makeT(S_DOMAIN_DOT, NUM, S_DOMAIN);
    makeT(S_DOMAIN_DOT, LOCALHOST, S_DOMAIN);
    makeT(S_EMAIL_DOMAIN_DOT, TLD, S_EMAIL);
    makeT(S_EMAIL_DOMAIN_DOT, DOMAIN, S_EMAIL_DOMAIN);
    makeT(S_EMAIL_DOMAIN_DOT, NUM, S_EMAIL_DOMAIN);
    makeT(S_EMAIL_DOMAIN_DOT, LOCALHOST, S_EMAIL_DOMAIN);
    makeT(S_TLD, DOT, S_DOMAIN_DOT);
    makeT(S_EMAIL, DOT, S_EMAIL_DOMAIN_DOT);
    makeT(S_TLD, COLON, S_TLD_COLON);
    makeT(S_TLD, SLASH, S_URL);
    makeT(S_TLD_COLON, NUM, S_TLD_PORT);
    makeT(S_TLD_PORT, SLASH, S_URL);
    makeT(S_EMAIL, COLON, S_EMAIL_COLON);
    makeT(S_EMAIL_COLON, NUM, S_EMAIL_PORT);
    var qsAccepting = [AMPERSAND, ASTERISK, AT, BACKSLASH, BACKTICK, CARET, DOLLAR, DOMAIN, EQUALS, HYPHEN, LOCALHOST, NUM, PERCENT, PIPE, PLUS, POUND, PROTOCOL, SLASH, SYM, TILDE, TLD, UNDERSCORE];
    var qsNonAccepting = [APOSTROPHE, CLOSEANGLEBRACKET, CLOSEBRACE, CLOSEBRACKET, CLOSEPAREN, COLON, COMMA, DOT, EXCLAMATION, OPENANGLEBRACKET, OPENBRACE, OPENBRACKET, OPENPAREN, QUERY, QUOTE, SEMI];
    makeT(S_URL, OPENBRACE, S_URL_OPENBRACE);
    makeT(S_URL, OPENBRACKET, S_URL_OPENBRACKET);
    makeT(S_URL, OPENANGLEBRACKET, S_URL_OPENANGLEBRACKET);
    makeT(S_URL, OPENPAREN, S_URL_OPENPAREN);
    makeT(S_URL_NON_ACCEPTING, OPENBRACE, S_URL_OPENBRACE);
    makeT(S_URL_NON_ACCEPTING, OPENBRACKET, S_URL_OPENBRACKET);
    makeT(S_URL_NON_ACCEPTING, OPENANGLEBRACKET, S_URL_OPENANGLEBRACKET);
    makeT(S_URL_NON_ACCEPTING, OPENPAREN, S_URL_OPENPAREN);
    makeT(S_URL_OPENBRACE, CLOSEBRACE, S_URL);
    makeT(S_URL_OPENBRACKET, CLOSEBRACKET, S_URL);
    makeT(S_URL_OPENANGLEBRACKET, CLOSEANGLEBRACKET, S_URL);
    makeT(S_URL_OPENPAREN, CLOSEPAREN, S_URL);
    makeT(S_URL_OPENBRACE_Q, CLOSEBRACE, S_URL);
    makeT(S_URL_OPENBRACKET_Q, CLOSEBRACKET, S_URL);
    makeT(S_URL_OPENANGLEBRACKET_Q, CLOSEANGLEBRACKET, S_URL);
    makeT(S_URL_OPENPAREN_Q, CLOSEPAREN, S_URL);
    makeT(S_URL_OPENBRACE_SYMS, CLOSEBRACE, S_URL);
    makeT(S_URL_OPENBRACKET_SYMS, CLOSEBRACKET, S_URL);
    makeT(S_URL_OPENANGLEBRACKET_SYMS, CLOSEANGLEBRACKET, S_URL);
    makeT(S_URL_OPENPAREN_SYMS, CLOSEPAREN, S_URL);
    makeMultiT(S_URL_OPENBRACE, qsAccepting, S_URL_OPENBRACE_Q);
    makeMultiT(S_URL_OPENBRACKET, qsAccepting, S_URL_OPENBRACKET_Q);
    makeMultiT(S_URL_OPENANGLEBRACKET, qsAccepting, S_URL_OPENANGLEBRACKET_Q);
    makeMultiT(S_URL_OPENPAREN, qsAccepting, S_URL_OPENPAREN_Q);
    makeMultiT(S_URL_OPENBRACE, qsNonAccepting, S_URL_OPENBRACE_SYMS);
    makeMultiT(S_URL_OPENBRACKET, qsNonAccepting, S_URL_OPENBRACKET_SYMS);
    makeMultiT(S_URL_OPENANGLEBRACKET, qsNonAccepting, S_URL_OPENANGLEBRACKET_SYMS);
    makeMultiT(S_URL_OPENPAREN, qsNonAccepting, S_URL_OPENPAREN_SYMS);
    makeMultiT(S_URL_OPENBRACE_Q, qsAccepting, S_URL_OPENBRACE_Q);
    makeMultiT(S_URL_OPENBRACKET_Q, qsAccepting, S_URL_OPENBRACKET_Q);
    makeMultiT(S_URL_OPENANGLEBRACKET_Q, qsAccepting, S_URL_OPENANGLEBRACKET_Q);
    makeMultiT(S_URL_OPENPAREN_Q, qsAccepting, S_URL_OPENPAREN_Q);
    makeMultiT(S_URL_OPENBRACE_Q, qsNonAccepting, S_URL_OPENBRACE_Q);
    makeMultiT(S_URL_OPENBRACKET_Q, qsNonAccepting, S_URL_OPENBRACKET_Q);
    makeMultiT(S_URL_OPENANGLEBRACKET_Q, qsNonAccepting, S_URL_OPENANGLEBRACKET_Q);
    makeMultiT(S_URL_OPENPAREN_Q, qsNonAccepting, S_URL_OPENPAREN_Q);
    makeMultiT(S_URL_OPENBRACE_SYMS, qsAccepting, S_URL_OPENBRACE_Q);
    makeMultiT(S_URL_OPENBRACKET_SYMS, qsAccepting, S_URL_OPENBRACKET_Q);
    makeMultiT(S_URL_OPENANGLEBRACKET_SYMS, qsAccepting, S_URL_OPENANGLEBRACKET_Q);
    makeMultiT(S_URL_OPENPAREN_SYMS, qsAccepting, S_URL_OPENPAREN_Q);
    makeMultiT(S_URL_OPENBRACE_SYMS, qsNonAccepting, S_URL_OPENBRACE_SYMS);
    makeMultiT(S_URL_OPENBRACKET_SYMS, qsNonAccepting, S_URL_OPENBRACKET_SYMS);
    makeMultiT(S_URL_OPENANGLEBRACKET_SYMS, qsNonAccepting, S_URL_OPENANGLEBRACKET_SYMS);
    makeMultiT(S_URL_OPENPAREN_SYMS, qsNonAccepting, S_URL_OPENPAREN_SYMS);
    makeMultiT(S_URL, qsAccepting, S_URL);
    makeMultiT(S_URL_NON_ACCEPTING, qsAccepting, S_URL);
    makeMultiT(S_URL, qsNonAccepting, S_URL_NON_ACCEPTING);
    makeMultiT(S_URL_NON_ACCEPTING, qsNonAccepting, S_URL_NON_ACCEPTING);
    makeT(S_MAILTO, TLD, S_MAILTO_EMAIL);
    makeT(S_MAILTO, DOMAIN, S_MAILTO_EMAIL);
    makeT(S_MAILTO, NUM, S_MAILTO_EMAIL);
    makeT(S_MAILTO, LOCALHOST, S_MAILTO_EMAIL);
    makeMultiT(S_MAILTO_EMAIL, qsAccepting, S_MAILTO_EMAIL);
    makeMultiT(S_MAILTO_EMAIL, qsNonAccepting, S_MAILTO_EMAIL_NON_ACCEPTING);
    makeMultiT(S_MAILTO_EMAIL_NON_ACCEPTING, qsAccepting, S_MAILTO_EMAIL);
    makeMultiT(S_MAILTO_EMAIL_NON_ACCEPTING, qsNonAccepting, S_MAILTO_EMAIL_NON_ACCEPTING);
    var localpartAccepting = [AMPERSAND, APOSTROPHE, ASTERISK, BACKSLASH, BACKTICK, CARET, CLOSEBRACE, DOLLAR, DOMAIN, EQUALS, HYPHEN, NUM, OPENBRACE, PERCENT, PIPE, PLUS, POUND, QUERY, SLASH, SYM, TILDE, TLD, UNDERSCORE];
    makeMultiT(S_DOMAIN, localpartAccepting, S_LOCALPART);
    makeT(S_DOMAIN, AT, S_LOCALPART_AT);
    makeMultiT(S_TLD, localpartAccepting, S_LOCALPART);
    makeT(S_TLD, AT, S_LOCALPART_AT);
    makeMultiT(S_DOMAIN_DOT, localpartAccepting, S_LOCALPART);
    makeMultiT(S_LOCALPART, localpartAccepting, S_LOCALPART);
    makeT(S_LOCALPART, AT, S_LOCALPART_AT);
    makeT(S_LOCALPART, DOT, S_LOCALPART_DOT);
    makeMultiT(S_LOCALPART_DOT, localpartAccepting, S_LOCALPART);
    makeT(S_LOCALPART_AT, TLD, S_EMAIL_DOMAIN);
    makeT(S_LOCALPART_AT, DOMAIN, S_EMAIL_DOMAIN);
    makeT(S_LOCALPART_AT, NUM, S_EMAIL_DOMAIN);
    makeT(S_LOCALPART_AT, LOCALHOST, S_EMAIL);
    return S_START;
  }
  function run(start, input, tokens) {
    var len = tokens.length;
    var cursor = 0;
    var multis = [];
    var textTokens = [];
    while (cursor < len) {
      var state = start;
      var secondState = null;
      var nextState = null;
      var multiLength = 0;
      var latestAccepting = null;
      var sinceAccepts = -1;
      while (cursor < len && !(secondState = takeT(state, tokens[cursor].t))) {
        textTokens.push(tokens[cursor++]);
      }
      while (cursor < len && (nextState = secondState || takeT(state, tokens[cursor].t))) {
        secondState = null;
        state = nextState;
        if (state.accepts()) {
          sinceAccepts = 0;
          latestAccepting = state;
        } else if (sinceAccepts >= 0) {
          sinceAccepts++;
        }
        cursor++;
        multiLength++;
      }
      if (sinceAccepts < 0) {
        for (var i2 = cursor - multiLength; i2 < cursor; i2++) {
          textTokens.push(tokens[i2]);
        }
      } else {
        if (textTokens.length > 0) {
          multis.push(parserCreateMultiToken(Text, input, textTokens));
          textTokens = [];
        }
        cursor -= sinceAccepts;
        multiLength -= sinceAccepts;
        var Multi3 = latestAccepting.t;
        var subtokens = tokens.slice(cursor - multiLength, cursor);
        multis.push(parserCreateMultiToken(Multi3, input, subtokens));
      }
    }
    if (textTokens.length > 0) {
      multis.push(parserCreateMultiToken(Text, input, textTokens));
    }
    return multis;
  }
  function parserCreateMultiToken(Multi3, input, tokens) {
    var startIdx = tokens[0].s;
    var endIdx = tokens[tokens.length - 1].e;
    var value = input.substr(startIdx, endIdx - startIdx);
    return new Multi3(value, tokens);
  }
  var warn = typeof console !== "undefined" && console && console.warn || function() {
  };
  var INIT = {
    scanner: null,
    parser: null,
    pluginQueue: [],
    customProtocols: [],
    initialized: false
  };
  function reset() {
    INIT.scanner = null;
    INIT.parser = null;
    INIT.pluginQueue = [];
    INIT.customProtocols = [];
    INIT.initialized = false;
  }
  function registerPlugin(name, plugin) {
    for (var i2 = 0; i2 < INIT.pluginQueue.length; i2++) {
      if (name === INIT.pluginQueue[i2][0]) {
        warn('linkifyjs: plugin "'.concat(name, '" already registered - will be overwritten'));
        INIT.pluginQueue[i2] = [name, plugin];
        return;
      }
    }
    INIT.pluginQueue.push([name, plugin]);
    if (INIT.initialized) {
      warn('linkifyjs: already initialized - will not register plugin "'.concat(name, '" until you manually call linkify.init(). To avoid this warning, please register all plugins before invoking linkify the first time.'));
    }
  }
  function registerCustomProtocol(protocol) {
    if (INIT.initialized) {
      warn('linkifyjs: already initialized - will not register custom protocol "'.concat(protocol, '" until you manually call linkify.init(). To avoid this warning, please register all custom protocols before invoking linkify the first time.'));
    }
    if (!/^[a-z-]+$/.test(protocol)) {
      throw Error("linkifyjs: protocols containing characters other than a-z or - (hyphen) are not supported");
    }
    INIT.customProtocols.push(protocol);
  }
  function init() {
    INIT.scanner = {
      start: init$2(INIT.customProtocols),
      tokens: text
    };
    INIT.parser = {
      start: init$1(),
      tokens: multi
    };
    var utils = {
      createTokenClass
    };
    for (var i2 = 0; i2 < INIT.pluginQueue.length; i2++) {
      INIT.pluginQueue[i2][1]({
        scanner: INIT.scanner,
        parser: INIT.parser,
        utils
      });
    }
    INIT.initialized = true;
  }
  function tokenize(str) {
    if (!INIT.initialized) {
      init();
    }
    return run(INIT.parser.start, str, run$1(INIT.scanner.start, str));
  }
  function find(str) {
    var type = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : null;
    var tokens = tokenize(str);
    var filtered = [];
    for (var i2 = 0; i2 < tokens.length; i2++) {
      var token = tokens[i2];
      if (token.isLink && (!type || token.t === type)) {
        filtered.push(token.toObject());
      }
    }
    return filtered;
  }
  function test(str) {
    var type = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : null;
    var tokens = tokenize(str);
    return tokens.length === 1 && tokens[0].isLink && (!type || tokens[0].t === type);
  }

  // gamja/lib/index.js
  var html = htm_module_default.bind(v);

  // gamja/lib/linkify.js
  linkify_module_exports.options.defaults.defaultProtocol = "https";
  linkify_module_exports.registerCustomProtocol("irc");
  linkify_module_exports.registerCustomProtocol("ircs");
  linkify_module_exports.registerPlugin("ircChannel", ({ scanner, parser, utils }) => {
    const { POUND: POUND2, DOMAIN: DOMAIN2, TLD: TLD2, LOCALHOST: LOCALHOST2, UNDERSCORE: UNDERSCORE2, DOT: DOT2, HYPHEN: HYPHEN2 } = scanner.tokens;
    const START_STATE = parser.start;
    const Channel = utils.createTokenClass("ircChannel", {
      isLink: true,
      toHref() {
        return "irc:///" + this.toString();
      }
    });
    const HASH_STATE = START_STATE.tt(POUND2);
    const CHAN_STATE = HASH_STATE.tt(DOMAIN2, Channel);
    HASH_STATE.tt(TLD2, CHAN_STATE);
    HASH_STATE.tt(LOCALHOST2, CHAN_STATE);
    HASH_STATE.tt(POUND2, CHAN_STATE);
    CHAN_STATE.tt(UNDERSCORE2, CHAN_STATE);
    CHAN_STATE.tt(DOMAIN2, CHAN_STATE);
    CHAN_STATE.tt(TLD2, CHAN_STATE);
    CHAN_STATE.tt(LOCALHOST2, CHAN_STATE);
    const CHAN_DIVIDER_STATE = CHAN_STATE.tt(DOT2);
    CHAN_DIVIDER_STATE.tt(UNDERSCORE2, CHAN_STATE);
    CHAN_DIVIDER_STATE.tt(DOMAIN2, CHAN_STATE);
    CHAN_DIVIDER_STATE.tt(TLD2, CHAN_STATE);
    CHAN_DIVIDER_STATE.tt(LOCALHOST2, CHAN_STATE);
  });
  function linkify(text2, onClick) {
    let links = linkify_module_exports.find(text2);
    let children = [];
    let last = 0;
    links.forEach((match) => {
      if (!match.isLink) {
        return;
      }
      const prefix = text2.substring(last, match.start);
      children.push(prefix);
      children.push(html`
			<a
				href=${match.href}
				target="_blank"
				rel="noreferrer noopener"
				onClick=${onClick}
			>${match.value}</a>
		`);
      last = match.end;
    });
    const suffix = text2.substring(last);
    children.push(suffix);
    return children;
  }

  // gamja/lib/ansi.js
  var BOLD = "";
  var ITALIC = "";
  var UNDERLINE = "";
  var STRIKETHROUGH = "";
  var MONOSPACE = "";
  var COLOR = "";
  var COLOR_HEX = "";
  var REVERSE_COLOR = "";
  var RESET = "";
  function isDigit(ch) {
    return ch >= "0" && ch <= "9";
  }
  function strip(text2) {
    let out = "";
    for (let i2 = 0; i2 < text2.length; i2++) {
      let ch = text2[i2];
      switch (ch) {
        case BOLD:
        case ITALIC:
        case UNDERLINE:
        case STRIKETHROUGH:
        case MONOSPACE:
        case REVERSE_COLOR:
        case RESET:
          break;
        case COLOR:
          if (!isDigit(text2[i2 + 1])) {
            break;
          }
          i2++;
          if (isDigit(text2[i2 + 1])) {
            i2++;
          }
          if (text2[i2 + 1] == "," && isDigit(text2[i2 + 2])) {
            i2 += 2;
            if (isDigit(text2[i2 + 1])) {
              i2++;
            }
          }
          break;
        case COLOR_HEX:
          i2 += 6;
          break;
        default:
          out += ch;
      }
    }
    return out;
  }

  // gamja/state.js
  var SERVER_BUFFER = "*";
  var BufferType = {
    SERVER: "server",
    CHANNEL: "channel",
    NICK: "nick"
  };
  var ServerStatus = Client.Status;
  var Unread = {
    NONE: "",
    MESSAGE: "message",
    HIGHLIGHT: "highlight",
    compare(a3, b2) {
      const priority = {
        [Unread.NONE]: 0,
        [Unread.MESSAGE]: 1,
        [Unread.HIGHLIGHT]: 2
      };
      return priority[a3] - priority[b2];
    },
    union(a3, b2) {
      return Unread.compare(a3, b2) > 0 ? a3 : b2;
    }
  };
  var ReceiptType = {
    DELIVERED: "delivered",
    READ: "read"
  };
  function getNickURL(nick) {
    return "irc:///" + encodeURIComponent(nick) + ",isuser";
  }
  function getChannelURL(channel) {
    return "irc:///" + encodeURIComponent(channel);
  }
  function getBufferURL(buf) {
    switch (buf.type) {
      case BufferType.SERVER:
        return "irc:///";
      case BufferType.CHANNEL:
        return getChannelURL(buf.name);
      case BufferType.NICK:
        return getNickURL(buf.name);
    }
    throw new Error("Unknown buffer type: " + buf.type);
  }
  function getMessageURL(buf, msg) {
    let bufURL = getBufferURL(buf);
    if (msg.tags.msgid) {
      return bufURL + "?msgid=" + encodeURIComponent(msg.tags.msgid);
    } else {
      return bufURL + "?timestamp=" + encodeURIComponent(msg.tags.time);
    }
  }
  function getServerName(server, bouncerNetwork) {
    let netName = server.name;
    if (bouncerNetwork && bouncerNetwork.name && bouncerNetwork.name !== bouncerNetwork.host) {
      return bouncerNetwork.name;
    }
    if (netName) {
      return netName;
    }
    if (bouncerNetwork) {
      return bouncerNetwork.name || bouncerNetwork.host || "server";
    } else if (server.isBouncer) {
      return "bouncer";
    } else {
      return "server";
    }
  }
  function receiptFromMessage(msg) {
    if (!msg.tags.time) {
      throw new Error("Missing time message tag");
    }
    return { time: msg.tags.time };
  }
  function isReceiptBefore(a3, b2) {
    if (!b2) {
      return false;
    }
    if (!a3) {
      return true;
    }
    if (!a3.time || !b2.time) {
      throw new Error("Missing receipt time");
    }
    return a3.time <= b2.time;
  }
  function isMessageBeforeReceipt(msg, receipt) {
    if (!receipt) {
      return false;
    }
    if (!msg.tags.time) {
      throw new Error("Missing time message tag");
    }
    if (!receipt.time) {
      throw new Error("Missing receipt time");
    }
    return msg.tags.time <= receipt.time;
  }
  function updateState3(state, updater) {
    let updated;
    if (typeof updater === "function") {
      updated = updater(state, state);
    } else {
      updated = updater;
    }
    if (state === updated || !updated) {
      return;
    }
    return __spreadValues(__spreadValues({}, state), updated);
  }
  function isServerBuffer(buf) {
    return buf.type == BufferType.SERVER;
  }
  function compareBuffers(a3, b2) {
    if (a3.server != b2.server) {
      return a3.server > b2.server ? 1 : -1;
    }
    if (isServerBuffer(a3) != isServerBuffer(b2)) {
      return isServerBuffer(b2) ? 1 : -1;
    }
    if (a3.name != b2.name) {
      return a3.name > b2.name ? 1 : -1;
    }
    return 0;
  }
  function updateMembership(membership, letter, add, client) {
    let prefix = client.isupport.prefix();
    let prefixPrivs = new Map(parseMembershipModes(prefix).map((membership2, i2) => {
      return [membership2.prefix, i2];
    }));
    if (add) {
      let i2 = membership.indexOf(letter);
      if (i2 < 0) {
        membership += letter;
        membership = Array.from(membership).sort((a3, b2) => {
          return prefixPrivs.get(a3) - prefixPrivs.get(b2);
        }).join("");
      }
    } else {
      membership = membership.replace(letter, "");
    }
    return membership;
  }
  function insertMessage(list, msg) {
    if (list.length == 0) {
      return [msg];
    } else if (!findBatchByType(msg, "chathistory") || list[list.length - 1].tags.time <= msg.tags.time) {
      return list.concat(msg);
    }
    let insertBefore = -1;
    for (let i2 = 0; i2 < list.length; i2++) {
      let other = list[i2];
      if (msg.tags.time < other.tags.time) {
        insertBefore = i2;
        break;
      }
    }
    console.assert(insertBefore >= 0, "");
    list = [...list];
    list.splice(insertBefore, 0, msg);
    return list;
  }
  var lastServerID = 0;
  var lastBufferID = 0;
  var lastMessageKey = 0;
  var State2 = {
    create() {
      return {
        servers: /* @__PURE__ */ new Map(),
        buffers: /* @__PURE__ */ new Map(),
        activeBuffer: null,
        bouncerNetworks: /* @__PURE__ */ new Map()
      };
    },
    updateServer(state, id, updater) {
      let server = state.servers.get(id);
      if (!server) {
        return;
      }
      let updated = updateState3(server, updater);
      if (!updated) {
        return;
      }
      let servers = new Map(state.servers);
      servers.set(id, updated);
      return { servers };
    },
    updateBuffer(state, id, updater) {
      let buf = State2.getBuffer(state, id);
      if (!buf) {
        return;
      }
      let updated = updateState3(buf, updater);
      if (!updated) {
        return;
      }
      let buffers = new Map(state.buffers);
      buffers.set(buf.id, updated);
      return { buffers };
    },
    getActiveServerID(state) {
      let buf = state.buffers.get(state.activeBuffer);
      if (!buf) {
        return null;
      }
      return buf.server;
    },
    getBuffer(state, id) {
      switch (typeof id) {
        case "number":
          return state.buffers.get(id);
        case "object":
          if (id.id) {
            return state.buffers.get(id.id);
          }
          let serverID = id.server, name = id.name;
          if (!serverID) {
            serverID = State2.getActiveServerID(state);
          }
          if (!name) {
            name = SERVER_BUFFER;
          }
          let cm = CaseMapping.RFC1459;
          let server = state.servers.get(serverID);
          if (server) {
            cm = server.cm;
          }
          let nameCM = cm(name);
          for (let buf of state.buffers.values()) {
            if (buf.server === serverID && cm(buf.name) === nameCM) {
              return buf;
            }
          }
          return null;
        default:
          throw new Error("Invalid buffer ID type: " + typeof id);
      }
    },
    createServer(state) {
      lastServerID++;
      let id = lastServerID;
      let servers = new Map(state.servers);
      servers.set(id, {
        id,
        name: null,
        status: ServerStatus.DISCONNECTED,
        cm: CaseMapping.RFC1459,
        users: new CaseMapMap(null, CaseMapping.RFC1459),
        account: null,
        supportsSASLPlain: false,
        supportsAccountRegistration: false,
        reliableUserAccounts: false,
        statusMsg: null,
        isBouncer: false,
        bouncerNetID: null
      });
      return [id, { servers }];
    },
    createBuffer(state, name, serverID, client) {
      let buf = State2.getBuffer(state, { server: serverID, name });
      if (buf) {
        return [buf.id, null];
      }
      lastBufferID++;
      let id = lastBufferID;
      let type;
      if (name == SERVER_BUFFER) {
        type = BufferType.SERVER;
      } else if (client.isChannel(name)) {
        type = BufferType.CHANNEL;
      } else {
        type = BufferType.NICK;
      }
      let bufferList = Array.from(state.buffers.values());
      bufferList.push({
        id,
        name,
        type,
        server: serverID,
        serverInfo: null,
        joined: false,
        topic: null,
        members: new CaseMapMap(null, client.cm),
        messages: [],
        unread: Unread.NONE,
        prevReadReceipt: null
      });
      bufferList = bufferList.sort(compareBuffers);
      let buffers = new Map(bufferList.map((buf2) => [buf2.id, buf2]));
      return [id, { buffers }];
    },
    storeBouncerNetwork(state, id, attrs) {
      let bouncerNetworks = new Map(state.bouncerNetworks);
      bouncerNetworks.set(id, __spreadValues(__spreadValues({}, bouncerNetworks.get(id)), attrs));
      return { bouncerNetworks };
    },
    deleteBouncerNetwork(state, id) {
      let bouncerNetworks = new Map(state.bouncerNetworks);
      bouncerNetworks.delete(id);
      return { bouncerNetworks };
    },
    handleMessage(state, msg, serverID, client) {
      function updateServer(updater) {
        return State2.updateServer(state, serverID, updater);
      }
      function updateBuffer(name, updater) {
        return State2.updateBuffer(state, { server: serverID, name }, updater);
      }
      function updateUser(name, updater) {
        return updateServer((server) => {
          let users = new CaseMapMap(server.users);
          let updated = updateState3(users.get(name), updater);
          if (!updated) {
            return;
          }
          users.set(name, updated);
          return { users };
        });
      }
      if (findBatchByType(msg, "chathistory")) {
        return;
      }
      let target, channel, topic, targets, who, update, buffers;
      switch (msg.command) {
        case RPL_MYINFO:
          let serverInfo = {
            name: msg.params[1],
            version: msg.params[2]
          };
          return updateBuffer(SERVER_BUFFER, { serverInfo });
        case RPL_ISUPPORT:
          buffers = new Map(state.buffers);
          state.buffers.forEach((buf) => {
            if (buf.server != serverID) {
              return;
            }
            let members = new CaseMapMap(buf.members, client.cm);
            buffers.set(buf.id, __spreadProps(__spreadValues({}, buf), { members }));
          });
          return __spreadValues({
            buffers
          }, updateServer((server) => {
            return {
              name: client.isupport.network(),
              cm: client.cm,
              users: new CaseMapMap(server.users, client.cm),
              reliableUserAccounts: client.isupport.monitor() > 0 && client.isupport.whox(),
              statusMsg: client.isupport.statusMsg(),
              bouncerNetID: client.isupport.bouncerNetID()
            };
          }));
        case "CAP":
          return updateServer({
            supportsSASLPlain: client.supportsSASL("PLAIN"),
            supportsAccountRegistration: client.caps.enabled.has("draft/account-registration"),
            isBouncer: client.caps.enabled.has("soju.im/bouncer-networks")
          });
        case RPL_LOGGEDIN:
          return updateServer({ account: msg.params[2] });
        case RPL_LOGGEDOUT:
          return updateServer({ account: null });
        case "REGISTER":
        case "VERIFY":
          if (msg.params[0] === "SUCCESS") {
            return updateServer({ account: msg.params[1] });
          }
          break;
        case RPL_NOTOPIC:
          channel = msg.params[1];
          return updateBuffer(channel, { topic: null });
        case RPL_TOPIC:
          channel = msg.params[1];
          topic = msg.params[2];
          return updateBuffer(channel, { topic });
        case RPL_TOPICWHOTIME:
          break;
        case RPL_ENDOFNAMES:
          channel = msg.params[1];
          return updateBuffer(channel, (buf) => {
            let members = new CaseMapMap(null, buf.members.caseMap);
            msg.list.forEach((namreply) => {
              let membersList = namreply.params[3].split(" ");
              membersList.forEach((s2) => {
                let member = parseTargetPrefix(s2);
                members.set(member.name, member.prefix);
              });
            });
            return { members };
          });
          break;
        case RPL_WHOREPLY:
        case RPL_WHOSPCRPL:
          who = client.parseWhoReply(msg);
          if (who.flags !== void 0) {
            who.away = who.flags.indexOf("G") >= 0;
            who.operator = who.flags.indexOf("*") >= 0;
            delete who.flags;
          }
          who.offline = false;
          return updateUser(who.nick, who);
        case RPL_ENDOFWHO:
          target = msg.params[1];
          if (msg.list.length == 0 && !client.isChannel(target) && target.indexOf("*") < 0) {
            return updateUser(target, (user) => {
              return { offline: true };
            });
          }
          break;
        case "JOIN":
          channel = msg.params[0];
          if (client.isMyNick(msg.prefix.name)) {
            let [id, update2] = State2.createBuffer(state, channel, serverID, client);
            state = __spreadValues(__spreadValues({}, state), update2);
          }
          update = updateBuffer(channel, (buf) => {
            let members = new CaseMapMap(buf.members);
            members.set(msg.prefix.name, "");
            let joined = buf.joined || client.isMyNick(msg.prefix.name);
            return { members, joined };
          });
          state = __spreadValues(__spreadValues({}, state), update);
          who = { nick: msg.prefix.name, offline: false };
          if (msg.prefix.user) {
            who.username = msg.prefix.user;
          }
          if (msg.prefix.host) {
            who.hostname = msg.prefix.host;
          }
          if (msg.params.length > 2) {
            who.account = msg.params[1];
            if (who.account === "*") {
              who.account = null;
            }
            who.realname = msg.params[2];
          }
          update = updateUser(msg.prefix.name, who);
          state = __spreadValues(__spreadValues({}, state), update);
          return state;
        case "PART":
          channel = msg.params[0];
          return updateBuffer(channel, (buf) => {
            let members = new CaseMapMap(buf.members);
            members.delete(msg.prefix.name);
            let joined = buf.joined && !client.isMyNick(msg.prefix.name);
            return { members, joined };
          });
        case "KICK":
          channel = msg.params[0];
          let nick = msg.params[1];
          return updateBuffer(channel, (buf) => {
            let members = new CaseMapMap(buf.members);
            members.delete(nick);
            let joined = buf.joined && !client.isMyNick(nick);
            return { members, joined };
          });
        case "QUIT":
          buffers = new Map(state.buffers);
          state.buffers.forEach((buf) => {
            if (buf.server != serverID) {
              return;
            }
            if (!buf.members.has(msg.prefix.name)) {
              return;
            }
            let members = new CaseMapMap(buf.members);
            members.delete(msg.prefix.name);
            buffers.set(buf.id, __spreadProps(__spreadValues({}, buf), { members }));
          });
          state = __spreadProps(__spreadValues({}, state), { buffers });
          update = updateUser(msg.prefix.name, (user) => {
            if (!user) {
              return;
            }
            return { offline: true };
          });
          state = __spreadValues(__spreadValues({}, state), update);
          return state;
        case "NICK":
          let newNick = msg.params[0];
          buffers = new Map(state.buffers);
          state.buffers.forEach((buf) => {
            if (buf.server != serverID) {
              return;
            }
            if (!buf.members.has(msg.prefix.name)) {
              return;
            }
            let members = new CaseMapMap(buf.members);
            members.set(newNick, members.get(msg.prefix.name));
            members.delete(msg.prefix.name);
            buffers.set(buf.id, __spreadProps(__spreadValues({}, buf), { members }));
          });
          state = __spreadProps(__spreadValues({}, state), { buffers });
          update = updateServer((server) => {
            let users = new CaseMapMap(server.users);
            let user = users.get(msg.prefix.name);
            if (!user) {
              return;
            }
            users.set(newNick, user);
            users.delete(msg.prefix.name);
            return { users };
          });
          state = __spreadValues(__spreadValues({}, state), update);
          return state;
        case "SETNAME":
          return updateUser(msg.prefix.name, { realname: msg.params[0] });
        case "CHGHOST":
          return updateUser(msg.prefix.name, {
            username: msg.params[0],
            hostname: msg.params[1]
          });
        case "ACCOUNT":
          let account = msg.params[0];
          if (account === "*") {
            account = null;
          }
          return updateUser(msg.prefix.name, { account });
        case "AWAY":
          let awayMessage = msg.params[0];
          return updateUser(msg.prefix.name, { away: !!awayMessage });
        case "TOPIC":
          channel = msg.params[0];
          topic = msg.params[1];
          return updateBuffer(channel, { topic });
        case "MODE":
          target = msg.params[0];
          if (!client.isChannel(target)) {
            return;
          }
          let prefix = client.isupport.prefix();
          let prefixByMode = new Map(parseMembershipModes(prefix).map((membership) => {
            return [membership.mode, membership.prefix];
          }));
          return updateBuffer(target, (buf) => {
            let members = new CaseMapMap(buf.members);
            forEachChannelModeUpdate(msg, client.isupport, (mode, add, arg) => {
              if (prefixByMode.has(mode)) {
                let nick2 = arg;
                let membership = members.get(nick2);
                let letter = prefixByMode.get(mode);
                members.set(nick2, updateMembership(membership, letter, add, client));
              }
            });
            return { members };
          });
        case RPL_MONONLINE:
          targets = msg.params[1].split(",");
          for (let target2 of targets) {
            let prefix2 = parsePrefix(target2);
            let update2 = updateUser(prefix2.name, { offline: false });
            state = __spreadValues(__spreadValues({}, state), update2);
          }
          return state;
        case RPL_MONOFFLINE:
          targets = msg.params[1].split(",");
          for (let target2 of targets) {
            let prefix2 = parsePrefix(target2);
            let update2 = updateUser(prefix2.name, { offline: true });
            state = __spreadValues(__spreadValues({}, state), update2);
          }
          return state;
      }
    },
    addMessage(state, msg, bufID) {
      lastMessageKey++;
      msg.key = lastMessageKey;
      return State2.updateBuffer(state, bufID, (buf) => {
        let messages = insertMessage(buf.messages, msg);
        return { messages };
      });
    }
  };

  // gamja/store.js
  var PREFIX = "gamja_";
  var Item2 = class {
    constructor(k2) {
      this.k = PREFIX + k2;
    }
    load() {
      let v2 = localStorage.getItem(this.k);
      if (!v2) {
        return null;
      }
      return JSON.parse(v2);
    }
    put(v2) {
      if (v2) {
        localStorage.setItem(this.k, JSON.stringify(v2));
      } else {
        localStorage.removeItem(this.k);
      }
    }
  };
  var autoconnect = new Item2("autoconnect");
  var naggedProtocolHandler = new Item2("naggedProtocolHandler");
  function debounce(f2, delay) {
    let timeout = null;
    return (...args) => {
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        timeout = null;
        f2(...args);
      }, delay);
    };
  }
  var Buffer2 = class {
    constructor() {
      __publicField(this, "raw", new Item2("buffers"));
      __publicField(this, "m", null);
      let obj = this.raw.load();
      this.m = new Map(Object.entries(obj || {}));
      let saveImmediately = this.save.bind(this);
      this.save = debounce(saveImmediately, 500);
      document.addEventListener("visibilitychange", () => {
        if (document.visibilityState === "hidden") {
          saveImmediately();
        }
      });
    }
    key(buf) {
      return JSON.stringify({
        name: buf.name,
        server: {
          url: buf.server.url,
          nick: buf.server.nick,
          bouncerNetwork: buf.server.bouncerNetwork
        }
      });
    }
    save() {
      if (this.m.size > 0) {
        this.raw.put(Object.fromEntries(this.m));
      } else {
        this.raw.put(null);
      }
    }
    get(buf) {
      return this.m.get(this.key(buf));
    }
    put(buf) {
      let key = this.key(buf);
      let updated = !this.m.has(key);
      let prev = this.m.get(key) || {};
      let unread = prev.unread;
      if (buf.unread !== void 0 && buf.unread !== prev.unread) {
        unread = buf.unread;
        updated = true;
      }
      let receipts = __spreadValues({}, prev.receipts);
      if (buf.receipts) {
        Object.keys(buf.receipts).forEach((k2) => {
          if (!receipts[k2] || receipts[k2].time < buf.receipts[k2].time) {
            receipts[k2] = buf.receipts[k2];
            updated = true;
          }
        });
        if (receipts[ReceiptType.DELIVERED] < receipts[ReceiptType.READ]) {
          receipts[ReceiptType.DELIVERED] = receipts[ReceiptType.READ];
          updated = true;
        }
      }
      if (!updated) {
        return false;
      }
      this.m.set(this.key(buf), {
        name: buf.name,
        unread,
        receipts,
        server: {
          url: buf.server.url,
          nick: buf.server.nick,
          bouncerNetwork: buf.server.bouncerNetwork
        }
      });
      this.save();
      return true;
    }
    delete(buf) {
      this.m.delete(this.key(buf));
      this.save();
    }
    list(server) {
      let buffers = [];
      for (const buf of this.m.values()) {
        if (buf.server.url !== server.url || buf.server.nick !== server.nick || buf.server.bouncerNetwork !== server.bouncerNetwork) {
          continue;
        }
        buffers.push(buf);
      }
      return buffers;
    }
    clear(server) {
      if (server) {
        for (const buf of this.m.values()) {
          this.m.delete(this.key(buf));
        }
      } else {
        this.m = /* @__PURE__ */ new Map();
      }
      this.save();
    }
  };

  // gamja/components/membership.js
  var names = {
    "~": "owner",
    "&": "admin",
    "@": "op",
    "%": "halfop",
    "+": "voice"
  };
  function Membership(props) {
    if (!this.props.value) {
      return null;
    }
    const name = names[this.props.value[0]] || "";
    return html`
		<span class="membership ${name}" title=${name}>
			${this.props.value}
		</span>
	`;
  }

  // gamja/components/buffer.js
  function djb2(s2) {
    let hash = 5381;
    for (let i2 = 0; i2 < s2.length; i2++) {
      hash = (hash << 5) + hash + s2.charCodeAt(i2);
      hash = hash >>> 0;
    }
    return hash;
  }
  function Nick(props) {
    function handleClick(event) {
      event.preventDefault();
      props.onClick();
    }
    let colorIndex = djb2(props.nick) % 16 + 1;
    return html`
		<a href=${getNickURL(props.nick)} class="nick nick-${colorIndex}" onClick=${handleClick}>${props.nick}</a>
	`;
  }
  function Timestamp({ date, url }) {
    if (!date) {
      return html`<spam class="timestamp">--:--:--</span>`;
    }
    let hh = date.getHours().toString().padStart(2, "0");
    let mm = date.getMinutes().toString().padStart(2, "0");
    let ss = date.getSeconds().toString().padStart(2, "0");
    let timestamp = `${hh}:${mm}:${ss}`;
    return html`
		<a
			href=${url}
			class="timestamp"
			title=${date.toLocaleString()}
			onClick=${(event) => event.preventDefault()}
		>
			${timestamp}
		</a>
	`;
  }
  function canFoldMessage(msg) {
    switch (msg.command) {
      case "JOIN":
      case "PART":
      case "QUIT":
      case "NICK":
        return true;
    }
    return false;
  }
  var LogLine = class extends _ {
    shouldComponentUpdate(nextProps) {
      return this.props.message !== nextProps.message;
    }
    render() {
      let msg = this.props.message;
      let buf = this.props.buffer;
      let server = this.props.server;
      let onNickClick = this.props.onNickClick;
      let onChannelClick = this.props.onChannelClick;
      let onVerifyClick = this.props.onVerifyClick;
      function createNick(nick) {
        return html`
				<${Nick} nick=${nick} onClick=${() => onNickClick(nick)}/>
			`;
      }
      function createChannel(channel) {
        return html`
				<a href=${getChannelURL(channel)} onClick=${onChannelClick}>
					${channel}
				</a>
			`;
      }
      let lineClass = "";
      let content;
      let invitee, target, account;
      switch (msg.command) {
        case "NOTICE":
        case "PRIVMSG":
          target = msg.params[0];
          let text2 = msg.params[1];
          let ctcp = parseCTCP(msg);
          if (ctcp) {
            if (ctcp.command == "ACTION") {
              lineClass = "me-tell";
              content = html`* ${createNick(msg.prefix.name)} ${linkify(strip(ctcp.param), onChannelClick)}`;
            } else {
              content = html`
						${createNick(msg.prefix.name)} has sent a CTCP command: ${ctcp.command} ${ctcp.param}
					`;
            }
          } else {
            lineClass = "talk";
            let prefix = "<", suffix = ">";
            if (msg.command == "NOTICE") {
              prefix = suffix = "-";
            }
            content = html`${prefix}${createNick(msg.prefix.name)}${suffix} ${linkify(strip(text2), onChannelClick)}`;
          }
          let status = null;
          let allowedPrefixes = server.statusMsg;
          if (target !== buf.name && allowedPrefixes) {
            let parts = parseTargetPrefix(target, allowedPrefixes);
            if (parts.name === buf.name) {
              content = [html`(<${Membership} value=${parts.prefix}/>)`, " ", content];
            }
          }
          if (msg.isHighlight) {
            lineClass += " highlight";
          }
          break;
        case "JOIN":
          content = html`
				${createNick(msg.prefix.name)} has joined
			`;
          break;
        case "PART":
          content = html`
				${createNick(msg.prefix.name)} has left
			`;
          break;
        case "QUIT":
          content = html`
				${createNick(msg.prefix.name)} has quit
			`;
          break;
        case "NICK":
          let newNick = msg.params[0];
          content = html`
				${createNick(msg.prefix.name)} is now known as ${createNick(newNick)}
			`;
          break;
        case "KICK":
          content = html`
				${createNick(msg.params[1])} was kicked by ${createNick(msg.prefix.name)} (${msg.params.slice(2)})
			`;
          break;
        case "MODE":
          target = msg.params[0];
          content = html`
				* ${createNick(msg.prefix.name)} sets mode ${msg.params.slice(1).join(" ")}
			`;
          if (buf.name !== target) {
            content = html`${content} on ${target}`;
          }
          break;
        case "TOPIC":
          let topic = msg.params[1];
          content = html`
				${createNick(msg.prefix.name)} changed the topic to: ${linkify(strip(topic), onChannelClick)}
			`;
          break;
        case "INVITE":
          invitee = msg.params[0];
          let channel = msg.params[1];
          if (buf.type === BufferType.SERVER) {
            lineClass = "talk";
            content = html`
					You have been invited to ${createChannel(channel)} by ${createNick(msg.prefix.name)}
				`;
          } else {
            content = html`
					${createNick(msg.prefix.name)} has invited ${createNick(invitee)} to the channel
				`;
          }
          break;
        case RPL_WELCOME:
          let nick = msg.params[0];
          content = html`Connected to server, your nickname is ${nick}`;
          break;
        case RPL_INVITING:
          invitee = msg.params[1];
          content = html`${createNick(invitee)} has been invited to the channel`;
          break;
        case RPL_MOTD:
          lineClass = "motd";
          content = linkify(strip(msg.params[1]), onChannelClick);
          break;
        case RPL_LOGGEDIN:
          account = msg.params[2];
          content = html`You are now authenticated as ${account}`;
          break;
        case RPL_LOGGEDOUT:
          content = html`You are now unauthenticated`;
          break;
        case "REGISTER":
          let handleVerifyClick = function(event) {
            event.preventDefault();
            onVerifyClick(account, reason);
          };
          account = msg.params[1];
          let reason = msg.params[2];
          switch (msg.params[0]) {
            case "SUCCESS":
              content = html`A new account has been created, you are now authenticated as ${account}`;
              break;
            case "VERIFICATION_REQUIRED":
              content = html`A new account has been created, but you need to <a href="#" onClick=${handleVerifyClick}>verify it</a>: ${linkify(reason)}`;
              break;
          }
          break;
        case "VERIFY":
          account = msg.params[1];
          content = html`The new account has been verified, you are now authenticated as ${account}`;
          break;
        case RPL_UMODEIS:
          let mode = msg.params[1];
          if (mode) {
            content = html`Your user mode is ${mode}`;
          } else {
            content = html`You have no user mode`;
          }
          break;
        case RPL_CHANNELMODEIS:
          content = html`Channel mode is ${msg.params.slice(2).join(" ")}`;
          break;
        case RPL_CREATIONTIME:
          let date = new Date(parseInt(msg.params[2], 10) * 1e3);
          content = html`Channel was created on ${date.toLocaleString()}`;
          break;
        default:
          if (isError(msg.command) && msg.command != ERR_NOMOTD) {
            lineClass = "error";
          }
          content = html`${msg.command} ${linkify(msg.params.join(" "))}`;
      }
      if (!content) {
        return null;
      }
      return html`
			<div class="logline ${lineClass}" data-key=${msg.key}>
				<${Timestamp} date=${new Date(msg.tags.time)} url=${getMessageURL(buf, msg)}/>
				${" "}
				${content}
			</div>
		`;
    }
  };
  function createNickList(nicks, createNick) {
    if (nicks.length === 0) {
      return null;
    } else if (nicks.length === 1) {
      return createNick(nicks[0]);
    }
    let l2 = nicks.slice(0, nicks.length - 1).map((nick, i2) => {
      if (i2 === 0) {
        return createNick(nick);
      } else {
        return [", ", createNick(nick)];
      }
    });
    l2.push(" and ");
    l2.push(createNick(nicks[nicks.length - 1]));
    return l2;
  }
  var FoldGroup = class extends _ {
    shouldComponentUpdate(nextProps) {
      return this.props.messages[0] !== nextProps.messages[0] || this.props.messages[this.props.messages.length - 1] !== nextProps.messages[nextProps.messages.length - 1];
    }
    render() {
      let msgs = this.props.messages;
      let buf = this.props.buffer;
      let onNickClick = this.props.onNickClick;
      function createNick(nick) {
        return html`
				<${Nick} nick=${nick} onClick=${() => onNickClick(nick)}/>
			`;
      }
      let byCommand = {
        "JOIN": [],
        "PART": [],
        "QUIT": [],
        "NICK": []
      };
      msgs.forEach((msg) => {
        byCommand[msg.command].push(msg);
      });
      let first = true;
      let content = [];
      ["JOIN", "PART", "QUIT"].forEach((cmd) => {
        if (byCommand[cmd].length === 0) {
          return;
        }
        let nicks = new Set(byCommand[cmd].map((msg) => msg.prefix.name));
        let plural = nicks.size > 1;
        let action;
        switch (cmd) {
          case "JOIN":
            action = plural ? "have joined" : "has joined";
            break;
          case "PART":
            action = plural ? "have left" : "has left";
            break;
          case "QUIT":
            action = plural ? "have quit" : "has quit";
            break;
        }
        if (first) {
          first = false;
        } else {
          content.push(", ");
        }
        content.push(createNickList([...nicks], createNick));
        content.push(" " + action);
      });
      byCommand["NICK"].forEach((msg) => {
        if (first) {
          first = false;
        } else {
          content.push(", ");
        }
        let newNick = msg.params[0];
        content.push(html`
				${createNick(msg.prefix.name)} is now known as ${createNick(newNick)}
			`);
      });
      let lastMsg = msgs[msgs.length - 1];
      let firstDate = new Date(msgs[0].tags.time);
      let lastDate = new Date(lastMsg.tags.time);
      let timestamp = html`
			<${Timestamp} date=${firstDate} url=${getMessageURL(buf, msgs[0])}/>
		`;
      if (lastDate - firstDate > 60 * 100) {
        timestamp = [
          timestamp,
          " \u2014 ",
          html`
					<${Timestamp} date=${lastDate} url=${getMessageURL(buf, lastMsg)}/>
				`
        ];
      }
      return html`
			<div class="logline" data-key=${msgs[0].key}>
				${timestamp}
				${" "}
				${content}
			</div>
		`;
    }
  };
  var notificationsSupported = false;
  if (window.Notification) {
    notificationsSupported = true;
    if (Notification.permission === "default") {
      try {
        new Notification("");
      } catch (err) {
        if (err.name === "TypeError") {
          notificationsSupported = false;
        }
      }
    }
  }
  var NotificationNagger = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", { nag: false });
      this.handleClick = this.handleClick.bind(this);
      this.state.nag = this.shouldNag();
    }
    shouldNag() {
      return notificationsSupported && Notification.permission === "default";
    }
    handleClick(event) {
      event.preventDefault();
      Notification.requestPermission((permission) => {
        this.setState({ nag: this.shouldNag() });
      });
    }
    render() {
      if (!this.state.nag) {
        return null;
      }
      return html`
			<div class="logline">
				<${Timestamp}/>
				${" "}
				<a href="#" onClick=${this.handleClick}>Turn on desktop notifications</a> to get notified about new messages
			</div>
		`;
    }
  };
  var ProtocolHandlerNagger = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", { nag: true });
      this.handleClick = this.handleClick.bind(this);
      this.state.nag = !naggedProtocolHandler.load();
    }
    handleClick(event) {
      event.preventDefault();
      let url = window.location.origin + window.location.pathname + "?open=%s";
      try {
        navigator.registerProtocolHandler("irc", url);
        navigator.registerProtocolHandler("ircs", url);
      } catch (err) {
        console.error("Failed to register protocol handler: ", err);
      }
      naggedProtocolHandler.put(true);
      this.setState({ nag: false });
    }
    render() {
      if (!navigator.registerProtocolHandler || !this.state.nag) {
        return null;
      }
      let name = this.props.bouncerName || "this bouncer";
      return html`
			<div class="logline">
				<${Timestamp}/>
				${" "}
				<a href="#" onClick=${this.handleClick}>Register our protocol handler</a> to open IRC links with ${name}
			</div>
		`;
    }
  };
  function AccountNagger({ server, onAuthClick, onRegisterClick }) {
    let accDesc = "an account on this server";
    if (server.name) {
      accDesc = "a " + server.name + " account";
    }
    function handleAuthClick(event) {
      event.preventDefault();
      onAuthClick();
    }
    function handleRegisterClick(event) {
      event.preventDefault();
      onRegisterClick();
    }
    let msg = [html`
		You are unauthenticated on this server,
		${" "}
		<a href="#" onClick=${handleAuthClick}>login</a>
		${" "}
	`];
    if (server.supportsAccountRegistration) {
      msg.push(html`or <a href="#" onClick=${handleRegisterClick}>register</a> ${accDesc}`);
    } else {
      msg.push(html`if you have ${accDesc}`);
    }
    return html`
		<div class="logline">
			<${Timestamp}/> ${msg}
		</div>
	`;
  }
  var DateSeparator = class extends _ {
    constructor(props) {
      super(props);
    }
    shouldComponentUpdate(nextProps) {
      return this.props.date.getTime() !== nextProps.date.getTime();
    }
    render() {
      let date = this.props.date;
      let text2 = date.toLocaleDateString([], { year: "numeric", month: "2-digit", day: "2-digit" });
      return html`
			<div class="separator date-separator">
				${text2}
			</div>
		`;
    }
  };
  function UnreadSeparator(props) {
    return html`<div class="separator unread-separator">New messages</div>`;
  }
  function sameDate(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();
  }
  var Buffer3 = class extends _ {
    shouldComponentUpdate(nextProps) {
      return this.props.buffer !== nextProps.buffer;
    }
    render() {
      let buf = this.props.buffer;
      if (!buf) {
        return null;
      }
      let server = this.props.server;
      let bouncerNetwork = this.props.bouncerNetwork;
      let serverName = server.name;
      let children = [];
      if (buf.type == BufferType.SERVER) {
        children.push(html`<${NotificationNagger}/>`);
      }
      if (buf.type == BufferType.SERVER && server.isBouncer && !server.bouncerNetID) {
        children.push(html`<${ProtocolHandlerNagger} bouncerName=${serverName}/>`);
      }
      if (buf.type == BufferType.SERVER && server.status == ServerStatus.REGISTERED && server.supportsSASLPlain && !server.account) {
        children.push(html`
				<${AccountNagger}
					server=${server}
					onAuthClick=${this.props.onAuthClick}
					onRegisterClick=${this.props.onRegisterClick}
				/>
			`);
      }
      let onChannelClick = this.props.onChannelClick;
      let onNickClick = this.props.onNickClick;
      let onVerifyClick = this.props.onVerifyClick;
      function createLogLine(msg) {
        return html`
				<${LogLine}
					key=${"msg-" + msg.key}
					message=${msg}
					buffer=${buf}
					server=${server}
					onChannelClick=${onChannelClick}
					onNickClick=${onNickClick}
					onVerifyClick=${onVerifyClick}
				/>
			`;
      }
      function createFoldGroup(msgs) {
        let partIndexes = /* @__PURE__ */ new Map();
        let keep = [];
        msgs.forEach((msg, i2) => {
          if (msg.command === "PART" || msg.command === "QUIT") {
            partIndexes.set(msg.prefix.name, i2);
          }
          if (msg.command === "JOIN" && partIndexes.has(msg.prefix.name)) {
            keep[partIndexes.get(msg.prefix.name)] = false;
            partIndexes.delete(msg.prefix.name);
            keep.push(false);
          } else {
            keep.push(true);
          }
        });
        msgs = msgs.filter((msg, i2) => keep[i2]);
        if (msgs.length === 0) {
          return null;
        } else if (msgs.length === 1) {
          return createLogLine(msgs[0]);
        }
        return html`
				<${FoldGroup}
					key=${"fold-" + msgs[0].key + "-" + msgs[msgs.length - 1].key}
					messages=${msgs}
					buffer=${buf}
					server=${server}
					onNickClick=${onNickClick}
				/>
			`;
      }
      let hasUnreadSeparator = false;
      let prevDate = new Date();
      let foldMessages = [];
      buf.messages.forEach((msg) => {
        let sep = [];
        if (!hasUnreadSeparator && buf.type != BufferType.SERVER && !isMessageBeforeReceipt(msg, buf.prevReadReceipt)) {
          sep.push(html`<${UnreadSeparator} key="unread"/>`);
          hasUnreadSeparator = true;
        }
        let date = new Date(msg.tags.time);
        if (!sameDate(prevDate, date)) {
          sep.push(html`<${DateSeparator} key=${"date-" + date} date=${date}/>`);
        }
        prevDate = date;
        if (sep.length > 0) {
          children.push(createFoldGroup(foldMessages));
          children.push(sep);
          foldMessages = [];
        }
        if (canFoldMessage(msg)) {
          foldMessages.push(msg);
          return;
        }
        if (foldMessages.length > 0) {
          children.push(createFoldGroup(foldMessages));
          foldMessages = [];
        }
        children.push(createLogLine(msg));
      });
      children.push(createFoldGroup(foldMessages));
      return html`
			<div class="logline-list">
				${children}
			</div>
		`;
    }
  };

  // gamja/components/buffer-list.js
  function BufferItem(props) {
    function handleClick(event) {
      event.preventDefault();
      props.onClick();
    }
    function handleMouseDown(event) {
      if (event.button === 1) {
        event.preventDefault();
        props.onClose();
      }
    }
    let name = props.buffer.name;
    if (props.buffer.type == BufferType.SERVER) {
      name = getServerName(props.server, props.bouncerNetwork);
    }
    let classes2 = ["type-" + props.buffer.type];
    if (props.active) {
      classes2.push("active");
    }
    if (props.buffer.unread != Unread.NONE) {
      classes2.push("unread-" + props.buffer.unread);
    }
    return html`
		<li class="${classes2.join(" ")}">
			<a
				href=${getBufferURL(props.buffer)}
				onClick=${handleClick}
				onMouseDown=${handleMouseDown}
			>${name}</a>
		</li>
	`;
  }
  function BufferList(props) {
    let items = Array.from(props.buffers.values()).map((buf) => {
      let server = props.servers.get(buf.server);
      let bouncerNetwork = null;
      if (server.bouncerNetID) {
        bouncerNetwork = props.bouncerNetworks.get(server.bouncerNetID);
      }
      return html`
			<${BufferItem}
				key=${buf.id}
				buffer=${buf}
				server=${server}
				bouncerNetwork=${bouncerNetwork}
				onClick=${() => props.onBufferClick(buf)}
				onClose=${() => props.onBufferClose(buf)}
				active=${props.activeBuffer == buf.id}
			/>
		`;
    });
    return html`<ul>${items}</ul>`;
  }

  // gamja/components/buffer-header.js
  var UserStatus = {
    HERE: "here",
    GONE: "gone",
    OFFLINE: "offline"
  };
  function NickStatus(props) {
    let textMap = {
      [UserStatus.HERE]: "User is online",
      [UserStatus.GONE]: "User is away",
      [UserStatus.OFFLINE]: "User is offline"
    };
    let text2 = textMap[props.status];
    return html`<span class="status status-${props.status}" title=${text2}>●</span>`;
  }
  function BufferHeader(props) {
    let fullyConnected = props.server.status === ServerStatus.REGISTERED;
    if (props.bouncerNetwork) {
      fullyConnected = fullyConnected && props.bouncerNetwork.state === "connected";
    }
    let description = null, actions = [];
    switch (props.buffer.type) {
      case BufferType.SERVER:
        switch (props.server.status) {
          case ServerStatus.DISCONNECTED:
            description = "Disconnected";
            break;
          case ServerStatus.CONNECTING:
            description = "Connecting...";
            break;
          case ServerStatus.REGISTERING:
            description = "Logging in...";
            break;
          case ServerStatus.REGISTERED:
            if (props.bouncerNetwork) {
              switch (props.bouncerNetwork.state) {
                case "disconnected":
                  description = "Bouncer disconnected from network";
                  break;
                case "connecting":
                  description = "Bouncer connecting to network...";
                  break;
                case "connected":
                  description = `Connected to ${props.bouncerNetwork.host || "network"}`;
                  break;
              }
            } else if (props.buffer.serverInfo) {
              let serverInfo = props.buffer.serverInfo;
              description = `Connected to ${serverInfo.name}`;
            } else {
              description = "Connected";
            }
            break;
        }
        let joinButton = html`
			<button
				key="join"
				onClick=${props.onJoin}
			>Join channel</button>
		`;
        let reconnectButton = html`
			<button
				key="reconect"
				onClick=${props.onReconnect}
			>Reconnect</button>
		`;
        if (props.server.isBouncer) {
          if (props.server.bouncerNetID) {
            if (fullyConnected) {
              actions.push(joinButton);
            }
            if (props.server.status === ServerStatus.REGISTERED) {
              actions.push(html`
						<button
							key="manage"
							onClick=${props.onManageNetwork}
						>Manage network</button>
					`);
            }
          } else {
            if (fullyConnected) {
              actions.push(html`
						<button
							key="add"
							onClick=${props.onAddNetwork}
						>Add network</button>
					`);
            } else if (props.server.status === ServerStatus.DISCONNECTED) {
              actions.push(reconnectButton);
            }
            actions.push(html`
					<button
						key="disconnect"
						class="danger"
						onClick=${props.onClose}
					>Disconnect</button>
				`);
          }
        } else {
          if (fullyConnected) {
            actions.push(joinButton);
          } else if (props.server.status === ServerStatus.DISCONNECTED) {
            actions.push(reconnectButton);
          }
          actions.push(html`
				<button
					key="disconnect"
					class="danger"
					onClick=${props.onClose}
				>Disconnect</button>
			`);
        }
        break;
      case BufferType.CHANNEL:
        if (props.buffer.topic) {
          description = linkify(strip(props.buffer.topic), props.onChannelClick);
        }
        if (props.buffer.joined) {
          actions.push(html`
				<button
					key="part"
					class="danger"
					onClick=${props.onClose}
				>Leave</button>
			`);
        } else {
          if (fullyConnected) {
            actions.push(html`
					<button
						key="join"
						onClick=${props.onJoin}
					>Join</button>
				`);
          }
          actions.push(html`
				<button
					key="part"
					class="danger"
					onClick=${props.onClose}
				>Close</button>
			`);
        }
        break;
      case BufferType.NICK:
        if (props.user) {
          let status = UserStatus.HERE;
          if (props.user.offline) {
            status = UserStatus.OFFLINE;
          } else if (props.user.away) {
            status = UserStatus.GONE;
          }
          let realname = props.buffer.name;
          if (isMeaningfulRealname(props.user.realname, props.buffer.name)) {
            realname = strip(props.user.realname || "");
          }
          let details = [];
          if (props.user.username && props.user.hostname) {
            details.push(`${props.user.username}@${props.user.hostname}`);
          }
          if (props.user.account) {
            let desc = `This user is verified and has logged in to the server with the account ${props.user.account}.`;
            let item;
            if (props.user.account === props.buffer.name) {
              item = "authenticated";
            } else {
              item = `authenticated as ${props.user.account}`;
            }
            details.push(html`<abbr title=${desc}>${item}</abbr>`);
          } else if (props.server.reliableUserAccounts) {
            let desc = "This user has not been verified and is not logged in.";
            details.push(html`<abbr title=${desc}>unauthenticated</abbr>`);
          }
          if (props.user.operator) {
            let desc = "This user is a server operator, they have administrator privileges.";
            details.push(html`<abbr title=${desc}>server operator</abbr>`);
          }
          details = details.map((item, i2) => {
            if (i2 === 0) {
              return item;
            }
            return [", ", item];
          });
          if (details.length > 0) {
            details = ["(", details, ")"];
          }
          description = html`<${NickStatus} status=${status}/> ${realname} ${details}`;
        }
        actions = html`
			<button
				key="close"
				class="danger"
				onClick=${props.onClose}
			>Close</button>
		`;
        break;
    }
    let name = props.buffer.name;
    if (props.buffer.type == BufferType.SERVER) {
      name = getServerName(props.server, props.bouncerNetwork);
    }
    return html`
		<div class="title">${name}</div>
		${description ? html`<div class="description">${description}</div>` : null}
		<div class="actions">${actions}</div>
	`;
  }

  // gamja/components/member-list.js
  var MemberItem = class extends _ {
    constructor(props) {
      super(props);
      this.handleClick = this.handleClick.bind(this);
    }
    shouldComponentUpdate(nextProps) {
      return this.props.nick !== nextProps.nick || this.props.membership !== nextProps.membership || this.props.user !== nextProps.user;
    }
    handleClick(event) {
      event.preventDefault();
      this.props.onClick();
    }
    render() {
      const membmap = {
        "~": "owner",
        "&": "admin",
        "@": "op",
        "%": "halfop",
        "+": "voice"
      };
      const membclass = membmap[this.props.membership[0]] || "";
      let membership = "";
      if (this.props.membership) {
        membership = html`
				<span class="membership ${membclass}" title=${membclass}>
					${this.props.membership}
				</span>
			`;
      }
      ;
      let title = null;
      let user = this.props.user;
      let classes2 = ["nick"];
      if (user) {
        let mask = "";
        if (user.username && user.hostname) {
          mask = `${user.username}@${user.hostname}`;
        }
        if (isMeaningfulRealname(user.realname, this.props.nick)) {
          title = strip(user.realname);
          if (mask) {
            title = `${title} (${mask})`;
          }
        } else {
          title = mask;
        }
        if (user.account) {
          title += `
Authenticated as ${user.account}`;
        }
        if (user.away) {
          classes2.push("away");
          title += "\nAway";
        }
      }
      return html`
			<li>
				<a
					href=${getNickURL(this.props.nick)}
					class=${classes2.join(" ")}
					title=${title}
					onClick=${this.handleClick}
				>
					<${Membership} value=${this.props.membership}/>
					${this.props.nick}
				</a>
			</li>
		`;
    }
  };
  function sortMembers(a3, b2) {
    let [nickA, membA] = a3, [nickB, membB] = b2;
    const prefixPrivs = ["~", "&", "@", "%", "+"];
    let i2 = prefixPrivs.indexOf(membA[0]), j2 = prefixPrivs.indexOf(membB[0]);
    if (i2 < 0) {
      i2 = prefixPrivs.length;
    }
    if (j2 < 0) {
      j2 = prefixPrivs.length;
    }
    if (i2 !== j2) {
      return i2 - j2;
    }
    return nickA < nickB ? -1 : 1;
  }
  var MemberList = class extends _ {
    shouldComponentUpdate(nextProps) {
      return this.props.members !== nextProps.members || this.props.users !== nextProps.users;
    }
    render() {
      return html`
			<ul>
				${Array.from(this.props.members).sort(sortMembers).map(([nick, membership]) => html`
					<${MemberItem}
						key=${nick}
						nick=${nick}
						membership=${membership}
						user=${this.props.users.get(nick)}
						onClick=${() => this.props.onNickClick(nick)}
					/>
				`)}
			</ul>
		`;
    }
  };

  // gamja/components/connect-form.js
  var ConnectForm = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", {
        url: "",
        pass: "",
        nick: "",
        password: "",
        rememberMe: false,
        username: "",
        realname: "",
        autojoin: true
      });
      __publicField(this, "nickInput", p());
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      if (props.params) {
        this.state = __spreadProps(__spreadValues({}, this.state), {
          url: props.params.url || "",
          nick: props.params.nick || "",
          rememberMe: props.params.autoconnect || false,
          username: props.params.username || "",
          realname: props.params.realname || ""
        });
      }
    }
    handleChange(event) {
      let target = event.target;
      let value = target.type == "checkbox" ? target.checked : target.value;
      this.setState({ [target.name]: value });
    }
    handleSubmit(event) {
      event.preventDefault();
      if (this.props.connecting) {
        return;
      }
      let params = {
        url: this.state.url,
        pass: this.state.pass,
        nick: this.state.nick,
        autoconnect: this.state.rememberMe,
        username: this.state.username,
        realname: this.state.realname,
        saslPlain: null,
        autojoin: []
      };
      if (this.state.password) {
        params.saslPlain = {
          username: params.username || params.nick,
          password: this.state.password
        };
      } else if (this.props.auth === "external") {
        params.saslExternal = true;
      }
      if (this.state.autojoin) {
        params.autojoin = this.props.params.autojoin || [];
      }
      this.props.onSubmit(params);
    }
    componentDidMount() {
      if (this.nickInput.current) {
        this.nickInput.current.focus();
      }
    }
    render() {
      let disabled = this.props.connecting;
      let serverURL = null;
      if (!this.props.params || !this.props.params.url) {
        serverURL = html`
				<label>
					Server URL:<br/>
					<input
						type="text"
						name="url"
						value=${this.state.url}
						disabled=${disabled}
						inputmode="url"
					/>
				</label>
				<br/><br/>
			`;
      }
      let status = null;
      if (this.props.connecting) {
        status = html`
				<p>Connecting...</p>
			`;
      } else if (this.props.error) {
        status = html`
				<p class="error-text">${linkify(this.props.error)}</p>
			`;
      }
      let auth = null;
      if (this.props.auth !== "disabled" && this.props.auth !== "external") {
        auth = html`
				<label>
					Password:<br/>
					<input
						type="password"
						name="password"
						value=${this.state.password}
						disabled=${disabled}
						required=${this.props.auth === "mandatory"}
						placeholder=${this.props.auth !== "mandatory" ? "(optional)" : ""}
					/>
				</label>
				<br/><br/>
			`;
      }
      let autojoin = null;
      let channels = this.props.params.autojoin || [];
      if (channels.length > 0) {
        let s2 = channels.length > 1 ? "s" : "";
        autojoin = html`
				<label>
					<input
						type="checkbox"
						name="autojoin"
						checked=${this.state.autojoin}
					/>
					Auto-join channel${s2} <strong>${channels.join(", ")}</strong>
				</label>
				<br/><br/>
			`;
      }
      return html`
			<form onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<h2>Connect to IRC</h2>

				<label>
					Nickname:<br/>
					<input
						type="username"
						name="nick"
						value=${this.state.nick}
						disabled=${disabled}
						ref=${this.nickInput}
						required
					/>
				</label>
				<br/><br/>

				${auth}

				${autojoin}

				<label>
					<input
						type="checkbox"
						name="rememberMe"
						checked=${this.state.rememberMe}
						disabled=${disabled}
					/>
					Remember me
				</label>
				<br/><br/>

				<details>
					<summary role="button">Advanced options</summary>

					<br/>

					${serverURL}

					<label>
						Username:<br/>
						<input
							type="username"
							name="username"
							value=${this.state.username}
							disabled=${disabled}
							placeholder="Same as nickname"
						/>
					</label>
					<br/><br/>

					<label>
						Real name:<br/>
						<input
							type="text"
							name="realname"
							value=${this.state.realname}
							disabled=${disabled}
							placeholder="Same as nickname"
						/>
					</label>
					<br/><br/>

					<label>
						Server password:<br/>
						<input
							type="password"
							name="pass"
							value=${this.state.pass}
							disabled=${disabled}
							placeholder="None"
						/>
					</label>
					<br/><br/>
				</details>

				<br/>
				<button disabled=${disabled}>Connect</button>

				${status}
			</form>
		`;
    }
  };

  // gamja/components/join-form.js
  var JoinForm = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", {
        channel: "#"
      });
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      if (props.channel) {
        this.state.channel = props.channel;
      }
    }
    handleChange(event) {
      let target = event.target;
      let value = target.type == "checkbox" ? target.checked : target.value;
      this.setState({ [target.name]: value });
    }
    handleSubmit(event) {
      event.preventDefault();
      let params = {
        channel: this.state.channel
      };
      this.props.onSubmit(params);
    }
    render() {
      return html`
			<form onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<label>
					Channel:<br/>
					<input type="text" name="channel" value=${this.state.channel} autofocus required/>
				</label>
				<br/>

				<br/>
				<button>Join</button>
			</form>
		`;
    }
  };

  // gamja/keybindings.js
  function getSiblingBuffer(buffers, bufID, delta) {
    let bufList = Array.from(buffers.values());
    let i2 = bufList.findIndex((buf) => buf.id === bufID);
    if (i2 < 0) {
      return null;
    }
    i2 = (i2 + bufList.length + delta) % bufList.length;
    return bufList[i2];
  }
  var keybindings = [
    {
      key: "h",
      altKey: true,
      description: "Mark all messages as read",
      execute: (app) => {
        app.setState((state) => {
          let buffers = /* @__PURE__ */ new Map();
          state.buffers.forEach((buf) => {
            buffers.set(buf.id, __spreadProps(__spreadValues({}, buf), {
              unread: Unread.NONE,
              prevReadReceipt: null
            }));
            let receipts = {};
            if (buf.messages.length > 0) {
              let lastMsg = buf.messages[buf.messages.length - 1];
              receipts[ReceiptType.READ] = receiptFromMessage(lastMsg);
            }
            let client = app.clients.get(buf.server);
            app.bufferStore.put({
              name: buf.name,
              server: client.params,
              unread: Unread.NONE,
              receipts
            });
          });
          return { buffers };
        });
      }
    },
    {
      key: "a",
      altKey: true,
      description: "Jump to next buffer with activity",
      execute: (app) => {
        let firstServerBuffer = null;
        let target = null;
        for (let buf of app.state.buffers.values()) {
          if (!firstServerBuffer && buf.type === BufferType.SERVER) {
            firstServerBuffer = buf;
          }
          if (buf.unread === Unread.NONE) {
            continue;
          }
          if (!target || Unread.compare(buf.unread, target.unread) > 0) {
            target = buf;
          }
        }
        if (!target) {
          target = firstServerBuffer;
        }
        if (target) {
          app.switchBuffer(target);
        }
      }
    },
    {
      key: "ArrowUp",
      altKey: true,
      description: "Jump to the previous buffer",
      execute: (app) => {
        let prev = getSiblingBuffer(app.state.buffers, app.state.activeBuffer, -1);
        if (prev) {
          app.switchBuffer(prev);
        }
      }
    },
    {
      key: "ArrowDown",
      altKey: true,
      description: "Jump to the next buffer",
      execute: (app) => {
        let next = getSiblingBuffer(app.state.buffers, app.state.activeBuffer, 1);
        if (next) {
          app.switchBuffer(next);
        }
      }
    }
  ];
  function setup(app) {
    let byKey = {};
    keybindings.forEach((binding) => {
      if (!byKey[binding.key]) {
        byKey[binding.key] = [];
      }
      byKey[binding.key].push(binding);
    });
    window.addEventListener("keydown", (event) => {
      let candidates = byKey[event.key];
      if (!candidates) {
        return;
      }
      candidates = candidates.filter((binding) => {
        return !!binding.altKey == event.altKey && !!binding.ctrlKey == event.ctrlKey;
      });
      if (candidates.length != 1) {
        return;
      }
      event.preventDefault();
      candidates[0].execute(app);
    });
  }

  // gamja/commands.js
  function getActiveClient(app) {
    let buf = app.state.buffers.get(app.state.activeBuffer);
    if (!buf) {
      throw new Error("Not connected to server");
    }
    return app.clients.get(buf.server);
  }
  function getActiveTarget(app) {
    let activeBuffer = app.state.buffers.get(app.state.activeBuffer);
    if (!activeBuffer) {
      throw new Error("Not in a buffer");
    }
    return activeBuffer.name;
  }
  function getActiveChannel(app) {
    let activeBuffer = app.state.buffers.get(app.state.activeBuffer);
    if (!activeBuffer || activeBuffer.type !== BufferType.CHANNEL) {
      throw new Error("Not in a channel");
    }
    return activeBuffer.name;
  }
  function setUserHostMode(app, args, mode) {
    let nick = args[0];
    if (!nick) {
      throw new Error("Missing nick");
    }
    let activeChannel = getActiveChannel(app);
    let client = getActiveClient(app);
    client.whois(nick).then((whois) => {
      const info = whois[RPL_WHOISUSER].params;
      const user = info[2];
      const host = info[3];
      client.send({
        command: "MODE",
        params: [activeChannel, mode, `*!${user}@${host}`]
      });
    });
  }
  function markServerBufferUnread(app) {
    let activeBuffer = app.state.buffers.get(app.state.activeBuffer);
    if (!activeBuffer || activeBuffer.type === BufferType.SERVER) {
      return;
    }
    app.setBufferState({ server: activeBuffer.server }, (buf) => {
      return { unread: Unread.union(buf.unread, Unread.MESSAGE) };
    });
  }
  var join = {
    usage: "<name> [password]",
    description: "Join a channel",
    execute: (app, args) => {
      let channel = args[0];
      if (!channel) {
        throw new Error("Missing channel name");
      }
      if (args.length > 1) {
        app.open(channel, null, args[1]);
      } else {
        app.open(channel);
      }
    }
  };
  var kick = {
    usage: "<nick> [comment]",
    description: "Remove a user from the channel",
    execute: (app, args) => {
      let nick = args[0];
      let activeChannel = getActiveChannel(app);
      let params = [activeChannel, nick];
      if (args.length > 1) {
        params.push(args.slice(1).join(" "));
      }
      getActiveClient(app).send({ command: "KICK", params });
    }
  };
  function givemode(app, args, mode) {
    let nick = args[0];
    if (!nick) {
      throw new Error("Missing nick");
    }
    let activeChannel = getActiveChannel(app);
    getActiveClient(app).send({
      command: "MODE",
      params: [activeChannel, mode, nick]
    });
  }
  var commands_default = {
    "away": {
      usage: "[message]",
      description: "Set away message",
      execute: (app, args) => {
        const params = [];
        if (args.length) {
          params.push(args.join(" "));
        }
        getActiveClient(app).send({ command: "AWAY", params });
      }
    },
    "ban": {
      usage: "[nick]",
      description: "Ban a user from the channel, or display the current ban list",
      execute: (app, args) => {
        if (args.length == 0) {
          let activeChannel = getActiveChannel(app);
          getActiveClient(app).send({
            command: "MODE",
            params: [activeChannel, "+b"]
          });
        } else {
          return setUserHostMode(app, args, "+b");
        }
      }
    },
    "buffer": {
      usage: "<name>",
      description: "Switch to a buffer",
      execute: (app, args) => {
        let name = args[0];
        for (let buf of app.state.buffers.values()) {
          if (buf.name === name) {
            app.switchBuffer(buf);
            return;
          }
        }
        throw new Error("Unknown buffer");
      }
    },
    "close": {
      description: "Close the current buffer",
      execute: (app, args) => {
        let activeBuffer = app.state.buffers.get(app.state.activeBuffer);
        if (!activeBuffer || activeBuffer.type == BufferType.SERVER) {
          throw new Error("Not in a user or channel buffer");
        }
        app.close(activeBuffer.id);
      }
    },
    "deop": {
      usage: "<nick>",
      description: "Remove operator status for a user on this channel",
      execute: (app, args) => givemode(app, args, "-o")
    },
    "devoice": {
      usage: "<nick>",
      description: "Remove voiced status for a user on this channel",
      execute: (app, args) => givemode(app, args, "-v")
    },
    "disconnect": {
      description: "Disconnect from the server",
      execute: (app, args) => {
        app.disconnect();
      }
    },
    "help": {
      description: "Show help menu",
      execute: (app, args) => {
        app.openHelp();
      }
    },
    "invite": {
      usage: "<nick>",
      description: "Invite a user to the channel",
      execute: (app, args) => {
        let nick = args[0];
        if (!nick) {
          throw new Error("Missing nick");
        }
        let activeChannel = getActiveChannel(app);
        getActiveClient(app).send({ command: "INVITE", params: [
          nick,
          activeChannel
        ] });
      }
    },
    "j": join,
    "join": join,
    "kick": kick,
    "kickban": {
      usage: "<target>",
      description: "Ban a user and removes them from the channel",
      execute: (app, args) => {
        kick.execute(app, args);
        ban.execute(app, args);
      }
    },
    "lusers": {
      usage: "[<mask> [<target>]]",
      description: "Request user statistics about the network",
      execute: (app, args) => {
        getActiveClient(app).send({ command: "LUSERS", params: args });
        markServerBufferUnread(app);
      }
    },
    "me": {
      usage: "<action>",
      description: "Send an action message to the current buffer",
      execute: (app, args) => {
        let action = args.join(" ");
        let target = getActiveTarget(app);
        let text2 = `ACTION ${action}`;
        app.privmsg(target, text2);
      }
    },
    "mode": {
      usage: "[target] [modes] [mode args...]",
      description: "Query or change a channel or user mode",
      execute: (app, args) => {
        let target = args[0];
        if (!target || target.startsWith("+") || target.startsWith("-")) {
          let activeChannel = getActiveChannel(app);
          args = [activeChannel, ...args];
        }
        getActiveClient(app).send({ command: "MODE", params: args });
      }
    },
    "motd": {
      usage: "[server]",
      description: "Get the Message Of The Day",
      execute: (app, args) => {
        getActiveClient(app).send({ command: "MOTD", params: args });
        markServerBufferUnread(app);
      }
    },
    "msg": {
      usage: "<target> <message>",
      description: "Send a message to a nickname or a channel",
      execute: (app, args) => {
        let target = args[0];
        let text2 = args.slice(1).join(" ");
        getActiveClient(app).send({ command: "PRIVMSG", params: [target, text2] });
      }
    },
    "nick": {
      usage: "<nick>",
      description: "Change current nickname",
      execute: (app, args) => {
        let newNick = args[0];
        getActiveClient(app).send({ command: "NICK", params: [newNick] });
      }
    },
    "notice": {
      usage: "<target> <message>",
      description: "Send a notice to a nickname or a channel",
      execute: (app, args) => {
        let target = args[0];
        let text2 = args.slice(1).join(" ");
        getActiveClient(app).send({ command: "NOTICE", params: [target, text2] });
      }
    },
    "op": {
      usage: "<nick>",
      description: "Give a user operator status on this channel",
      execute: (app, args) => givemode(app, args, "+o")
    },
    "part": {
      usage: "[reason]",
      description: "Leave a channel",
      execute: (app, args) => {
        let reason = args.join(" ");
        let activeChannel = getActiveChannel(app);
        let params = [activeChannel];
        if (reason) {
          params.push(reason);
        }
        getActiveClient(app).send({ command: "PART", params });
      }
    },
    "query": {
      usage: "<nick> [message]",
      description: "Open a buffer to send messages to a nickname",
      execute: (app, args) => {
        let nick = args[0];
        if (!nick) {
          throw new Error("Missing nickname");
        }
        app.open(nick);
        if (args.length > 1) {
          let text2 = args.slice(1).join(" ");
          app.privmsg(nick, text2);
        }
      }
    },
    "quiet": {
      usage: "[nick]",
      description: "Quiet a user in the channel, or display the current quiet list",
      execute: (app, args) => {
        if (args.length == 0) {
          getActiveClient(app).send({
            command: "MODE",
            params: [getActiveChannel(app), "+q"]
          });
        } else {
          return setUserHostMode(app, args, "+q");
        }
      }
    },
    "quit": {
      description: "Quit",
      execute: (app, args) => {
        app.close({ name: SERVER_BUFFER });
      }
    },
    "quote": {
      usage: "<command>",
      description: "Send a raw IRC command to the server",
      execute: (app, args) => {
        let msg;
        try {
          msg = parseMessage(args.join(" "));
        } catch (err) {
          throw new Error("Failed to parse IRC command: " + err.message);
        }
        getActiveClient(app).send(msg);
      }
    },
    "reconnect": {
      description: "Reconnect to the server",
      execute: (app, args) => {
        app.reconnect();
      }
    },
    "setname": {
      usage: "<realname>",
      description: "Change current realname",
      execute: (app, args) => {
        let newRealname = args.join(" ");
        let client = getActiveClient(app);
        if (!client.caps.enabled.has("setname")) {
          throw new Error("Server doesn't support changing the realname");
        }
        client.send({ command: "SETNAME", params: [newRealname] });
      }
    },
    "stats": {
      usage: "<query> [server]",
      description: "Request server statistics",
      execute: (app, args) => {
        let query = args[0];
        if (!query) {
          throw new Error("Missing query");
        }
        let params = [query];
        if (args.length > 1) {
          params.push(args.slice(1).join(" "));
        }
        getActiveClient(app).send({ command: "STATS", params });
        markServerBufferUnread(app);
      }
    },
    "topic": {
      usage: "<topic>",
      description: "Change the topic of the current channel",
      execute: (app, args) => {
        let activeChannel = getActiveChannel(app);
        let params = [activeChannel];
        if (args.length > 0) {
          params.push(args.join(" "));
        }
        getActiveClient(app).send({ command: "TOPIC", params });
      }
    },
    "unban": {
      usage: "<nick>",
      description: "Remove a user from the ban list",
      execute: (app, args) => {
        return setUserHostMode(app, args, "-b");
      }
    },
    "unquiet": {
      usage: "<nick>",
      description: "Remove a user from the quiet list",
      execute: (app, args) => {
        return setUserHostMode(app, args, "-q");
      }
    },
    "unvoice": {
      usage: "<nick>",
      description: "Remove a user from the voiced list",
      execute: (app, args) => givemode(app, args, "-v")
    },
    "voice": {
      usage: "<nick>",
      description: "Give a user voiced status on this channel",
      execute: (app, args) => givemode(app, args, "+v")
    },
    "who": {
      usage: "<mask>",
      description: "Retrieve a list of users",
      execute: (app, args) => {
        getActiveClient(app).send({ command: "WHO", params: args });
        markServerBufferUnread(app);
      }
    },
    "whois": {
      usage: "<nick>",
      description: "Retrieve information about a user",
      execute: (app, args) => {
        let nick = args[0];
        if (!nick) {
          throw new Error("Missing nick");
        }
        getActiveClient(app).send({ command: "WHOIS", params: [nick] });
        markServerBufferUnread(app);
      }
    },
    "whowas": {
      usage: "<nick> [count]",
      description: "Retrieve information about an offline user",
      execute: (app, args) => {
        if (args.length < 1) {
          throw new Error("Missing nick");
        }
        getActiveClient(app).send({ command: "WHOWAS", params: args });
        markServerBufferUnread(app);
      }
    },
    "list": {
      usage: "[filter]",
      description: "Retrieve a list of channels from a network",
      execute: (app, args) => {
        getActiveClient(app).send({ command: "LIST", params: args });
        markServerBufferUnread(app);
      }
    }
  };

  // gamja/components/help.js
  function KeyBindingsHelp() {
    let l2 = keybindings.map((binding) => {
      let keys = [];
      if (binding.ctrlKey) {
        keys.psuh("Ctrl");
      }
      if (binding.altKey) {
        keys.push("Alt");
      }
      keys.push(binding.key);
      keys = keys.map((name, i2) => {
        return html`
				${i2 > 0 ? "+" : null}
				<kbd>${name}</kbd>
			`;
      });
      return html`
			<dt>${keys}</dt>
			<dd>${binding.description}</dd>
		`;
    });
    if (!window.matchMedia("(pointer: none)").matches) {
      l2.push(html`
			<dt><strong>Middle mouse click</strong></dt>
			<dd>Close buffer</dd>
		`);
    }
    return html`<dl>${l2}</dl>`;
  }
  function CommandsHelp() {
    let l2 = Object.keys(commands_default).map((name) => {
      let cmd = commands_default[name];
      let usage = [html`<strong>/${name}</strong>`];
      if (cmd.usage) {
        usage.push(" " + cmd.usage);
      }
      return html`
			<dt><code>${usage}</code></dt>
			<dd>${cmd.description}</dd>
		`;
    });
    return html`<dl>${l2}</dl>`;
  }
  function Help() {
    return html`
		<h3>Key bindings</h3>
		<${KeyBindingsHelp}/>

		<h3>Commands</h3>
		<${CommandsHelp}/>
	`;
  }

  // gamja/components/network-form.js
  var defaultParams = {
    name: "",
    host: "",
    port: 6697,
    nickname: "",
    username: "",
    realname: "",
    pass: ""
  };
  var NetworkForm = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "prevParams", null);
      __publicField(this, "state", __spreadProps(__spreadValues({}, defaultParams), {
        autojoin: true
      }));
      this.prevParams = __spreadValues({}, defaultParams);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      if (props.params) {
        Object.keys(defaultParams).forEach((k2) => {
          if (props.params[k2] !== void 0) {
            this.state[k2] = props.params[k2];
            this.prevParams[k2] = props.params[k2];
          }
        });
      }
    }
    handleChange(event) {
      let target = event.target;
      let value = target.type == "checkbox" ? target.checked : target.value;
      this.setState({ [target.name]: value });
    }
    handleSubmit(event) {
      event.preventDefault();
      let params = {};
      Object.keys(defaultParams).forEach((k2) => {
        if (!this.props.isNew && this.prevParams[k2] == this.state[k2]) {
          return;
        }
        if (this.props.isNew && defaultParams[k2] == this.state[k2]) {
          return;
        }
        params[k2] = this.state[k2];
      });
      let autojoin = this.state.autojoin ? this.props.autojoin : null;
      this.props.onSubmit(params, autojoin);
    }
    render() {
      let removeNetwork = null;
      if (!this.props.isNew) {
        removeNetwork = html`
				<button type="button" class="danger" onClick=${() => this.props.onRemove()}>
					Remove network
				</button>
			`;
      }
      let autojoin = null;
      if (this.props.autojoin) {
        autojoin = html`
				<label>
					<input
						type="checkbox"
						name="autojoin"
						checked=${this.state.autojoin}
					/>
					Auto-join channel <strong>${this.props.autojoin}</strong>
				</label>
				<br/><br/>
			`;
      }
      return html`
			<form onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<label>
					Hostname:<br/>
					<input type="text" name="host" value=${this.state.host} autofocus required/>
				</label>
				<br/><br/>

				${autojoin}

				<details>
					<summary role="button">Advanced options</summary>

					<br/>

					<label>
						Port:<br/>
						<input type="number" name="port" value=${this.state.port}/>
					</label>
					<br/><br/>

					<label>
						Network name:<br/>
						<input type="text" name="name" value=${this.state.name}/>
					</label>
					<br/><br/>

					<label>
						Nickname:<br/>
						<input type="username" name="nickname" value=${this.state.nickname}/>
					</label>
					<br/><br/>

					<label>
						Username:<br/>
						<input type="username" name="username" value=${this.state.username}/>
					</label>
					<br/><br/>

					<label>
						Real name:<br/>
						<input type="text" name="realname" value=${this.state.realname}/>
					</label>
					<br/><br/>

					<label>
						Server password:<br/>
						<input type="password" name="pass" value=${this.state.pass} placeholder="None"/>
					</label>
					<br/>
				</details>

				<br/>
				${removeNetwork}
				${" "}
				<button>
					${this.props.isNew ? "Add network" : "Save network"}
				</button>
			</form>
		`;
    }
  };

  // gamja/components/auth-form.js
  var NetworkForm2 = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", {
        username: "",
        password: ""
      });
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      if (props.username) {
        this.state.username = props.username;
      }
    }
    handleChange(event) {
      let target = event.target;
      let value = target.type == "checkbox" ? target.checked : target.value;
      this.setState({ [target.name]: value });
    }
    handleSubmit(event) {
      event.preventDefault();
      this.props.onSubmit(this.state.username, this.state.password);
    }
    render() {
      return html`
			<form onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<label>
					Username:<br/>
					<input type="username" name="username" value=${this.state.username} required/>
				</label>
				<br/><br/>

				<label>
					Password:<br/>
					<input type="password" name="password" value=${this.state.password} required autofocus/>
				</label>
				<br/><br/>

				<button>Login</button>
			</form>
		`;
    }
  };

  // gamja/components/register-form.js
  var RegisterForm = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", {
        email: "",
        password: ""
      });
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
      let target = event.target;
      let value = target.type == "checkbox" ? target.checked : target.value;
      this.setState({ [target.name]: value });
    }
    handleSubmit(event) {
      event.preventDefault();
      this.props.onSubmit(this.state.email, this.state.password);
    }
    render() {
      return html`
			<form onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<label>
					E-mail:<br/>
					<input
						type="email"
						name="email"
						value=${this.state.email}
						required=${this.props.emailRequired}
						placeholder=${this.props.emailRequired ? null : "(optional)"}
						autofocus
					/>
				</label>
				<br/><br/>

				<label>
					Password:<br/>
					<input type="password" name="password" value=${this.state.password} required/>
				</label>
				<br/><br/>

				<button>Register</button>
			</form>
		`;
    }
  };

  // gamja/components/verify-form.js
  var RegisterForm2 = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", {
        code: ""
      });
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
      let target = event.target;
      let value = target.type == "checkbox" ? target.checked : target.value;
      this.setState({ [target.name]: value });
    }
    handleSubmit(event) {
      event.preventDefault();
      this.props.onSubmit(this.state.code);
    }
    render() {
      return html`
			<form onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<p>Your account <strong>${this.props.account}</strong> has been created, but a verification code is required to complete the registration.</p>

				<p>${linkify(this.props.message)}</p>

				<label>
					Verification code:<br/>
					<input type="text" name="code" value=${this.state.code} required autofocus autocomplete="off"/>
				</label>
				<br/><br/>

				<button>Verify account</button>
			</form>
		`;
    }
  };

  // gamja/components/composer.js
  var Composer = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", {
        text: ""
      });
      __publicField(this, "textInput", p());
      __publicField(this, "lastAutocomplete", null);
      this.handleInput = this.handleInput.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleInputKeyDown = this.handleInputKeyDown.bind(this);
      this.handleWindowKeyDown = this.handleWindowKeyDown.bind(this);
      this.handleWindowPaste = this.handleWindowPaste.bind(this);
    }
    handleInput(event) {
      this.setState({ [event.target.name]: event.target.value });
      if (this.props.readOnly && event.target.name === "text" && !event.target.value) {
        event.target.blur();
      }
    }
    handleSubmit(event) {
      event.preventDefault();
      this.props.onSubmit(this.state.text);
      this.setState({ text: "" });
    }
    handleInputKeyDown(event) {
      let input = event.target;
      if (!this.props.autocomplete || event.key !== "Tab") {
        return;
      }
      if (input.selectionStart !== input.selectionEnd) {
        return;
      }
      event.preventDefault();
      let carretPos = input.selectionStart;
      let text2 = this.state.text;
      let autocomplete;
      if (this.lastAutocomplete && this.lastAutocomplete.text === text2 && this.lastAutocomplete.carretPos === carretPos) {
        autocomplete = this.lastAutocomplete;
      } else {
        this.lastAutocomplete = null;
        let wordStart;
        for (wordStart = carretPos - 1; wordStart >= 0; wordStart--) {
          if (text2[wordStart] === " ") {
            break;
          }
        }
        wordStart++;
        let wordEnd;
        for (wordEnd = carretPos; wordEnd < text2.length; wordEnd++) {
          if (text2[wordEnd] === " ") {
            break;
          }
        }
        let word = text2.slice(wordStart, wordEnd);
        if (!word) {
          return;
        }
        let replacements = this.props.autocomplete(word);
        if (replacements.length === 0) {
          return;
        }
        autocomplete = {
          text: text2,
          carretPos: input.selectionStart,
          prefix: text2.slice(0, wordStart),
          suffix: text2.slice(wordEnd),
          replacements,
          replIndex: -1
        };
      }
      let n3 = autocomplete.replacements.length;
      if (event.shiftKey) {
        autocomplete.replIndex--;
      } else {
        autocomplete.replIndex++;
      }
      autocomplete.replIndex = (autocomplete.replIndex + n3) % n3;
      let repl = autocomplete.replacements[autocomplete.replIndex];
      if (!autocomplete.prefix && !autocomplete.suffix) {
        if (repl.startsWith("/")) {
          repl += " ";
        } else {
          repl += ": ";
        }
      }
      autocomplete.text = autocomplete.prefix + repl + autocomplete.suffix;
      autocomplete.carretPos = autocomplete.prefix.length + repl.length;
      input.value = autocomplete.text;
      input.selectionStart = autocomplete.carretPos;
      input.selectionEnd = input.selectionStart;
      this.lastAutocomplete = autocomplete;
      this.setState({ text: autocomplete.text });
    }
    handleWindowKeyDown(event) {
      if (document.activeElement !== document.body && document.activeElement.tagName !== "SECTION") {
        return;
      }
      if (event.altKey || event.ctrlKey || event.metaKey) {
        return;
      }
      if ([...event.key].length !== 1) {
        return;
      }
      if (this.state.text) {
        return;
      }
      if (this.props.readOnly || this.props.commandOnly && event.key !== "/") {
        return;
      }
      event.preventDefault();
      this.setState({ text: event.key }, () => {
        this.focus();
      });
    }
    handleWindowPaste(event) {
      if (document.activeElement !== document.body && document.activeElement.tagName !== "SECTION") {
        return;
      }
      if (this.props.readOnly) {
        return;
      }
      if (!this.textInput.current) {
        return;
      }
      let text2 = event.clipboardData.getData("text");
      event.preventDefault();
      event.stopImmediatePropagation();
      this.textInput.current.focus();
      this.textInput.current.setRangeText(text2, void 0, void 0, "end");
      this.setState({ text: this.textInput.current.value });
    }
    componentDidMount() {
      window.addEventListener("keydown", this.handleWindowKeyDown);
      window.addEventListener("paste", this.handleWindowPaste);
    }
    componentWillUnmount() {
      window.removeEventListener("keydown", this.handleWindowKeyDown);
      window.removeEventListener("paste", this.handleWindowPaste);
    }
    focus() {
      if (!this.textInput.current) {
        return;
      }
      document.activeElement.blur();
      this.textInput.current.focus();
    }
    render() {
      let className = "";
      if (this.props.readOnly && !this.state.text) {
        className = "read-only";
      }
      let placeholder = "Type a message";
      if (this.props.commandOnly) {
        placeholder = "Type a command (see /help)";
      }
      return html`
			<form
				id="composer"
				class=${className}
				onInput=${this.handleInput}
				onSubmit=${this.handleSubmit}
			>
				<input
					type="text"
					name="text"
					ref=${this.textInput}
					value=${this.state.text}
					autocomplete="off"
					placeholder=${placeholder}
					enterkeyhint="send"
					onKeyDown=${this.handleInputKeyDown}
				/>
			</form>
		`;
    }
  };

  // gamja/components/scroll-manager.js
  var store = /* @__PURE__ */ new Map();
  var ScrollManager = class extends _ {
    constructor(props) {
      super(props);
      this.handleScroll = this.handleScroll.bind(this);
    }
    isAtBottom() {
      let target = this.props.target.current;
      return target.scrollTop >= target.scrollHeight - target.offsetHeight;
    }
    saveScrollPosition() {
      let target = this.props.target.current;
      let sticky = target.querySelectorAll(this.props.stickTo);
      let stickToKey = null;
      if (!this.isAtBottom()) {
        for (let i2 = 0; i2 < sticky.length; i2++) {
          let el = sticky[i2];
          if (el.offsetTop >= target.scrollTop + target.offsetTop) {
            stickToKey = el.dataset.key;
            break;
          }
        }
      }
      store.set(this.props.scrollKey, stickToKey);
    }
    restoreScrollPosition() {
      let target = this.props.target.current;
      let stickToKey = store.get(this.props.scrollKey);
      if (!stickToKey) {
        target.firstChild.scrollIntoView({ block: "end" });
      } else {
        let stickTo = target.querySelector('[data-key="' + stickToKey + '"]');
        if (stickTo) {
          stickTo.scrollIntoView();
        }
      }
      if (target.scrollTop == 0) {
        this.props.onScrollTop();
      }
    }
    handleScroll() {
      if (this.props.target.current.scrollTop == 0) {
        this.props.onScrollTop();
      }
    }
    componentDidMount() {
      this.restoreScrollPosition();
      this.props.target.current.addEventListener("scroll", this.handleScroll);
    }
    componentWillReceiveProps(nextProps) {
      if (this.props.scrollKey !== nextProps.scrollKey || this.props.children !== nextProps.children) {
        this.saveScrollPosition();
      }
    }
    componentDidUpdate(prevProps) {
      if (!this.props.target.current) {
        return;
      }
      this.restoreScrollPosition();
    }
    componentWillUnmount() {
      this.props.target.current.removeEventListener("scroll", this.handleScroll);
      this.saveScrollPosition();
    }
    render() {
      return this.props.children;
    }
  };

  // gamja/components/dialog.js
  var Dialog = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "body", p());
      this.handleCloseClick = this.handleCloseClick.bind(this);
      this.handleBackdropClick = this.handleBackdropClick.bind(this);
      this.handleKeyDown = this.handleKeyDown.bind(this);
    }
    dismiss() {
      this.props.onDismiss();
    }
    handleCloseClick(event) {
      event.preventDefault();
      this.dismiss();
    }
    handleBackdropClick(event) {
      if (event.target.className == "dialog") {
        this.dismiss();
      }
    }
    handleKeyDown(event) {
      if (event.key == "Escape") {
        this.dismiss();
      }
    }
    componentDidMount() {
      window.addEventListener("keydown", this.handleKeyDown);
      let autofocus = this.body.current.querySelector("input[autofocus]");
      if (autofocus) {
        autofocus.focus();
      }
    }
    componentWillUnmount() {
      window.removeEventListener("keydown", this.handleKeyDown);
    }
    render() {
      return html`
			<div class="dialog" onClick=${this.handleBackdropClick}>
				<div class="dialog-body" ref=${this.body}>
					<div class="dialog-header">
						<h2>${this.props.title}</h2>
						<button class="dialog-close" onClick=${this.handleCloseClick}>×</button>
					</div>
					${this.props.children}
				</div>
			</div>
		`;
    }
  };

  // gamja/components/app.js
  var baseConfig = {
    server: {}
  };
  var configPromise = fetch("./config.json").then((resp) => {
    if (resp.ok) {
      return resp.json();
    }
    if (resp.status !== 404) {
      console.error("Failed to fetch config: HTTP error:", resp.status, resp.statusText);
    }
    return {};
  }).catch((err) => {
    console.error("Failed to fetch config:", err);
    return {};
  }).then((config) => {
    return __spreadValues(__spreadValues({}, baseConfig), config);
  });
  var CHATHISTORY_MAX_SIZE = 4e3;
  function isProduction() {
    try {
      return true;
    } catch (err) {
      return false;
    }
  }
  function parseQueryString() {
    let query = window.location.search.substring(1);
    let params = {};
    query.split("&").forEach((s2) => {
      if (!s2) {
        return;
      }
      let pair = s2.split("=");
      params[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || "");
    });
    return params;
  }
  function splitHostPort(str) {
    let host = str;
    let port = null;
    let i2 = str.lastIndexOf(":");
    if (i2 > 0 && !str.endsWith("]")) {
      host = str.slice(0, i2);
      port = parseInt(str.slice(i2 + 1), 10);
    }
    if (host.startsWith("[") && host.endsWith("]")) {
      host = host.slice(1, host.length - 1);
    }
    return { host, port };
  }
  function fillConnectParams(params) {
    let host = window.location.host || "localhost:8080";
    let proto = "wss:";
    if (window.location.protocol != "https:") {
      proto = "ws:";
    }
    let path = window.location.pathname || "/";
    if (!window.location.host) {
      path = "/";
    }
    params = __spreadValues({}, params);
    if (!params.url) {
      params.url = proto + "//" + host + path + "socket";
    }
    if (params.url.startsWith("/")) {
      params.url = proto + "//" + host + params.url;
    }
    if (params.url.indexOf("://") < 0) {
      params.url = proto + "//" + params.url;
    }
    if (!params.username) {
      params.username = params.nick;
    }
    if (!params.realname) {
      params.realname = params.nick;
    }
    return params;
  }
  function showNotification(title, options2) {
    if (!window.Notification || Notification.permission !== "granted") {
      return null;
    }
    try {
      return new Notification(title, options2);
    } catch (err) {
      console.error("Failed to show notification: ", err);
      return null;
    }
  }
  function getReceipt(stored, type) {
    if (!stored || !stored.receipts) {
      return null;
    }
    return stored.receipts[ReceiptType.READ];
  }
  function getLatestReceipt(bufferStore, server, type) {
    let buffers = bufferStore.list(server);
    let last = null;
    for (let buf of buffers) {
      if (buf.name === "*") {
        continue;
      }
      let receipt = getReceipt(buf, type);
      if (isReceiptBefore(last, receipt)) {
        last = receipt;
      }
    }
    return last;
  }
  var lastErrorID = 0;
  var App = class extends _ {
    constructor(props) {
      super(props);
      __publicField(this, "state", __spreadProps(__spreadValues({}, State2.create()), {
        connectParams: {
          url: null,
          pass: null,
          username: null,
          realname: null,
          nick: null,
          saslPlain: null,
          saslExternal: false,
          autoconnect: false,
          autojoin: [],
          ping: 0
        },
        connectForm: true,
        loading: true,
        dialog: null,
        dialogData: null,
        error: null,
        openPanels: {
          bufferList: false,
          memberList: false
        }
      }));
      __publicField(this, "debug", !isProduction());
      __publicField(this, "config", __spreadValues({}, baseConfig));
      __publicField(this, "clients", /* @__PURE__ */ new Map());
      __publicField(this, "endOfHistory", /* @__PURE__ */ new Map());
      __publicField(this, "receipts", /* @__PURE__ */ new Map());
      __publicField(this, "buffer", p());
      __publicField(this, "composer", p());
      __publicField(this, "switchToChannel", null);
      __publicField(this, "autoOpenURL", null);
      __publicField(this, "messageNotifications", /* @__PURE__ */ new Set());
      this.handleConnectSubmit = this.handleConnectSubmit.bind(this);
      this.handleJoinSubmit = this.handleJoinSubmit.bind(this);
      this.handleBufferListClick = this.handleBufferListClick.bind(this);
      this.handleBufferListClose = this.handleBufferListClose.bind(this);
      this.toggleBufferList = this.toggleBufferList.bind(this);
      this.toggleMemberList = this.toggleMemberList.bind(this);
      this.handleComposerSubmit = this.handleComposerSubmit.bind(this);
      this.handleChannelClick = this.handleChannelClick.bind(this);
      this.handleNickClick = this.handleNickClick.bind(this);
      this.autocomplete = this.autocomplete.bind(this);
      this.handleBufferScrollTop = this.handleBufferScrollTop.bind(this);
      this.dismissDialog = this.dismissDialog.bind(this);
      this.handleAddNetworkClick = this.handleAddNetworkClick.bind(this);
      this.handleNetworkSubmit = this.handleNetworkSubmit.bind(this);
      this.handleNetworkRemove = this.handleNetworkRemove.bind(this);
      this.handleDismissError = this.handleDismissError.bind(this);
      this.handleAuthSubmit = this.handleAuthSubmit.bind(this);
      this.handleRegisterSubmit = this.handleRegisterSubmit.bind(this);
      this.handleVerifyClick = this.handleVerifyClick.bind(this);
      this.handleVerifySubmit = this.handleVerifySubmit.bind(this);
      this.bufferStore = new Buffer2();
      configPromise.then((config) => {
        this.handleConfig(config);
        return config;
      });
    }
    handleConfig(config) {
      this.setState({ loading: false });
      let connectParams = __spreadValues({}, this.state.connectParams);
      if (config.server) {
        if (typeof config.server.url === "string") {
          connectParams.url = config.server.url;
        }
        if (Array.isArray(config.server.autojoin)) {
          connectParams.autojoin = config.server.autojoin;
        } else if (typeof config.server.autojoin === "string") {
          connectParams.autojoin = [config.server.autojoin];
        }
        if (typeof config.server.nick === "string") {
          connectParams.nick = config.server.nick;
        }
        if (typeof config.server.autoconnect === "boolean") {
          connectParams.autoconnect = config.server.autoconnect;
        }
        if (config.server.auth === "external") {
          connectParams.saslExternal = true;
        }
        if (typeof config.server.ping === "number") {
          connectParams.ping = config.server.ping;
        }
      }
      let autoconnect2 = autoconnect.load();
      if (autoconnect2) {
        connectParams = __spreadProps(__spreadValues(__spreadValues({}, connectParams), autoconnect2), {
          autoconnect: true,
          autojoin: []
        });
      }
      let autojoin = [];
      let queryParams = parseQueryString();
      if (typeof queryParams.server === "string" && (!connectParams.url || !queryParams.server)) {
        connectParams.url = queryParams.server;
        config.server.auth = null;
      }
      if (typeof queryParams.nick === "string") {
        connectParams.nick = queryParams.nick;
      }
      if (typeof queryParams.channels === "string") {
        autojoin = queryParams.channels.split(",");
      }
      if (typeof queryParams.open === "string") {
        this.autoOpenURL = parseURL(queryParams.open);
      }
      if (queryParams.debug === "1") {
        this.debug = true;
      }
      if (window.location.hash) {
        autojoin = window.location.hash.split(",");
      }
      this.config = config;
      if (autojoin.length > 0) {
        if (connectParams.autoconnect) {
          this.autoOpenURL = { host: "", entity: autojoin[0] };
        } else {
          connectParams.autojoin = autojoin;
        }
      }
      this.setState({ connectParams });
      if (connectParams.autoconnect) {
        this.setState({ connectForm: false });
        this.connect(connectParams);
      }
    }
    showError(err) {
      console.error("App error: ", err);
      let text2;
      if (err instanceof Error) {
        let l2 = [];
        while (err) {
          l2.push(err.message);
          err = err.cause;
        }
        text2 = l2.join(": ");
      } else {
        text2 = String(err);
      }
      this.setState({ error: text2 });
      lastErrorID++;
      return lastErrorID;
    }
    dismissError(id) {
      if (id && id !== lastErrorID) {
        return;
      }
      this.setState({ error: null });
    }
    handleDismissError(event) {
      event.preventDefault();
      this.dismissError();
    }
    setServerState(id, updater, callback) {
      this.setState((state) => {
        return State2.updateServer(state, id, updater);
      }, callback);
    }
    setBufferState(id, updater, callback) {
      this.setState((state) => {
        return State2.updateBuffer(state, id, updater);
      }, callback);
    }
    syncBufferUnread(serverID, name) {
      let client = this.clients.get(serverID);
      let stored = this.bufferStore.get({ name, server: client.params });
      if (client.caps.enabled.has("draft/chathistory") && stored) {
        this.setBufferState({ server: serverID, name }, { unread: stored.unread });
      }
      if (!stored) {
        this.bufferStore.put({
          name,
          server: client.params,
          unread: Unread.NONE
        });
      }
    }
    createBuffer(serverID, name) {
      let client = this.clients.get(serverID);
      let id = null;
      let isNew = false;
      this.setState((state) => {
        let updated;
        [id, updated] = State2.createBuffer(state, name, serverID, client);
        isNew = !!updated;
        return updated;
      });
      if (isNew) {
        this.syncBufferUnread(serverID, name);
      }
      return id;
    }
    sendReadReceipt(client, storedBuffer) {
      if (!client.caps.enabled.has("soju.im/read")) {
        return;
      }
      let readReceipt = storedBuffer.receipts[ReceiptType.READ];
      if (storedBuffer.name === "*" || !readReceipt) {
        return;
      }
      client.send({
        command: "READ",
        params: [storedBuffer.name, "timestamp=" + readReceipt.time]
      });
    }
    switchBuffer(id) {
      let buf;
      this.setState((state) => {
        buf = State2.getBuffer(state, id);
        if (!buf) {
          return;
        }
        let client = this.clients.get(buf.server);
        let stored = this.bufferStore.get({ name: buf.name, server: client.params });
        let prevReadReceipt = getReceipt(stored, ReceiptType.READ);
        let update = State2.updateBuffer(state, buf.id, {
          unread: Unread.NONE,
          prevReadReceipt
        });
        return __spreadProps(__spreadValues({}, update), { activeBuffer: buf.id });
      }, () => {
        if (!buf) {
          return;
        }
        if (this.buffer.current) {
          this.buffer.current.focus();
        }
        let client = this.clients.get(buf.server);
        for (let notif of this.messageNotifications) {
          if (client.cm(notif.data.bufferName) === client.cm(buf.name)) {
            notif.close();
          }
        }
        if (buf.messages.length > 0) {
          let lastMsg = buf.messages[buf.messages.length - 1];
          let stored = {
            name: buf.name,
            server: client.params,
            unread: Unread.NONE,
            receipts: { [ReceiptType.READ]: receiptFromMessage(lastMsg) }
          };
          if (this.bufferStore.put(stored)) {
            this.sendReadReceipt(client, stored);
          }
        }
        let server = this.state.servers.get(buf.server);
        if (buf.type === BufferType.NICK && !server.users.has(buf.name)) {
          this.whoUserBuffer(buf.name, buf.server);
        }
      });
    }
    addMessage(serverID, bufName, msg) {
      let client = this.clients.get(serverID);
      msg.isHighlight = isHighlight(msg, client.nick, client.cm) || isServerBroadcast(msg);
      if (!msg.tags) {
        msg.tags = {};
      }
      if (!msg.tags.time) {
        msg.tags.time = formatDate(new Date());
      }
      let stored = this.bufferStore.get({ name: bufName, server: client.params });
      let deliveryReceipt = getReceipt(stored, ReceiptType.DELIVERED);
      let readReceipt = getReceipt(stored, ReceiptType.READ);
      let isDelivered = isMessageBeforeReceipt(msg, deliveryReceipt);
      let isRead = isMessageBeforeReceipt(msg, readReceipt);
      if (client.isMyNick(msg.prefix.name)) {
        isRead = true;
      }
      let msgUnread = Unread.NONE;
      if ((msg.command == "PRIVMSG" || msg.command == "NOTICE") && !isRead) {
        let target = msg.params[0];
        let text2 = msg.params[1];
        let kind;
        if (msg.isHighlight) {
          msgUnread = Unread.HIGHLIGHT;
          kind = "highlight";
        } else if (client.isMyNick(target)) {
          msgUnread = Unread.HIGHLIGHT;
          kind = "private message";
        } else {
          msgUnread = Unread.MESSAGE;
        }
        if (msgUnread == Unread.HIGHLIGHT && !isDelivered && !parseCTCP(msg)) {
          let title = "New " + kind + " from " + msg.prefix.name;
          if (client.isChannel(bufName)) {
            title += " in " + bufName;
          }
          let notif = showNotification(title, {
            body: strip(text2),
            requireInteraction: true,
            tag: "msg,server=" + serverID + ",from=" + msg.prefix.name + ",to=" + bufName,
            data: { bufferName: bufName, message: msg }
          });
          if (notif) {
            notif.addEventListener("click", () => {
              this.switchBuffer({ server: serverID, name: bufName });
            });
            notif.addEventListener("close", () => {
              this.messageNotifications.delete(notif);
            });
            this.messageNotifications.add(notif);
          }
        }
      }
      if (msg.command === "INVITE" && client.isMyNick(msg.params[0])) {
        msgUnread = Unread.HIGHLIGHT;
        let channel = msg.params[1];
        let notif = new Notification("Invitation to " + channel, {
          body: msg.prefix.name + " has invited you to " + channel,
          requireInteraction: true,
          tag: "invite,server=" + serverID + ",from=" + msg.prefix.name + ",channel=" + channel,
          actions: [{
            action: "accept",
            title: "Accept"
          }]
        });
        if (notif) {
          notif.addEventListener("click", (event) => {
            if (event.action === "accept") {
              let stored2 = {
                name: bufName,
                server: client.params,
                receipts: { [ReceiptType.READ]: receiptFromMessage(msg) }
              };
              if (this.bufferStore.put(stored2)) {
                this.sendReadReceipt(client, stored2);
              }
              this.open(channel, serverID);
            } else {
              this.switchBuffer({ server: serverID, name: bufName });
            }
          });
        }
      }
      if ((!client.isMyNick(msg.prefix.name) || client.isMyNick(bufName)) && (msg.command != "PART" && msg.comand != "QUIT")) {
        this.createBuffer(serverID, bufName);
      }
      let bufID = { server: serverID, name: bufName };
      this.setState((state) => State2.addMessage(state, msg, bufID));
      this.setBufferState(bufID, (buf) => {
        let unread = buf.unread;
        let prevReadReceipt = buf.prevReadReceipt;
        let receipts = { [ReceiptType.DELIVERED]: receiptFromMessage(msg) };
        if (this.state.activeBuffer !== buf.id) {
          unread = Unread.union(unread, msgUnread);
        } else {
          receipts[ReceiptType.READ] = receiptFromMessage(msg);
        }
        if (client.isMyNick(msg.prefix.name) && !isMessageBeforeReceipt(msg, prevReadReceipt)) {
          prevReadReceipt = receiptFromMessage(msg);
        }
        let stored2 = {
          name: buf.name,
          server: client.params,
          unread,
          receipts
        };
        if (this.bufferStore.put(stored2)) {
          this.sendReadReceipt(client, stored2);
        }
        return { unread, prevReadReceipt };
      });
    }
    connect(params) {
      let serverID = null;
      this.setState((state) => {
        let update;
        [serverID, update] = State2.createServer(state);
        return update;
      });
      this.setState({ connectParams: params });
      let client = new Client(fillConnectParams(params));
      client.debug = this.debug;
      this.clients.set(serverID, client);
      this.setServerState(serverID, { status: client.status });
      let errorID = null;
      client.addEventListener("status", () => {
        this.setServerState(serverID, { status: client.status });
        switch (client.status) {
          case Client.Status.DISCONNECTED:
            this.setServerState(serverID, { account: null });
            this.setState((state) => {
              let buffers = new Map(state.buffers);
              state.buffers.forEach((buf) => {
                if (buf.server !== serverID) {
                  return;
                }
                buffers.set(buf.id, __spreadProps(__spreadValues({}, buf), { joined: false }));
              });
              return { buffers };
            });
            break;
          case Client.Status.REGISTERED:
            this.setState({ connectForm: false });
            if (errorID) {
              this.dismissError(errorID);
            }
            break;
        }
      });
      client.addEventListener("message", (event) => {
        this.handleMessage(serverID, event.detail.message);
      });
      client.addEventListener("error", (event) => {
        errorID = this.showError(event.detail);
      });
      this.createBuffer(serverID, SERVER_BUFFER);
      if (!this.state.activeBuffer) {
        this.switchBuffer({ server: serverID, name: SERVER_BUFFER });
      }
      if (params.autojoin.length > 0) {
        this.switchToChannel = params.autojoin[0];
      }
    }
    disconnect(serverID) {
      if (!serverID) {
        serverID = State2.getActiveServerID(this.state);
      }
      let client = this.clients.get(serverID);
      if (client) {
        this.clients.delete(serverID);
        client.disconnect();
      }
    }
    reconnect(serverID) {
      if (!serverID) {
        serverID = State2.getActiveServerID(this.state);
      }
      let client = this.clients.get(serverID);
      if (client) {
        client.reconnect();
      }
    }
    serverFromBouncerNetwork(bouncerNetworkID) {
      for (let [id, client] of this.clients) {
        if (client.params.bouncerNetwork === bouncerNetworkID) {
          return id;
        }
      }
      return null;
    }
    routeMessage(serverID, msg) {
      let client = this.clients.get(serverID);
      let chatHistoryBatch = findBatchByType(msg, "chathistory");
      let target, channel, affectedBuffers;
      switch (msg.command) {
        case "MODE":
          target = msg.params[0];
          if (client.isChannel(target)) {
            return [target];
          }
          return [SERVER_BUFFER];
        case "NOTICE":
        case "PRIVMSG":
          target = msg.params[0];
          if (client.isMyNick(target)) {
            if (client.cm(msg.prefix.name) === client.cm(client.serverPrefix.name)) {
              target = SERVER_BUFFER;
            } else {
              target = msg.prefix.name;
            }
          }
          if (msg.command === "NOTICE" && !State2.getBuffer(this.state, { server: serverID, name: target })) {
            target = SERVER_BUFFER;
          }
          let allowedPrefixes = client.isupport.statusMsg();
          if (allowedPrefixes) {
            let parts = parseTargetPrefix(target, allowedPrefixes);
            if (client.isChannel(parts.name)) {
              target = parts.name;
            }
          }
          return [target];
        case "JOIN":
          channel = msg.params[0];
          if (!client.isMyNick(msg.prefix.name)) {
            return [channel];
          }
          return [];
        case "PART":
          channel = msg.params[0];
          return [channel];
        case "KICK":
          channel = msg.params[0];
          return [channel];
        case "QUIT":
          affectedBuffers = [];
          if (chatHistoryBatch) {
            affectedBuffers.push(chatHistoryBatch.params[0]);
          } else {
            this.state.buffers.forEach((buf) => {
              if (buf.server != serverID) {
                return;
              }
              if (!buf.members.has(msg.prefix.name) && client.cm(buf.name) !== client.cm(msg.prefix.name)) {
                return;
              }
              affectedBuffers.push(buf.name);
            });
          }
          return affectedBuffers;
        case "NICK":
          let newNick = msg.params[0];
          affectedBuffers = [];
          if (chatHistoryBatch) {
            affectedBuffers.push(chatHistoryBatch.params[0]);
          } else {
            this.state.buffers.forEach((buf) => {
              if (buf.server != serverID) {
                return;
              }
              if (!buf.members.has(msg.prefix.name)) {
                return;
              }
              affectedBuffers.push(buf.name);
            });
            if (client.isMyNick(newNick)) {
              affectedBuffers.push(SERVER_BUFFER);
            }
          }
          return affectedBuffers;
        case "TOPIC":
          channel = msg.params[0];
          return [channel];
        case "INVITE":
          channel = msg.params[1];
          let bufName = channel;
          if (!State2.getBuffer(this.state, { server: serverID, name: channel })) {
            bufName = SERVER_BUFFER;
          }
          return [bufName];
        case RPL_CHANNELMODEIS:
        case RPL_CREATIONTIME:
        case RPL_INVITELIST:
        case RPL_ENDOFINVITELIST:
        case RPL_EXCEPTLIST:
        case RPL_ENDOFEXCEPTLIST:
        case RPL_BANLIST:
        case RPL_ENDOFBANLIST:
        case RPL_QUIETLIST:
        case RPL_ENDOFQUIETLIST:
          channel = msg.params[1];
          return [channel];
        case RPL_INVITING:
          channel = msg.params[2];
          return [channel];
        case RPL_YOURHOST:
        case RPL_MYINFO:
        case RPL_ISUPPORT:
        case RPL_ENDOFMOTD:
        case ERR_NOMOTD:
        case RPL_AWAY:
        case RPL_NOTOPIC:
        case RPL_TOPIC:
        case RPL_TOPICWHOTIME:
        case RPL_NAMREPLY:
        case RPL_ENDOFNAMES:
        case RPL_MONONLINE:
        case RPL_MONOFFLINE:
        case RPL_SASLSUCCESS:
        case "AWAY":
        case "SETNAME":
        case "CHGHOST":
        case "ACCOUNT":
        case "CAP":
        case "AUTHENTICATE":
        case "PING":
        case "PONG":
        case "BATCH":
        case "TAGMSG":
        case "CHATHISTORY":
        case "ACK":
        case "BOUNCER":
        case "READ":
          return [];
        default:
          return [SERVER_BUFFER];
      }
    }
    handleMessage(serverID, msg) {
      let client = this.clients.get(serverID);
      let destBuffers = this.routeMessage(serverID, msg);
      if (findBatchByType(msg, "chathistory")) {
        destBuffers.forEach((bufName) => {
          this.addMessage(serverID, bufName, msg);
        });
        return;
      }
      this.setState((state) => State2.handleMessage(state, msg, serverID, client));
      let target, channel;
      switch (msg.command) {
        case RPL_WELCOME:
          this.fetchBacklog(serverID);
          break;
        case RPL_ENDOFMOTD:
        case ERR_NOMOTD:
          let join2 = [];
          for (let buf of this.bufferStore.list(client.params)) {
            if (buf.name === "*") {
              continue;
            }
            if (client.isChannel(buf.name)) {
              if (client.caps.enabled.has("soju.im/bouncer-networks")) {
                continue;
              }
              join2.push(buf.name);
            } else {
              this.createBuffer(serverID, buf.name);
              this.whoUserBuffer(buf.name, serverID);
            }
          }
          let server = this.state.servers.get(serverID);
          let bouncerNetID = server.bouncerNetID;
          let bouncerNetwork = null;
          if (bouncerNetID) {
            bouncerNetwork = this.state.bouncerNetworks.get(bouncerNetID);
          }
          if (!bouncerNetwork || bouncerNetwork.state === "connected") {
            join2 = join2.concat(client.params.autojoin);
            client.params.autojoin = [];
          }
          if (join2.length > 0) {
            client.send({
              command: "JOIN",
              params: [join2.join(",")]
            });
          }
          let serverHost = bouncerNetwork ? bouncerNetwork.host : "";
          if (this.autoOpenURL && serverHost === this.autoOpenURL.host) {
            this.openURL(this.autoOpenURL);
            this.autoOpenURL = null;
          }
        case "JOIN":
          channel = msg.params[0];
          if (client.isMyNick(msg.prefix.name)) {
            this.syncBufferUnread(serverID, channel);
          }
          if (channel == this.switchToChannel) {
            this.switchBuffer({ server: serverID, name: channel });
            this.switchToChannel = null;
          }
          break;
        case "BOUNCER":
          if (msg.params[0] !== "NETWORK") {
            break;
          }
          if (client.isupport.bouncerNetID()) {
            break;
          }
          let id = msg.params[1];
          let attrs = null;
          if (msg.params[2] !== "*") {
            attrs = parseTags(msg.params[2]);
          }
          let isNew = false;
          this.setState((state) => {
            if (!attrs) {
              return State2.deleteBouncerNetwork(state, id);
            } else {
              isNew = !state.bouncerNetworks.has(id);
              return State2.storeBouncerNetwork(state, id, attrs);
            }
          }, () => {
            if (!attrs) {
              let serverID2 = this.serverFromBouncerNetwork(id);
              if (serverID2) {
                this.close({ server: serverID2, name: SERVER_BUFFER });
              }
            } else if (isNew) {
              this.connect(__spreadProps(__spreadValues({}, client.params), {
                bouncerNetwork: id
              }));
            }
            if (attrs && attrs.state === "connected") {
              let serverID2 = this.serverFromBouncerNetwork(id);
              let client2 = this.clients.get(serverID2);
              if (client2 && client2.status === Client.Status.REGISTERED && client2.params.autojoin && client2.params.autojoin.length > 0) {
                client2.send({
                  command: "JOIN",
                  params: [client2.params.autojoin.join(",")]
                });
                client2.params.autojoin = [];
              }
            }
          });
          break;
        case "BATCH":
          if (!msg.params[0].startsWith("-")) {
            break;
          }
          let name = msg.params[0].slice(1);
          let batch = client.batches.get(name);
          if (!batch || batch.type !== "soju.im/bouncer-networks") {
            break;
          }
          if (this.autoOpenURL && this.autoOpenURL.host && !this.findBouncerNetIDByHost(this.autoOpenURL.host)) {
            this.openURL(this.autoOpenURL);
            this.autoOpenURL = null;
          }
          break;
        case "READ":
          target = msg.params[0];
          let bound = msg.params[1];
          if (!client.isMyNick(msg.prefix.name) || bound === "*" || !bound.startsWith("timestamp=")) {
            break;
          }
          let readReceipt = { time: bound.replace("timestamp=", "") };
          let stored = this.bufferStore.get({ name: target, server: client.params });
          if (isReceiptBefore(readReceipt, getReceipt(stored, ReceiptType.READ))) {
            break;
          }
          for (let notif of this.messageNotifications) {
            if (client.cm(notif.data.bufferName) !== client.cm(target)) {
              continue;
            }
            if (isMessageBeforeReceipt(notif.data.message, readReceipt)) {
              notif.close();
            }
          }
          this.setBufferState({ server: serverID, name: target }, (buf) => {
            let unread = Unread.NONE;
            for (let i2 = buf.messages.length - 1; i2 >= 0; i2--) {
              let msg2 = buf.messages[i2];
              if (msg2.command !== "PRIVMSG" && msg2.command !== "NOTICE") {
                continue;
              }
              if (isMessageBeforeReceipt(msg2, readReceipt)) {
                break;
              }
              if (msg2.isHighlight || client.isMyNick(buf.name)) {
                unread = Unread.HIGHLIGHT;
                break;
              }
              unread = Unread.MESSAGE;
            }
            this.bufferStore.put({
              name: target,
              server: client.params,
              unread,
              receipts: { [ReceiptType.READ]: readReceipt }
            });
            return { unread };
          });
          break;
        default:
          if (isError(msg.command) && msg.command != ERR_NOMOTD) {
            let description = msg.params[msg.params.length - 1];
            this.showError(description);
          }
      }
      destBuffers.forEach((bufName) => {
        this.addMessage(serverID, bufName, msg);
      });
    }
    fetchBacklog(serverID) {
      let client = this.clients.get(serverID);
      if (!client.caps.enabled.has("draft/chathistory")) {
        return;
      }
      if (client.caps.enabled.has("soju.im/bouncer-networks") && !client.params.bouncerNetwork) {
        return;
      }
      let lastReceipt = getLatestReceipt(this.bufferStore, client.params, ReceiptType.DELIVERED);
      if (!lastReceipt) {
        return;
      }
      let now = formatDate(new Date());
      client.fetchHistoryTargets(now, lastReceipt.time).then((targets) => {
        targets.forEach((target) => {
          let from = lastReceipt;
          let to = { time: now };
          let stored = this.bufferStore.get({ name: target.name, server: client.params });
          let readReceipt = getReceipt(stored, ReceiptType.READ);
          if (isReceiptBefore(from, readReceipt)) {
            from = readReceipt;
          }
          let buf = State2.getBuffer(this.state, { server: serverID, name: target.name });
          if (buf && buf.messages.length > 0) {
            let lastMsg = buf.messages[buf.messages.length - 1];
            from = receiptFromMessage(lastMsg);
          }
          client.fetchHistoryBetween(target.name, from, to, CHATHISTORY_MAX_SIZE).catch((err) => {
            console.error("Failed to fetch backlog for '" + target.name + "': ", err);
            this.showError("Failed to fetch backlog for '" + target.name + "'");
          });
        });
      });
    }
    handleConnectSubmit(connectParams) {
      this.dismissError();
      if (connectParams.autoconnect) {
        autoconnect.put(connectParams);
      } else {
        autoconnect.put(null);
      }
      let activeBuffer = this.state.buffers.get(this.state.activeBuffer);
      if (activeBuffer) {
        this.close(activeBuffer.server);
      }
      this.connect(connectParams);
    }
    handleChannelClick(event) {
      let handled = this.openURL(event.target.href);
      if (handled) {
        event.preventDefault();
      }
    }
    findBouncerNetIDByHost(host) {
      for (let [id, bouncerNetwork] of this.state.bouncerNetworks) {
        if (bouncerNetwork.host === host) {
          return id;
        }
      }
      return null;
    }
    openURL(url) {
      if (typeof url === "string") {
        url = parseURL(url);
      }
      if (!url) {
        return false;
      }
      let { host, port } = splitHostPort(url.host);
      let serverID;
      if (!url.host) {
        serverID = State2.getActiveServerID(this.state);
      } else {
        let bouncerNetID = this.findBouncerNetIDByHost(host);
        if (!bouncerNetID) {
          let client = this.clients.values().next().value;
          if (!client || !client.caps.enabled.has("soju.im/bouncer-networks")) {
            return false;
          }
          let params = { host };
          if (typeof port === "number") {
            params.port = port;
          }
          this.openDialog("network", { params, autojoin: url.entity });
          return true;
        }
        for (let [id, server] of this.state.servers) {
          if (server.bouncerNetID === bouncerNetID) {
            serverID = id;
            break;
          }
        }
      }
      if (!serverID) {
        return false;
      }
      let buf = State2.getBuffer(this.state, { server: serverID, name: url.entity || SERVER_BUFFER });
      if (buf) {
        this.switchBuffer(buf.id);
      } else {
        this.openDialog("join", { server: serverID, channel: url.entity });
      }
      return true;
    }
    handleNickClick(nick) {
      this.open(nick);
    }
    whoUserBuffer(target, serverID) {
      let client = this.clients.get(serverID);
      client.who(target, {
        fields: ["flags", "hostname", "nick", "realname", "username", "account"]
      });
      client.monitor(target);
      if (client.caps.enabled.has("soju.im/read")) {
        client.send({
          command: "READ",
          params: [target]
        });
      }
    }
    open(target, serverID, password) {
      if (!serverID) {
        serverID = State2.getActiveServerID(this.state);
      }
      let client = this.clients.get(serverID);
      if (client.isServer(target)) {
        this.switchBuffer({ server: serverID });
      } else if (client.isChannel(target)) {
        this.switchToChannel = target;
        client.join(target, password).catch((err) => {
          this.showError(err);
        });
      } else {
        this.whoUserBuffer(target, serverID);
        this.createBuffer(serverID, target);
        this.switchBuffer({ server: serverID, name: target });
      }
    }
    close(id) {
      let buf = State2.getBuffer(this.state, id);
      if (!buf) {
        return;
      }
      let client = this.clients.get(buf.server);
      switch (buf.type) {
        case BufferType.SERVER:
          this.setState((state) => {
            let buffers = new Map(state.buffers);
            for (let [id2, b2] of state.buffers) {
              if (b2.server === buf.server) {
                buffers.delete(id2);
              }
            }
            let activeBuffer = state.activeBuffer;
            if (activeBuffer && state.buffers.get(activeBuffer).server === buf.server) {
              if (buffers.size > 0) {
                activeBuffer = buffers.keys().next().value;
              } else {
                activeBuffer = null;
              }
            }
            return { buffers, activeBuffer };
          });
          let disconnectAll = client && !client.params.bouncerNetwork && client.caps.enabled.has("soju.im/bouncer-networks");
          this.disconnect(buf.server);
          this.setState((state) => {
            let servers = new Map(state.servers);
            servers.delete(buf.server);
            let connectForm = state.connectForm;
            if (servers.size == 0) {
              connectForm = true;
            }
            return { servers, connectForm };
          });
          if (disconnectAll) {
            for (let serverID of this.clients.keys()) {
              this.close({ server: serverID, name: SERVER_BUFFER });
            }
            this.bufferStore.clear();
          } else {
            this.bufferStore.clear(client.params);
          }
          if (buf.server == 1) {
            autoconnect.put(null);
          }
          break;
        case BufferType.CHANNEL:
          if (buf.joined) {
            client.send({ command: "PART", params: [buf.name] });
          }
        case BufferType.NICK:
          this.switchBuffer({ name: SERVER_BUFFER });
          this.setState((state) => {
            let buffers = new Map(state.buffers);
            buffers.delete(buf.id);
            return { buffers };
          });
          client.unmonitor(buf.name);
          this.bufferStore.delete({ name: buf.name, server: client.params });
          break;
      }
    }
    executeCommand(s2) {
      let parts = s2.split(" ");
      let name = parts[0].toLowerCase().slice(1);
      let args = parts.slice(1);
      let cmd = commands_default[name];
      if (!cmd) {
        this.showError(`Unknown command "${name}" (run "/help" to get a command list)`);
        return;
      }
      try {
        cmd.execute(this, args);
      } catch (error) {
        console.error(`Failed to execute command "${name}":`, error);
        this.showError(error.message);
      }
    }
    privmsg(target, text2) {
      if (target == SERVER_BUFFER) {
        this.showError("Cannot send message in server buffer");
        return;
      }
      let serverID = State2.getActiveServerID(this.state);
      let client = this.clients.get(serverID);
      let msg = { command: "PRIVMSG", params: [target, text2] };
      client.send(msg);
      if (!client.caps.enabled.has("echo-message")) {
        msg.prefix = { name: client.nick };
        this.addMessage(serverID, target, msg);
      }
    }
    handleComposerSubmit(text2) {
      if (!text2) {
        return;
      }
      if (text2.startsWith("//")) {
        text2 = text2.slice(1);
      } else if (text2.startsWith("/")) {
        this.executeCommand(text2);
        return;
      }
      let buf = this.state.buffers.get(this.state.activeBuffer);
      if (!buf) {
        return;
      }
      this.privmsg(buf.name, text2);
    }
    handleBufferListClick(id) {
      this.switchBuffer(id);
      this.closeBufferList();
    }
    handleBufferListClose(id) {
      this.close(id);
      this.closeBufferList();
    }
    toggleBufferList() {
      this.setState((state) => {
        let openPanels = __spreadProps(__spreadValues({}, state.openPanels), {
          bufferList: !state.openPanels.bufferList
        });
        return { openPanels };
      });
    }
    toggleMemberList() {
      this.setState((state) => {
        let openPanels = __spreadProps(__spreadValues({}, state.openPanels), {
          memberList: !state.openPanels.memberList
        });
        return { openPanels };
      });
    }
    closeBufferList() {
      this.setState((state) => {
        let openPanels = __spreadProps(__spreadValues({}, state.openPanels), {
          bufferList: false
        });
        return { openPanels };
      });
    }
    closeMemberList() {
      this.setState((state) => {
        let openPanels = __spreadProps(__spreadValues({}, state.openPanels), {
          memberList: false
        });
        return { openPanels };
      });
    }
    handleJoinClick(buf) {
      switch (buf.type) {
        case BufferType.SERVER:
          this.openDialog("join", { server: buf.server });
          break;
        case BufferType.CHANNEL:
          let client = this.clients.get(buf.server);
          client.send({ command: "JOIN", params: [buf.name] });
          break;
      }
    }
    handleJoinSubmit(data) {
      this.open(data.channel, this.state.dialogData.server);
      this.dismissDialog();
    }
    autocomplete(prefix) {
      function fromList(l2, prefix2) {
        prefix2 = prefix2.toLowerCase();
        let repl = [];
        for (let item of l2) {
          if (item.toLowerCase().startsWith(prefix2)) {
            repl.push(item);
          }
        }
        return repl;
      }
      if (prefix.startsWith("/")) {
        let repl = fromList(Object.keys(commands_default), prefix.slice(1));
        return repl.map((cmd) => "/" + cmd);
      }
      if (prefix.startsWith("#")) {
        let chanNames = [];
        for (const buf2 of this.state.buffers.values()) {
          if (buf2.name.startsWith("#")) {
            chanNames.push(buf2.name);
          }
        }
        return fromList(chanNames, prefix);
      }
      let buf = this.state.buffers.get(this.state.activeBuffer);
      if (!buf || !buf.members) {
        return [];
      }
      return fromList(buf.members.keys(), prefix);
    }
    openHelp() {
      this.openDialog("help");
    }
    handleBufferScrollTop() {
      let buf = this.state.buffers.get(this.state.activeBuffer);
      if (!buf || buf.type == BufferType.SERVER) {
        return;
      }
      let client = this.clients.get(buf.server);
      if (!client || !client.caps.enabled.has("draft/chathistory") || !client.caps.enabled.has("server-time")) {
        return;
      }
      if (this.endOfHistory.get(buf.id)) {
        return;
      }
      let before;
      if (buf.messages.length > 0) {
        before = buf.messages[0].tags["time"];
      } else {
        before = formatDate(new Date());
      }
      this.endOfHistory.set(buf.id, true);
      let limit = 100;
      if (client.caps.enabled.has("draft/event-playback")) {
        limit = 200;
      }
      client.fetchHistoryBefore(buf.name, before, limit).then((result) => {
        this.endOfHistory.set(buf.id, !result.more);
      });
    }
    openDialog(name, data) {
      this.setState({ dialog: name, dialogData: data });
    }
    dismissDialog() {
      this.setState({ dialog: null, dialogData: null });
    }
    setDialogLoading(promise) {
      const setLoading = (loading) => {
        this.setState((state) => {
          return { dialogData: __spreadProps(__spreadValues({}, state.dialogData), { loading }) };
        });
      };
      setLoading(true);
      promise.finally(() => setLoading(false));
    }
    handleAuthClick(serverID) {
      let client = this.clients.get(serverID);
      this.openDialog("auth", { username: client.nick });
    }
    handleAuthSubmit(username, password) {
      let serverID = State2.getActiveServerID(this.state);
      let client = this.clients.get(serverID);
      let promise = client.authenticate("PLAIN", { username, password }).then(() => {
        this.dismissDialog();
        let firstClient = this.clients.values().next().value;
        if (client !== firstClient) {
          return;
        }
        let autoconnect2 = autoconnect.load();
        if (!autoconnect2) {
          return;
        }
        console.log("Saving SASL PLAIN credentials");
        autoconnect2 = __spreadProps(__spreadValues({}, autoconnect2), {
          saslPlain: { username, password }
        });
        autoconnect.put(autoconnect2);
      });
      this.setDialogLoading(promise);
    }
    handleRegisterClick(serverID) {
      let client = this.clients.get(serverID);
      let emailRequired = client.checkAccountRegistrationCap("email-required");
      this.openDialog("register", { emailRequired });
    }
    handleRegisterSubmit(email, password) {
      let serverID = State2.getActiveServerID(this.state);
      let client = this.clients.get(serverID);
      let promise = client.registerAccount(email, password).then((data) => {
        this.dismissDialog();
        if (data.verificationRequired) {
          this.handleVerifyClick(data.account, data.message);
        }
        let firstClient = this.clients.values().next().value;
        if (client !== firstClient) {
          return;
        }
        let autoconnect2 = autoconnect.load();
        if (!autoconnect2) {
          return;
        }
        console.log("Saving account registration credentials");
        autoconnect2 = __spreadProps(__spreadValues({}, autoconnect2), {
          saslPlain: { username: data.account, password }
        });
        autoconnect.put(autoconnect2);
      });
      this.setDialogLoading(promise);
    }
    handleVerifyClick(account, message) {
      this.openDialog("verify", { account, message });
    }
    handleVerifySubmit(code) {
      let serverID = State2.getActiveServerID(this.state);
      let client = this.clients.get(serverID);
      let promise = client.verifyAccount(this.state.dialogData.account, code).then(() => {
        this.dismissDialog();
      });
      this.setDialogLoading(promise);
    }
    handleAddNetworkClick() {
      this.openDialog("network");
    }
    handleManageNetworkClick(serverID) {
      let server = this.state.servers.get(serverID);
      let bouncerNetID = server.bouncerNetID;
      let bouncerNetwork = this.state.bouncerNetworks.get(bouncerNetID);
      this.openDialog("network", {
        id: bouncerNetID,
        params: bouncerNetwork
      });
    }
    handleNetworkSubmit(attrs, autojoin) {
      let client = this.clients.values().next().value;
      if (this.state.dialogData && this.state.dialogData.id) {
        if (Object.keys(attrs).length == 0) {
          this.dismissDialog();
          return;
        }
        client.send({
          command: "BOUNCER",
          params: ["CHANGENETWORK", this.state.dialogData.id, formatTags(attrs)]
        });
      } else {
        attrs = __spreadProps(__spreadValues({}, attrs), { tls: "1" });
        client.createBouncerNetwork(attrs).then((id) => {
          if (!autojoin) {
            return;
          }
          let serverID = this.serverFromBouncerNetwork(id);
          let client2 = this.clients.get(serverID);
          client2.params.autojoin = [autojoin];
          this.switchToChannel = autojoin;
        });
      }
      this.dismissDialog();
    }
    handleNetworkRemove() {
      let client = this.clients.values().next().value;
      client.send({
        command: "BOUNCER",
        params: ["DELNETWORK", this.state.dialogData.id]
      });
      this.dismissDialog();
    }
    componentDidMount() {
      setup(this);
    }
    render() {
      if (this.state.loading) {
        return html`<section id="connect"></section>`;
      }
      let activeBuffer = null, activeServer = null, activeBouncerNetwork = null;
      if (this.state.buffers.get(this.state.activeBuffer)) {
        activeBuffer = this.state.buffers.get(this.state.activeBuffer);
        activeServer = this.state.servers.get(activeBuffer.server);
        let bouncerNetID = activeServer.bouncerNetID;
        if (bouncerNetID) {
          activeBouncerNetwork = this.state.bouncerNetworks.get(bouncerNetID);
        }
      }
      if (this.state.connectForm) {
        let status = activeServer ? activeServer.status : ServerStatus.DISCONNECTED;
        let connecting = status === ServerStatus.CONNECTING || status === ServerStatus.REGISTERING;
        return html`
				<section id="connect">
					<${ConnectForm}
						error=${this.state.error}
						params=${this.state.connectParams}
						auth=${this.config.server.auth}
						connecting=${connecting}
						onSubmit=${this.handleConnectSubmit}
					/>
				</section>
			`;
      }
      let bufferHeader = null;
      if (activeBuffer) {
        let activeUser = null;
        if (activeBuffer.type == BufferType.NICK) {
          activeUser = activeServer.users.get(activeBuffer.name);
        }
        bufferHeader = html`
				<section id="buffer-header">
					<${BufferHeader}
						buffer=${activeBuffer}
						server=${activeServer}
						user=${activeUser}
						bouncerNetwork=${activeBouncerNetwork}
						onChannelClick=${this.handleChannelClick}
						onClose=${() => this.close(activeBuffer)}
						onJoin=${() => this.handleJoinClick(activeBuffer)}
						onReconnect=${() => this.reconnect()}
						onAddNetwork=${this.handleAddNetworkClick}
						onManageNetwork=${() => this.handleManageNetworkClick(activeBuffer.server)}
					/>
				</section>
			`;
      }
      let memberList = null;
      if (activeBuffer && activeBuffer.type == BufferType.CHANNEL) {
        memberList = html`
				<section
						id="member-list"
						class=${this.state.openPanels.memberList ? "expand" : ""}
				>
					<button
						class="expander"
						onClick=${this.toggleMemberList}
					>
						<span></span>
						<span></span>
					</button>
					<section>
						<section id="member-list-header">
							${activeBuffer.members.size} users
						</section>
						<${MemberList}
							members=${activeBuffer.members}
							users=${activeServer.users}
							onNickClick=${this.handleNickClick}
						/>
					</section>
				</section>
			`;
      }
      let dialog = null;
      let dialogData = this.state.dialogData || {};
      let dialogBody;
      switch (this.state.dialog) {
        case "network":
          let isNew = !dialogData.id;
          let title = isNew ? "Add network" : "Edit network";
          dialog = html`
				<${Dialog} title=${title} onDismiss=${this.dismissDialog}>
					<${NetworkForm}
						onSubmit=${this.handleNetworkSubmit}
						onRemove=${this.handleNetworkRemove}
						params=${dialogData.params}
						autojoin=${dialogData.autojoin}
						isNew=${isNew}
					/>
				</>
			`;
          break;
        case "help":
          dialog = html`
				<${Dialog} title="Help" onDismiss=${this.dismissDialog}>
					<${Help}/>
				</>
			`;
          break;
        case "join":
          dialog = html`
				<${Dialog} title="Join channel" onDismiss=${this.dismissDialog}>
					<${JoinForm} channel=${dialogData.channel} onSubmit=${this.handleJoinSubmit}/>
				</>
			`;
          break;
        case "auth":
          if (dialogData.loading) {
            dialogBody = html`<p>Logging in…</p>`;
          } else {
            dialogBody = html`
					<${NetworkForm2} username=${dialogData.username} onSubmit=${this.handleAuthSubmit}/>
				`;
          }
          dialog = html`
				<${Dialog} title="Login to ${getServerName(activeServer, activeBouncerNetwork)}" onDismiss=${this.dismissDialog}>
					${dialogBody}
				</>
			`;
          break;
        case "register":
          if (dialogData.loading) {
            dialogBody = html`<p>Creating account…</p>`;
          } else {
            dialogBody = html`
					<${RegisterForm} emailRequired=${dialogData.emailRequired} onSubmit=${this.handleRegisterSubmit}/>
				`;
          }
          dialog = html`
				<${Dialog} title="Register a new ${getServerName(activeServer, activeBouncerNetwork)} account" onDismiss=${this.dismissDialog}>
					${dialogBody}
				</>
			`;
          break;
        case "verify":
          if (dialogData.loading) {
            dialogBody = html`<p>Verifying account…</p>`;
          } else {
            dialogBody = html`
					<${RegisterForm2} account=${dialogData.account} message=${dialogData.message} onSubmit=${this.handleVerifySubmit}/>
				`;
          }
          dialog = html`
				<${Dialog} title="Verify ${getServerName(activeServer, activeBouncerNetwork)} account" onDismiss=${this.dismissDialog}>
					${dialogBody}
				</>
			`;
          break;
      }
      let error = null;
      if (this.state.error) {
        error = html`
				<div id="error-msg">
					${this.state.error}
					${" "}
					<button onClick=${this.handleDismissError}>×</button>
				</div>
			`;
      }
      let composerReadOnly = false;
      if (activeServer && activeServer.status !== ServerStatus.REGISTERED) {
        composerReadOnly = true;
      }
      let commandOnly = false;
      if (activeBuffer && activeBuffer.type === BufferType.SERVER) {
        commandOnly = true;
      }
      return html`
			<section
					id="buffer-list"
					class=${this.state.openPanels.bufferList ? "expand" : ""}
			>
				<${BufferList}
					buffers=${this.state.buffers}
					servers=${this.state.servers}
					bouncerNetworks=${this.state.bouncerNetworks}
					activeBuffer=${this.state.activeBuffer}
					onBufferClick=${this.handleBufferListClick}
					onBufferClose=${this.handleBufferListClose}
				/>
				<button
					class="expander"
					onClick=${this.toggleBufferList}
				>
					<span></span>
					<span></span>
				</button>
			</section>
			${bufferHeader}
			<${ScrollManager}
				target=${this.buffer}
				stickTo=".logline"
				scrollKey=${this.state.activeBuffer}
				onScrollTop=${this.handleBufferScrollTop}
			>
				<section id="buffer" ref=${this.buffer} tabindex="-1">
					<${Buffer3}
						buffer=${activeBuffer}
						server=${activeServer}
						bouncerNetwork=${activeBouncerNetwork}
						onChannelClick=${this.handleChannelClick}
						onNickClick=${this.handleNickClick}
						onAuthClick=${() => this.handleAuthClick(activeBuffer.server)}
						onRegisterClick=${() => this.handleRegisterClick(activeBuffer.server)}
						onVerifyClick=${this.handleVerifyClick}
					/>
				</section>
			</>
			${memberList}
			<${Composer}
				ref=${this.composer}
				readOnly=${composerReadOnly}
				onSubmit=${this.handleComposerSubmit}
				autocomplete=${this.autocomplete}
				commandOnly=${commandOnly}
			/>
			${dialog}
			${error}
		`;
    }
  };

  // src/components/Chat/Widget.js
  var html2 = htm_module_default.bind(v);
  var showChat = false;
  var chatRenderedFirstTime = false;
  var changeInterval = null;
  if (!window.location.search) {
    d2 = new Date();
    searchParams = new URLSearchParams(window.location.search);
    searchParams.set("nick", "konsumee" + d2.getSeconds() + d2.getMinutes() + d2.getHours());
    window.location.search = searchParams.toString();
  }
  var d2;
  var searchParams;
  function clickHandler() {
    showChat = !showChat;
    if (!chatRenderedFirstTime) {
      S(v(App), document.getElementById("irc-chat"));
      chatRenderedFirstTime = true;
    }
  }
  var Widget = {
    onupdate: function() {
      changeInterval = window.setInterval(() => {
        var totalItems = document.getElementsByClassName("logline").length;
        if (totalItems) {
          document.getElementsByClassName("logline")[totalItems - 1].scrollIntoView();
        }
      }, 200);
    },
    onremove: function() {
      window.clearInterval(changeInterval);
    },
    view: function(vnode) {
      var ircChatStateClass = !showChat ? "hide" : "show";
      return (0, import_mithril9.default)("div.chat-widget", [
        (0, import_mithril9.default)(`div#irc-chat`, { class: ircChatStateClass }),
        (0, import_mithril9.default)("div.trigger-bubble", {
          onclick: clickHandler
        }, [
          (0, import_mithril9.default)("img", {
            src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAUVBMVEUAAAD///////////////////////////////////////////////////////////////////////////////////////////////////////8IN+deAAAAGnRSTlMAAwgJEBk0TVheY2R5eo+ut8jb5OXs8fX2+cjRDTIAAADsSURBVHgBldZbkoMgFIThRgQv8SKKgGf/C51UnJqaRI30/9zfe+NQUQ3TvG7bOk9DVeCmshmj/CuOTYnrdBfkUOg0zlOtl9OWVuEk4+QyZ3DIevmSt/ioTvK1VH/s5bY3YdM9SBZ/mUUyWgx+U06ycgp7D8msxSvtc4HXL9BLdj2elSEfhBJAI0QNgJEBI1BEBsQClVBVGDgwYOLAhJkDM1YOrNg4sLFAsLJgZsHEgoEFFQt0JAFGFjQsKAMJ0LFAexKgZYFyJIDxJIBNJEDNAtSJBLCeBDCOBFAPzwFA94ED+zmhwDO9358r8ANtIsMXi7qVAwAAAABJRU5ErkJggg==",
            alt: "chatbubble-icon"
          })
        ])
      ]);
    }
  };

  // src/components/Hero/HeroImage.js
  var import_mithril10 = __toESM(require_mithril());
  var img = require_hedgerabbhog_v3();
  var HeroImage = {
    view: function(vnode) {
      return (0, import_mithril10.default)("section", [
        (0, import_mithril10.default)(".relative flex justify-center items-center mt-8", [
          (0, import_mithril10.default)("img.mx-auto", { src: img })
        ])
      ]);
    }
  };

  // src/components/Icons/Brand.js
  var import_mithril11 = __toESM(require_mithril());
  var Brand = {
    logoFull: import_mithril11.default.trust(`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 169.159 41.662"><path d="M149.141 36.228s3.723 2.394 7.386 3.022c4.406.755 9.19-.63 9.19-.63s-.902-1.964-2.392-3.776c-1.62-1.971-4.574-3.945-4.574-3.945s-1.98.33-3.986.503c-1.933.168-6.38-.377-6.38-.377s.587 1.007.714 2.476c.168 1.93.042 2.727.042 2.727z" fill="#c45fed"/><path d="M147.295 29.934s5.102 1.32 10.24.461c7.288-1.217 11.624-5.917 11.624-5.917s-1.47-.881-2.56-1.427c-1.091-.545-2.98-1.175-2.98-1.175s-1.298 2.165-5.833 5.036c-4.574 2.896-10.491 3.022-10.491 3.022z" fill="#ed5fdc"/><path d="M145.322 29.052s3.913.703 8.352-.88c11.706-4.177 14.268-15.738 14.268-15.738s-1.05-.168-3.022.168c-1.972.336-4.154.923-4.154.923s-1.03 4.917-5.162 9.484c-5.53 5.329-10.282 6.043-10.282 6.043z" fill="#ff5597"/><path d="M144.651 28.465s4.654-.928 8.687-4.239c8.438-6.926 7.68-19.094 7.68-19.094s-2.686.924-4.407 1.847a26.903 26.903 0 0 0-3.063 1.93s.658 2.699-.798 8.477c-1.553 6.167-8.099 11.079-8.099 11.079z" fill="#ff7355"/><path d="M142.436 29.319c-1.749 6.999 5.152 10.328 5.152 10.328s2.202-6.048-.273-8.801c-1.384-1.54-4.88-1.527-4.88-1.527z" fill="#806c9d" paint-order="markers fill stroke"/><path d="M143.83 28.13s3.382-1.575 6.643-7.435C155.646 11.4 150.595 0 150.595 0s-2.516 2.37-3.851 4.092c-1.336 1.721-3.211 4.721-3.211 4.721s2.171 3.763 2.438 8.332c.267 4.57-2.141 10.986-2.141 10.986z" fill="#ffde55"/><path d="M134.987.089s-1.158 3.442-1.425 5.757c-.267 2.314-.326 4.807-.326 4.807s3.302 1.464 6.003 4.762c3.823 4.666 4.116 11.885 4.116 11.885s2.943-6.403 1.523-12.993C142.953 5.375 134.987.09 134.987.09z" fill="#b0ff55"/><path d="M142.554 27.864s-1.279-3.408-6.487-6.462c-3.489-2.046-6.065-1.877-6.065-1.877s-1.306-2.492-1.9-5.133c-.593-2.641-.741-5.342-.741-5.342s3.474.606 7.48 3.514c8.79 6.182 7.713 15.3 7.713 15.3z" fill="#55ff5b"/><path d="M141.871 28.13s-2.346-2.141-7.706-1.834c-4.102.235-6.24 2.131-6.24 2.131s-2.084-1.783-3.479-3.653c-1.394-1.87-2.04-3.854-2.04-3.854s3.87-1.013 8.132-.599c8.305.808 11.333 7.81 11.333 7.81z" fill="#55ffa7"/><path d="M141.396 28.813s-2.75-.228-5.979 1.753c-2.88 1.767-3.961 4.33-3.961 4.33s-1.9-.445-3.294-1.127c-1.395-.683-3.235-2.078-3.235-2.078s1.403-1.931 5.29-3.614c6.725-2.91 11.18.736 11.18.736z" fill="#55f1ff"/><path d="M141.817 29.627s-4.962-.09-8.079 3.498c-2.674 3.078-3.054 5.57-3.054 5.57s1.335.83 3.917.8c2.875.18 5.015-.504 5.015-.504s-.35-.702-.475-3.412c-.135-2.92 2.676-5.952 2.676-5.952z" fill="#55a9ff"/><path d="M141.782 41.662s1.039-.475 2.166-1.395c1.128-.92 1.395-1.305 1.395-1.305s-1.038-.643-2.48-2.503c-1.927-2.485-1.289-5.272-1.289-5.272s-2.01 2.597-1.502 6.142c.404 2.808 1.71 4.333 1.71 4.333z" fill="#7255ff"/><g style="line-height:1.25" font-weight="400" font-size="40" font-family="League Spartan" letter-spacing="0" word-spacing="0" fill="currentColor"><path d="m365.924 768.96-6.784 6.304v-17.408h-4.096v27.52h4.096v-7.104l7.328 7.104h4.928l-8.928-8.608 8.32-7.808zm14.687 16.896c5.12 0 8.736-3.712 8.736-8.64 0-4.928-3.616-8.736-8.736-8.736-5.152 0-8.864 3.808-8.864 8.736 0 4.928 3.712 8.64 8.864 8.64zm0-3.488c-2.944 0-4.896-2.176-4.896-5.152 0-3.008 1.952-5.248 4.896-5.248 2.912 0 4.832 2.24 4.832 5.248 0 2.976-1.92 5.152-4.832 5.152zm22.08-13.888c-2.848 0-5.28 1.92-5.632 3.264v-2.784h-4.096v16.416h4.096v-8.992c0-2.784 2.208-4.832 4.448-4.832s3.84 1.376 3.84 4.384v9.44h4.128V775.2c0-4.768-3.488-6.72-6.784-6.72zm17.536 0c-3.52 0-6.656 1.664-6.656 4.608 0 3.136 2.72 4.352 5.152 4.928 1.952.448 3.488.992 3.488 2.528 0 1.344-1.408 2.272-3.52 2.272-2.432 0-4.064-1.376-4.352-1.6l-1.536 2.848c.256.192 2.4 1.792 6.176 1.792 3.52 0 7.328-1.312 7.328-5.344 0-3.232-2.624-4.288-5.248-4.896-2.176-.512-3.52-1.184-3.52-2.432s1.376-1.888 3.104-1.888c2.08 0 3.552 1.088 3.744 1.216l1.536-2.56c-.192-.128-2.56-1.472-5.696-1.472zm13.76 8.576v-8.096h-4.032v8.64c0 4.896 2.976 8.256 8.256 8.256s8.256-3.36 8.256-8.256v-8.64h-4.032v8.096c0 3.264-1.312 5.312-4.224 5.312s-4.224-2.048-4.224-5.312zm37.344-8.576c-2.976 0-5.28 1.92-5.824 3.584-.544-1.6-1.952-3.584-4.896-3.584-2.816 0-5.024 1.696-5.472 2.944v-2.464h-4.064v16.416h4.064v-8.832c0-3.424 1.792-4.96 3.84-4.96 2.144 0 3.296 1.728 3.296 4.96v8.832h3.808v-8.832c0-3.328 1.824-4.96 3.872-4.96 2.496 0 3.264 1.728 3.264 4.96v8.832h4.032v-9.568c0-5.024-2.24-7.328-5.92-7.328zm12.864-4.608c1.408 0 2.592-1.152 2.592-2.56 0-1.408-1.216-2.56-2.592-2.56a2.568 2.568 0 0 0-2.56 2.56c0 1.408 1.12 2.56 2.56 2.56zm-2.112 21.504h4.096V768.96h-4.096z" style="-inkscape-font-specification:'League Spartan'" transform="translate(-312.334 -651.895) scale(.8797)" aria-label="konsumi"/></g></svg>`),
    leaf1: import_mithril11.default.trust(`<svg xmlns="http://www.w3.org/2000/svg" width="240" viewBox="0 0 202.3 228.541"><path d="M51.77 226.75C170.41 244.427 202.3 125.414 202.3 125.414s-23.299-4.38-83.12 19.424c-81.268 32.335-67.41 81.914-67.41 81.914z" fill="#ffde55"/><path d="M34.695 134.087C120.84 92.742 88.704 0 88.704 0S64.118 13.386 35.296 55.81c-39.153 57.634-.601 78.277-.601 78.277Z" fill="#ff5597"/><path d="M4.12 152.08c-16.062 31.882 20.028 40.308 20.028 40.308s14.025-17.338 7.822-29.818c-9.634-19.382-27.85-10.49-27.85-10.49Z" fill="#8b55ff"/></svg>`),
    leaf2: import_mithril11.default.trust(`<svg xmlns="http://www.w3.org/2000/svg" width="240" viewBox="0 0 146.899 215.841"><path d="M22.628 119.488c40.328-22.052 35.07-53.345 35.07-53.345S42.585 68.913 30.9 80.557c-24.744 24.662-8.272 38.931-8.272 38.931z" fill="#76ed5f"/><path d="M102.157 59.148c47.1-8.767 43.653-57.86 43.653-57.86s-13.65 3.27-33.141 20.045c-26.48 22.788-10.512 37.815-10.512 37.815Z" fill="#c45fed"/><path d="M73.333 153.274C33.761 126.267 1.58 163.498 1.58 163.498s11.982 7.31 37.63 9.167c34.844 2.524 34.124-19.391 34.124-19.391z" fill="#55f1ff"/><path d="M110.276 190.732C72.452 176.196 53.201 213.1 53.201 213.1s11.284 3.687 32.757.23c29.171-4.698 24.318-22.597 24.318-22.597z" fill="#806c9d"/><path d="M40.934 28.019c-28.955 1.75-30.55 31.503-30.55 31.503s12.6.719 25.564-7.904c17.611-11.714 4.986-23.6 4.986-23.6z" fill="#ff7355"/><path d="M75.328 123.592c-24.564-2.094-29.57 22.73-29.57 22.73s13.971 5.11 25.935-.546c16.253-7.681 3.635-22.184 3.635-22.184z" fill="#55a9ff"/><path d="M104.698 7.556c-15.377 12.96-3.101 30.05-3.101 30.05s10.531-4.077 14.17-14.24c4.944-13.805-11.069-15.81-11.069-15.81z" fill="#7255ff"/></svg>`)
  };

  // src/components/Hero/Hero.js
  var Hero = {
    view: function(vnode) {
      return (0, import_mithril12.default)(
        Section,
        {
          class: "max-w-6xl mx-auto px-4 sm:px-6 relative",
          childrenClass: "pt-10 pb-12 md:pt-16 md:pb-20"
        },
        [
          (0, import_mithril12.default)("#chat-container", (0, import_mithril12.default)(Widget)),
          (0, import_mithril12.default)(".absolute left-0 bottom-0 -ml-20 lg:block pointer-events-none", [
            (0, import_mithril12.default)(SVG, Brand.leaf1)
          ]),
          (0, import_mithril12.default)(".relative pt-32 pb-10 md:pt-40 md:pb-16", [
            (0, import_mithril12.default)(".max-w-3xl mx-auto text-center pb-12 md:pb-16", [
              (0, import_mithril12.default)("h1.mb-5 text-4xl md:text-7xl font-bold	leading-10 ", vnode.attrs.title),
              (0, import_mithril12.default)("p.mb-20 text-xl md:text-2xl mt-10", vnode.attrs.description),
              (0, import_mithril12.default)(HeroImage, { media: vnode.attrs.media })
            ])
          ])
        ]
      );
    }
  };

  // src/components/Tabs/Tabs.js
  var import_mithril16 = __toESM(require_mithril());

  // src/components/Tabs/Button.js
  var import_mithril14 = __toESM(require_mithril());

  // src/components/Tabs/Icons.js
  var import_mithril13 = __toESM(require_mithril());
  var Icons = {
    more: import_mithril13.default.trust(`<svg class="w-3 h-3 fill-current text-purple-400 flex-shrink-0 ml-2" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 5H0v2h6v4l6-5-6-5z" />
                                    </svg>`),
    button: import_mithril13.default.trust(`<svg class="w-4 h-4 fill-current text-purple-600 mr-2" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
    <path d="M8 9v6a8 8 0 008-8V1a8 8 0 00-8 8zM0 6v3a6 6 0 006 6v-3a6 6 0 00-6-6z" />
</svg>`)
  };

  // src/components/Tabs/Button.js
  var Button = {
    view: function(vnode) {
      var stateClass = vnode.attrs.active ? "" : "opacity-50";
      return (0, import_mithril14.default)(
        "button.flex items-center font-medium py-2 px-4 m-2 bg-gray-800 rounded-full group transition duration-500",
        { onclick: function() {
          vnode.attrs.clickAction(vnode.attrs.id);
        }, class: stateClass },
        [
          (0, import_mithril14.default)(SVG, Icons.button),
          (0, import_mithril14.default)(
            "span.text-gray-400 group-hover:text-gray-200 transition-colors duration-150 ease-in-out",
            vnode.attrs.title
          )
        ]
      );
    }
  };

  // src/components/Tabs/Item.js
  var import_mithril15 = __toESM(require_mithril());
  var Item3 = {
    view: function(vnode) {
      var _a, _b;
      var stateClass = vnode.attrs.active ? "block" : "hidden";
      return (0, import_mithril15.default)("div.w-full", { class: stateClass }, [
        (0, import_mithril15.default)("article.relative max-w-md mx-auto md:max-w-none", [
          (0, import_mithril15.default)("figure.md:absolute md:inset-y-0 md:right-0 md:w-1/2", [
            (0, import_mithril15.default)("img.w-full h-full object-cover", { src: (_a = vnode.attrs.media) == null ? void 0 : _a.src, alt: (_b = vnode.attrs.media) == null ? void 0 : _b.alt })
          ]),
          (0, import_mithril15.default)(".relative bg-gray-800 py-8 md:py-16 px-6 md:pr-16 md:max-w-lg lg:max-w-xl", [
            (0, import_mithril15.default)("h4.h4 mb-2", vnode.attrs.title),
            (0, import_mithril15.default)("p.text-lg text-gray-400", import_mithril15.default.trust(vnode.attrs.description.replace(/(?:\r\n|\r|\n)/g, "<br/>")))
          ])
        ])
      ]);
    }
  };

  // src/components/Tabs/Tabs.js
  var currentTab = 0;
  var clickAction = function(id) {
    console.log("click tab item" + id);
    currentTab = id;
  };
  var Tabs = {
    oninit: function(vnode) {
      this.currentTab = vnode.attrs.currentTab;
    },
    view: function(vnode) {
      var _a, _b;
      return (0, import_mithril16.default)("section", [
        (0, import_mithril16.default)(".max-w-6xl mx-auto px-4 sm:px-6", [
          (0, import_mithril16.default)(".py-12 md:py-20 border-t border-gray-800", [
            (0, import_mithril16.default)(".max-w-3xl mx-auto text-center pb-12", [
              (0, import_mithril16.default)("h2.h2 mb-4", vnode.attrs.title),
              (0, import_mithril16.default)("p.text-xl text-gray-400", vnode.attrs.description)
            ]),
            (0, import_mithril16.default)("div", [
              (0, import_mithril16.default)(
                ".flex flex-wrap justify-center -m-2",
                (_a = sort.sortBy(vnode.attrs.tabs, "priority")) == null ? void 0 : _a.map(
                  function(tab, index) {
                    var active = index == currentTab ? true : false;
                    return (0, import_mithril16.default)(Button, {
                      active,
                      clickAction,
                      id: index,
                      title: tab["tab-label"]
                    });
                  }.bind(this)
                )
              ),
              (0, import_mithril16.default)(
                "div.relative flex flex-col mt-16",
                (_b = vnode.attrs.tabs) == null ? void 0 : _b.map(
                  function(tab, index) {
                    var active = index == currentTab ? true : false;
                    return (0, import_mithril16.default)(Item3, {
                      title: tab.title,
                      description: tab.content,
                      icon: tab.icon,
                      active,
                      id: index,
                      media: tab.media
                    });
                  }.bind(this)
                )
              )
            ])
          ])
        ])
      ]);
    }
  };

  // src/components/Target/Target.js
  var import_mithril18 = __toESM(require_mithril());

  // src/components/Target/Item.js
  var import_mithril17 = __toESM(require_mithril());
  var Item4 = {
    view: function(vnode) {
      return (0, import_mithril17.default)("div.mt-6", [
        (0, import_mithril17.default)("article.relative max-w-md mx-auto md:max-w-none", [
          (0, import_mithril17.default)("h4.h4 mb-2", { class: vnode.attrs.colorClasses }, [
            (0, import_mithril17.default)("span", { class: vnode.attrs.colorClasses }, "."),
            (0, import_mithril17.default)("span", vnode.attrs.title)
          ]),
          (0, import_mithril17.default)("p.text-lg text-gray-400", vnode.attrs.content)
        ])
      ]);
    }
  };

  // src/components/Target/Target.js
  var Target = {
    view: function(vnode) {
      return (0, import_mithril18.default)("section", [
        (0, import_mithril18.default)(".max-w-6xl mx-auto px-4 sm:px-6", [
          (0, import_mithril18.default)(".py-12 md:py-20 border-t border-gray-800", [
            (0, import_mithril18.default)(".max-w-3xl mx-auto text-center pb-12", [
              (0, import_mithril18.default)("h2.h2 mb-4", vnode.attrs.title)
            ]),
            (0, import_mithril18.default)(".max-w-xl md:max-w-none md:w-full mx-auto md:col-span-5 lg:col-span-6 mb-8 md:mb-0 md:rtl", [
              (0, import_mithril18.default)("img.mx-auto md:max-w-none", { src: vnode.attrs.image })
            ]),
            (0, import_mithril18.default)("div.max-w-xl md:max-w-none md:w-full mx-auto md:col-span-7 lg:col-span-6", [
              (0, import_mithril18.default)("div.max-w-3xl mx-auto text-center pb-12", [
                (0, import_mithril18.default)(".font-architects-daughter text-xl text-purple-600 mb-6 px-10", vnode.attrs.subtitle),
                (0, import_mithril18.default)("p.text-xl text-gray-400 mb-6 px-10", vnode.attrs.description)
              ]),
              (0, import_mithril18.default)("div.md:pl-4 lg:pl-12 xl:pl-16", [
                vnode.attrs.items.map(function(item) {
                  return (0, import_mithril18.default)(Item4, __spreadValues({}, item));
                })
              ])
            ])
          ])
        ])
      ]);
    }
  };

  // src/components/Hero/Simple.js
  var import_mithril20 = __toESM(require_mithril());

  // src/components/Form/Contact.js
  var import_mithril19 = __toESM(require_mithril());
  var Contact = {
    view: function(vnode) {
      return (0, import_mithril19.default)("form.max-w-xl mx-auto mt-12", { action: "https://forms.konsumi.rocks/", method: "post" }, [
        (0, import_mithril19.default)("h3.h3 my-8", "Schreib uns direkt"),
        (0, import_mithril19.default)("input", { type: "hidden", value: "Contact Form", name: "_formName" }),
        (0, import_mithril19.default)("input", { type: "hidden", value: "https://konsumi.rocks/#thank-you", name: "_redirectTo" }),
        (0, import_mithril19.default)("input", { type: "email", value: "", name: "_t_email", style: "display: none;" }),
        (0, import_mithril19.default)("div.mb-4", [
          (0, import_mithril19.default)("div.w-full px-3", [
            (0, import_mithril19.default)("label.block text-gray-300 text-sm font-medium mb-1", { for: "email" }, "E-Mail"),
            (0, import_mithril19.default)("input.form-input w-full text-gray-300", { id: "email", type: "email", name: "_replyTo", required: true })
          ]),
          (0, import_mithril19.default)("div.mb-4", [
            (0, import_mithril19.default)("div.w-full px-3 mt-8", [
              (0, import_mithril19.default)("label.block text-gray-300 text-sm font-medium mb-1", { for: "Message" }, "Nachricht"),
              (0, import_mithril19.default)("textarea.form-textarea w-full text-gray-300", { rows: 4, id: "Message", name: "Message", required: true })
            ])
          ]),
          (0, import_mithril19.default)("div.mt-6", [
            (0, import_mithril19.default)("div.w-full px-3", [
              (0, import_mithril19.default)("button.form-input w-full text-gray-300", { id: "Send", type: "submit", name: "Send", required: true }, "Senden")
            ])
          ])
        ])
      ]);
    }
  };

  // src/components/Hero/Simple.js
  var Simple = {
    view: function(vnode) {
      return (0, import_mithril20.default)(
        Section,
        {
          class: "max-w-6xl mx-auto px-4 sm:px-6 relative",
          childrenClass: "pt-10 pb-12 md:pt-5 md:pb-5"
        },
        [
          (0, import_mithril20.default)(".absolute left-0 bottom-0 -ml-20 lg:block pointer-events-none", [
            (0, import_mithril20.default)(SVG, Brand.leaf1)
          ]),
          (0, import_mithril20.default)(".relative pt-32 pb-10 md:pt-40 md:pb-16", [
            (0, import_mithril20.default)(".max-w-3xl mx-auto text-center pb-12 md:pb-16", [
              (0, import_mithril20.default)("h1.mb-4 text-5xl font-bold	", vnode.attrs.title),
              (0, import_mithril20.default)("p.mb-20 text-2xl mt-8", vnode.attrs.description),
              (0, import_mithril20.default)(Contact)
            ])
          ])
        ]
      );
    }
  };

  // src/components/Block/Text.js
  var import_mithril22 = __toESM(require_mithril());

  // src/components/Form/Button.js
  var import_mithril21 = __toESM(require_mithril());
  var Button2 = {
    clickHandler: function(vnode) {
      var _a;
      if (((_a = vnode.attrs) == null ? void 0 : _a.action) !== void 0) {
        vnode.attrs.action();
      }
    },
    view: function(vnode) {
      return (0, import_mithril21.default)(
        "a.btn",
        {
          href: vnode.attrs.href,
          class: vnode.attrs.class,
          onclick: this.clickHandler.bind(vnode)
        },
        vnode.attrs.label
      );
    }
  };

  // src/components/Block/Text.js
  var Text2 = {
    view: function(vnode) {
      return (0, import_mithril22.default)(
        Section,
        {
          class: "max-w-6xl mx-auto px-4 sm:px-6",
          childrenClass: "pt-10 pb-4 md:pt-16 md:pb-1"
        },
        [
          (0, import_mithril22.default)(Header, {
            title: vnode.attrs.title,
            subtitle: vnode.attrs.subtitle
          }),
          (0, import_mithril22.default)(
            "div.max-w-xs mx-auto sm:max-w-none sm:flex sm:justify-center mt-2",
            sort.sortBy(vnode.attrs.buttons, "priority").map(function(item) {
              return (0, import_mithril22.default)("div.mx-1", [
                (0, import_mithril22.default)(Button2, {
                  class: `${item.classes} w-full`,
                  label: item.title,
                  href: item.href,
                  action: item.action
                })
              ]);
            })
          )
        ]
      );
    }
  };

  // src/views/Home.js
  var heroContent = Home.getElements()["hero-video"];
  var whatContent = Home.getElements()["what-is-konsumi"];
  var whyContent = Home.getElements()["kirschen"];
  var aimContent = Home.getElements()["fakten"];
  var futureContent = Home.getElements()["future"];
  var casesContent = Home.getElements()["cases"];
  var collabContent = Home.getElements()["collaborate"];
  var roadmapContent = Home.getElements()["roadmap"];
  var Home2 = {
    view: function() {
      return (0, import_mithril23.default)("div", [
        (0, import_mithril23.default)(Hero, {
          buttons: heroContent.actions,
          title: heroContent.title,
          description: heroContent.description,
          media: heroContent.media
        }),
        (0, import_mithril23.default)(".absolute right-0 top-10 hidden lg:block pointer-events-none", [
          (0, import_mithril23.default)(SVG, Brand.leaf2)
        ]),
        (0, import_mithril23.default)(Process, {
          title: whatContent.title,
          subtitle: whatContent.subtitle,
          description: whatContent.description,
          items: whatContent.items
        }),
        (0, import_mithril23.default)(Tabs, {
          title: whyContent.title,
          description: whyContent.description,
          tabs: whyContent.tabs,
          currentTab: 0
        }),
        (0, import_mithril23.default)(Target, {
          title: aimContent.title,
          image: "",
          subtitle: aimContent.subtitle,
          items: aimContent.items,
          description: aimContent.description
        }),
        (0, import_mithril23.default)(Process, {
          title: futureContent.title,
          subtitle: futureContent.subtitle,
          description: futureContent.description,
          items: futureContent.items
        }),
        (0, import_mithril23.default)(Target, {
          title: casesContent.title,
          image: "",
          subtitle: casesContent.subtitle,
          items: casesContent.items,
          description: casesContent.description
        }),
        (0, import_mithril23.default)(Text2, {
          title: roadmapContent.title,
          subtitle: roadmapContent.subtitle,
          buttons: roadmapContent.actions
        }),
        (0, import_mithril23.default)(Simple, {
          buttons: collabContent.actions,
          title: collabContent.title,
          description: collabContent.description
        })
      ]);
    }
  };

  // src/layouts/Landing/Landing.js
  var import_mithril36 = __toESM(require_mithril());

  // src/components/Header/Header.js
  var import_mithril28 = __toESM(require_mithril());

  // src/components/Header/Logo.js
  var import_mithril24 = __toESM(require_mithril());
  var Logo = {
    view: function(vnode) {
      return (0, import_mithril24.default)(".flex-shrink-0 mr-4", [
        (0, import_mithril24.default)("a.block", { href: "/", ariaLabel: "konsumi" }, [
          (0, import_mithril24.default)(
            "div.w-32 h-8 fill-current font-bold m-2 mt-0 text-lg text-gray-200",
            (0, import_mithril24.default)(SVG, Brand.logoFull)
          )
        ])
      ]);
    }
  };

  // src/components/Header/Navigation.js
  var import_mithril27 = __toESM(require_mithril());

  // src/components/Header/Icons.js
  var import_mithril25 = __toESM(require_mithril());
  var Icons2 = {
    hamburger: import_mithril25.default.trust(`<svg class="w-6 h-6 fill-current text-gray-300 hover:text-gray-200 transition duration-150 ease-in-out" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <rect y="4" width="24" height="2" rx="1" />
  <rect y="11" width="24" height="2" rx="1" />
  <rect y="18" width="24" height="2" rx="1" />
  </svg>`)
  };

  // src/models/Navigation.js
  var import_mithril26 = __toESM(require_mithril());
  var Navigation = {
    mainNavigationItems: [
      { title: "Chat", href: "https://web.libera.chat/gamja/#konsumi" },
      { title: "Code", href: "https://codeberg.org/konsumi" },
      { title: "Documentation", href: "https://docs.konsumi.rocks/" }
    ]
  };

  // src/components/Header/Navigation.js
  var mobileMenuExpanded = false;
  function toggleMobileMenu() {
    mobileMenuExpanded = !mobileMenuExpanded;
  }
  var Navigation2 = {
    view: function(vnode) {
      return (0, import_mithril27.default)(".nav-wrapper", [
        (0, import_mithril27.default)("nav.hidden md:flex md:flex-grow", [
          (0, import_mithril27.default)(
            "ul.flex flex-grow justify-end flex-wrap items-center",
            Navigation.mainNavigationItems.map(function(item) {
              return (0, import_mithril27.default)("li", [
                (0, import_mithril27.default)(
                  "a.text-gray-300 hover:text-gray-200 px-4 py-2 flex items-center transition duration-150 ease-in-out",
                  { href: item.href },
                  item.title
                )
              ]);
            })
          )
        ]),
        (0, import_mithril27.default)("div.md:hidden", [
          (0, import_mithril27.default)(
            "button.hamburger",
            {
              class: mobileMenuExpanded ? "active" : "",
              onclick: toggleMobileMenu,
              ariaExpanded: mobileMenuExpanded,
              ariaControls: "mobile-nav"
            },
            [
              (0, import_mithril27.default)(SVG, Icons2.hamburger)
            ]
          ),
          (0, import_mithril27.default)(
            "nav.absolute top-full z-20 left-0 w-full px-4 sm:px-6 overflow-hidden transition-all duration-300 ease-in-out",
            {
              xRef: "mobileNav",
              onclickoutside: function() {
                mobileMenuExpanded = false;
              },
              class: mobileMenuExpanded ? "block" : "hidden"
            },
            [
              (0, import_mithril27.default)(
                "ul.bg-gray-800 px-4 py-2",
                Navigation.mainNavigationItems.map(function(item) {
                  return (0, import_mithril27.default)("li", [
                    (0, import_mithril27.default)(
                      "a.flex text-gray-300 hover:text-gray-200 py-2",
                      { href: item.href },
                      item.title
                    )
                  ]);
                })
              )
            ]
          )
        ])
      ]);
    }
  };

  // src/components/Header/Header.js
  var Header2 = {
    view: function(vnode) {
      return (0, import_mithril28.default)("header.absolute w-full z-30", [
        (0, import_mithril28.default)("div.max-w-6xl mx-auto px-4 sm:px-6", [
          (0, import_mithril28.default)("div.flex items-center justify-between h-20", [(0, import_mithril28.default)(Logo), (0, import_mithril28.default)(Navigation2)])
        ])
      ]);
    }
  };

  // src/components/Footer/Footer.js
  var import_mithril34 = __toESM(require_mithril());

  // src/components/Footer/Info.js
  var import_mithril29 = __toESM(require_mithril());
  var Info = {
    view: function(vnode) {
      return (0, import_mithril29.default)("div.mb-2", [
        (0, import_mithril29.default)("a.inline-block", { href: "/", ariaLabel: "konsumi" }, [
          (0, import_mithril29.default)(
            "div.w-8 h-8 fill-current text-purple-600 font-bold m-2 text-lg",
            "KONSUMI"
          ),
          (0, import_mithril29.default)("div.text-gray-400", "distribute anything from anyone fully controled by yourself. go and create without limits.")
        ])
      ]);
    }
  };

  // src/components/Footer/BlockList.js
  var import_mithril30 = __toESM(require_mithril());
  var BlockList = {
    view: function(vnode) {
      return (0, import_mithril30.default)("div.text-sm", [
        (0, import_mithril30.default)("h6.text-gray-200 font-medium mb-1", vnode.attrs.title || ""),
        (0, import_mithril30.default)("ul", [
          vnode.attrs.items.map(function(item) {
            if (item) {
              return (0, import_mithril30.default)("li.mb-1", [
                (0, import_mithril30.default)(
                  "a.text-gray-400 hover:text-gray-100 transition duration-150 ease-in-out",
                  { href: item.href },
                  item.title
                )
              ]);
            }
            return "";
          })
        ])
      ]);
    }
  };

  // src/components/Footer/BottomLine.js
  var import_mithril32 = __toESM(require_mithril());

  // src/components/Icons/Social.js
  var import_mithril31 = __toESM(require_mithril());
  var Social = {
    twitter: import_mithril31.default.trust(`<svg class="w-8 h-8 fill-current" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
    <path d="M24 11.5c-.6.3-1.2.4-1.9.5.7-.4 1.2-1 1.4-1.8-.6.4-1.3.6-2.1.8-.6-.6-1.5-1-2.4-1-1.7 0-3.2 1.5-3.2 3.3 0 .3 0 .5.1.7-2.7-.1-5.2-1.4-6.8-3.4-.3.5-.4 1-.4 1.7 0 1.1.6 2.1 1.5 2.7-.5 0-1-.2-1.5-.4 0 1.6 1.1 2.9 2.6 3.2-.3.1-.6.1-.9.1-.2 0-.4 0-.6-.1.4 1.3 1.6 2.3 3.1 2.3-1.1.9-2.5 1.4-4.1 1.4H8c1.5.9 3.2 1.5 5 1.5 6 0 9.3-5 9.3-9.3v-.4c.7-.5 1.3-1.1 1.7-1.8z" />
</svg>`),
    github: import_mithril31.default.trust(`<svg class="w-8 h-8 fill-current" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
<path d="M16 8.2c-4.4 0-8 3.6-8 8 0 3.5 2.3 6.5 5.5 7.6.4.1.5-.2.5-.4V22c-2.2.5-2.7-1-2.7-1-.4-.9-.9-1.2-.9-1.2-.7-.5.1-.5.1-.5.8.1 1.2.8 1.2.8.7 1.3 1.9.9 2.3.7.1-.5.3-.9.5-1.1-1.8-.2-3.6-.9-3.6-4 0-.9.3-1.6.8-2.1-.1-.2-.4-1 .1-2.1 0 0 .7-.2 2.2.8.6-.2 1.3-.3 2-.3s1.4.1 2 .3c1.5-1 2.2-.8 2.2-.8.4 1.1.2 1.9.1 2.1.5.6.8 1.3.8 2.1 0 3.1-1.9 3.7-3.7 3.9.3.4.6.9.6 1.6v2.2c0 .2.1.5.6.4 3.2-1.1 5.5-4.1 5.5-7.6-.1-4.4-3.7-8-8.1-8z" />
</svg>`),
    facebook: import_mithril31.default.trust(`<svg class="w-8 h-8 fill-current" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
<path d="M14.023 24L14 17h-3v-3h3v-2c0-2.7 1.672-4 4.08-4 1.153 0 2.144.086 2.433.124v2.821h-1.67c-1.31 0-1.563.623-1.563 1.536V14H21l-1 3h-2.72v7h-3.257z" />
</svg>`),
    instagram: import_mithril31.default.trust(`<svg class="w-8 h-8 fill-current" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
<circle cx="20.145" cy="11.892" r="1" />
<path d="M16 20c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4zm0-6c-1.103 0-2 .897-2 2s.897 2 2 2 2-.897 2-2-.897-2-2-2z" />
<path d="M20 24h-8c-2.056 0-4-1.944-4-4v-8c0-2.056 1.944-4 4-4h8c2.056 0 4 1.944 4 4v8c0 2.056-1.944 4-4 4zm-8-14c-.935 0-2 1.065-2 2v8c0 .953 1.047 2 2 2h8c.935 0 2-1.065 2-2v-8c0-.935-1.065-2-2-2h-8z" />
</svg>`),
    linkedin: import_mithril31.default.trust(`<svg class="w-8 h-8 fill-current" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
<path d="M23.3 8H8.7c-.4 0-.7.3-.7.7v14.7c0 .3.3.6.7.6h14.7c.4 0 .7-.3.7-.7V8.7c-.1-.4-.4-.7-.8-.7zM12.7 21.6h-2.3V14h2.4v7.6h-.1zM11.6 13c-.8 0-1.4-.7-1.4-1.4 0-.8.6-1.4 1.4-1.4.8 0 1.4.6 1.4 1.4-.1.7-.7 1.4-1.4 1.4zm10 8.6h-2.4v-3.7c0-.9 0-2-1.2-2s-1.4 1-1.4 2v3.8h-2.4V14h2.3v1c.3-.6 1.1-1.2 2.2-1.2 2.4 0 2.8 1.6 2.8 3.6v4.2h.1z" />
</svg>`),
    codeberg: import_mithril31.default.trust(`<svg class="fill-current" width="32" height="32" viewBox="-2.125 -2.125 8.466 8.466" xmlns="http://www.w3.org/2000/svg">
<defs><linearGradient id="a"><stop style="stop-color:#2185d0;stop-opacity:0" offset="0"/><stop offset=".495" style="stop-color:#2185d0;stop-opacity:.48923996"/><stop style="stop-color:#2185d0;stop-opacity:.63279623" offset="1"/></linearGradient><linearGradient xlink:href="#b" id="c" x1="42519.285" y1="-7078.789" x2="42575.336" y2="-6966.931" gradientUnits="userSpaceOnUse"/><linearGradient id="b"><stop style="stop-color:#666;stop-opacity:0" offset="0"/><stop offset=".495" style="stop-color:#666;stop-opacity:.30000001"/><stop style="stop-color:#666;stop-opacity:.30000001" offset="1"/></linearGradient></defs><path style="font-variation-settings:normal;opacity:1;vector-effect:none;fill:url(#c);fill-opacity:1;stroke:none;stroke-width:3.67846;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:2;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke markers fill;stop-color:#000;stop-opacity:1" d="M42519.285-7078.79a.76.568 0 0 0-.738.675l33.586 125.888a87.182 87.182 0 0 0 39.381-33.763l-71.565-92.52a.76.568 0 0 0-.664-.28z" transform="matrix(.02428 0 0 .02428 -1030.156 172.97)"/><path style="opacity:1;fill:#666;fill-opacity:1;stroke-width:17.0055;paint-order:markers fill stroke;stop-color:#000" d="M11249.461-1883.696c-12.74 0-23.067 10.327-23.067 23.067 0 4.333 1.22 8.58 3.522 12.251l19.232-24.863c.138-.18.486-.18.624 0l19.233 24.864a23.068 23.068 0 0 0 3.523-12.252c0-12.74-10.327-23.067-23.067-23.067z" transform="translate(-1030.156 172.97) scale(.09176)"/>
</svg>`)
  };

  // src/components/Footer/BottomLine.js
  var BottomLine = {
    view: function(vnode) {
      return (0, import_mithril32.default)("ul.flex mb-4 md:order-1 md:ml-4 md:mb-0", [
        (0, import_mithril32.default)("li.ml-4", [
          (0, import_mithril32.default)("a.flex justify-center items-center text-purple-600 bg-gray-800 hover:text-gray-100 hover:bg-purple-600 rounded-full transition duration-150 ease-in-out", { href: "https://codeberg.org/konsumi", target: "_blank", ariaLabel: "Codeberg" }, [
            (0, import_mithril32.default)(SVG, Social.codeberg)
          ])
        ])
      ]);
    }
  };

  // src/models/FooterProject.js
  var import_mithril33 = __toESM(require_mithril());
  var FooterProject = {
    navigationBlock: [
      { title: "about", href: "https://docs.konsumi.rocks/" },
      { title: "mission", href: "https://docs.konsumi.rocks/mission/" },
      { title: "team", href: "https://docs.konsumi.rocks/team/" }
    ]
  };

  // src/components/Footer/Footer.js
  var Footer = {
    view: function(vnode) {
      return (0, import_mithril34.default)("footer", [
        (0, import_mithril34.default)(".py-12 md:py-16", [
          (0, import_mithril34.default)(".max-w-6xl mx-auto px-4 sm:px-6", [
            (0, import_mithril34.default)(".grid md:grid-cols-12 gap-8 lg:gap-20 mb-8 md:mb-12", [
              (0, import_mithril34.default)("div.md:col-span-4 lg:col-span-5", [
                (0, import_mithril34.default)(Info)
              ]),
              (0, import_mithril34.default)("div.md:col-span-8 lg:col-span-7 grid sm:grid-cols-3 gap-8", [
                (0, import_mithril34.default)("div", ""),
                (0, import_mithril34.default)("div.text-sm", [
                  (0, import_mithril34.default)("h6.text-gray-200 font-medium mb-1", "Project Board"),
                  (0, import_mithril34.default)("ul", [
                    (0, import_mithril34.default)("li.mb-1", [
                      (0, import_mithril34.default)("p.text-gray-400", "Oliver Sch\xF6rwerth"),
                      (0, import_mithril34.default)("p.text-gray-400", "Querstra\xDFe 17"),
                      (0, import_mithril34.default)("p.text-gray-400", "39264 Wertlau"),
                      (0, import_mithril34.default)("a.text-gray-400 hover:text-gray-100 transition duration-150 ease-in-out", { href: "mailto:olli@konsumi.rocks?Subject=Hello%20konsumi" }, "olli @ konsumi.rocks")
                    ])
                  ])
                ]),
                (0, import_mithril34.default)(BlockList, { title: "Project", items: FooterProject.navigationBlock })
              ])
            ]),
            (0, import_mithril34.default)(".md:flex md:items-center md:justify-between", [
              (0, import_mithril34.default)(BottomLine),
              (0, import_mithril34.default)("div.text-gray-400 text-sm mr-4", "\xA9 2022 - konsumi - erlebe virtuelle interaktion neu - offen und selbstbestimmt")
            ]),
            (0, import_mithril34.default)("div.text-gray-600 text-sm", [
              (0, import_mithril34.default)("a", { href: "https://www.freepik.com/free-vector/abstract-technology-design_1165261.htm" }, "Image by pinnacleanimates on Freepik")
            ]),
            (0, import_mithril34.default)("div.text-gray-600 text-sm", [
              (0, import_mithril34.default)("a", { href: "https://www.freepik.com/free-vector/space-rocket-flying-space-with-moon-stars-background-print-vector-illustration_1158410.htm" }, "Image by macrovector on Freepik")
            ])
          ])
        ])
      ]);
    }
  };

  // src/components/Footer/SubscribeForm.js
  var import_mithril35 = __toESM(require_mithril());
  var SubscribeForm = {
    view: function(vnode) {
      return (0, import_mithril35.default)("section", [
        (0, import_mithril35.default)("div.max-w-6xl mx-auto px-4 sm:px-6", [
          (0, import_mithril35.default)(".relative bg-purple-600 py-10 px-8 md:py-16 md:px-12", [
            (0, import_mithril35.default)(".relative flex flex-col lg:flex-row justify-between items-center", [
              (0, import_mithril35.default)(".mb-6 lg:mr-16 lg:mb-0 text-center lg:text-left lg:w-1/2", [
                (0, import_mithril35.default)("h3.h3 text-white mb-2", "Wir sind Open Source"),
                (0, import_mithril35.default)("p.text-purple-200 text-lg", "Du bist Entwickler und willst Teil der L\xF6sung sein?")
              ]),
              (0, import_mithril35.default)("form.w-full lg:w-1/2", [
                (0, import_mithril35.default)(".flex flex-col sm:flex-row justify-center max-w-xs mx-auto sm:max-w-md lg:max-w-none", [
                  (0, import_mithril35.default)("a.btn text-purple-600 bg-purple-100 hover:bg-white shadow", { href: "https://codeberg.org/konsumi", target: "_blank" }, "Hilf uns auf Codeberg")
                ])
              ])
            ])
          ])
        ])
      ]);
    }
  };

  // src/layouts/Landing/Landing.js
  var Landing = {
    view: function(vnode) {
      return (0, import_mithril36.default)(
        "main.k-app font-inter antialiased bg-gray-900 text-gray-200 tracking-tight",
        [
          (0, import_mithril36.default)(".flex flex-col overflow-hidden", (0, import_mithril36.default)(Header2)),
          (0, import_mithril36.default)("main.flex-grow", [
            (0, import_mithril36.default)("section", vnode.children),
            (0, import_mithril36.default)(SubscribeForm)
          ]),
          (0, import_mithril36.default)(Footer)
        ]
      );
    }
  };

  // src/index.js
  import_mithril37.default.route(document.body, "", {
    "/": {
      render: function() {
        return (0, import_mithril37.default)(Landing, (0, import_mithril37.default)(Home2));
      }
    }
  });
})();
/*! (c) 2020 Andrea Giammarchi */
//# sourceMappingURL=index.js.map
